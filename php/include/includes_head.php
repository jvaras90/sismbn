<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> <?php echo $sistemaNombre . " | Sistema " ?></title>
    <link href="http://misionbuenasnuevas.info/wp-content/uploads/2016/02/16x16-mbn.png" rel="shortcut icon" />
    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- editor -->
    <!--<link href="http://netdnax|.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">-->
    <!--<link href="externo/font-awesome.css" rel="stylesheet" type="text/css"/>-->
    <!--    <link href="css/editor/external/google-code-prettify/prettify.css" rel="stylesheet">
        <link href="css/editor/index.css" rel="stylesheet">-->
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <!--<link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.1.css" />-->
    <link href="css/icheck/flat/green.css" rel="stylesheet" />
    <link href="css/floatexamples.css" rel="stylesheet" type="text/css" />    
    <link rel="stylesheet" type="text/css" href="css/progressbar/bootstrap-progressbar-3.3.0.css">

    <!-- ion_range -->
    <link rel="stylesheet" href="css/normalize.css" />
    <link rel="stylesheet" href="css/ion.rangeSlider.css" />
    <link rel="stylesheet" href="css/ion.rangeSlider.skinFlat.css" />

    <!-- select2 -->
    <link href="css/select/select2.min.css" rel="stylesheet">
    <link href="css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">
    
    
    <link href="css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>

    <!-- switchery -->
    <link rel="stylesheet" href="css/switchery/switchery.min.css" />

    <script src="js/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js">></script>
    <!--<script src="externo/jquery-ui.min.js" type="text/javascript"></script>-->

    <script type="text/javascript" src="js/llena_inputs.js"></script>
    <script type="text/javascript" src="js/llena_canales.js"></script>
    <!--<script src="js/nprogress.js"></script>
    <script src="js/autocomplete/jquery.autocomplete.js"></script>
        <script>
        NProgress.start();
    </script>-->

    <!-- editor -->
    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    <!--jeditable-->
    <script type="text/javascript" src="js/jquery.jeditable.js"></script>
    <script type="text/javascript" src="js/js.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" />
    <!--<link href="externo/jquery-ui.css" rel="stylesheet" type="text/css"/>-->

    <!--fin jeditable-->
    <!--<script src="js/jquery.min.js"></script>-->

    <script>
        $(document).ready(function () {
            $("#resultadoBusqueda").html('<p>JQUERY VACIO</p>');
        });

        function buscar() {
            var textoBusqueda = $("input#busqueda").val();

            if (textoBusqueda != "") {
                $.post("buscar.php", {valorBusqueda: textoBusqueda}, function (mensaje) {
                    $("#resultadoBusqueda").html(mensaje);
                });
            } else {
                $("#resultadoBusqueda").html('<p>JQUERY VACIO</p>');
            }
            ;
        }
        ;
    </script>

</head>