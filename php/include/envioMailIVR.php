<?php
require("phpmailer/class.phpmailer.php");
$mail = new PHPMailer(); //instancia a classe de mailer
$mail->PluginDir = "phpmailer/";
//$mail->setLanguage('pt'); // linguagem do mailer - portuguÊs
$from = "support@atcmultiservicios.com";
$fromName = "Sistema IVR";
$host = 'smtp.gmail.com';
$port = 465;
$secure = true;
$pass = 'svp04rW34Tc125';

$mail->Mailer = "smtp";
$mail->SMTPAuth = $secure;
$mail->Username = $from;
$mail->Password = $pass;

$mail->Host = $host;
$mail->Port = $port;
$mail->SMTPSecure = $secure;

$mail->From = $from;
$mail->FromName = $fromName;
$mail->addReplyTo($from, $fromName);

$mail->isHTML(true);
$mail->isSMTP();

$mail->CharSet = 'utf-8';
$mail->WordWrap = 70;

if (isset($_GET['nom_camp'])) {
    $nom_camp = $_GET['nom_camp'];
} else {
    $nom_camp = "Nombre Campaña";
}

//Envia Correo a las personas indicadas//
//**************************************/
function Conectarse() { //Función para conectarse a la BD
    if (!($link = mysqli_connect("172.20.30.202", "root", "dBi1E7YhIv1", "base_ivr"))) { //Cambia estos datos
        //dBi1E7YhIv1
        //Db4tcyMS1stlc3
        //zerokernel
        echo "Error conectando a la base de datos.";
        exit();
    }
    return $link;
}

$enlacec = Conectarse();
$mailQuery = "select concat(nombres,' ', ape_paterno) nombre,e.dni_creo_campana , p.mail as email,
        count(idcliente) as total_enviados,
        (
            (count(case when  status = '0' and flag1 = '1' then 'No Contestados'end))+
            (count(case when  status = 'humano' and billsec>=23 and telefono like '9%' then 'Maquina'  end))+
            (count(case when  status = 'maquina'  then 'Maquina1'  end))
        ) as no_contestados,
        (
            (count(case when  status = 'humano' and billsec<23 then 'Contestado' end)) + 
            (count(case when  status = 'humano' and billsec>=23 and telefono not like '9%' then 'Contestado1'  end)) 
        ) as contestados
          from envio_ivr e 
          inner join personal p on e.dni_creo_campana = p.dni
          where nombre_campana='" . $nom_camp . "'";
$mailQuery1 = mysqli_query($enlacec, $mailQuery);
while ($row = mysqli_fetch_array($mailQuery1)) {
    $nombre = $row['nombre'];
    $email = $row['email'];
    $total = $row['total_enviados'];
    $contestados = $row['contestados'];
    $noContestados = $row['no_contestados'];
}
$mail->addAddress('' . $email . '', '' . $nombre . '');
//$mail->addAddress('rpiedra@atcmultiservicios.com', 'Ricardina Piedra');
//$mail->addAddress('msaravia@atcmultiservicios.com', 'Marco Saravia');
$mail->addAddress('hvaras@atcmultiservicios.com', 'Harold Varas');

$mail->Subject = 'Campaña de IVR Finalizada';
$mail->Body = 'La Campaña ' . $nom_camp . ' a Finalizado<br>
               Puede verificarlo en <a href="http://172.20.30.202/new/">Este link</a> al conectarse.<br><br>
               
<table  style="text-align:center; margin: auto" width="320" border="0" cellspacing="0" cellpadding="0">
    <thead>
        <tr bgcolor="#333333" style="color:#ffffff">
            <td style="width:15%">Total</td>
            <td style="width:25%">Contestados</td>
            <td style="width:35%">No Contestados</td>
        </tr>
    </thead>
    <tbody>
        <tr bgcolor="#F0F0F0">
            <td>' . $total . ' </td>
            <td>' . $contestados . '  </td>
            <td>' . $noContestados . ' </td>
        </tr>
    </tbody>
</table>
               Sistemas IVR - ATC Multiservicios.<br>
               (Este email es generado automaticamente. Favor de no responder.)';
$mail->altBody = 'La Campaña ' . $nom_camp . ' a Finalizado<br>
               Puede verificarlo en <a href="http://172.20.30.202/new/">Este link</a> al conectarse.<br><br>

               Sistemas IVR - ATC Multiservicios.<br>
               (Este email es generado automaticamente. Favor de no responder.)';


$envio = $mail->Send();
if ($envio) {
    echo "Email enviado correctamente";
} else {
    echo "Erro: " . $mail->ErrorInfo;
}
?>
<!--
<table  style="text-align:center; margin: auto" width="320" border="0" cellspacing="0" cellpadding="0">
    <thead>
        <tr bgcolor="#333333" style="color:#ffffff">
            <td style="width:15%">Total</td>
            <td style="width:25%">Contestados</td>
            <td style="width:35%">No Contestados</td>
        </tr>
    </thead>
    <tbody>
        <tr bgcolor="#F0F0F0">
            <td>' . $total . ' </td>
            <td>' . $contestados . '  </td>
            <td>' . $noContestados . ' </td>
        </tr>
    </tbody>
</table>-->