<!DOCTYPE html>
<?php
error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
session_start();
clearstatcache();
if ($_SESSION['idusuario'] == "") {

    //si no existe, va a la página de autenticacion    
    header("Location: salir");
    //salimos de este script
    ?>
    <script>
        window.location.href = "index.php";
    </script>
    <?php
    exit();
}
include 'informacion.php';
require('includes_head.php');
if (isset($_GET['mod'])) {
    $mod = $_GET['mod'];
}
//
$color = "#2774E6";
$userAgent = $_SERVER[HTTP_USER_AGENT];
$userAgent1 = strtolower($userAgent);
//echo $userAgent . "<br>";
//echo $userAgent1 . "<br>";
if (strpos($userAgent1, "android") !== false || strpos($userAgent1, "iphone") !== false) {
    //print ("<P>OS Client: Android $_SERVER[REMOTE_ADDR]");
    $newDimensionNav = "width: 1000px;";
    $newDimensionBody = 'style="' . $newDimensionNav . '"';
} else {
    $newDimensionNav = "";
    $newDimensionBody = "";
}
?>
<style>
    .navbar-brand {
        color: #bcc0cd;
        font-size: 30px;
        font-weight: 100;
        line-height: 35px;
        margin-top: 25px;
        padding: 0;
        font-style: italic;
        font-weight: bold;
    }
    .navbar-brand .naranja{
        color: #ff7d4b;
        font-size: 38px;
        font-family: 'URW Chancery L';
        font-style: italic;
        text-decoration: underline;
        font-weight: bold;
    }
    p.az {
        line-height: 5px;
    }
    .navbar-brand .azul {
        color: #0000fe;
        font-size: 13px;
    }
</style>
<!-- Calendario Color -->
<style> 
    .ui-datepicker-header {
        color: white;
        background: #f6a828 url(images/ui-bg_gloss-wave_35_f6a828_500x100.png) 50% 50% repeat-x;
    }
    .ui-datepicker .ui-datepicker-prev {
        left: 2px;
    }
    .ui-datepicker .ui-datepicker-prev, .ui-datepicker .ui-datepicker-next {
        position: absolute;
        top: 2px;
        width: 1.8em;
        height: 1.8em;
    }
    .ui-datepicker-calendar th span {
        color: #8888a2;
    }
    .ui-state-highlight{
        color: red;
    }
</style>
<body style="<?php echo $newDimensionNav; ?>;background-color: <?php echo $color; ?>" class="nav-md"  >
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col" >
                <div class="left_col scroll-view">

                    <div class="navbar nav_title" style="border: 2;">
                        <a href="inicio" class="site_title"><i class="fa fa-newspaper-o"></i><span> GNMision </span></a>
                    </div>
                    <!--                    <div class="navbar nav_title" style="border: 0;">
                    
                                            <div class="hidden">
                                                <div style="
                                                     width: 120px;
                                                     height:53px;
                                                     background-image: url(images/mbnLogo.jpeg); 
                                                     background-repeat: no-repeat;
                                                     background-position-x: 5px;
                                                     float: right;
                                                     border-width: 1px;
                                                     background-size: 100% 100%;">
                                                </div>
                                            </div>
                    
                                            <br><br><div class="clearfix"></div>
                                        </div><br><br>-->

                    <div class="clearfix"></div>
                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="images/Fotos/<?php echo $_SESSION['foto']; ?>" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span style="color: white;">Bienvenido(a),</span>
                            <h2><?php echo $_SESSION['nombres']; ?></h2>
                            <small class="label"><?php echo $_SESSION['nivel']; ?></small>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->
                    <br />
                    <br>
                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <h3> Administracion y Gestión </h3>                         
                            <ul class="nav side-menu" >
                                <li><a href="inicio"><i class="fa fa-home"></i> Inicio</a></li>
                                <li><a><i class="fa fa-group"></i> Agenda Virtual <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu"  style="display: none" >
                                        <div class="col-md-1"></div><label class="label"><i class="fa fa-tags"></i> Registros</label>
                                        <li><a href="listAgndPer?mod=lPersonas&idpais=<?php echo $_SESSION['idpais']; ?>&idsede=<?php echo $_SESSION['idsede']; ?>&idgrupo=<?php echo $_SESSION['idgrupos']; ?>&S=<?php echo $_SESSION['sexo']; ?>"> List. General</a>
                                        </li> 
                                        <li><a href="listAgndMin?mod=lMinistros&idpais=<?php echo $_SESSION['idpais']; ?>&iddepartamento=<?php echo $_SESSION['iddepartamento']; ?>&idcargo=61&S=M">List. Ministros</a>
                                        </li>
                                        <li><a href="listAgnReZo?mod=lReunZonal&idpais=<?php echo $_SESSION['idpais']; ?>&idsede=<?php echo $_SESSION['idsede']; ?>&idreunion_granzona=0&idreunion_zonal=63">List. Reunion Zonal</a>
                                        </li>
                                        <li><a href="repor1">Filtros</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-flag"></i> World Camp 2016 <span class="fa fa-chevron-down"></span></a>
<!--                                        <li><a href="listPers?mod=lPersonas&idpais=<?php // echo $_SESSION['idpais'];                                            ?>">Listado General</a>-->
                                    <ul class="nav child_menu"  style="display: none" >
                                        <div class="col-md-1"></div><label class="label"><i class="fa fa-tags"></i> Registros</label>
                                        <li><a href="listPers?mod=lPersonas&idpais=36">Listado General</a>
                                        </li> 

                                        <?php if (!isset($_GET['mod'])) { ?>
                                            <li><a href="#modalNuevaPersona" data-toggle="modal"> Registro de Personas</a>
                                            </li>
                                        <?php } ?>
                                        <?php if ($_SESSION['id_nivel'] == '1') { ?>
                                            <li><a href="newInsc?mod=nInscripcion&idpais=<?php echo $_SESSION['idpais']; ?>&S=M&idpersona=0">Incripción</a>
                                            </li>
                                        <?php } ?>
                                        <?php if ($_SESSION['id_nivel'] == '1' || $_SESSION['id_nivel'] == '2') { ?>
                                            <li><a href="listInc?mod=lInscripcion&idpais=<?php echo $_SESSION['idpais']; ?>&S=M&idpersona=0">Listado General Inscripción</a>
                                            </li>
                                        <?php } ?>
                                        <div class="col-md-1"></div><label class="label"><i class="fa fa-bar-chart"></i> Reportes</label>
                                        <li><a href="repor1">Filtros x Grupos</a>
                                        </li>
                                        <li><a href="repor2">Resumen de Pagos</a>
                                        </li>
                                        <li><a href="repor3">Cantidad Varones y Mujeres</a>
                                        </li>
                                        <li><a href="repor4">Reporte de Pagos x Fecha</a>
                                        </li>
                                        <li><a href="repor5">Inscritos x Zona</a>
                                        </li>
                                        <div class="col-md-1"></div><label class="label"><i class="fa fa-navicon"></i> Asignaciones Grupos </label>
                                        <li><a href="newAsig?mod=nAsignacion">Nueva Asignación</a></li>
                                    </ul>  
                                    <div class="clearfix"></div>
                                </li>
                            </ul> 
                            <?php if (!isset($_GET['mod'])) { ?>
                                <!--############################################ MODAL NUEVO AGENTE ############################################--> 
                                <div class="modal fade" id="modalNuevaPersona" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <form class="form-horizontal" role="form" action="persNews" method="get">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title">Nueva Persona</h4>
                                                </div>
                                                <div class="modal-body">                        
                                                    <div class="col-md-12">
                                                        <label>Condición</label>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="hidden" name="mod" value="nPersona"/>
                                                                <span class="input-group-addon"><span class="glyphicon glyphicon-heart"></span></span>
                                                                <select class='form-control' name='idcategoria' id='idcategoria'  >
                                                                    <option value="1">Miembro</option>
                                                                    <option value="2">Invitado</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                                    <button type="submit" id="nuevoAgente" name="nuevoAgente" class="btn btn-primary">Siguiente >> </button>
                                                </div>
                                            </form>
                                        </div> <!--/.modal-content -->
                                    </div> <!--/.modal-dialog -->
                                </div> <!--/.modal -->
                                <div class="clearfix"></div>
                            <?php } ?>
                        </div>

                        <div class="menu_section">
                            <h3>Configuración</h3>
                            <ul class="nav side-menu">
                                <?php if ($_SESSION['id_nivel'] == '1' || $_SESSION['id_nivel'] == '2') { ?>
                                    <li><a><i class="fa fa-user"></i>Usuarios <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">
                                            <li><a href="userLista?mod=lUsuarios&int=1">Lista de Usuarios</a>
                                            </li>
                                        </ul>
                                    </li>
                                <?php } ?>  
                                <li><a><i class="fa fa-map-marker"></i> Ubicación<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <div class="col-md-1"></div><label class="label"><i class="fa fa-tags"></i>Localización</label>
                                        <!--<li><a href="listPais">País</a></li>-->
                                        <li><a href="listDepa?mod=lDepa&idpais=89">Departamento</a></li>
                                        <li><a href="listProv?mod=lProv&idpais=89&iddepartamento=1610">Provincia</a></li>
                                        <li><a href="listDist?mod=lDist&idpais=89&iddepartamento=1610&idprovincia=127">Distrito</a></li>
                                    </ul>
                                </li>
                                <?php if ($_SESSION['id_nivel'] == '1' || $_SESSION['id_nivel'] == '2') { ?>
                                    <li><a><i class="fa fa-database"></i> Mantenimientos <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">
                                            <div class="col-md-1"></div><label class="label"><i class="fa fa-tags"></i>Listas</label>
                                            <li><a href="listCarg?mod=lCarg">Cargos</a></li>
                                            <li><a href="listCate?mod=lCate">Categoría</a></li>
                                            <li><a href="listEsCi?mod=lEsCi">Estado Civil</a></li>
                                            <li><a href="listGrup?mod=lGrup">Grupos</a></li>
                                            <li><a href="listTiDo?mod=lTiDo">Tipo de Documento</a></li>
                                            <li><a href="listRzon?mod=lReZo">Reunion Zonal</a></li>
                                            <li><a href="listRgZo?mod=lRgZo">Gran Zona</a></li>
                                        </ul>
                                    </li>
                                <?php } ?>
                                <?php if ($_SESSION['id_nivel'] == '1') { ?>
                                    <li><a><i class="fa fa-gears"></i> Opciones<span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">
                                            <li><a href="listTusu?mod=lTiUs">Tipo de Usuario</a></li>
                                            <li><a href="listEsta?mod=lEsta">Estado</a></li>
                                        </ul>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="menu_section">
                            <ul class="nav side-menu">
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu" >
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>
                        <ul class="nav navbar-nav navbar-right">
                            <div class="col-md-1"></div>
                            <div class="hidden">
                                <small id="innerHTML"></small>
                            </div>
                            <li class="">
                                <a href="salir">
                                    <i class="fa fa-power-off"></i>
                                </a>
                            </li>
                            <li class="">
                                <a href="" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="images/Fotos/<?php echo $_SESSION['foto']; ?>"><?php echo $_SESSION['nombres']; ?>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <?php $id_personal = $_SESSION['idpersona']; ?>
                                    <li>
                                        <a href="persDetalle?mod=mPersona&idpersona=<?php echo $id_personal ?>&perfil=1">  Perfil</a>
                                    </li>
                                    <li>
                                        <a href="php/doc/ayuda.php" target="_blank" onclick="window.open(this.href, this.target, 'width=1100,height=700,top=0,left=700,resizable=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes');
                                                return false;">Ayuda</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="">
                                <a href="inicio">
                                    <i class="fa fa-flag"> <?php echo $_SESSION['pais']; ?></i>
                                </a>
                            </li>
                            <li class="">
                                <a href="inicio">
                                    <i class="fa fa-briefcase"> <?php echo $_SESSION['cargo']; ?></i>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <!-- /top navigation -->

