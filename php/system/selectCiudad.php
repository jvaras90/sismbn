<?php
if (isset($_POST['q'])) {
    $q = $_POST['q'];
    $mod = $_POST['mod'];
    ?>
    <select class='form-control  has-feedback-left' name='idciudad' id='idciudad' >
        <option value="0">Ciudad</option>
        <?php
        include_once 'crearConexion.php';
        $stmt = "select * from ciudad where idpais =" . $q . " order by descripcion";
        $res = $mysqlMBN->consultas($stmt);
        while ($rsc = mysqli_fetch_array($res)) {
            echo "<option value=" . $rsc['idciudad'] . " ";
            if ($idciudad == $rsc['idciudad']) {
                echo " selected ";
            }
            echo ">" . $rsc['descripcion'] . "</option>";
        }
        ?>
    </select>
    <?php
    if ($mod == 'mInscripcion' || $mod == 'nPersona'|| $mod == 'lPersona') {?>
        <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
    <?php }
}else if(isset ($_POST['m'])){
    $q = $_POST['m'];
    $mod = $_POST['mod'];
    $idciudad = $_POST['idciudad'];
    ?>
    <select class='form-control' name='idciudad' id='idciudad' >
        <option value="0">Ciudad</option>
        <?php
        include_once 'crearConexion.php';
        $stmt = "select * from ciudad where idpais =" . $q . " order by descripcion";
        $res = $mysqlMBN->consultas($stmt);
        while ($rsc = mysqli_fetch_array($res)) {
            echo "<option value=" . $rsc['idciudad'] . " ";
            if ($idciudad == $rsc['idciudad']) {
                echo " selected ";
            }
            echo ">" . $rsc['descripcion'] . "</option>";
        }
        ?>
    </select>
    <?php
} else {
    $qry = "select * from ciudad order by descripcion;";
    $res = $mysqlMBN->consultas($qry);
    while ($fila = mysqli_fetch_array($res)) {
        $id_ciudad = $fila['idciudad'];
        $descripcion = $fila['descripcion'];
        echo "<option value='$id_ciudad'";
        if ($idciudad == $id_ciudad) {
            echo "selected";
        }
        echo ">$descripcion</option>";
    }
}