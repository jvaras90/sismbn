<?php
if (isset($_POST['q'])) {
    $q = $_POST['q'];
    ?>
    <select class='form-control' name='idreunion_zonal' id='idreunion_zonal'  >
        <option value="0"> Zona </option>
        <?php
        include_once 'crearConexion.php';
        $stmt = "select * from reunion_zonal where idreunion_granzona =" . $q . " order by descripcion";
        $res = $mysqlMBN->consultas($stmt);
        while ($rsc = mysqli_fetch_array($res)) {
            echo "<option value=" . $rsc['idreunion_zonal'] . " ";
            if ($idreunion_zona1 == $rsc['idreunion_zonal']) {
                echo " selected ";
            }
            echo ">" . $rsc['descripcion'] . "</option>";
        }
        ?>
    </select>
    <?php
} else if (isset($_POST['m'])) {
    $q = $_POST['m'];
    $mod = $_POST['mod'];
    ?>
    <select class='form-control has-feedback-left' name='idreunion_zonal' id='idreunion_zonal'  >
        <option value="0"> Zona </option>
        <?php
        include_once 'crearConexion.php';
        $stmt = "select * from reunion_zonal where idpais =" . $q . " order by descripcion";
        $res = $mysqlMBN->consultas($stmt);
        while ($rsc = mysqli_fetch_array($res)) {
            echo "<option value=" . $rsc['idreunion_zonal'] . " ";
            if ($idreunion_zona1 == $rsc['idreunion_zonal']) {
                echo " selected ";
            }
            echo ">" . $rsc['descripcion'] . "</option>";
        }
        ?>
    </select>
    <?php
    if ($mod == 'mInscripcion' || $mod == 'nPersona') {
        echo '<span class="fa fa-bullseye form-control-feedback left" aria-hidden="true"></span>';
    }
} else {
    if (isset($_GET['idpersona'])) {
        if (isset($_GET['S'])) {
            $qry = "select * from reunion_zonal order by descripcion;";
        } else {
            $qry = "select * from reunion_zonal where idreunion_granzona = $idreunion_granzona1 order by descripcion;";
        }
    } else {
        $qry = "select * from reunion_zonal order by descripcion;";
    }

    $res = $mysqlMBN->consultas($qry);
    while ($fila = mysqli_fetch_array($res)) {
        $id_reunion_zonal = $fila['idreunion_zonal'];
        $descripcion = $fila['descripcion'];
        echo "<option value='$id_reunion_zonal'";
        if ($idreunion_zona1 == $id_reunion_zonal) {
            echo "selected";
        }
        echo ">$descripcion</option>";
    }
}