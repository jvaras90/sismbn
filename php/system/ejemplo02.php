<?php

$nombreArchivo = "Listado de Hermanos Casados";
//============================================================+
// File name   : example_061.php
// Begin       : 2010-05-24
// Last Update : 2014-01-25
//
// Description : Example 061 for TCPDF class
//               XHTML + CSS
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: XHTML + CSS
 * @author Nicola Asuni
 * @since 2010-05-25
 */
// Include the main TCPDF library (search for installation path).
//require_once('tcpdf_include.php');

require_once('../../pdf/example/tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('' . $nombreArchivo . '');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, ' Listado de Hermanos Casados');

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
//$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
//$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
// set image scale factor
//$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
    require_once(dirname(__FILE__) . '/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------
// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage();

/* NOTE:
 * *********************************************************
 * You can load external XHTML using :
 *
 * $html = file_get_contents('/path/to/your/file.html');
 *
 * External CSS files will be automatically loaded.
 * Sometimes you need to fix the path of the external CSS.
 * *********************************************************
 */

// define some HTML content with style

$html = '
<!-- EXAMPLE OF CSS STYLE -->
<style>
	h1 {
		color: navy;
		font-family: times;
		font-size: 24pt;
		text-decoration: underline;
                text-align:center;
	} 
	table.first {
                width: 480px;
		color: #003300;
		font-family: helvetica;
		font-size: 8pt;
		border-left: 1px solid black;
		border-right: 1px solid black;
		border-top: 1px solid black;
		border-bottom: 1px solid black;
		background-color: white;
                text-align:center;
                margin:auto;
	}
	td {
		border: 1px solid black;
		background-color: white;
	} 
	.lowercase {
		text-transform: lowercase;
	}
	.uppercase {
		text-transform: uppercase;
	}
	.capitalize {
		text-transform: capitalize;
	}    
</style> 
  <br>  ';

$tipo_documento = "DNI";
$a = "aa";
$edad = 28;
$condicion = "Miembro";
$foto = "46241427.jpg";
for ($x = 0; $x < 31; $x++) {
    $html .='
<br>
  <table class="first" border="0" cellspacing="0" cellpadding="0">
        <tbody>
                                    <tr>  
                                     <td  rowspan="8"  style="width:80px;height: 80px;"   >   
                                         <img src="images/46241427.jpg" style="width:80px;height: 80px; top:0px;">
                                         
                                        </td>                                
                                        <td colspan="8"><h2 class="brief"><i> Jean Harold Varas Ramirez [' . $condicion . '] </i></h2></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Fecha Nac: </strong></td>
                                        <td>  ' . $a . ' </td>
                                        <td><strong>Edad </strong></td>
                                        <td>  ' . $a . ' </td>
                                        <td><strong> ' . $tipo_documento . ': </strong></td>
                                        <td>  ' . $a . ' </td>
                                        <td><strong>Estado: </strong></td>
                                        <td>  ' . $a . ' </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Grupo: </strong></td>
                                        <td>  ' . $a . ' </td>
                                        <td><strong>Sexo: </strong></td>
                                        <td>  ' . $a . ' </td>
                                        <td><strong>Tel&eacute;fono: </strong></td>
                                        <td>  ' . $a . ' </td>
                                        <td><strong>Celular: </strong></td>
                                        <td>  ' . $a . ' </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Lugar Nac: </strong></td>
                                        <td colspan="3">' . $a . ' </td>
                                        <td><strong>R. Zonal: </strong></td>
                                        <td colspan="3">' . $a . ' </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Ocupación: </strong></td>
                                        <td colspan="3">' . $a . ' </td>
                                        <td><strong>Trab. Actual: </strong></td>
                                        <td colspan="3">' . $a . ' </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Cargo: </strong></td>
                                        <td colspan="3">' . $a . ' </td>
                                        <td><strong>E-mail: </strong></td>
                                        <td colspan="3">' . $condicion . '</td>  
                                    </tr> 
                                    <tr>
                                        <td><strong>Ciudad: </strong></td>
                                        <td colspan="8"> ' . $a . '</td> 
                                    </tr> 
                                    <tr>
                                        <td><strong>Dirección: </strong></td>
                                        <td colspan="8">' . $a . ' </td>                                            
                                    </tr>  
                                    </tbody>
                                    
</table><br>
';
}
$html .='


';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------
//Close and output PDF document
$pdf->Output('' . $nombreArchivo . '.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
