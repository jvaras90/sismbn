<?php

$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
$dias = array("Lunes","Martes","Miercoles","Jueves","Viernes","Sábado","Domingo");

$diaNum = date("d");
$diaNom = $dias[date('N') - 1];
$mesNum = date("m");
$mesNom = $meses[date('n') - 1];
$año = date("o");

$tittle = '';

//Variables
$tittle7 = 'Nombre completo del cliente';
$tittle8 = 'Número de tarjeta, Número de cuenta, Número de referido. (Distinción del producto)';

//Fechas
$tittle1 = "Fecha Larga : $diaNum de $mesNom del $año";
$tittle9 = 'Dia actual en formato de número :  '.$diaNum.' ';
$tittle10 = 'Dia actual en formato de nombre : '.$diaNom.'';
$tittle11 = 'Mes actual en formato de número : '.$mesNum.'';
$tittle12 = 'Mes actual en formato de nombre : '.$mesNom.'';
//$tittle13 = 'Un Dia anterior a la fecha limite [ Fecha Limite - 1 ] : '.$dia_anterior.'';

//Firmas
$tittle2 = 'Nombre del Supervisor';
$tittle3 = 'Celular de contacto al que se comunicará el cliente';
$tittle4 = 'Telefono Fijo de la empresa';
$tittle5 = 'Anexo del negocio';
$tittle6 = 'Correo del Supervisor';
