<?php
if (isset($_POST['q'])) {
    $q = $_POST['q'];
    ?>
    <select class='form-control' name='idreunion_granzona' id='idreunion_granzona' onchange="load_reunion_zonal(this.value)" >
        <option value="99"> Gran Zona </option>
        <?php
        include_once 'crearConexion.php';
        $stmt = "select * from reunion_granzona where idsede = " . $q . " and idestado = 1 order by descripcion";
        $res = $mysqlMBN->consultas($stmt);
        while ($rsc = mysqli_fetch_array($res)) {
            echo "<option value=" . $rsc['idreunion_granzona'] . " ";
            if ($idreunion_granzona1 == $rsc['idreunion_granzona']) {
                echo " selected ";
            }
            echo ">" . $rsc['descripcion'] . "</option>";
        }
        ?>
    </select>
    <?php
} else {
    $qry = "select * from reunion_granzona order by descripcion;";
    $res = $mysqlMBN->consultas($qry);
    while ($fila = mysqli_fetch_array($res)) {
        $id_reunion_granzona = $fila['idreunion_granzona'];
        $descripcion = $fila['descripcion'];
        echo "<option value='$id_reunion_granzona'";
        if ($idreunion_granzona1 == $id_reunion_granzona) {
            echo "selected";
        }
        echo ">$descripcion</option>";
    }
}