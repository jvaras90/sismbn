<?php

include_once 'cargadorClases.php';
//include 'crearConfigAnexos.php';
//include 'clases/CUpdate.php';
//include 'envioMail.php';
session_start();
$form = $_REQUEST['form'];

if (isset($_POST['form'])) {
    $form = $_POST['form'];
    if ($form == 'modificarPersona') {
        include 'crearConexion.php';
        //los que siempre estarán
        $idpersona = $_POST['idpersona'];
        $mod = $_POST['mod'];
        $nombre = $_POST['nombre'];
        $condicion = $_POST['condicion'];
        $aPaterno = $_POST['aPaterno'];
        $aMaterno = $_POST['aMaterno'];
        $idtipo_documento = $_POST['idtipo_documento'];
        $dni = $_POST['dni'];
        $sexo = $_POST['S'];
        $idestado_civil = $_POST['idestado_civil'];
        $fecha_nac = $_POST['fecha_nac1'];
        $lugar_nac = $_POST['lugar_nac'];
        $profesion = $_POST['profesion'];
        $ocupacion = $_POST['ocupacion'];
        $telefono = $_POST['telefono'];
        $celular = $_POST['celular'];
        $email = $_POST['email'];
        $referencia = $_POST['referencia'];
        $idpais = $_POST['idpais'];
        $iddepartamento = $_POST['iddepartamento'];
        $idprovincia = $_POST['idprovincia'];
        $iddistrito = $_POST['iddistrito'];
        $direccion = $_POST['direccion'];
        $idcategoria = $_POST['idcategoria'];
        $idgrupo = $_POST['idgrupo'];
        $idsede = $_POST['idsede'];

        if ($idcategoria == 1) {
            $fecha_salvacion = $_POST['fecha_salvacion1'];
            $s_referencia = $_POST['s_referencia'];
            $idgrupo = $_POST['idgrupo'];
            $idreunion_granzona = $_POST['idreunion_granzona'];
            $idreunion_zonal = $_POST['idreunion_zonal'];

            foreach ($_POST['idcargoM'] as $indice => $contenido) {
                $idCargosVarios[] = $contenido;
                $valores .= "$contenido,";
                $idcargos = substr($valores, 0, -1);
            }
        }

        $sqlU = "UPDATE persona SET nombres='$nombre', apaterno='$aPaterno', amaterno='$aMaterno', idtipo_documento='$idtipo_documento', 
        nro_documento='$dni', sexo='$sexo', telefono_fijo='$telefono', celular='$celular', lugar_nacimiento='$lugar_nac', idsede='$idsede',
        fec_nac='$fecha_nac', email='$email', referencia='$referencia', profesion='$profesion', ocupacion='$ocupacion', idcategoria='$idcategoria', 
        iddistrito='$iddistrito', direccion='$direccion', idestado_civil='$idestado_civil', idgrupos='$idgrupo' , idpais='$idpais' , iddepartamento='$iddepartamento',idprovincia='$idprovincia'
        WHERE idpersona='$idpersona';";
        //echo $sqlU . "<br>";
        $mysqlMBN->insUpdDel($sqlU);

        if ($idcategoria == 1) {
            $sqlUD = "UPDATE detalle_salvacion SET fecha_salvacion='$fecha_salvacion', referencia='$s_referencia', idcargo='$idcargos',
            idreunion_granzona='$idreunion_granzona', idreunion_zonal='$idreunion_zonal' WHERE idpersona='$idpersona';";
            //echo $sqlUD . "<br>";
            $mysqlMBN->insUpdDel($sqlUD);
        }
        header("Location: persDetalle?mod=$mod&idpersona=$idpersona&msj=3");
    } elseif ($form == 'modificarUsuarios') {
        include 'crearConexion.php';
        $idusuario = $_POST['idusuario'];
        $idtipo_usuario = $_POST['idtipo_usuario'];
        $idestado = $_POST['idestado'];
        $o_password = $_POST['o_password'];
        $password = $_POST['password'];
        $password2 = $_POST['repassword'];
        $sql = "select * from usuario where idusuario = $idusuario and pass = md5('$o_password') limit 1;";
        $res = $mysqlMBN->consultas($sql);
        $num_regs = mysqli_num_rows($res);
        //echo $num_regs;
        if ($num_regs > 0) {
            if ($password == "" && $password2 == "") {
                $sqlU = "UPDATE usuario SET idtipo_usuario='$idtipo_usuario', idestado='$idestado' WHERE idusuario='$idusuario';";
                $msj = 3;
            } else {
                $sqlU = "UPDATE usuario SET pass=md5('$password'), pass2='$password', idtipo_usuario='$idtipo_usuario', idestado='$idestado' WHERE idusuario='$idusuario';";
                $msj = 3;
            }
            $mysqlMBN->insUpdDel($sqlU);
        } else {
            $msj = 5; /* ingreso mal la contraseña */
        }
        header("Location: userLista?idusuario=$idusuario&msj=$msj");
    } else if ($form == 'modificarInscripcion') {
        include 'crearConexion.php';
        $idinscripcion = $_POST['idinscripcion'];
        $mod = $_POST['mod'];
        if (isset($_POST['volunt'])) {
            $voluntario = 1;
        } else {
            $voluntario = 0;
        }
        $nombres = $_POST['nombres'];
        $sexo = $_POST['S'];
        $aPaterno = $_POST['aPaterno'];
        $aMaterno = $_POST['aMaterno'];
        $idpersona = $_POST['idpersona'];
        $edad = $_POST['edad'];
        $nro_documento = $_POST['nro_documento'];
        $telefono = $_POST['telefono'];

        foreach ($_POST['idcargoM'] as $indice => $contenido) {
            $idCargosVarios[] = $contenido;
            $valores .= "$contenido,";
            $idcargos = substr($valores, 0, -1);
        }

        $idgrupo = $_POST['idgrupo'];
        $grupo = $_POST['grupo'];
        $idcategoria1 = $_POST['idcategoria1'];
        $idpais = $_POST['idpais'];
        $idreunion_zonal = $_POST['idreunion_zonal'];
        $idciudad = $_POST['idciudad'];
        $idtalla_polo = $_POST['idtalla_polo'];
        $idestado_pago = $_POST['idestado_pago'];
        $observacion = $_POST['observacion'];

        /* Actualizar la tabla persona */
        $sqlUP = "UPDATE persona SET nombres='$nombres', apaterno='$aPaterno', amaterno='$aMaterno', nro_documento='$nro_documento', 
        sexo='$sexo', idcategoria='$idcategoria1', idgrupos='$idgrupo' WHERE idpersona='$idpersona';";
        $mysqlMBN->insUpdDel($sqlUP);
        /* Actualizar la tabla detalle_salvacion */
        $sqlUD = "UPDATE detalle_salvacion SET idreunion_zonal='$idreunion_zonal' 
        WHERE idpersona='$idpersona';";
        $mysqlMBN->insUpdDel($sqlUD);
        /* Actualizar la tabla incripciones_2016 */
        $sqlUI = "UPDATE incripciones_2016 SET edad='$edad', idgrupo='$idgrupo', idciudad='$idciudad',
        dni='$nro_documento', cargo='$idcargos', sexo='$sexo', idreunion_zonal='$idreunion_zonal', idcategoria='$idcategoria1', 
        idestado_pago='$idestado_pago', celular='$telefono', idtalla_polo='$idtalla_polo', grupo='$grupo', 
        voluntario='$voluntario', observacion='$observacion' WHERE idinscripciones='$idinscripcion';";
        $mysqlMBN->insUpdDel($sqlUI);

        $sqlDel = "delete from boleta_2016 where idinscripcion = $idinscripcion;";
        $mysqlMBN->insUpdDel($sqlDel);
        foreach ($_REQUEST as $indice => $contenido) {
            if ($indice != "PHPSESSID") {
                if (substr($indice, 0, 9) == "fechaPago") {
                    $fecha_pago[] = $contenido;
                } elseif (substr($indice, 0, 9) == "nroBoleta") {
                    $nroBoleta[] = $contenido;
                } else if (substr($indice, 0, 10) == "montoSoles") {
                    $montoSoles[] = $contenido;
                } else if (substr($indice, 0, 12) == "montoDolares") {
                    $montoDolares[] = $contenido;
                }
            }
        }
        $cantidadBoletas = count($nroBoleta);
        if ($cantidadBoletas >= 1) { //si las bolestas son mayores igual a 0
            for ($x = 0; $x < $cantidadBoletas; $x++) {
                $insertarBoletas = "INSERT INTO boleta_2016 (idinscripcion,nro_boleta,monto_soles,monto_dolares,fecha_pago,fecha_registro,lastupdated,idusuario) VALUES ('$idinscripcion','$nroBoleta[$x]', '$montoSoles[$x]', '$montoDolares[$x]',  '$fecha_pago[$x]',now(), now()," . $_SESSION['idusuario'] . ");";
                //echo $insertarBoletas . "<br>";
                $mysqlMBN->insUpdDel($insertarBoletas);
            }
        }
        header("Location: listInc?mod=$mod&idinscripcion=$idinscripcion&msj=3");
    } else if ($form == 'modificarCargos') {
        $datos['descripcion'] = $_POST['descripcion'];
        $datos['idestado'] = $_POST['idestado'];
        $datos['idcargo'] = $_POST['idcargo'];
        $registro = new CUpdate($datos, 'cargo');
        header("Location: listCarg?idcargo=" . $_POST['idcargo'] . "&msj=3");
    } else if ($form == 'modificarCategorias') {
        $datos['descripcion'] = $_POST['descripcion'];
        $datos['idestado'] = $_POST['idestado'];
        $datos['idcategoria'] = $_POST['idcategoria'];
        $registro = new CUpdate($datos, 'categoria');
        header("Location: listCate?idcategoria=" . $_POST['idcategoria'] . "&msj=3");
    } else if ($form == 'modificarDepartamentos') {
        $datos['des_departamento'] = $_POST['descripcion'];
        $datos['idestado'] = $_POST['idestado'];
        $datos['iddepartamento'] = $_POST['iddepartamento'];
        $registro = new CUpdate($datos, 'departamento');
        header("Location: listDepa?idpais=" . $_POST['idpais'] . "&iddepartamento=" . $_POST['iddepartamento'] . "&msj=3");
    } else if ($form == 'modificarDistritos') {
        $datos['des_distrito'] = $_POST['descripcion'];
        $datos['idprovincia'] = $_POST['idprovincia'];
        $datos['idestado'] = $_POST['idestado'];
        $datos['iddistrito'] = $_POST['iddistrito'];
        $registro = new CUpdate($datos, 'distrito');
        header("Location: listDist?idprovincia=" . $_POST['idprovincia'] . "&iddistrito=" . $_POST['iddistrito'] . "&msj=3");
    } else if ($form == 'modificarEstados') {
        $datos['descripcion'] = $_POST['descripcion'];
        $datos['idestado'] = $_POST['idestado'];
        $registro = new CUpdate($datos, 'estado');
        header("Location: listEsta?idestado=" . $_POST['idestado'] . "&msj=3");
    } else if ($form == 'modificarGrupos') {
        $datos['descripcion'] = $_POST['descripcion'];
        $datos['idestado'] = $_POST['idestado'];
        $datos['idgrupos'] = $_POST['idgrupos'];
        $registro = new CUpdate($datos, 'grupos');
        header("Location: listGrup?idgrupos=" . $_POST['idgrupos'] . "&msj=3");
    } else if ($form == 'modificarEstadoCivil') {
        $datos['descripcion'] = $_POST['descripcion'];
        $datos['idestado'] = $_POST['idestado'];
        $datos['idestado_civil'] = $_POST['idestado_civil'];
        $registro = new CUpdate($datos, 'estado_civil');
        header("Location: listEsCi?idestadoCivil=" . $_POST['idestado_civil'] . "&msj=3");
    } else if ($form == 'modificarProvincias') {
        $datos['des_provincia'] = $_POST['descripcion'];
        $datos['iddepartamento'] = $_POST['iddepartamento'];
        $datos['idestado'] = $_POST['idestado'];
        $datos['idprovincia'] = $_POST['idprovincia'];
        $registro = new CUpdate($datos, 'provincia');
        header("Location: listProv?iddepartamento=" . $_POST['iddepartamento'] . "&idprovincia=" . $_POST['idprovincia'] . "&msj=3");
    } else if ($form == 'modificarreuGranZon') {
        $datos['descripcion'] = $_POST['descripcion'];
        $datos['idestado'] = $_POST['idestado'];
        $datos['idreunion_granzona'] = $_POST['idreunion_granzona'];
        $registro = new CUpdate($datos, 'reunion_granzona');
        header("Location: listRgZo?idreunion_granzona=" . $_POST['idreunion_granzona'] . "&msj=3");
    } else if ($form == 'modificarreuZonal') {
        $datos['idreunion_granzona'] = $_POST['idreunion_granzona'];
        $datos['descripcion'] = $_POST['descripcion'];
        $datos['idestado'] = $_POST['idestado'];
        $datos['idreunion_zonal'] = $_POST['idreunion_zonal'];
        $registro = new CUpdate($datos, 'reunion_zonal');
        header("Location: listRzon?idreunion_zonal=" . $_POST['idreunion_zonal'] . "&msj=3");
    } else if ($form == 'modificartipUsuarios') {
        $datos['descripcion'] = $_POST['descripcion'];
        $datos['idestado'] = $_POST['idestado'];
        $datos['idtipo_usuario'] = $_POST['idtipo_usuario'];
        $registro = new CUpdate($datos, 'tipo_usuario');
        header("Location: listTusu?idtipo_usuario=" . $_POST['idtipo_usuario'] . "&msj=3");
    } else if ($form == 'modificarTipoDocumento') {
        $datos['descripcion'] = $_POST['descripcion'];
        $datos['idestado'] = $_POST['idestado'];
        $datos['idtipo_documento'] = $_POST['idtipo_documento'];
        $registro = new CUpdate($datos, 'tipo_documento');
        header("Location: listTiDo?idtipo_documento=" . $_POST['idtipo_documento'] . "&msj=3");
    } else if ($form == 'formCambiarEstado') {
        include 'crearConexion.php';
        $mod = $_POST['mod'];
        $idpais = $_POST['idpais'];
        $idsede = $_POST['idsede'];
        $idgrupo = $_POST['idgrupo'];
        $idcategoria = $_POST['idcategoria'];
        $S = $_POST['S'];
        $idEstadoOld = $_POST['idestado_old'];
        $idpersona = $_POST['idpersona'];
        $idestado = $_POST['idestado2'];
        $motivo = $_POST['motivo'];
        $sql1 = "INSERT INTO sol_cambio_est (`idpersona`, `motivo`, `idestado_old`, `idestado_new`) VALUES ('$idpersona', '$motivo', '$idEstadoOld', '$idestado');";
        $sql2 = "UPDATE persona SET `idestado`='$idestado' WHERE `idpersona`='$idpersona';";
        echo "sql 1 : $sql1 <br>";
        echo "Sql 2 ; $sql2 <br>";
        echo "Location: listAgndPer?mod=$mod&idpais=$idpais&idsede=$idsede&idgrupo=$idgrupo&idcategoria=$idcategoria&S=$S&msj=3";

//$mysqlMBN->insUpdDel($sql1);
        //$mysqlMBN->insUpdDel($sql2);
        // header("Location: listAgndPer?mod=$mod&idpais=$idpais&idsede=$idsede&idgrupo=$idgrupo&idcategoria=$idcategoria&S=$S&msj=3");
    }
} elseif (isset($_GET['accion'])) {
    $accion = $_GET['accion'];
    include_once 'crearConexion.php';
    if ($accion == "quitar") {
        $table = $_GET['table'];
        $atrib = $_GET['atrib'];
        $id = $_GET['id'];
        $pag = $_GET['pag'];
        $sql = "update $table set idestado=2 where $atrib=$id;";
        echo $sql;
        $mysqlMBN->insUpdDel($sql);
        if (isset($_GET['idprovincia'])) {
            header("Location: $pag?idprovincia=" . $_GET['idprovincia'] . "&$atrib=$id&msj=4");
        } else if (isset($_GET['iddepartamento'])) {
            header("Location: $pag?iddepartamento=" . $_GET['iddepartamento'] . "&$atrib=$id&msj=4");
        } else {
            header("Location: $pag?$atrib=$id&msj=4");
        }
    }
}
