<?php
if (isset($_POST['q'])) {
    $q = $_POST['q'];
    ?>
    <select class='form-control' name='idprovincia' id='idprovincia'  onchange="load_distrito(this.value)" >
        <option value="0"> Seleccione </option>
        <?php
        include_once 'crearConexion.php';
        $stmt = "select * from provincia where iddepartamento =" . $q . " order by des_provincia";
        $res = $mysqlMBN->consultas($stmt);
        while ($rsc = mysqli_fetch_array($res)) {
            echo "<option value=" . $rsc['idprovincia'] . " ";
            if ($idprovincia == $rsc['idprovincia']) {
                echo " selected ";
            }
            echo ">" . $rsc['des_provincia'] . "</option>";
        }
        ?>
    </select>
    <?php
} else if (isset($_POST['n'])) {
    $q = $_POST['n'];
    $idprov = $_POST['idprov'];
    ?>
    <select class='form-control' name='idprovincia' id='idprovincia'  onchange="reloadPage2(this.value,<?php echo $q;?>)">
        <option value="0"> Seleccione </option>
        <?php
        include_once 'crearConexion.php';
        $stmt = "select * from provincia where iddepartamento =" . $q . " order by des_provincia";
        $res = $mysqlMBN->consultas($stmt);
        while ($rsc = mysqli_fetch_array($res)) {
            echo "<option value=" . $rsc['idprovincia'] . " ";
            if ($idprov == $rsc['idprovincia']) {
                echo " selected ";
            }
            echo ">" . $rsc['des_provincia'] . "</option>";
        }
        ?>
    </select>
    <?php
} else if (isset($_POST['m'])) {
    $m = $_POST['m'];
    ?>
    <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span>
    <select class='form-control' name='idprovincia2' id='idprovincia2' required>
        <option value="0">Seleccione</option>
        <?php
        include_once 'crearConexion.php';
        $stmt = "select * from provincia where iddepartamento =" . $m . " order by des_provincia";
        $res = $mysqlMBN->consultas($stmt);
        while ($rsc = mysqli_fetch_array($res)) {
            echo "<option value=" . $rsc['idprovincia'] . " ";
            if ($idprovincia == $rsc['idprovincia']) {
                echo " selected ";
            }
            echo ">" . $rsc['des_provincia'] . "</option>";
        }
        ?>
    </select>
    <?php
} else {
    if (isset($_GET['idpersona'])) {
        $qry = "select * from provincia where iddepartamento = $iddepartamento order by des_provincia;";
    } else {
        $qry = "select * from provincia order by des_provincia;";
    }
    $res = $mysqlMBN->consultas($qry);
    while ($fila = mysqli_fetch_array($res)) {
        $id_provincia = $fila['idprovincia'];
        $des_provincia = $fila['des_provincia'];
        echo "<option value='$id_provincia'";
        if ($idprovincia == $id_provincia) {
            echo "selected";
        }
        echo ">$des_provincia</option>";
    }
}
