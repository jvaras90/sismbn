<?php

session_start();
include_once 'cargadorClases.php';
//include_once 'clases/CInsert.php';
//include 'envioMail.php';
if (isset($_POST['form'])) {
    $form = $_POST['form'];
    // SISTEMA DE IDENTIFICACION PERSONAL, ANEXOS, NEGOCIOS
    if ($form == 'nuevoCargos') {
        $datos['descripcion'] = $_POST['descripcion'];
        $registro = new CInsert($datos, 'cargo');
        header("Location: listCarg?msj=2");
    } else if ($form == 'nuevoGrupoCamp') {
        $datos['idgrupo_camp'] = $_POST['idgrupo_camp'];
        $datos['descripcion'] = $_POST['descripcion'];
        $datos['capacidad'] = $_POST['capacidad'];
        $datos['sexo'] = $_POST['sexo'];
        $registro = new CInsert($datos, 'grupo_cab');
        header("Location: lnewAsig?mod=nAsignacion&msj=2");
    } else if ($form == 'nuevoCategorias') {
        $datos['descripcion'] = $_POST['descripcion'];
        $registro = new CInsert($datos, 'categoria');
        header("Location: listCate?idcategoria=" . $_POST['idcategoria'] . "&msj=2");
    } else if ($form == 'nuevotipUsuarios') {
        $datos['descripcion'] = $_POST['descripcion'];
        $registro = new CInsert($datos, 'tipo_usuario');
        header("Location: listTusu?msj=2");
    } else if ($form == 'nuevoreuZonal') {
        $datos['idreunion_granzona'] = $_POST['idreunion_granzona2'];
        $datos['descripcion'] = $_POST['descripcion'];
        $registro = new CInsert($datos, 'reunion_zonal');
        header("Location: listRzon?msj=2");
    } else if ($form == 'nuevoreuGranZon') {
        $datos['descripcion'] = $_POST['descripcion'];
        $registro = new CInsert($datos, 'reunion_granzona');
        header("Location: listRgZo?msj=2");
    } else if ($form == 'nuevoProvincias') {
        $datos['des_provincia'] = $_POST['descripcion'];
        $datos['iddepartamento'] = $_POST['iddepartamento2'];
        $registro = new CInsert($datos, 'provincia');
        header("Location: listProv?iddepartamento=" . $_POST['iddepartamento2'] . "&msj=2");
    } else if ($form == 'nuevoGrupos') {
        $datos['descripcion'] = $_POST['descripcion'];
        $registro = new CInsert($datos, 'grupos');
        header("Location: listGrup?msj=2");
    } else if ($form == 'nuevoEstadoCivil') {
        $datos['descripcion'] = $_POST['descripcion'];
        $registro = new CInsert($datos, 'estado_civil');
        header("Location: listEsCi?msj=2");
    } else if ($form == 'nuevoEstados') {
        $datos['descripcion'] = $_POST['descripcion'];
        $registro = new CInsert($datos, 'estado');
        header("Location: listEsta?msj=2");
    } else if ($form == 'nuevoDistritos') {
        $datos['des_distrito'] = $_POST['descripcion'];
        $datos['idprovincia'] = $_POST['idprovincia2'];
        $registro = new CInsert($datos, 'distrito');
        header("Location: listDist?idprovincia=" . $_POST['idprovincia2'] . "&msj=2");
    } else if ($form == 'nuevoDepartamentos') {
        $datos['idpais'] = $_POST['idpais2'];
        $datos['des_departamento'] = $_POST['descripcion'];
        $registro = new CInsert($datos, 'departamento');
        header("Location: listDepa?msj=2");
    } else if ($form == 'nuevoTipoDocumento') {
        $datos['descripcion'] = $_POST['descripcion'];
        $registro = new CInsert($datos, 'tipo_documento');
        header("Location: listTiDo?msj=2");
    } else if ($form == 'nuevoUsuario') {
        include 'crearConexion.php';
        $pass = $_POST['password'];
        $idpersona = $_POST['idpersona'];
        $user = $_POST['user'];
        $idtipo_usuario = $_POST['idtipo_usuario'];
        $sql = "INSERT INTO usuario (idpersona, user, pass, pass2, idtipo_usuario) VALUES ('$idpersona', '$user', md5('$pass'), '$pass', '$idtipo_usuario');";
        //echo $sql;
        //exit();
        $mysqlMBN->insUpdDel($sql);
        header("Location: userLista?msj=2");
    } else if ($form == 'newIncripcionOnline') {
        include 'crearConexion.php';
        $idcategoria = $_POST['idcategoria'];
        $nombre = $_POST['nombre'];
        $aPaterno = $_POST['aPaterno'];
        $aMaterno = $_POST['aMaterno'];
        $fllName = "$nombre $aPaterno $aMaterno";
        $idtipo_documento = $_POST['idtipo_documento'];
        $dni = $_POST['dni'];
        $sexo = $_POST['S'];
        $idestado_civil = $_POST['idestado_civil'];
        $fecha_nac = $_POST['fecha_nac1'];
        $lugar_nac = $_POST['lugar_nac'];
        $profesion = $_POST['profesion'];
        $ocupacion = $_POST['ocupacion'];
        $telefono_fijo = $_POST['telefono'];
        $celular = $_POST['celular'];
        if ($celular == "") {
            $telefono = $telefono_fijo;
        } else {
            $telefono = $celular;
        }
        $email = $_POST['email'];
        $referencia = $_POST['referencia'];
        $idpais = $_POST['idpais'];
        $iddepartamento = $_POST['iddepartamento'];
        $idprovincia = $_POST['idprovincia'];
        $iddistrito = $_POST['iddistrito'];
        $direccion = $_POST['direccion'];
        if ($nombre != "" && $aPaterno != "" && $aMaterno != "" && $iddistrito != 0 && $idpais != 0) {
            $sql1 = "INSERT INTO persona (nombres, apaterno, amaterno, idtipo_documento, nro_documento, sexo, telefono_fijo, celular, lugar_nacimiento, fec_nac, email, referencia, profesion, ocupacion, idcategoria,iddepartamento ,idprovincia, iddistrito, direccion, fecha_registro, idestado_civil, idgrupos, idpais) VALUES ('$nombre', '$aPaterno', '$aMaterno', '$idtipo_documento', '$dni','$sexo', '$telefono_fijo', '$celular', '$lugar_nac', '$fecha_nac', '$email', '$referencia', '$profesion', '$ocupacion', '$idcategoria', '$iddepartamento' ,'$idprovincia','$iddistrito', '$direccion', now(), '$idestado_civil', 9, '$idpais');";
            //echo $sql1;
            $mysqlMBN->insUpdDel($sql1);
            /* capturando el id de la persona agregada */
            $sqlC = "select * , YEAR(CURDATE())-YEAR(p.fec_nac) + IF(DATE_FORMAT(CURDATE(),'%m-%d') > DATE_FORMAT(p.fec_nac,'%m-%d'), 0, -1) edad 
        from persona p where nro_documento = '$dni' order by fecha_registro desc limit 1;";
            //echo $sqlC;
            $resC = $mysqlMBN->consultas($sqlC);
            $rowC = mysqli_fetch_array($resC);
            $idpersona = $rowC['idpersona'];

            /* agregando la persona a la tabla de inscripcion */
            if (isset($_POST['volunt'])) {
                $volunt = 1;
            } else {
                $volunt = 0;
            }
            $idtalla_polo = $_POST['idtalla_polo'];
            $idciudad = $_POST['idciudad'];
            $observacion = $_POST['observacion'];
            $edad = $_POST['edad'];
            $grupo = $_POST['grupo'];
            //$observacion = $_POST['observacion'];
            if (empty($_FILES["foto_fls"]["tmp_name"])) {
                $imagen = "user.png";
            } else {//se ejecuta la funcion para subir la imagen
                include('imagenUpload.php');
                $tipo = $_FILES["foto_fls"]["type"];
                $archivo = $_FILES["foto_fls"]["tmp_name"];
                $imagen = subir_imagen($tipo, $archivo, $idpersona);
            }
            $sql = "UPDATE persona SET foto='$imagen' WHERE idpersona='$idpersona'";
            $result = $mysqlMBN->insUpdDel($sql);
            $sql2 = "INSERT INTO incripciones_2016 (idpersona, edad, idgrupo, idciudad, dni, cargo, sexo, idreunion_zonal, idcategoria, idestado_pago, celular, idtalla_polo, grupo, voluntario, observacion, fecha_registro,idtipo_inscripcion) 
            VALUES ('$idpersona', '$edad', '9', '$idciudad', '$dni', '17', '$sexo', '0', '$idcategoria', '3', '$telefono', '$idtalla_polo', '$grupo', '$volunt', '$observacion', now(),3);";
            //echo $sql2;
            $mysqlMBN->insUpdDel($sql2);

            $sqlRW = "INSERT INTO registro_web (idpersona, fecha_registro) VALUES ('$idpersona', now());";
            $mysqlMBN->insUpdDel($sqlRW);

            include 'envioCorreos.php';
            $mail->addAddress('thewiner57@gmail.com', 'Harold Varas');
            $mail->addAddress('rossyiyf@gmail.com', 'Rossy IYF');
            $mail->addAddress('peruiyf@gmail.com', 'Peru IYF Varas');

            $mail->Subject = 'Un Invitado se ah registrado a través de la web';

            $mail->Body = 'El Sr(a). ' . $fllName . ' se ah registrado para el World Camp 2017 desde la pagina web <br>
                Datos de Contactabilidad: <br>
                Telefono: ' . $telefono . '<br>
                Correo: ' . $email . '<br>
                
        Puede revisar esta información en el siguiente link : http://sismbn.iyf.org.pe/index.php .';
            $mail->altBody = 'Puede revisar esta información en el siguiente  link : http://sismbn.iyf.org.pe/index.php .
                Sistema Registros de IYF <br>
               (Este email es generado automaticamente. Favor de no responder.)';
            $envio = $mail->Send();

            if ($envio) {
                echo '<script> window.location.href = "inscripcionOnline?msj=6";</script>';
                header("Location: inscripcionOnline?msj=6");
                echo "Email enviado correctamente";
            } else {
                //echo "Erro: " . $mail->ErrorInfo;
            }
        } else {
            echo '<script> window.location.href = "inscripcionOnline?mod=lPersonas&msj=7";</script>';
        }
    } else if ($form == 'nuevaPersonaAgnt') {
        include 'crearConexion.php';
        $idcategoria = $_POST['idcategoria'];
        $nombre = $_POST['nombre'];
        $aPaterno = $_POST['aPaterno'];
        $aMaterno = $_POST['aMaterno'];
        $idtipo_documento = $_POST['idtipo_documento'];
        $dni = $_POST['dni'];
        $sexo = $_POST['S'];
        //$edad = $_POST['edad'];
        $idestado_civil = $_POST['idestado_civil'];
        $fecha_nac = $_POST['fecha_nac1'];
        $lugar_nac = $_POST['lugar_nac'];
        $profesion = $_POST['profesion'];
        $ocupacion = $_POST['ocupacion'];
        $telefono_fijo = $_POST['telefono'];
        $celular = $_POST['celular'];
        $email = $_POST['email'];
        $referencia = $_POST['referencia'];

        $idpais = $_POST['idpais'];
        $iddepartamento = $_POST['iddepartamento'];
        $idprovincia = $_POST['idprovincia'];
        $iddistrito = $_POST['iddistrito'];
        $direccion = $_POST['direccion'];
        $idgrupo = $_POST['idgrupo'];
        $idsede = $_POST['idsede'];

        $sqlP = "INSERT INTO persona (nombres,apaterno,amaterno,idtipo_documento,nro_documento,sexo,telefono_fijo,celular,lugar_nacimiento,fec_nac,email,referencia,profesion,ocupacion,idcategoria,iddistrito,direccion,fecha_registro,idestado_civil,idgrupos,idpais,idprovincia,iddepartamento,idsede) 
        VALUES ('$nombre', '$aPaterno', '$aMaterno', '$idtipo_documento', '$dni', '$sexo', '$telefono_fijo', '$celular', '$lugar_nac', '$fecha_nac', '$email', '$referencia', '$profesion', '$ocupacion', '$idcategoria', '$iddistrito', '$direccion', now(), '$idestado_civil', '$idgrupo','$idpais','$idprovincia','$iddepartamento','$idsede');";
        $mysqlMBN->insUpdDel($sqlP);
        //echo $sqlP . "<br>";

        $sql = 'select * from persona where nro_documento="' . $dni . '" and nombres="' . $nombre . '" and apaterno="' . $aPaterno . '" and amaterno="' . $aMaterno . '" order by fecha_registro desc limit 1;';
        //echo $sql . "<br>";
        $res = $mysqlMBN->consultas($sql);
        $row = mysqli_fetch_array($res);
        $idpersona = $row['idpersona'];
        $num_regs = mysqli_num_rows($res);

        if (empty($_FILES["foto_fls"]["tmp_name"])) {
            $imagen = "user.png";
        } else {//se ejecuta la funcion para subir la imagen
            include('imagenUpload.php');
            $tipo = $_FILES["foto_fls"]["type"];
            $archivo = $_FILES["foto_fls"]["tmp_name"];
            $imagen = subir_imagen($tipo, $archivo, $idpersona);
        }
        $sql = "UPDATE persona SET foto='$imagen' WHERE idpersona='$idpersona'";
        $result = $mysqlMBN->insUpdDel($sql);
        echo $sql . "<br>";
        if ($idcategoria == 1) {
            $fecha_salvacion = $_POST['fecha_salvacion1'];
            $s_referencia = $_POST['s_referencia'];
            $idreunion_granzona = $_POST['idreunion_granzona'];
            $idreunion_zona1 = $_POST['idreunion_zonal'];
            foreach ($_POST['idcargoM'] as $indice => $contenido) {
                $idCargosVarios[] = $contenido;
                $valores .= "$contenido,";
                $idcargos = substr($valores, 0, -1);
            }
            if ($num_regs > 0) {
                $sqlDP = "INSERT INTO detalle_salvacion (idpersona,fecha_salvacion,referencia,idcargo,idreunion_granzona,idreunion_zonal) VALUES ('$idpersona', '$fecha_salvacion', '$s_referencia', '$idcargos', '$idreunion_granzona', '$idreunion_zona1');";
                $mysqlMBN->insUpdDel($sqlDP);
            }
            echo $sqlDP . "<br>";
        }
        echo "<h1><b>Los Datos Se Registraron Correctamente. Aguarde porfavor..</b></h1>";
        echo '<script> window.location.href = "agndNewPers?mod=nPersona&idcategoria=1";</script>';

        //echo '<script> window.location.href = "listAgndPer?mod=lPersonas&idpais=' . $idpais . '&iddepartamento=' . $iddepartamento . '&idgrupo=' . $idgrupo . '&S=' . $sexo . '&msj=2";</script>';
        //header("Location: listPers?msj=2");
    } else if ($form == 'nuevaPersona') {
        include 'crearConexion.php';
        $idcategoria = $_POST['idcategoria'];
        $nombre = $_POST['nombre'];
        $aPaterno = $_POST['aPaterno'];
        $aMaterno = $_POST['aMaterno'];
        $idtipo_documento = $_POST['idtipo_documento'];
        $dni = $_POST['dni'];
        $sexo = $_POST['S'];
        $edad = $_POST['edad'];
        $idestado_civil = $_POST['idestado_civil'];
        $fecha_nac = $_POST['fecha_nac1'];
        $lugar_nac = $_POST['lugar_nac'];
        $profesion = $_POST['profesion'];
        $ocupacion = $_POST['ocupacion'];
        $telefono_fijo = $_POST['telefono'];
        $celular = $_POST['celular'];
        if ($celular == "") {
            $telefono = $telefono_fijo;
        } else {
            $telefono = $celular;
        }
        $email = $_POST['email'];
        $referencia = $_POST['referencia'];
        $idpais = $_POST['idpais'];
        $iddepartamento = $_POST['iddepartamento'];
        $idprovincia = $_POST['idprovincia'];
        $iddistrito = $_POST['iddistrito'];
        $direccion = $_POST['direccion'];
        if ($idcategoria == 1) {
            $fecha_salvacion = $_POST['fecha_salvacion1'];
            $s_referencia = $_POST['s_referencia'];
            $idreunion_granzona = $_POST['idreunion_granzona'];
        }
        $idreunion_zonal = $_POST['idreunion_zonal'];
        $idgrupo = $_POST['idgrupo'];
        foreach ($_POST['idcargoM'] as $indice => $contenido) {
            $idCargosVarios[] = $contenido;
            $valores .= "$contenido,";
            $idcargos = substr($valores, 0, -1);
        }
        $sqlP = "INSERT INTO persona (nombres,apaterno,amaterno,idtipo_documento,nro_documento,sexo,telefono_fijo,celular,lugar_nacimiento,fec_nac,email,referencia,profesion,ocupacion,idcategoria,iddistrito,direccion,fecha_registro,idestado_civil,idgrupos,idpais,idprovincia,iddepartamento) 
        VALUES ('$nombre', '$aPaterno', '$aMaterno', '$idtipo_documento', '$dni', '$sexo', '$telefono_fijo', '$celular', '$lugar_nac', '$fecha_nac', '$email', '$referencia', '$profesion', '$ocupacion', '$idcategoria', '$iddistrito', '$direccion', now(), '$idestado_civil', '$idgrupo','$idpais','$idprovincia','$iddepartamento');";
        $mysqlMBN->insUpdDel($sqlP);
        //echo $sqlP . "<br>";

        $sql = 'select * from persona where nro_documento="' . $dni . '" and nombres="' . $nombre . '" and apaterno="' . $aPaterno . '" and amaterno="' . $aMaterno . '" order by fecha_registro desc limit 1;';
        //echo $sql . "<br>";
        $res = $mysqlMBN->consultas($sql);
        $row = mysqli_fetch_array($res);
        $idpersona = $row['idpersona'];
        $num_regs = mysqli_num_rows($res);

        if (empty($_FILES["foto_fls"]["tmp_name"])) {
            $imagen = "user.png";
        } else {//se ejecuta la funcion para subir la imagen
            include('imagenUpload.php');
            $tipo = $_FILES["foto_fls"]["type"];
            $archivo = $_FILES["foto_fls"]["tmp_name"];
            $imagen = subir_imagen($tipo, $archivo, $idpersona);
        }
        $sql = "UPDATE persona SET foto='$imagen' WHERE idpersona='$idpersona'";
        $result = $mysqlMBN->insUpdDel($sql);

        if ($idcategoria == 1) {
            if ($num_regs > 0) {
                $sqlDP = "INSERT INTO detalle_salvacion (idpersona,fecha_salvacion,referencia,idcargo,idreunion_granzona,idreunion_zonal) VALUES ('$idpersona', '$fecha_salvacion', '$s_referencia', '$idcargos', '$idreunion_granzona', '$idreunion_zonal');";
                $mysqlMBN->insUpdDel($sqlDP);
            }
            // echo $sqlDP . "<br>";
        }

        if (isset($_POST['volunt'])) {
            $volunt = 1;
        } else {
            $volunt = 0;
        }
        $idciudad = $_POST['idciudad'];
        $grupoCamp = $_POST['grupo'];
        $idtalla_polo = $_POST['idtalla_polo'];
        $idestado_pago = $_POST['idestado_pago'];
        $observacion = $_POST['observacion'];

        $sqlI = " INSERT INTO incripciones_2016 
        (idpersona, edad, idgrupo, idciudad, dni, cargo, sexo, idreunion_zonal, idcategoria, 
        idestado_pago, celular, idtalla_polo, grupo, voluntario, observacion,fecha_registro) 
        VALUES ('$idpersona', '$edad', '$idgrupo', '$idciudad', '$dni', '$idcargos', '$sexo', '$idreunion_zonal', '$idcategoria', 
        '$idestado_pago', '$telefono', '$idtalla_polo', '$grupoCamp', '$volunt', '$observacion',now());";
        //echo $sqlI . "<br>";
        $mysqlMBN->insUpdDel($sqlI);


        $sqlA = "select max(idinscripciones) idinscripciones from incripciones_2016 where idpersona = $idpersona;";
        $resA = $mysqlMBN->consultas($sqlA);
        $row = mysqli_fetch_array($resA);
        $idinscripcion = $row['idinscripciones'];

        foreach ($_REQUEST as $indice => $contenido) {
            if ($indice != "PHPSESSID") {
                if (substr($indice, 0, 9) == "nroBoleta") {
                    $nroBoleta[] = $contenido;
                } else if (substr($indice, 0, 10) == "montoSoles") {
                    $montoSoles[] = $contenido;
                } else if (substr($indice, 0, 12) == "montoDolares") {
                    $montoDolares[] = $contenido;
                }
            }
        }
        $cantidadBoletas = count($nroBoleta);

        for ($x = 0; $x < $cantidadBoletas; $x++) {
            $insertarBoletas = "
            INSERT INTO boleta_2016 (idinscripcion,nro_boleta,monto_soles,monto_dolares,fecha_pago,fecha_registro,lastupdated) VALUES ('$idinscripcion', '$nroBoleta[$x]', '$montoSoles[$x]', '$montoDolares[$x]', now(),now(), now());";
            //echo $insertarBoletas . "<br>";
            $mysqlMBN->insUpdDel($insertarBoletas);
        }
        echo "<h1><b>Los Datos Se Registraron Correctamente. Aguarde porfavor..</b></h1>";
        echo '<script> window.location.href = "listPers?idpais=' . $idpais . '&msj=2";</script>';
        //header("Location: listPers?msj=2");
    } else if ($form == 'nuevaInscripcion') {
        include 'crearConexion.php';
        if (isset($_POST['volunt'])) {
            $volunt = 1;
        } else {
            $volunt = 0;
        }
        $idpersona = $_POST['idpersona'];
        $idpais = $_POST['idpais'];
        $sexo = $_POST['S'];
        $edad = $_POST['edad'];
        $nro_documento = $_POST['nro_documento'];
        $idgrupo = $_POST['idgrupo'];
        $grupo = $_POST['grupo'];
        $valores = "";
        foreach ($_POST['idcargoM'] as $indice => $contenido) {
            $idCargosVarios[] = $contenido;
            $valores .= "$contenido,";
            $idcargos = substr($valores, 0, -1);
        }
        $idciudad = $_POST['idciudad'];
        $idreunion_zonal = $_POST['idreunion_zonal'];
        $idcategoria = $_POST['idcategoria1'];
        $telefono = $_POST['telefono'];
        $idtalla_polo = $_POST['idtalla_polo'];
        $idestado_pago = $_POST['idestado_pago'];
        $observacion = $_POST['observacion'];
        $sqlI = " INSERT INTO incripciones_2016 
        (idpersona, edad, idgrupo, idciudad, dni, cargo, sexo, idreunion_zonal, idcategoria, 
        idestado_pago, celular, idtalla_polo, grupo, voluntario, observacion,fecha_registro) 
        VALUES ('$idpersona', '$edad', '$idgrupo', '$idciudad', '$nro_documento', '$idcargos', '$sexo', '$idreunion_zonal', '$idcategoria', 
        '$idestado_pago', '$telefono', '$idtalla_polo', '1', '$volunt', '$observacion',now());";
        //echo $sqlI . "<br>";
        $mysqlMBN->insUpdDel($sqlI);

        $sqlA = "select max(idinscripciones) idinscripciones from incripciones_2016 where idpersona = $idpersona;";
        $resA = $mysqlMBN->consultas($sqlA);
        $row = mysqli_fetch_array($resA);
        $idinscripcion = $row['idinscripciones'];

        foreach ($_REQUEST as $indice => $contenido) {
            if ($indice != "PHPSESSID") {
                if (substr($indice, 0, 9) == "nroBoleta") {
                    $nroBoleta[] = $contenido;
                } else if (substr($indice, 0, 10) == "montoSoles") {
                    $montoSoles[] = $contenido;
                } else if (substr($indice, 0, 12) == "montoDolares") {
                    $montoDolares[] = $contenido;
                }
            }
        }
        $cantidadBoletas = count($nroBoleta);

        for ($x = 0; $x < $cantidadBoletas; $x++) {
            $insertarBoletas = "
            INSERT INTO boleta_2016 (idinscripcion,nro_boleta,monto_soles,monto_dolares,fecha_registro,lastupdated,idusuario) VALUES ('$idinscripcion', '$nroBoleta[$x]', '$montoSoles[$x]', '$montoDolares[$x]', now(), now()," . $_SESSION['idusuario'] . ");";
            //echo $insertarBoletas . "<br>";
            $mysqlMBN->insUpdDel($insertarBoletas);
        }
        //echo "<h2>Se registraron los datos correctamente</h2>";
        //echo "<a href='newInsc?mod=nInscripcion&accion=buscar&idpais=$idpais&S=$sexo&idpersona=0'>Regresar las inscripciones</a>";
        echo '<script> window.location.href = "listInc?mod=lInscripcion&idpais=89&S=M&idpersona=0&msj=2";</script>';
//        $ = $_POST[''];
//        $ = $_POST[''];
        // header("Location: listPers?msj=2");
    } else if ($form == 'cambiarCondicion') {
        include 'crearConexion.php';
        $sexo = $_POST['S'];
        $fecha_salvacion = $_POST['fecha_salvacion1'];
        $s_referencia = $_POST['s_referencia'];
        $idreunion_granzona = $_POST['idreunion_granzona'];
        $idreunion_zonal = $_POST['idreunion_zonal'];
        $idpersona = $_POST['idpersona'];
        foreach ($_POST['idcargoM'] as $indice => $contenido) {
            $idCargosVarios[] = $contenido;
            $valores .= "$contenido,";
            $idcargos = substr($valores, 0, -1);
        }


        $sqlP = "UPDATE persona SET idcategoria='1' WHERE idpersona='$idpersona';";
        //echo $sqlP . "<br>";
        $mysqlMBN->insUpdDel($sqlP);

        $sqlI = "INSERT INTO detalle_salvacion (idpersona,fecha_salvacion,referencia,idcargo,idreunion_granzona,idreunion_zonal) VALUES ('$idpersona', '$fecha_salvacion', '$s_referencia', '$idcargos', '$idreunion_granzona', '$idreunion_zonal');";
        //echo $sqlI . "<br>";
//        exit();
        $mysqlMBN->insUpdDel($sqlI);
        echo '<script> window.location.href = "persDetalle?mod=mPersonas&idpersona=' . $idpersona . '&S=' . $sexo . '&msj=3";</script>';
    }
} elseif (isset($_GET['accion'])) {
    $accion = $_GET['accion'];
    if ($accion == "Asumir_ticket") { //ASUMIR TICKET
        include 'crearConexionSMS.php';
        $idticket = $_GET['idticket'];
        $idpersonal = $_GET['idpersonal'];
        $sql = "insert into tck_user_ticket (id_user,id_ticket) values ($idpersonal,$idticket);";
        $res = $mysqlTICK->insUpdDel($sql);
        $sqlEstadoTicket = "update tck_ticket set estado = 2 where id = $idticket";
        $mysqlTICK->insUpdDel($sqlEstadoTicket);
        $sqlCli = "select mail,concat(nombres,' ',apaterno,' ', amaterno) nombre from tck_ticket  tt 
            inner join enviosms.personal ep on tt.cliente = ep.id_ticket_personal where id = $idticket;";
        $resCli = $mysqlTICK->consultas($sqlCli);
        while ($row = mysqli_fetch_array($resCli)) {
            $mail->addAddress('' . $row['mail'] . '', '' . $row['nombre'] . '');
        }
        $mail->Subject = 'Ticket ATC';
        $mail->Body = 'Su Ticket ya fue asignado a uno e nuestros técnicos para su pronta solución<br>
	Puede verificarlo en <a href="http://172.20.30.204/sistemaIntegrado">Este link</a> al conectarse.<br><br>
	Sistemas ATC.<br><br>
	(Este email es generado automaticamente. Favor de no responder.)';
        $mail->altBody = 'Su Ticket ya fue asignado a uno e nuestros técnicos para su pronta solución. 
	Puede verificarlo en (http://172.20.30.204/sistemaIntegrado)al conectarse. 
	Sistemas ATC..
	(Este email es generado automaticamente. Favor de no responder.)';
        $envio = $mail->Send();
        if ($envio) {
            echo "Email enviado correctamente";
        } else {
            echo "Erro: " . $mail->ErrorInfo;
        }
        header("Location: tickListaC");
    }
}    