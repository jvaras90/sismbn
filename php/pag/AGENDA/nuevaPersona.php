<?php
require('../../include/header.php');
include '../../system/mensajesAlerta.php';
include '../../system/crearConexion.php';
clearstatcache();

$idcategoria = $_GET['idcategoria'];
?>


<script src="js/ajax.js" type="text/javascript"></script>
<script type="text/javascript">
//    $(document).ready(function () {
//        var listaC = document.getElementById("idcategoria");
//        $('#idcategoria').change(function () {
//            window.location.href = "persNews?idcategoria=" + listaC.value;
//        });
//    });

    function reloadPageC(idC) {
        window.location.href = "agndNewPers?mod=nPersona&idcategoria=" + idC;
    }

    function cargarGrupo(id) {
        if (id >= 60) {
            grupo = "SILVER";
        } else if (id >= 39) {
            grupo = "SENIOR";
        } else if (id >= 31) {
            grupo = "GRACE";
        } else if (id >= 24) {
            grupo = "FAITH";
        } else if (id >= 17) {
            grupo = "TRUTH";
        } else if (id >= 11) {
            grupo = "JUNIOR";
        } else {
            grupo = "Niños";
        }
//        switch (id) {
//            case "6":/*Adolescente */
//                grupo = "JUNIOR";
//                break;
//            case "5":/*Joven Menor */
//                grupo = "TRUTH";
//                break;
//            case "4":/* Joven Mayor */
//                grupo = "DREAM";
//                break;
//            case "1":/* Silver */
//                grupo = "SILVER";
//                break;
//            case "2":/* Senior */
//                grupo = "SENIOR";
//                break;
//            default :
//                grupo = "";
//        }
        //alert("idGrupo: " + id + " descripcion: " + grupo + " ");
        $("#grupo").val("" + grupo + "");
    }
    function validarFormPersona(boton) {
        //var button = $('#registrarPersonalInscripcion').val();
        var idgrupo = $('#idgrupo').val();
        var idsede = $('#idsede').val();
        var idestado_pago = $('#idestado_pago').val();
        var idpais = $('#idpais').val();
        var idestado_civil = $('#idestado_civil').val();
        if (idgrupo == '0') {
            alert('Seleccione el grupo de la persona (Silver,Casado,Joven o Adolescente) ');
            $('#idgrupo').focus();
            return false;
        } else if (idestado_civil == '0') {
            alert('Seleccione el estado civil del participante');
            $('#idestado_civil').focus();
            return false;
        } else if (idpais == '0') {
            alert('Seleccione el Pais del participante');
            $('#idpais').focus();
            return false;
        } else if (idsede == '0') {
            alert('Seleccione la SEDE a la que pertenece la persona ');
            $('#idsede').focus();
            return false;
        } else if (idestado_pago == '0') {
            alert('Seleccione el estado en el que se encuentra el pago ');
            $('#idestado_pago').focus();
            return false;
        } else if (!$("#registrarPersonal input[name='S']:radio").is(':checked')) {
            alert('Seleccione el sexo de la persona ');
            $('#S').focus();
            return false;
        } else {
            boton.disabled = true;
            boton.value = 'Registrando...';
            boton.form.submit();
            return true;
        }
    }



</script>

<SCRIPT LANGUAGE="JavaScript">
    function NumCheck(e, field) {
        key = e.keyCode ? e.keyCode : e.which
        // backspace
        if (key == 8)
            return true
        // 0-9
        if (key > 47 && key < 58) {
            if (field.value == "")
                return true
            regexp = /.[0-9]{3}$/
            return !(regexp.test(field.value))
        }
        // .
        if (key == 46) {
            if (field.value == "")
                return false
            regexp = /^[0-9]+$/
            return regexp.test(field.value)
        }
        // other key
        return false
    }
</script>
<script>
    $(function () {
        // Obtener la referencia a la lista
        var lista = document.getElementById("iddepartamento");
        var lista1 = document.getElementById("idprovincia");
        var idpais = $('#idpais').val();
        // Obtener el índice de la opción que se ha seleccionado
        var indiceSeleccionado = lista.selectedIndex;
        var indiceSeleccionado1 = lista1.selectedIndex;
        // Con el índice y el array "options", obtener la opción seleccionada
        var opcionSeleccionada = lista.options[indiceSeleccionado];
        var opcionSeleccionada1 = lista1.options[indiceSeleccionado1];
        // Obtener el valor y el texto de la opción seleccionada
        //var textoSeleccionado = opcionSeleccionada.text;
        var valorSeleccionado = opcionSeleccionada.value;
        var valorSeleccionado1 = opcionSeleccionada1.value;
        load_departamento(idpais);
        load_provincia(0);
        load_distrito(0);

    });
</script>

<!-- page content -->
<div class="right_col" <?php echo $newDimensionBody; ?> role="main">
    <form id="registrarPersonal" class="form-horizontal form-label-left" method="post" action="registrarA" enctype="multipart/form-data">
        <div class="page-title">
            <h1>Registros de Participantes </h1>
        </div>
        <br>

        <input type="hidden" name="condicionMod" id="condicionMod" value="<?php echo $_GET['mod']; ?>" >
        <div class="clearfix"></div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <br>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="col-md-3 col-sm-3 col-xs-3" id="cliente">
                    </div>
                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Condición</label>
                    <div class="col-md-3 col-sm-3 col-xs-3" id="cliente">
                        <select class='form-control' name='idcategoria' id='idcategoria' onchange="reloadPageC(this.value);" >
                            <?php include_once "../../system/selectCategoria.php"; ?>
                        </select>
                    </div>
                    <?php if ($_SESSION['id_nivel'] == 1) { ?>
                        <div class="col-md-1 col-sm-1 col-xs-3" id="nuevoGrupo" title="Agregar Nueva Condición">
                            <a href="#modalNuevoCategorias" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                        </div>
                    <?php } ?>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Datos Personales</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Nombre</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" >
                                    <input name="nombre" id="nombre" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="3" data-validate-words="1" placeholder="" maxlength="300" autocomplete="" type="text" required>
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Subir Imagen</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" >
                                    <input class="avatar-input" id="foto_fls" name="foto_fls" type="file">
                                    <input type="hidden" name="foto_hdn" value="<?php echo $foto ?>">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">A. Paterno</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" >
                                    <input name="aPaterno" id="aPaterno" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="2" data-validate-words="1" placeholder="" maxlength="300" autocomplete="" type="text" required >
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">A. Materno</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="aMaterno">
                                    <input name="aMaterno" id="aMaterno" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="1" data-validate-words="1" placeholder="" maxlength="300" autocomplete="" type="text" required >
                                </div>
                            </div> 
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Tipo Documento</label>
                                <div class="col-md-3 col-sm-3 col-xs-3" >
                                    <select class='form-control' name='idtipo_documento' id='idtipo_documento'>
                                        <?php include_once "../../system/selectTipoDocumento.php"; ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Tipo de Documento">
                                    <a href="#modalNuevoTipoDocumento" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Número Doc.</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="dni">
                                    <input name="dni" id="dni" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="off" type="text" value="<?php echo "$nro_documento"; ?>">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Sexo</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" >
                                    <div class="radio-inline">
                                        <label>
                                            <input class="flat" type="radio" <?php
                                            if ($sexo == "M") {
                                                echo "checked";
                                            }
                                            ?> value="M" id="S" name="S"> M
                                        </label>
                                    </div>
                                    <div class="radio-inline">
                                        <label>
                                            <input class="flat" type="radio" <?php
                                            if ($sexo == "F") {
                                                echo "checked";
                                            }
                                            ?> value="F" id="S" name="S"> F
                                        </label>
                                    </div> 
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Estado Civil</label>
                                <div class="col-md-3 col-sm-3 col-xs-3" >
                                    <select class='form-control' name='idestado_civil' id='idestado_civil'>
                                        <option value="0">Seleccione</option>
                                        <?php include_once "../../system/selectEstadoCivil.php"; ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Estado Civil">
                                    <a href="#modalNuevoEstadoCivil" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Fecha Nac.</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" >
                                    <input type="text" id="fecha_nac1"  name="fecha_nac1"  class="form-control col-md-3 col-sm-3 col-xs-3" data-inputmask="'mask': '9999-99-99'">
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Edad</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" title="Cargara autmaticamente al ingresar el año de Nacimiento" >
                                    <input name="edad" id="edad" class="form-control col-md-3 col-sm-3 col-xs-3"  placeholder=""  type="text" disabled="">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Lugar Nac.</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" >
                                    <input name="lugar_nac" id="lugar_nac" class="form-control col-md-3 col-sm-3 col-xs-3" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="" type="text">
                                </div>
                                <?php if ($idcategoria == 2) { ?>

                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Grupo </label>
                                    <div class="col-md-3 col-sm-3 col-xs-3" title="Asignado para las reuniones Grupales">
                                        <select class='form-control' name='idgrupo' id='idgrupo' >
                                            <option value="0">Seleccione</option>
                                            <?php include_once "../../system/selectGrupo.php"; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nueva Reunion Grupal">
                                        <a href="#modalNuevoGrupos" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Profesión</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" >
                                    <input name="profesion" id="profesion" class="form-control col-md-3 col-sm-3 col-xs-3" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="245" autocomplete="" type="text">
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Trabajo Actual</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" >
                                    <input name="ocupacion" id="ocupacion" class="form-control col-md-3 col-sm-3 col-xs-3" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="245" autocomplete="" type="text" >
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Contactabilidad</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Teléfono</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" >
                                    <input name="telefono" id="telefono" class="form-control col-md-3 col-sm-3 col-xs-3" data-inputmask="'mask': '999-9999'" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="" type="text" >
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Celular</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" >
                                    <input name="celular" id="celular" class="form-control col-md-3 col-sm-3 col-xs-3" data-inputmask="'mask': '999-999-999'" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="" type="text" >
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Email</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="cliente">
                                    <input type="text" id="email" name="email" class="form-control col-md-7 col-sm-7 col-xs-7" >
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Persona de Referencia</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="cliente">
                                    <input type="text" id="referencia" name="referencia" class="form-control col-md-7 col-sm-7 col-xs-7" >
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Datos de Ubicación</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Pais  <?php //echo $iddepartamento;                                                                                                                                                                                      ?></label>
                                <div class="col-md-3 col-sm-3 col-xs-3" id="cliente">
                                    <select class='form-control' name='idpais' id='idpais' onchange="load_departamento(this.value)" >
                                        <option value="0">Seleccione</option>
                                        <?php include_once "../../system/selectPais.php"; ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Pais">
                                    <a href="#modalNuevoPais" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Departamento  <?php //echo $iddepartamento;                                                                                                                                                                                      ?></label>
                                <div class="col-md-3 col-sm-3 col-xs-3" id="myDivDep">
                                    <select class='form-control' name='iddepartamento' id='iddepartamento' onchange="load_provincia(this.value)" >
                                        <option value="0">Seleccione Departamento</option>
                                        <?php //include_once "../../system/selectDepartamento.php";   ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Departamento">
                                    <a href="#modalNuevoDepartamentos" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Provincia  <?php // echo $idprovincia;                                                                                                                                                                                      ?></label>
                                <div class="col-md-3 col-sm-3 col-xs-3" id="myDivProv">
                                    <select class='form-control' name='idprovincia' id='idprovincia' onchange="load_distrito(this.value)" >
                                        <option value="0">Seleccione Provincia</option>
                                        <?php //include_once "../../system/selectProvincia.php";   ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nueva Provincia">
                                    <a href="#modalNuevoProvincias" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Distrito <?php // echo $iddistrito;                                                                                                                                                                                      ?></label>
                                <div class="col-md-3 col-sm-3 col-xs-3" id="myDivDist">
                                    <select class='form-control' name='iddistrito' id='iddistrito'>
                                        <option value="0">Seleccione Distrito</option>
                                        <?php //include_once "../../system/selectDistrito.php";   ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Distrito">
                                    <a href="#modalNuevoDistritos" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Dirección</label>
                                <div class="col-md-10 col-sm-10 col-xs-12" id="cliente">
                                    <input type="text" id="direccion" name="direccion" required="required" placeholder="" class="form-control col-md-7 col-sm-7 col-xs-7" value="<?php echo "$direccion"; ?>">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2"> SEDE <?php // echo $iddistrito;                                                                                                                                                                         ?></label>
                                <div class="col-md-3 col-sm-3 col-xs-3" id="myDivSede">
                                    <select class='form-control' name='idsede' id='idsede' <?php
                                    if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                        echo "disabled";
                                    }
                                    ?>>
                                        <option value="0">Seleccione</option>
                                        <?php include_once "../../system/selectSede.php"; ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nueva Sede">
                                    <a href="#modalNuevaSede" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            if ($idcategoria == 1) {
                ?>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Datos de Miembro</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="item form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Fec. Salvación</label>
                                    <div class="col-md-4 col-sm-4 col-xs-4" >
                                        <input type="text" id="fecha_salvacion1"  name="fecha_salvacion1"  class="form-control col-md-3 col-sm-3 col-xs-3" data-inputmask="'mask': '9999-99-99'" required>
                                    </div>
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Predicó  </label>
                                    <div class="col-md-4 col-sm-4 col-xs-4" >
                                        <input name="s_referencia" id="s_referencia" class="form-control col-md-3 col-sm-3 col-xs-3" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="off" type="text"  value="<?php echo $referencia; ?>">
                                    </div>
                                </div>
                                <div id="selectMultiple"  class="item form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2" title="Puerto de salida">Cargo </label>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <select class="select2_multiple form-control" multiple="multiple" name='idcargoM[]' id='idcargoM[]'>
                                            <option value="11" selected="">Hermano(a)</option>
                                            <?php include '../../system/selectCargo.php' ?>   
                                        </select>
                                    </div> 
                                    <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Cargo">
                                        <a href="#modalNuevoCargos" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                    </div>    
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Grupo </label>
                                    <div class="col-md-3 col-sm-3 col-xs-3" title="Asignado para las reuniones Grupales">
                                        <select class='form-control' name='idgrupo' id='idgrupo'  onload="cargarGrupo(this.value)" >
                                            <option value="0">Seleccione</option>
                                            <?php include_once "../../system/selectGrupo.php"; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nueva Reunion Grupal">
                                        <a href="#modalNuevoGrupos" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Gran Zona </label>
                                    <div class="col-md-3 col-sm-3 col-xs-3" id="myDivProv">
                                        <select class='form-control' name='idreunion_granzona' id='idreunion_granzona' onchange="load_reunion_zonal(this.value)" >
                                            <option value="0">Seleccione</option>
                                            <?php include_once "../../system/selectReunionGranZona.php"; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nueva Gran Zona">
                                        <a  href="#modalNuevoreuGranZon" data-toggle="modal" class = "btn btn-round btn-success btn-sm" ><i class = "fa fa-plus-square"></i></a>
                                    </div>
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Reunion Zonal </label>
                                    <div class="col-md-3 col-sm-3 col-xs-3" id="myDivReuZon" title="Asignado para las reuniones Zonales">
                                        <select class='form-control' name='idreunion_zonal' id='idreunion_zonal'>
                                            <?php include_once "../../system/selectReunionZonal.php"; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nueva Reunion Zonal">
                                        <a href="#modalNuevoreuZonal" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <div style="text-align: center;">
                <!--<a href="listPers?mod=lPersona" class="btn btn-danger"><i class="fa fa-rotate-left"></i> Regresar</a>-->
<!--                <input type="button" value="Registrar" id="actualizarPersonal" name="actualizarPersonal" class="btn btn-primary" onclick="this.disabled = true;
                        this.value = 'Registrando...';
                        this.form.submit()"/>-->
                <input type="button" value="Registrar" id="registrarPersonalInscripcion" name="registrarPersonalInscripcion" class="btn btn-primary" onclick="validarFormPersona(this)">
                <!--<button onclick="validar()">Registrar</button>-->
                <input type="hidden" name="form" value="nuevaPersonaAgnt"/>
            </div>


        </div>
        <div class="clearfix"></div>
    </form>
    <?php
    if ($_SESSION['id_nivel'] == 1 || $_SESSION['id_nivel'] == 2) {
        include '../VISTAS/listaModals.php';
    }
    ?>
    <script>
        $(document).ready(function () {
            $(".select2_single").select2({
                placeholder: "Select a state",
                allowClear: true
            });
            $(".select2_group").select2({});
            $(".select2_multiple").select2({
                maximumSelectionLength: <?php echo $num_regs_cargos; ?>,
                placeholder: "Seleccione Maximo " + <?php echo $num_regs_cargos; ?> + " Cargos",
                allowClear: true
            });

        });
    </script>
    <!-- input_mask -->
    <script>
        $(document).ready(function () {
            $(":input").inputmask();
        });
    </script>
    <!-- /input mask -->
    <script>
        function agregarBoleta() {
            //capturamos los valores
            var nro_boleta = $('#nro_boleta').val();
            var monto_soles = $('#monto_soles').val();
            var monto_dolares = $('#monto_dolares').val();

            var cat = document.getElementById("table_boleta").rows.length;
            if (nro_boleta.length >= 3) {
                $('#table_boleta').append('<tr id="' + cat + '" class="impreso"><td><input type="hidden" name="nroBoleta' + nro_boleta + '" value="' + nro_boleta + '"/>' + nro_boleta + '</td><td><input type="hidden" name="montoSoles' + nro_boleta + '" value="' + monto_soles + '"/>' + monto_soles + '</td><td><input type="hidden" name="montoDolares' + monto_dolares + '" value="' + monto_dolares + '"/>' + monto_dolares + '</td><td><button type="button" class="btn btn-danger" onclick="eliminarAgente(' + cat + ');"><i class="fa fa-minus-square"></i> Quitar</button></td></tr>');
            }

            //limiamos los textbox
            $('#nro_boleta').val("");
            $('#monto_soles').val("");
            $('#monto_dolares').val("");
        }

        function eliminarAgente(id)
        {
            $('#' + id).remove();
        }
    </script>

    <!-- switchery -->
    <script src="js/switchery/switchery.min.js"></script>
    <?php require('../../include/footer.php'); ?>