<?php
require('../../include/header.php');
include '../../system/mensajesAlerta.php';
include_once '../../system/crearConexion.php';
clearstatcache();
$titulo = "Cargos";
$hoja = "Cargos";
$idpais = $_GET['idpais'];
//if (isset($_GET['iddepartamento'])) {
//    $iddepartamento = $_GET['iddepartamento'];
//} else {
//    $iddepartamento = 0;
//}
if (isset($_GET['idsede'])) {
    $idsede = $_GET['idsede'];
} else {
    $idsede = 0;
}
$idgrupo = $_GET['idgrupo'];
$idcategoria = $_GET['idcategoria'];
?>

<!-- page content -->
<script>
    $(function () {
        //var idpais =  $("#idpais").val();
        load_sede(<?php echo "$idpais,$idsede"; ?>);
        //load_ciudad1(<?php //echo "$idpais,$idciudad";     ?>);
    });
</script>
<script src="js/ajax.js" type="text/javascript"></script>
<!-- switchery -->
<div class="right_col" <?php echo $newDimensionBody; ?> role="main">
    <div class="page-title">
        <div class="title_left">
            <h3>Listado general de los miembros de la Mision</h3>
            <?php
            ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <div class="col-md-12 col-sm-12 col-xs-12">
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">

                    <form action="" method="GET" id="formFiltros" name="formFiltros">
                        <table style="background-color: white;text-align: center;margin: auto">
                            <input type="hidden" id="mod" name="mod" value="<?php echo $_GET['mod']; ?>">
                            <tr style="height: 2px;">
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong>Pais</strong></td>
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong>Sede</strong></td>
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong>Grupo</strong></td>
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
                                <td colspan="2"  style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong>Condición</strong></td>
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
                                <td colspan="2"  style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong>Sexo</strong></td>
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
                                <td colspan="1" rowspan="2" style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong> <input class="btn btn-danger" type="submit" id="filtrar" name="filtrar" value="Filtrar" form="formFiltros" ></strong></td>
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
                                <td colspan="1" rowspan="2" style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong><a href="#modalNuevaPersona" class="btn btn-success" data-toggle="modal"><i class="fa fa-user"></i> Nuevo</a></strong></td>
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
                            </tr>
                            <tr>
                                <td style="background-color: #2774e6; padding: 5px; margin: 1px;">
                                    <select name="idpais" id="idpais" class="form-control" onchange="load_sede(this.value,<?php echo $idsede; ?>)">
                                        <option value="0">Pais</option>                                         
                                        <?php include '../../system/selectPais.php'; ?>
                                    </select> 
                                </td>
                                <td colspan="1"  style="text-align: center;padding: 1px; margin: 1px;"><strong>&nbsp;</strong></td>
                                <td style="background-color: #2774e6; padding: 5px; margin: 1px;">
                                    <div id="myDivSede">
                                        <select name="idsede" id="idsede" class="form-control">
                                            <option value="0">Sede</option>                                         
                                            <?php include '../../system/selectSede.php'; ?>
                                        </select> 
                                    </div>                                    
                                </td>
                                <td colspan="1"  style="text-align: center;padding: 1px; margin: 1px;"><strong>&nbsp;</strong></td>
                                <td style="background-color: #2774e6; padding: 5px; margin: 1px;">
                                    <select name="idgrupo" id="idgrupo" class="form-control">
                                        <option value="99">Grupo</option>
                                        <?php include '../../system/selectGrupo.php'; ?>
                                    </select> 
                                </td>
                                <td colspan="1"  style="text-align: center;padding: 1px; margin: 1px;"><strong>&nbsp;</strong></td>
                                <td style="background-color: #2774e6; padding: 5px; margin: 1px;">
                                    <select name="idcategoria" id="idcategoria" class="form-control">
                                        <option value="99">Condición</option>
                                        <?php include '../../system/selectCategoria.php'; ?>
                                    </select> 
                                </td>
                                <td colspan="1"  style="text-align: center;padding: 1px; margin: 1px;"><strong>&nbsp;</strong></td>
                                <td style="text-align: center;background-color: white; padding: 5px; margin: 1px;">
                                    <div class="radio-inline" title="Varón">
                                        <label class="">
                                            <input class="flat" type="radio" <?php
                                            if ($_GET['S'] == "M") {
                                                echo "checked";
                                            }
                                            ?> value="M" id="S" name="S"> M
                                        </label>
                                    </div>
                                </td>
                                <td style="text-align: center;background-color: white; padding: 5px; margin: 1px;">
                                    <div class="radio-inline" title="Mujer">
                                        <label>
                                            <input class="flat" type="radio" <?php
                                            if ($_GET['S'] == "F") {
                                                echo "checked";
                                            }
                                            ?> value="F" id="S" name="S"> F
                                        </label>
                                    </div>
                                </td>
                                <td colspan="1"  style="text-align: center;padding: 1px; margin: 1px;"><strong>&nbsp;</strong></td>
                            </tr>
                        </table>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table  id="example"  class="table table-striped   "> 
                    <tbody>
                        <?php include '../VISTAS/ageTablePersonas.php'; ?>                                    
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Modal Nueva Persona -->
    <!-- Modal Cambiar Estado -->
    <?PHP include '../VISTAS/listaModals.php'; ?>

    <div class="clearfix"></div>
    <?php
    require('../../include/footer.php');
    ?>
