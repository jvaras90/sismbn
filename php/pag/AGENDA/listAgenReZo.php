<?php
require('../../include/header.php');
include '../../system/mensajesAlerta.php';
include_once '../../system/crearConexion.php';
clearstatcache();
$titulo = "Cargos";
$hoja = "Cargos";
$idpais = $_GET['idpais'];
if (isset($_GET['iddepartamento'])) {
    $iddepartamento = $_GET['iddepartamento'];
} else {
    $iddepartamento = 0;
}
$idgrupo = $_GET['idgrupo'];
$idsede = $_GET['idsede'];
$idreunion_zona1 = $_GET['idreunion_zonal'];
$idreunion_granzona1 = $_GET['idreunion_granzona'];
?>
<!-- page content -->
<script>
    $(function () {
        //var idpais =  $("#idpais").val();
        //load_departamento3(<?php echo "$idpais,$iddepartamento"; ?>);
        //load_ciudad1(<?php //echo "$idpais,$idciudad";       ?>);
    });
</script>
<script src="js/ajax.js" type="text/javascript"></script>
<!-- switchery -->
<div class="right_col" <?php echo $newDimensionBody; ?> role="main">
    <div class="page-title">
        <div class="title_left">
            <h3>Listado de los participantes Inscritos</h3>
            <?php
            ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <form action="" method="GET">
            <div class="x_panel">
                <div class="x_title">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <table style="background-color: white;text-align: center;margin: auto">
                            <input type="hidden" id="mod" name="mod" value="<?php echo $_GET['mod']; ?>">
                            <tr style="height: 2px;">
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong>Pais</strong></td>
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong>Sede</strong></td>
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong>Gran Zona</strong></td>
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong>Reunion Zonal</strong></td>
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
                                <td colspan="2"  style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong>Sexo</strong></td>
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
                                <td colspan="1" rowspan="2" style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong> <input class="btn btn-danger" type="submit" value="Filtrar"></strong></td>
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
                                <td colspan="1" rowspan="2" style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong><a href="#modalNuevaPersona" class="btn btn-success" data-toggle="modal"><i class="fa fa-user"></i> Nuevo</a></strong></td>
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
                            </tr>
                            <tr>
                                <td style="background-color: #2774e6; padding: 5px; margin: 1px;">
                                    <select name="idpais" id="idpais" class="form-control" onchange="load_sede1(this.value)">
                                        <option value="0">Pais</option>                                         
                                        <?php include '../../system/selectPais.php'; ?>
                                    </select> 
                                </td>
                                <td colspan="1"  style="text-align: center;padding: 1px; margin: 1px;"><strong>&nbsp;</strong></td>
                                <td style="background-color: #2774e6; padding: 5px; margin: 1px;">
                                    <div id="myDivSede">
                                        <select name="idsede" id="idsede" class="form-control" onchange="load_reunion_granZona(this.value)">
                                            <option value="0">Sede</option>                                         
                                            <?php include '../../system/selectSede.php'; ?>
                                        </select> 
                                    </div>                                    
                                </td>
                                <td colspan="1"  style="text-align: center;padding: 1px; margin: 1px;"><strong>&nbsp;</strong></td>
                                <td style="background-color: #2774e6; padding: 5px; margin: 1px;">
                                    <div id="myDivGranZ">
                                        <select name="idreunion_granzona" id="idreunion_granzona" class="form-control" onchange="load_reunion_zonal(this.value)" >
                                            <option value="99">Gran Zona</option>                                         
                                            <?php include '../../system/selectReunionGranZona.php'; ?>
                                        </select> 
                                    </div>
                                </td>
                                <td colspan="1"  style="text-align: center;padding: 1px; margin: 1px;"><strong>&nbsp;</strong></td>
                                <td style="background-color: #2774e6; padding: 5px; margin: 1px;" id="myDivReuZon"  >
                                    <select name="idreunion_zonal" id="idreunion_zonal" class="form-control">
                                        <option value="99">Reunion Zonal</option>
                                        <?php include '../../system/selectReunionZonal.php'; ?>
                                    </select> 
                                </td>
                                <td colspan="1"  style="text-align: center;padding: 1px; margin: 1px;"><strong>&nbsp;</strong></td>
                                <td style="text-align: center;background-color: white; padding: 5px; margin: 1px;">
                                    <div class="radio-inline" title="Varón">
                                        <label class="">
                                            <input class="flat" type="radio" <?php
                                            if ($_GET['S'] == "M") {
                                                echo "checked";
                                            }
                                            ?> value="M" id="S" name="S"> M
                                        </label>
                                    </div>
                                </td>
                                <td style="text-align: center;background-color: white; padding: 5px; margin: 1px;">
                                    <div class="radio-inline" title="Mujer">
                                        <label>
                                            <input class="flat" type="radio" <?php
                                            if ($_GET['S'] == "F") {
                                                echo "checked";
                                            }
                                            ?> value="F" id="S" name="S"> F
                                        </label>
                                    </div>
                                </td>
                                <td colspan="1"  style="text-align: center;padding: 1px; margin: 1px;"><strong>&nbsp;</strong></td>

                            </tr>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table  id="example"  class="table table-striped   ">
<!--                        <thead style="background-color: rgb(39, 116, 230)">
                            <tr class="headings">

                                <th class="column-title"></th>
                                <th class="column-title no-link last"><span class="nobr">Accion</span>
                                </th>
                            </tr>
                        </thead>-->
                        <tbody>
                            <?php include '../VISTAS/ageTablePersonas.php'; ?>                                    
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
    </div>
    <!-- Modal Nueva Persona-->
    <?PHP include '../VISTAS/listaModals.php'; ?>

    <div class="clearfix"></div>
    <?php
    require('../../include/footer.php');
    ?>
