<?php
require('../../include/header.php');
include '../../system/mensajesAlerta.php';
include_once '../../system/crearConexion.php';
clearstatcache();
$titulo = "Provincias";
$hoja = "Provincias";
$idpais = $_GET['idpais'];
$iddepartamento = $_GET['iddepartamento'];
?>
<script src="js/ajax.js" type="text/javascript"></script>
<?php if (!isset($_GET['idprovincia'])) { ?>
    <script type="text/javascript">
        $(document).ready(function () {
            var lista2 = document.getElementById("idpais");
            $('#idpais').change(function () {
                window.location = "listProv?idpais=" + lista2.value + "&iddepartamento=<?php echo $_GET['iddepartamento']; ?>";
            });

            // Obtener la referencia a la lista
            // var lista3 = document.getElementById("idpais");
            // var lista1 = document.getElementById("idprovincia");
            // Obtener el índice de la opción que se ha seleccionado
            var indiceSeleccionado = lista2.selectedIndex;
            //var indiceSeleccionado1 = lista1.selectedIndex;
            // Con el índice y el array "options", obtener la opción seleccionada
            var opcionSeleccionada = lista2.options[indiceSeleccionado];
            //var opcionSeleccionada1 = lista1.options[indiceSeleccionado1];
            // Obtener el valor y el texto de la opción seleccionada
            //var textoSeleccionado = opcionSeleccionada.text;
            var valorSeleccionado = opcionSeleccionada.value;
            //var valorSeleccionado1 = opcionSeleccionada1.value;
            //alert(valorSeleccionado);
            load_departamento3(valorSeleccionado,<?php echo $iddepartamento; ?>);
            //load_distrito(valorSeleccionado1);



        });
        function  reloadPage(id) {
            window.location = "listProv?idpais=<?php echo $_GET['idpais']; ?>&iddepartamento=" + id;
        }
    </script>
<?php } ?>
<!-- page content -->
<div class="right_col" role="main">

    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Gestión de Mantenimiento</h3>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <div class="col-md-6">
                            <h2>Lista de <?php echo $titulo; ?><small> </small> </h2>
                        </div>
                        <div class="col-md-4">

                        </div>
                        <div class="col-md-2">
                            <a href="#modalNuevo<?php echo $hoja; ?>" class="btn btn-success" data-toggle="modal"><i class="fa fa-plus-square-o"></i> Nuevo</a>
                        </div>
                        <?php if (!isset($_GET['idprovincia'])) { ?>
                            <div title="Seleccione el Pais">
                                <div class="col-md-1">
                                    <label class="label label-primary">Pais: </label>
                                </div>
                                <div class="col-md-4" >

                                    <select class='form-control' name='idpais' id='idpais' onchange="load_departamento1(this.value)" >
                                        <option value="0">Seleccione</option>
                                        <?php include_once '../../system/selectPais.php'; ?>
                                    </select>

                                </div>
                            </div>
                            <div title="Seleccione el Departamento" >
                                <div class="col-md-3">
                                    <label class="label label-primary">Departamento: </label>
                                </div>
                                <div class="col-md-4" id="myDivDep">

                                </div>
                            </div>
                        <?php } ?>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table  id="example"  class="table table-striped responsive-utilities jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th class="column-title">Id Provincia</th>
                                    <th class="column-title">Provincia</th>
                                    <th class="column-title">Estado</th>
                                    <th class="column-title">Gestión</th>
                                    <!--<th class="column-title no-link last"><span class="nobr">Accion</span>-->
                                    </th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php include '../VISTAS/manTable' . $hoja . '.php'; ?>                                    
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!-- EL FORM MODIFICAR -->
            <?php
            if (isset($_GET['idprovincia'])) {
                $idprovincia = $_GET['idprovincia'];
                $sqlMail = "select * from provincia where idprovincia = $idprovincia;";
                $resMail = $mysqlMBN->consultas($sqlMail);
                while ($rowMail = mysqli_fetch_array($resMail)) {
                    //$idprovincia = $rowMail['idprovincia'];
                    $des_provincia = $rowMail['des_provincia'];
                    $iddepartamento = $rowMail['iddepartamento'];
                    $idestado = $rowMail['idestado'];
                }
                ?>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Actualización de <?php echo $titulo; ?> <small><b><?php echo $correo ?></b></small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-12 col-sm-9 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_content">
                                        <form class="form-horizontal form-label-left" method="post" action="modificarB" id="actualiza">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-4 col-xs-12">Departamento <span class="required">*</span></label>
                                                <div class="col-md-8 col-sm-6 col-xs-12">
                                                    <select class='form-control' name='iddepartamento1' id='iddepartamento1' >
                                                        <option value="0">Seleccione</option>
                                                        <?php include_once '../../system/selectDepartamento.php'; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Provincia <span class="required">*</span>
                                                </label>
                                                <div class="col-md-8 col-sm-6 col-xs-12" id="cliente">
                                                    <input type="text" id="descripcion" name="descripcion" required class="form-control col-md-7 col-xs-12" value="<?php echo $des_provincia; ?>"  >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-4 col-xs-12" >Estado <span class="required">*</span></label>
                                                <div class="col-md-8 col-sm-6 col-xs-12">
                                                    <select class='form-control' name='idestado' id='idestado' >
                                                        <option value="0">Seleccione</option>
                                                        <?php include_once '../../system/selectEstado.php'; ?>
                                                    </select>
                                                </div>
                                            </div>                                           
                                            <div class="panel-footer">
                                                <div style="text-align: center;">
                                                    <a href="listProv?idpais=<?php echo $_GET['idpais']; ?>&iddepartamento=<?php echo $_GET['iddepartamento']; ?>" class="btn btn-danger">Cancelar</a>
                                                    <input type="button" value="Actualizar" id="actualizar" name="actualizar" class="btn btn-primary" onclick="this.disabled = true;
                                                            this.value = 'Actualizando...';
                                                            this.form.submit()"/>
                                                    <input type="hidden" name="form" value="modificar<?php echo $hoja; ?>"/>
                                                    <input type="hidden" name="iddepartamento" value="<?php echo $_GET['iddepartamento']; ?>"/>
                                                    <input type="hidden" name="idprovincia" value="<?php echo $idprovincia; ?>"/>
                                                </div>
                                            </div>                                          
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <?php
            }
            include '../VISTAS/listaModals.php';
            ?>
        </div>
    </div>
    <?php
    require('../../include/footer.php');
    