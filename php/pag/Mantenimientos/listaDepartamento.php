<?php
require('../../include/header.php');
include '../../system/mensajesAlerta.php';
include_once '../../system/crearConexion.php';
clearstatcache();
$titulo = "Departamentos";
$hoja = "Departamentos";
$idpais = $_GET['idpais'];
?>

<script type="text/javascript">
    $(document).ready(function () {
        var lista = document.getElementById("idpais");
        $('#idpais').change(function () {
            window.location = "listDepa?idpais=" + lista.value;
        });
    });
</script>
<!-- page content -->
<div class="right_col" role="main">

    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Gestión de Mantenimiento</h3>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <div class="col-md-5">
                            <h2>Lista de <?php echo $titulo; ?><small> </small> </h2>
                        </div>
                        <div class="col-md-5" title="Seleccione el Pais">
                            <?php if (!isset($_GET['iddepartamento'])) { ?>
                                <select class='form-control' name='idpais' id='idpais' >
                                    <option value="0">Seleccione</option>
                                    <?php include_once '../../system/selectPais.php'; ?>
                                </select>
                            <?php } ?>
                        </div>
                        <div class="col-md-2">
                            <a href="#modalNuevo<?php echo $hoja; ?>" class="btn btn-success" data-toggle="modal"><i class="fa fa-plus-square-o"></i> Nuevo</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table  id="example"  class="table table-striped responsive-utilities jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th class="column-title">Id Departamento</th>
                                    <th class="column-title">Descripcion</th>
                                    <th class="column-title">Estado</th>
                                    <th class="column-title">Gestión</th>
                                    <!--<th class="column-title no-link last"><span class="nobr">Accion</span>-->
                                    </th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php include '../VISTAS/manTable' . $hoja . '.php'; ?>                                    
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!-- EL FORM MODIFICAR -->
            <?php
            if (isset($_GET['iddepartamento'])) {
                $iddepartamento = $_GET['iddepartamento'];
                $sql1 = "select * from departamento where iddepartamento = $iddepartamento;";
                $res1 = $mysqlMBN->consultas($sql1);
                while ($row1 = mysqli_fetch_array($res1)) {
                    $descripcion1 = $row1['des_departamento'];
                    $idestado = $row1['idestado'];
                }
                ?>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Actualización de <?php echo $titulo; ?> <small><b><?php echo $correo ?></b></small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-12 col-sm-9 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_content">
                                        <form class="form-horizontal form-label-left" method="post" action="modificarB" id="actualiza">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-4 col-xs-12">Pais <span class="required">*</span></label>
                                                <div class="col-md-8 col-sm-6 col-xs-12">
                                                    <select class='form-control' name='idpais1' id='idpais1' >
                                                        <option value="0">Seleccione</option>
                                                        <?php include_once '../../system/selectPais.php'; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Departamento <span class="required">*</span>
                                                </label>
                                                <div class="col-md-8 col-sm-6 col-xs-12" id="cliente">
                                                    <input type="text" id="descripcion" name="descripcion" required class="form-control col-md-7 col-xs-12" value="<?php echo $descripcion1; ?>"  >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-4 col-xs-12" title="Negocio de la campaña">Estado <span class="required">*</span></label>
                                                <div class="col-md-8 col-sm-6 col-xs-12">
                                                    <select class='form-control' name='idestado' id='idestado' >
                                                        <option value="0">Seleccione</option>
                                                        <?php include_once '../../system/selectEstado.php'; ?>
                                                    </select>
                                                </div>
                                            </div>                                            
                                            <div class="panel-footer">
                                                <div style="text-align: center;">
                                                    <a href="listDepa?idpais=<?php echo $idpais; ?>" class="btn btn-danger">Cancelar</a>
                                                    <input type="button" value="Actualizar" id="actualizar" name="actualizar" class="btn btn-primary" onclick="this.disabled = true;
                                                            this.value = 'Actualizando...';
                                                            this.form.submit()"/>
                                                    <input type="hidden" name="form" value="modificar<?php echo $hoja; ?>"/>
                                                    <input type="hidden" name="iddepartamento" value="<?php echo $iddepartamento; ?>"/>
                                                    <input type="hidden" name="idpais" value="<?php echo $_GET['idpais']; ?>"/>
                                                </div>
                                            </div>                                          
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <?php
            }
            include '../VISTAS/listaModals.php';
            ?>
        </div>
    </div>
    <?php
    require('../../include/footer.php');
    