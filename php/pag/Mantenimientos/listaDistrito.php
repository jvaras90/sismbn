<?php
require('../../include/header.php');
include '../../system/mensajesAlerta.php';
include_once '../../system/crearConexion.php';
clearstatcache();
$titulo = "Distritos";
$hoja = "Distritos";
$idpais = $_GET['idpais'];
$iddepartamento = $_GET['iddepartamento'];
$idprovincia = $_GET['idprovincia'];
?>
<script src="js/ajax.js" type="text/javascript"></script>
<?php if (!isset($_GET['iddistrito'])) { ?>
    <script type="text/javascript">
        $(document).ready(function () {
            var lista2 = document.getElementById("idpais");
            $('#idpais').change(function () {
                window.location = "listDist?idpais=" + lista2.value + "&iddepartamento=0&idprovincia=0";
            });

            load_departamento3(<?php echo $idpais; ?>,<?php echo $iddepartamento; ?>);
            load_provincia3(<?php echo $iddepartamento; ?>,<?php echo $idprovincia; ?>);
        });
    //        function  reloadPage(id) {
    //            window.location = "listDist?idpais=<?php echo $_GET['idpais']; ?>&iddepartamento=" + id + "&idprovincia=<?php echo $_GET['idprovincia']; ?>";
    //            load_provincia3(id);
    //        }
        function  reloadPage2(idprov, iddep) {
            window.location = "listDist?idpais=<?php echo $_GET['idpais']; ?>&iddepartamento=" + iddep + "&idprovincia=" + idprov;
            //load_provincia3(iddep, idprov);
        }
    </script>
<?php } ?>
<!-- page content -->
<div class="right_col" role="main">

    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Gestión de Mantenimiento</h3>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <div class="col-md-3">
                            <h2>Lista de <?php echo $titulo; ?><small> </small> </h2>
                        </div>
                        <div class="col-md-7">
                            <?php if (!isset($_GET['iddistrito'])) { ?>
                                <div title="Seleccione el Pais">
                                    <div class="col-md-2">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="label label-primary">Pais: </label>
                                    </div>
                                    <div class="col-md-8" >

                                        <select class='form-control' name='idpais' id='idpais' onchange="load_departamento3(this.value)" >
                                            <option value="0">Seleccione</option>
                                            <?php include_once '../../system/selectPais.php'; ?>
                                        </select>

                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="col-md-2">
                            <a href="#modalNuevo<?php echo $hoja; ?>" class="btn btn-success" data-toggle="modal"><i class="fa fa-plus-square-o"></i> Nuevo</a>
                        </div>
                        <?php if (!isset($_GET['iddistrito'])) { ?>
                            <div title="Seleccione el Departamento" >
                                <div class="col-md-2">
                                    <label class="label label-primary">Departamento</label>
                                </div>
                                <div class="col-md-4" id="myDivDep">

                                </div>
                            </div>
                            <div  title="Seleccione la Provincia" >
                                <div class="col-md-2">
                                    <label class="label label-primary">Provincia</label>
                                </div>
                                <div class="col-md-4" title="Seleccione Provincia" id="myDivProv">

                                </div>
                            </div>
                        <?php } ?>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table  id="example"  class="table table-striped responsive-utilities jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th class="column-title">Id Distrito</th>
                                    <th class="column-title">Descripcion</th>
                                    <th class="column-title">Estado</th>
                                    <th class="column-title">Gestión</th>
                                    <!--<th class="column-title no-link last"><span class="nobr">Accion</span>-->
                                    </th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php include '../VISTAS/manTable' . $hoja . '.php'; ?>                                    
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!-- EL FORM MODIFICAR -->
            <?php
            if (isset($_GET['iddistrito'])) {
                $iddistrito = $_GET['iddistrito'];
                $sql1 = "select * from distrito where iddistrito = $iddistrito;";
                $res1 = $mysqlMBN->consultas($sql1);
                while ($row1 = mysqli_fetch_array($res1)) {
                    $descripcion = $row1['des_distrito'];
                    $idestado = $row1['idestado'];
                }
                ?>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Actualización de <?php echo $titulo; ?> <small><b><?php echo $correo ?></b></small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-12 col-sm-9 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_content">
                                        <form class="form-horizontal form-label-left" method="post" action="modificarB" id="actualiza">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-4 col-xs-12">Provincia <span class="required">*</span></label>
                                                <div class="col-md-8 col-sm-6 col-xs-12">
                                                    <select class='form-control' name='idprovincia1' id='idprovincia1' >
                                                        <option value="0">Seleccione</option>
                                                        <?php include_once '../../system/selectProvincia.php'; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Distrito <span class="required">*</span>
                                                </label>
                                                <div class="col-md-8 col-sm-6 col-xs-12" id="cliente">
                                                    <input type="text" id="descripcion" name="descripcion" required class="form-control col-md-7 col-xs-12" value="<?php echo $descripcion; ?>"  >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-4 col-xs-12" >Estado <span class="required">*</span></label>
                                                <div class="col-md-8 col-sm-6 col-xs-12">
                                                    <select class='form-control' name='idestado' id='idestado' >
                                                        <option value="0">Seleccione</option>
                                                        <?php include_once '../../system/selectEstado.php'; ?>
                                                    </select>
                                                </div>
                                            </div>                                            
                                            <div class="panel-footer">
                                                <div style="text-align: center;">
                                                    <a href="listDist?idpais=<?php echo $_GET['idpais']; ?>&iddepartamento=<?php echo $_GET['iddepartamento']; ?>&idprovincia=<?php echo $_GET['idprovincia']; ?>" class="btn btn-danger">Cancelar</a>
                                                    <input type="button" value="Actualizar" id="actualizar" name="actualizar" class="btn btn-primary" onclick="this.disabled = true;
                                                            this.value = 'Actualizando...';
                                                            this.form.submit()"/>
                                                    <input type="hidden" name="form" value="modificar<?php echo $hoja; ?>"/>
                                                    <input type="hidden" name="idprovincia" value="<?php echo $_GET['idprovincia']; ?>"/>
                                                    <input type="hidden" name="iddistrito" value="<?php echo $iddistrito; ?>"/>
                                                </div>
                                            </div>                                          
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <?php
            }
            include '../VISTAS/listaModals.php';
            ?>
        </div>
    </div>
    <?php
    require('../../include/footer.php');
    ?>
