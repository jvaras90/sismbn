<?php
$consulta = "select *,e.descripcion estado from provincia c 
inner join estado e on e.idestado = c.idestado where iddepartamento=$iddepartamento order by des_provincia;";
$result = $mysqlMBN->consultas($consulta);
$registros = mysqli_num_rows($result);
echo "Total de Registros: " . $registros . "<br>";
while ($row = mysqli_fetch_array($result)) {
    $idprovincia = $row['idprovincia'];
    $des_provincia = $row['des_provincia'];
    $iddepartamento = $row['iddepartamento'];
    $idestado = $row['idestado'];
    $estado = $row['estado'];
    ?>
    <tr class="even pointer">
        <td class="a-center "><input type="checkbox" class="flat" name="table_records" ></td>
        <td class=""><?php echo $idprovincia; ?></td>
        <td class=""><?php echo $des_provincia; ?></td>
        <td class=""><?php echo $estado; ?></td>
        <td class="last">
            <form method="GET" id="exclude_form_<?php echo $id ?>" action="">
                <div style="float: left;" title="Editar <?php echo $titulo; ?>"> 
                    <a href = "listProv?idpais=<?php echo $_GET['idpais']; ?>&iddepartamento=<?php echo $_GET['iddepartamento']; ?>&idprovincia=<?php echo $idprovincia; ?>" class = "btn btn-round btn-primary btn-sm " ><i class = "fa fa-pencil-square"></i></a>
                </div>
                <div style="float: left;" title="Eliminar <?php echo $titulo; ?>">
                    <button type="button" class="btn btn-round btn-danger btn-sm" onclick="eliminar(<?php echo "'quitar','$des_provincia','provincia','idprovincia', $idprovincia,'listProv'" ?>);"  > 
                        <i class="fa fa-minus-square"></i> 
                    </button>
                </div>
            </form>
        </td>
    </tr>
<?php } ?>

<script>
    function eliminar(accion, nombre, table, atributo, id, pag)
    {
        if (confirm('Esta Apunto de eliminar a "' + nombre + '" de la tabla ' + table + ' \n¿Desea Continuar?'))
        {
            window.location.href = 'modificarB?accion=' + accion + '&table=' + table + '&atrib=' + atributo + '&id=' + id + '&pag=' + pag + '&iddepartamento=' + <?php echo $_GET['iddepartamento']; ?>;
        } else
        {
            alert('Se canceló ésta Operacion.');
        }

    }
</script>