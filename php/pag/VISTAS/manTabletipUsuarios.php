<?php
$consulta = "select idtipo_usuario,rz.descripcion tipo_user,rz.idestado, e.descripcion estado from tipo_usuario rz
inner join estado e on e.idestado = rz.idestado ;";
$result = $mysqlMBN->consultas($consulta);
$registros = mysqli_num_rows($result);
echo "Total de Anexos: " . $registros . "<br>";
while ($row = mysqli_fetch_array($result)) {
    $idtipo_usuario = $row['idtipo_usuario'];
    $tipo_user = $row['tipo_user'];
    $idestado = $row['idestado'];
    $estado = $row['estado'];
    ?>
    <tr class="even pointer">
        <td class="a-center "><input type="checkbox" class="flat" name="table_records" ></td>
        <td class=""><?php echo $idtipo_usuario; ?></td>
        <td class=""><?php echo $tipo_user; ?></td>
        <td class=""><?php echo $estado; ?></td>
        <td class="last">

            <form method="GET" id="exclude_form_<?php echo $id ?>" action="">
                <div style="float: left;" title="Editar <?php echo $titulo; ?>"> 
                    <a href = "listTusu?idtipo_usuario=<?php echo $idtipo_usuario ?>" class = "btn btn-round btn-primary btn-sm " ><i class = "fa fa-pencil-square"></i></a>
                </div>
                <div style="float: left;" title="Eliminar <?php echo $titulo; ?>">
                    <button type="button" class="btn btn-round btn-danger btn-sm" onclick="eliminar(<?php echo "'quitar','$tipo_user','tipo_usuario','idtipo_usuario', $idtipo_usuario,'listTusu'" ?>);"  > 
                        <i class="fa fa-minus-square"></i> 
                    </button>
                </div>
            </form>
        </td>
    </tr>
<?php } ?>

<script>
    function eliminar(accion, nombre, table, atributo, id, pag)
    {
        if (confirm('Esta Apunto de eliminar a "' + nombre + '" de la tabla ' + table + ' \n¿Desea Continuar?'))
        {
            window.location.href = 'modificarB?accion=' + accion + '&table=' + table + '&atrib=' + atributo + '&id=' + id + '&pag=' + pag;
        } else
        {
            alert('Se canceló ésta Operacion.');
        }

    }
</script>