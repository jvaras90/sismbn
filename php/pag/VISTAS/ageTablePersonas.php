<?php
$criterio = " where '1'='1' and p.idestado = 1 ";
$idreunion_zona1 = $_GET['idreunion_granzona'];
$idreunion_granzona = $_GET['idreunion_zonal'];

if (isset($_GET['S']) && $_GET['S'] != "") {
    $criterio .= "and p.sexo='" . $_GET['S'] . "' ";
}
if (isset($_GET['idcargo'])) {
    $criterio .= "and ds.idcargo like '%" . $_GET['idcargo'] . "%' ";
}
//if (isset($_GET['idreunion_granzona']) && $_GET['idreunion_granzona'] != "99") {
//    $criterio .= " and ds.idcargo like  '%" . $_GET['idreunion_granzona'] . "%' ";
//}
if (isset($_GET['idreunion_zonal']) && $_GET['idreunion_zonal'] != "99") {
    $criterio .= "and ds.idreunion_zonal='" . $_GET['idreunion_zonal'] . "' ";
}
if (isset($_GET['idgrupo']) && $_GET['idgrupo'] != "99") {
    $criterio .= "and p.idgrupos=" . $_GET['idgrupo'] . " ";
}
if (isset($_GET['idcategoria']) && $_GET['idcategoria'] != "99") {
    $criterio .= "and p.idcategoria=" . $_GET['idcategoria'] . " ";
}
if (isset($_GET['idpais']) && $_GET['idpais'] != "0") {
    $criterio .= "and p.idpais=" . $_GET['idpais'] . " ";
}
if (isset($_GET['iddepartamento']) && $_GET['iddepartamento'] != "0") {
    $criterio .= "and p.iddepartamento=" . $_GET['iddepartamento'] . " ";
}
if (isset($_GET['idsede']) && $_GET['idsede'] != "0") {
    $criterio .= "and p.idsede=" . $_GET['idsede'] . " ";
}
if ($_SESSION['id_nivel'] != 1) {
    $criterio .= "and p.idestado=1 ";
}


$consulta = "select p.idpersona,concat(nombres,' ',apaterno,' ',amaterno) as nombre,p.foto,p.telefono_fijo,p.celular,p.lugar_nacimiento,p.idestado p_idestado,
p.email,p.profesion,p.ocupacion,p.idtipo_documento,td.descripcion documento,p.nro_documento,p.idgrupos,gru.desc_camp grupo,p.idpais,p.fec_nac,
pa.des_pais pais,p.iddepartamento,dep.des_departamento , ds.idcargo,p.sexo,ds.idreunion_zonal,rz.descripcion reunion_zonal,
p.idcategoria,cat.descripcion condicion,p.direccion,p.idestado_civil,ec.descripcion estado_civil,
YEAR(CURDATE())-YEAR(p.fec_nac) + IF(DATE_FORMAT(CURDATE(),'%m-%d') > DATE_FORMAT(p.fec_nac,'%m-%d'),0,-1) edad 
from persona p 
inner join tipo_documento td on td.idtipo_documento=p.idtipo_documento 
inner join categoria cat on cat.idcategoria=p.idcategoria 
inner join grupos gru on gru.idgrupos=p.idgrupos 
inner join pais pa on pa.idpais=p.idpais 
inner join departamento dep on dep.iddepartamento=p.iddepartamento 
inner join estado_civil ec on ec.idestado_civil=p.idestado_civil 
left join detalle_salvacion ds on ds.idpersona=p.idpersona 
left join reunion_zonal rz on rz.idreunion_zonal=ds.idreunion_zonal 
$criterio 
order by nombre 
";
//echo $consulta;
$cadena = str_replace("%", "_3", $criterio);
//$cadena = str_replace("+", "_2", $cadena1);
//$cadena = $consulta;
$result = $mysqlMBN->consultas($consulta);
$registros = mysqli_num_rows($result);
if ($_SESSION['id_nivel'] == 1 || $_SESSION['id_nivel'] == 2) {
    include 'seccionDescargaPDF.php';
}
echo "Total de Registros: " . $registros . " ";
while ($row = mysqli_fetch_array($result)) {
    $des_total1 = "";
    $idpersona = $row['idpersona'];
    $nombres = $row['nombre'];
    $foto = $row['foto'];
    $telefono_fijo = $row['telefono_fijo'];
    $celular = $row['celular'];
    $documento = $row['documento'];
    $nro_documento = $row['nro_documento'];
    $idgrupos = $row['idgrupos'];
    $grupo = $row['grupo'];
    $idpais = $row['idpais'];
    $pais = $row['pais'];
    $iddepartamento = $row['iddepartamento'];
    $des_departamento = $row['des_departamento'];
    $idcargo = $row['idcargo'];
    $sexo = $row['sexo'];
    $idreunion_zonal = $row['idreunion_zonal'];
    $reunion_zonal = $row['reunion_zonal'];
    $idcategoria = $row['idcategoria'];
    $condicion = $row['condicion'];
    $fec_nac = $row['fec_nac'];
    if ($fec_nac == '0000-00-00') {
        $edad = "S/N";
    } else {
        $edad = $row['edad'];
    }
    $direccion = $row['direccion'];
    $idestado_civil = $row['idestado_civil'];
    $estado_civil = $row['estado_civil'];
    $lugar_nacimiento = $row['lugar_nacimiento'];
    $email = $row['email'];
    $profesion = $row['profesion'];
    $ocupacion = $row['ocupacion'];
    $cargo = $row['idcargo'];
    $pIdestado = $row['p_idestado'];
    $sqry2 = "select * from cargo where idcargo in ($cargo);";
    //echo $sqry2."<br>";
    $res2 = $mysqlMBN->consultasLibre($sqry2);
    while ($row2 = mysqli_fetch_array($res2)) {
        $des_total1 .= $row2['descripcion'] . ",";
    }
    $des_cargo1 = substr($des_total1, 0, -1);
    ?>

    <tr class="even pointer">

        <td>
            <div class="col-md-12 col-sm-12 col-xs-12 ">
                <div class="col-md-12 col-sm-12 col-xs-12 ">
                    <div class="col-md-3 col-sm-3 col-xs-3  text-center">
                        <img src="images/Fotos/<?php echo $foto; ?>" alt="" style="width: 150px;height: 150px;" class="img-circle img-responsive">
                    </div>
                    <div class="col-md-7 col-sm-7 col-xs-7 ">
                        <table style="text-align:center; margin: auto" width="600" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td colspan="8"><h2 class="brief"><i><?php
                                                echo "$nombres <b>[$condicion] </b>";
                                                if ($_SESSION['id_nivel'] == 1 || $_SESSION['id_nivel'] == 2) {
                                                    echo "(idpersona: " . $idpersona . ")";
                                                }
                                                ?></i></h2></td>
                                </tr>
                                <tr>
                                    <td><strong>Fecha Nac: </strong></td>
                                    <td><?php echo $fec_nac; ?></td>
                                    <td><strong>Edad </strong></td>
                                    <td><?php echo $edad; ?></td>
                                    <td><strong><?php echo $documento; ?>: </strong></td>
                                    <td><?php echo $nro_documento; ?></td>
                                    <td><strong>Estado : </strong></td>
                                    <td><?php echo $estado_civil; ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Grupo: </strong></td>
                                    <td><?php echo $grupo; ?></td>
                                    <td><strong>Sexo: </strong></td>
                                    <td><?php echo $sexo; ?></td>
                                    <td><strong>Teléfono: </strong></td>
                                    <td><?php echo $telefono_fijo ?></td>
                                    <td><strong>Celular: </strong></td>
                                    <td><?php echo $celular; ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Lugar Nac: </strong></td>
                                    <td colspan="3"><?php echo $lugar_nacimiento; ?></td>
                                    <td><strong> E-mail: </strong></td>
                                    <td colspan="3"><?php echo $email; ?></td>   
                                </tr>
                                <tr>
                                    <td><strong>Profesión: </strong></td>
                                    <td colspan="3"><?php echo $profesion; ?></td>
                                    <td><strong>Trabajo Actual: </strong></td>
                                    <td colspan="3"><?php echo $ocupacion; ?></td>   
                                </tr>
                                <tr>
                                    <td><strong>Cargo: </strong></td>
                                    <td colspan="3"><?php echo $des_cargo1; ?></td>
                                    <td><strong>Reunion Zonal: </strong></td>
                                    <td colspan="3"><?php echo $reunion_zonal; ?></td>  
                                </tr>
                                <tr>
                                    <td><strong>Departamento: </strong></td>
                                    <td colspan="3"><?php echo $des_departamento; ?></td> 
                                </tr>
                                <tr>                                    
                                    <td><strong>Dirección: </strong></td>
                                    <td colspan="7"><?php echo $direccion; ?></td>
                                </tr>
                                <tr>
                                    <td colspan="8">
                                        <br>
                                        <a href="persDetalle?mod=mPersonas&mod_a=<?php echo $_GET['mod'] ?>&idpersona=<?php echo $idpersona ?>" class="btn btn-success btn-sm" target="_blank"><i class="fa fa-user"></i> Ver Perfil</a>
                                        <?php if ($_SESSION['id_nivel'] == '1' || $_SESSION['id_nivel'] == '2') { ?>
                                            <a href="#modalCambiarEstado" data-toggle="modal" class="btn btn-danger" onclick="mostrar('<?php echo $idpersona; ?>')"title="Cambiar Estado"><i class="fa fa-minus"></i></a>                                         
                                        <?php } ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3">

                    </div>
                </div>
            </div>
        </td>
    </tr>
<?php } ?>

