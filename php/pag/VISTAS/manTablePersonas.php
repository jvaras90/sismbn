<?php
$criterio = " where '1'='1' ";

if (isset($_GET['S']) && $_GET['S'] != "") {
    $criterio .= " and p.sexo= '" . $_GET['S'] . "' ";
}
if (isset($_GET['idgrupo']) && $_GET['idgrupo'] != "99") {
    $criterio .= " and p.idgrupos = " . $_GET['idgrupo'] . "  ";
}
if (isset($_GET['idpais']) && $_GET['idpais'] != "0") {
    $criterio .= " and p.idpais = " . $_GET['idpais'] . "  ";
}
if (isset($_GET['idciudad']) && $_GET['idciudad'] != "0") {
    $criterio .= " and i.idciudad = " . $_GET['idciudad'] . "  ";
}
if ($_SESSION['id_nivel'] != 1) {
    $criterio .= " and p.idestado=1 ";
}


$consulta = "select i.idinscripciones, p.idpersona,concat(nombres,' ',apaterno,' ',amaterno) as nombre,p.foto,p.idtipo_documento,td.descripcion documento,
i.edad,i.idgrupo,gru.desc_camp grupo_camp,p.idpais , pa.des_pais,
i.idciudad, ci.descripcion ciudad,i.dni,i.cargo,i.sexo,i.idreunion_zonal, rz.descripcion reunion_zonal,
p.idcategoria, cat.descripcion condicion,i.celular,i.idtalla_polo , tp.descripcion talla_polo , i.grupo, i.voluntario,
i.idestado_pago , ep.descripcion estado_pago
from incripciones_2016 i 
inner join persona p on p.idpersona = i.idpersona 
inner join tipo_documento td on td.idtipo_documento = p.idtipo_documento
inner join categoria cat on cat.idcategoria = p.idcategoria
inner join grupos gru on gru.idgrupos = i.idgrupo
inner join ciudad  ci on ci.idciudad = i.idciudad
inner join pais pa on pa.idpais = p.idpais
inner join reunion_zonal rz on rz.idreunion_zonal = i.idreunion_zonal
inner join talla_polo tp on tp.idtalla_polo = i.idtalla_polo
inner join estado_pago ep on ep.idestado_pago = i.idestado_pago
$criterio     
;";
//echo $consulta;
$result = $mysqlMBN->consultas($consulta);
$registros = mysqli_num_rows($result);
echo "Total de Registros: " . $registros . "<br>";
while ($row = mysqli_fetch_array($result)) {
    $des_total1 = "";
    $idinscripciones = $row['idinscripciones'];
    $idpersona = $row['idpersona'];
    $nombres = $row['nombre'];
    $foto = $row['foto'];
    $documento = $row['documento'];
    $edad = $row['edad'];
    $idgrupo = $row['idgrupo'];
    $grupo_camp = $row['grupo_camp'];
    $idpais = $row['idpais'];
    $pais = $row['des_pais'];
    $idciudad = $row['idciudad'];
    $ciudad = $row['ciudad'];
    $dni = $row['dni'];
    $cargo = $row['cargo'];
    $sexo = $row['sexo'];
    $idreunion_zonal = $row['idreunion_zonal'];
    $reunion_zonal = $row['reunion_zonal'];
    $idcategoria = $row['idcategoria'];
    $condicion = $row['condicion'];
    $celular = $row['celular'];
    $idtalla_polo = $row['idtalla_polo'];
    $talla_polo = $row['talla_polo'];
    $grupo = $row['grupo'];
    $voluntario = $row['voluntario'];
    $idestado_pago = $row['idestado_pago'];
    $estado_pago = $row['estado_pago'];
//    $ = $row['idgrupo'];
//    $ = $row['idgrupo'];
//    $ = $row['idgrupo'];
//    
//    
    $sqry2 = "select * from cargo where idcargo in ($cargo);";
    //echo "$sqry2.<br>";
    $res2 = $mysqlMBN->consultasLibre($sqry2);
    while ($row2 = mysqli_fetch_array($res2)) {
        $des_total1 .= $row2['descripcion'] . ",";
    }
    $des_cargo1 = substr($des_total1, 0, -1);
    ?>
    <tr class="even pointer">

        <td>
            <div class="col-md-12 col-sm-12 col-xs-12 ">
                <div class="col-md-12 col-sm-12 col-xs-12 ">
                    <div class="col-md-3 col-sm-3 col-xs-3  text-center">
                        <img src="images/Fotos/<?php echo $foto; ?>" alt="" style="width: 150px;height: 150px;" class="img-circle img-responsive"> <br>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-9 ">
                        <table style="text-align:center; margin: auto" width="600" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td colspan="8"><h2 class="brief"><i><?php
                                                echo $nombres . " <b>($pais)</b>";
                                                if ($_SESSION['id_nivel'] == 1 || $_SESSION['id_nivel'] == 2) {
                                                    echo "<b>(idpersona: " . $idpersona . ") (idinscripcion: " . $idinscripciones . ")</b>";
                                                }
                                                ?></i></h2></td>
                                </tr>
                                <tr>
                                    <td><strong>Edad: </strong></td>
                                    <td><?php echo $edad; ?></td>
                                    <td><strong>Estado </strong></td>
                                    <td><?php echo $grupo_camp; ?></td>
                                    <td><strong><?php echo $documento; ?>: </strong></td>
                                    <td><?php echo $dni; ?></td>
                                    <td><strong>Talla de Polo: </strong></td>
                                    <td><?php echo $talla_polo; ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Grupo: </strong></td>
                                    <td><?php echo $grupo; ?></td>
                                    <td><strong>Sexo: </strong></td>
                                    <td><?php echo $sexo; ?></td>
                                    <td><strong>Celular: </strong></td>
                                    <td><?php echo $celular; ?></td>
                                    <td><strong>Voluntario: </strong></td>
                                    <td><?php
                                        if ($voluntario == 1) {
                                            echo "si";
                                        } else {
                                            echo "no";
                                        }
                                        ?></td>
                                </tr>
                                <tr>
                                    <td><strong>DNI: </strong></td>
                                    <td colspan="3"><?php echo $dni; ?></td>
                                    <td><strong>Zona: </strong></td>
                                    <td colspan="3"><?php echo $reunion_zonal; ?></td>   
                                </tr>
                                <tr>
                                    <td><strong>Estado de Pago: </strong></td>
                                    <td colspan="3"><?php echo $estado_pago; ?></td>
                                    <td><strong>Lugar: </strong></td>
                                    <td colspan="3"><?php echo $ciudad; ?></td>   
                                </tr>
                                <tr>
                                    <td><strong>Cargo: </strong></td>
                                    <td colspan="3"><?php echo $des_cargo1; ?></td>
                                    <td><strong>Condición: </strong></td>
                                    <td colspan="3"><?php echo $condicion; ?></td>  
                                </tr>
                                <tr>
                                    <td colspan="8">
                                        <a href="persDetalle?mod=mPersonas&idpersona=<?php echo $idpersona ?>" class="btn btn-success btn-sm"><i class="fa fa-user"></i> Ver Perfil</a>
                                        <a href="listInc?mod=mInscripcion&idinscripcion=<?php echo $idinscripciones ?>" class="btn btn-primary btn-sm"><i class="fa fa-list"></i> Ver Inscripción</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </td>
    </tr>
<?php } ?>

