<?php
include_once '../../system/crearConexion.php';
$idpersona = $_GET['idpersona'];
$idestado = $_GET['idestado'];

$sql = "select idpersona,concat(nombres,' ',apaterno,' ',amaterno) as nombre,idestado from persona where idpersona = " . $idpersona;
$result = $mysqlMBN->consultas($sql);
$row = mysqli_fetch_array($result);
$nombres = $row['nombre'];
$pIdestado = $row['idestado'];
//echo $sql;
?>
<script type="text/javascript">
    $(function () {
        $('#cambiarEstado').attr('disabled', 'disabled');
    });
</script>

<div style="text-align: center;">
    <h2><b>Cambiar estado de <?php echo $nombres; ?></b></h2>
</div>
<div class="col-md-12" title="Seleccione el nuevo estado">
    <div class="form-group">
        <label>Estado</label>
        <div class="input-group">
            <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span>
            <select class='form-control' name='idestado2' id='idestado2' onchange="valSelectCambio()">
                <option value="0" >Seleccione</option>
                <?php include '../../system/selectEstado.php'; ?>
            </select>
        </div>
    </div>
</div>
<div class="col-md-12" title="Ingrese el motivo por el que cambiará el estado">
    <div class="form-group">
        <label>Motivo</label>
        <div class="input-group">
            <span class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span></span>
            <textarea id="motivo" name="motivo" style="resize: none;" class="form-control"  onkeydown="quitarDis()" onkeyup="agregarDis()"></textarea> 
        </div>
    </div>
</div>
<input type="hidden" name="idpersona" value="<?php echo $idpersona; ?>"/> 
<input type="hidden" name="idestado_old" value="<?php echo $pIdestado; ?>"/> 