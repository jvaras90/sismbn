<?php
$consulta = "select idreunion_zonal,rz.descripcion reunion_zonal,rz.idreunion_granzona,
rgz.descripcion reuGranZona, rz.idestado,e.descripcion estado
from reunion_zonal rz 
inner join estado e on e.idestado = rz.idestado 
inner join reunion_granzona rgz on rz.idreunion_granzona = rgz.idreunion_granzona
order by rz.descripcion;";
$result = $mysqlMBN->consultas($consulta);
$registros = mysqli_num_rows($result);
echo "Total de Registros: " . $registros . "<br>";
while ($row = mysqli_fetch_array($result)) {
    $idreunion_zonal = $row['idreunion_zonal'];
    $reunion_zonal = $row['reunion_zonal'];
    $idreunion_granzona = $row['idreunion_granzona'];
    $reuGranZona = $row['reuGranZona'];
    $idestado = $row['idestado'];
    $estado = $row['estado'];
    ?>
    <tr class="even pointer">
        <td class="a-center "><input type="checkbox" class="flat" name="table_records" ></td>
        <td class=""><?php echo $idreunion_zonal; ?></td>
        <td class=""><?php echo $reunion_zonal; ?></td>
        <td class=""><?php echo $reuGranZona; ?></td>
        <td class=""><?php echo $estado; ?></td>
        <td class="last">

            <form method="GET" id="exclude_form_<?php echo $id ?>" action="">
                <div style="float: left;" title="Editar <?php echo $titulo; ?>"> 
                    <a href = "listRzon?idreunion_zonal=<?php echo $idreunion_zonal ?>" class = "btn btn-round btn-primary btn-sm " ><i class = "fa fa-pencil-square"></i></a>
                </div>
                <div style="float: left;" title="Eliminar <?php echo $titulo; ?>">
                    <button type="button" class="btn btn-round btn-danger btn-sm" onclick="eliminar(<?php echo "'quitar','$reunion_zonal','reunion_zonal','idreunion_zonal', $idreunion_zonal,'listRzon'" ?>);"  > 
                        <i class="fa fa-minus-square"></i> 
                    </button>
                </div>
            </form>
        </td>
    </tr>
<?php } ?>

<script>
    function eliminar(accion, nombre, table, atributo, id, pag)
    {
        if (confirm('Esta Apunto de eliminar a "' + nombre + '" de la tabla ' + table + ' \n¿Desea Continuar?'))
        {
            window.location.href = 'modificarB?accion=' + accion + '&table=' + table + '&atrib=' + atributo + '&id=' + id + '&pag=' + pag ;
        } else
        {
            alert('Se canceló ésta Operacion.');
        }

    }
</script>
</script>