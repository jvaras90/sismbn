<?php
// 5 pag * 7 regstros
$num_regis_tot = 35;
$num_regis_lim = $num_regis_tot - 1;
if ($registros >= $num_regis_tot) {
    $num_paginas = $registros / $num_regis_lim;
    $findme = '.';
    $pos = strpos($num_paginas, $findme);
    //si existe decimales
    if ($pos) {
        //echo "existe en la posición $pos <br>";
        $rest = substr($num_paginas, 0, $pos);
        //echo $rest . "<br>";
        $partes = $rest + 1;
    } else {
        $partes = $rest;
    }
} else {
    $partes = 1;
}
$constant = $num_regis_lim;
$lim_i = 0;
?>
<div style="margin: auto; text-align: center;">
    <strong >   Descargar en PDF :    &nbsp;</strong> 
    <?php
    for ($x = 1; $x <= $partes; $x++) {
        $limite = "$lim_i,$constant";
        $lim_i = $lim_i + $constant;
        $mod = $_GET['mod'];
        if ($mod == 'lPersonas') {
            $variables = "idgrupo=" . $_GET['idgrupo'] . "";
        } else if ($mod == 'lMinistros') {
            $variables = "idpais=" . $_GET['idpais'] . "&iddepartamento=" . $_GET['iddepartamento'] . "&idcargo=" . $_GET['idcargo'] . "";
        } else if ($mod == 'lReunZonal') {
            $variables = "idpais=" . $_GET['idpais'] . "&idreunion_granzona=" . $_GET['idreunion_granzona'] . "&idreunion_zonal=" . $_GET['idreunion_zonal'] . "";
        }
        ?> 
        <a href="convPDF?mod=<?php echo $_GET['mod']; ?>&<?php echo $variables; ?>&sexo=<?php echo $_GET['S']; ?>&limite=<?php echo $limite; ?>&parte=<?php echo $x; ?>&sql=<?php echo $cadena; ?>"  title="Descargar la <?php echo $x; ?>° Parte del listado" class="label btn-danger btn-sm " target="blank"><i class="fa fa-download"></i>&nbsp;&nbsp;Parte <?php echo $x; ?></a>&nbsp;&nbsp;&nbsp;&nbsp; 
    <?php } ?>
</div><br>