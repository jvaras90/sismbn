<?php
$criterio = " where '1'='1' ";

if (isset($_GET['S']) && $_GET['S'] != "") {
    $criterio .= " and p.sexo= '" . $_GET['S'] . "' ";
}
if (isset($_GET['idgrupo']) && $_GET['idgrupo'] != "99") {
    $criterio .= " and p.idgrupos = " . $_GET['idgrupo'] . "  ";
}
if ($_SESSION['id_nivel'] != 1) {
    $criterio .= " and p.idestado=1 ";
}
$consulta = "select p.idpersona,concat(nombres,' ',apaterno,' ',amaterno) as nombre,
p.idtipo_documento , td.descripcion documento,p.nro_documento,
sexo,telefono_fijo,celular,
lugar_nacimiento,p.fec_nac,email,
profesion,ocupacion,p.idcategoria, cat.descripcion categoria ,
p.iddistrito,dis.des_distrito distrito,direccion,referencia,p.foto,fecha_registro ,
YEAR(CURDATE())-YEAR(p.fec_nac) + IF(DATE_FORMAT(CURDATE(),'%m-%d') > DATE_FORMAT(p.fec_nac,'%m-%d'), 0, -1) edad 
from persona p 
inner join tipo_documento td on td.idtipo_documento = p.idtipo_documento
inner join categoria cat on cat.idcategoria = p.idcategoria
inner join distrito dis on dis.iddistrito = p.iddistrito
$criterio 
order by nombre
;";
//echo $consulta;
$result = $mysqlMBN->consultas($consulta);
$registros = mysqli_num_rows($result);
echo "Total de Registros: " . $registros . "<br>";
while ($row = mysqli_fetch_array($result)) {
    $idpersona = $row['idpersona'];
    $nombres = $row['nombre'];
    $foto = $row['foto'];
    $documento = $row['documento'];
    $nro_documento = $row['nro_documento'];
    $sexo = $row['sexo'];
    $edad = $row['edad'];
    $telefono_fijo = $row['telefono_fijo'];
    $celular = $row['celular'];
    $fec_nac = $row['fec_nac'];
    $categoria = $row['categoria'];
    $profesion = $row['profesion'];
    $ocupacion = $row['ocupacion'];
    $email = $row['email'];
    $referencia = $row['referencia'];
    $nacimiento = $row['lugar_nacimiento'];
    $distrito = $row['distrito'];
    $direccion = $row['direccion'];
    ?>
    <tr class="even pointer">

        <td>
            <div class="col-md-12 col-sm-12 col-xs-12 ">
                <div class="col-md-12 col-sm-12 col-xs-12 ">
                    <div class="col-md-3 col-sm-3 col-xs-3  text-center">
                        <img src="images/Fotos/<?php echo $foto; ?>" alt="" style="width: 150px;height: 150px;" class="img-circle img-responsive"> <br>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-9 ">
                        <table style="text-align:center; margin: auto" width="600" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td colspan="8"><h2 class="brief"><i><?php echo $nombres; ?></i></h2></td>
                                </tr>
                                <tr>
                                    <td><strong>Edad: </strong></td>
                                    <td><?php if($fec_nac == "" || $fec_nac =="0000-00-00") {echo "N/A";} else { echo $edad;} ?></td>
                                    <td><strong>Fecha Nac: </strong></td>
                                    <td><?php echo $fec_nac; ?></td>
                                    <td><strong><?php echo $documento ;?>: </strong></td>
                                    <td><?php  echo $nro_documento; ?></td>
                                    <td><strong>Sexo: </strong></td>
                                    <td><?php echo $sexo; ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Lugar Nac: </strong></td>
                                    <td><?php echo $nacimiento; ?></td>
                                    <td><strong>Condición: </strong></td>
                                    <td><?php echo $categoria; ?></td>
                                    <td><strong>Teléfono Fijo: </strong></td>
                                    <td><?php echo $telefono_fijo; ?></td>
                                    <td><strong>Celular: </strong></td>
                                    <td><?php echo $celular; ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Profesión: </strong></td>
                                    <td colspan="3"><?php echo $profesion; ?></td>
                                    <td><strong>Ocupación: </strong></td>
                                    <td colspan="3"><?php echo $ocupacion; ?></td>   
                                </tr>
                                <tr>
                                    <td><strong>Email: </strong></td>
                                    <td colspan="3"><?php echo $email; ?></td>
                                    <td><strong>Referencia: </strong></td>
                                    <td colspan="3"><?php echo $referencia; ?></td>   
                                </tr>
                                <tr>
                                    <td><strong>Distrito: </strong></td>
                                    <td colspan="3"><?php echo $distrito; ?></td>
                                    <td><strong>Direccion: </strong></td>
                                    <td colspan="3"><?php echo $direccion; ?></td>   
                                </tr>
                                <tr>
                                    <td colspan="8">
                                        <a href="persDetalle?mod=mPersonas&idpersona=<?php echo $idpersona ?>" class="btn btn-primary btn-sm"><i class="fa fa-user"></i> Ver Detalles</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </td>
    </tr>
<?php } ?>

