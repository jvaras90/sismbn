<?php
$idnegocio = $_GET['id_neg'];
if ($_GET['id_neg'] != 69) {
    $consulta = "select a.idanexo,a.anexo,a.callerid,a.idnegocio,n.descripcion negocio, a.idestado ,
        e.descripcion estado, n.idcontexto , c.descripcion contexto
from anexo a
inner join negocio n on n.idnegocio = a.idnegocio
inner join estado e on e.idestado = a.idestado
inner join contexto c on c.idcontexto = n.idcontexto
where a.idnegocio = $idnegocio
order by anexo ;
    ";
} else {
    $consulta = "select a.idanexo,a.anexo,a.callerid,a.idnegocio,n.descripcion negocio, a.idestado ,
        e.descripcion estado, n.idcontexto , c.descripcion contexto
from anexo a
inner join negocio n on n.idnegocio = a.idnegocio
inner join estado e on e.idestado = a.idestado
inner join contexto c on c.idcontexto = n.idcontexto
order by anexo ;";
}
$result = $mysqlLOG->consultas($consulta);
$registros = mysqli_num_rows($result);
echo "Total de Anexos: " . $registros . "<br>";
while ($row = mysqli_fetch_array($result)) {
    $idanexo = $row['idanexo'];
    $anexo = $row['anexo'];
    $callerid = $row['callerid'];
    $idnegocio = $row['idnegocio'];
    $negocio = $row['negocio'];
    $idestado = $row['idestado'];
    $estado = $row['estado'];
    $idcontexto = $row['idcontexto'];
    $contexto = $row['contexto'];
    ?> <tr class="even pointer">
        <td class=""><?php echo $anexo; ?></td>
        <td class=""><?php echo $callerid; ?></td>
        <td class=""><?php echo $contexto; ?></td>
        <td class="">   <?php echo $estado; ?>        </td>
        <td class="last">

            <form method="GET" id="exclude_form_<?php echo $id ?>" action="">
                <div style="float: left;" title="Editar Anexo"> 
                    <a href = "listaAnex2?int=1&anex=1&id_neg=<?php if ($_GET['id_neg'] != 69) {echo $idnegocio;} else {echo 69;}?>&idanexo=<?php echo $idanexo ?>" class = "btn btn-round btn-primary btn-sm " ><i class = "fa fa-pencil-square"></i></a>
                </div>
                <div style="float: left;" title="Eliminar Anexo">
                    <button type="button" class="btn btn-round btn-danger btn-sm" onclick="eliminar(<?php echo "'$anexo','quitar_anexo',$idanexo " ?>);"  > 
                        <i class="fa fa-minus-square"></i> 
                    </button>
                </div>
                <!--<a href="smsdetUser?idpersonal=<?php //echo $idPersonal                   ?>" class="btn btn-danger" style="width: 100%;"><i class="fa fa-minus-square"></i></a>-->
                <input type="hidden" value="<?php echo $idPersonal ?>"/>
            </form>
        </td>
    </tr>
<?php } ?>

<script>
    function eliminar(nombre, accion, idanexo)
    {
        if (confirm('Esta Apunto de eliminar el anexo : ' + nombre + ' de la lista \n¿Desea Continuar?'))
        {
            window.location.href = 'modificarB?accion=' + accion + '&idanexo=' + idanexo + '&id_neg=<?php echo $_GET['id_neg']; ?>';
        }
        else
        {
            alert('Se canceló ésta Operacion.');
        }

    }
</script>