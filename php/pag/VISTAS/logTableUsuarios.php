<?php
$consulta = "select idusuario, u.idpersona, 
 concat (p.nombres, ' ', p.apaterno,' ', p.amaterno) nombre, p.nro_documento,
 u.user, u.pass2,u.idtipo_usuario,tu.descripcion tipo_usuario, u.idestado , e.descripcion estado
 from usuario u 
 inner join persona p on p.idpersona = u.idpersona
 inner join tipo_usuario tu on tu.idtipo_usuario = u.idtipo_usuario
 inner join estado e on e.idestado = u.idestado
 ;";
$result = $mysqlMBN->consultas($consulta);
$registros = mysqli_num_rows($result);
echo "Total de Anexos: " . $registros . "<br>";
while ($row = mysqli_fetch_array($result)) {
    $idusuario = $row['idusuario'];
    $idpersona = $row['idpersona'];
    $nombre = $row['nombre'];
    $nro_documento = $row['nro_documento'];
    $user = $row['user'];
    $pass2 = $row['pass2'];
    $idtipo_usuario = $row['idtipo_usuario'];
    $tipo_usuario = $row['tipo_usuario'];
    $idestado = $row['idestado'];
    $estado = $row['estado'];
    ?>
    <tr class="even pointer">
        <td class="a-center "><input type="checkbox" class="flat" name="table_records" ></td>
        <td class=""><?php echo $idusuario; ?></td>
        <td class=""><?php echo $nombre; ?></td>
        <td class=""><?php echo $nro_documento; ?></td>
        <td class=""><?php echo $user; ?></td>
        <?php if ($_SESSION['id_nivel'] == 1) { ?>
            <td class=""><?php echo $pass2; ?></td>
        <?php } ?>
        <td class=""><?php echo $tipo_usuario; ?></td>
        <td class=""><?php echo $estado; ?></td>
        <td class="last">

            <form method="GET" id="exclude_form_<?php echo $id ?>" action="">
                <div style="float: left;" title="Editar <?php echo $titulo; ?>"> 
                    <a href = "userLista?mod=mUsuario&idusuario=<?php echo $idusuario ?>" class = "btn btn-round btn-primary btn-sm " ><i class = "fa fa-pencil-square"></i></a>
                </div>
                <div style="float: left;" title="Eliminar <?php echo $titulo; ?>">
                    <button type="button" class="btn btn-round btn-danger btn-sm" onclick="eliminar(<?php echo "'quitar','$user','usuario','idusuario', $idusuario,'userLista'" ?>);"  > 
                        <i class="fa fa-minus-square"></i> 
                    </button>
                </div>
            </form>
        </td>
    </tr>
<?php } ?>

<script>
    function eliminar(accion, nombre, table, atributo, id, pag)
    {
        if (confirm('Esta Apunto de eliminar a "' + nombre + '" de la tabla ' + table + ' \n¿Desea Continuar?'))
        {
            window.location.href = 'modificarB?accion=' + accion + '&table=' + table + '&atrib=' + atributo + '&id=' + id + '&pag=' + pag;
        } else
        {
            alert('Se canceló ésta Operacion.');
        }

    }
</script>