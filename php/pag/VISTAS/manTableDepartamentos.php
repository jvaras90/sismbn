<?php
$consulta = "select *,e.descripcion estado from departamento c 
inner join estado e on e.idestado = c.idestado where idpais=$idpais order by des_departamento;";
//echo $consulta;
$result = $mysqlMBN->consultas($consulta);
$registros = mysqli_num_rows($result);
echo "Total de Anexos: " . $registros . "<br>";
while ($row = mysqli_fetch_array($result)) {
    $iddepartamento = $row['iddepartamento'];
    $des_departamento = $row['des_departamento'];
    $idestado = $row['idestado'];
    $estado = $row['estado'];
    ?>
    <tr class="even pointer">
        <td class="a-center "><input type="checkbox" class="flat" name="table_records" ></td>
        <td class=""><?php echo $iddepartamento; ?></td>
        <td class=""><?php echo $des_departamento; ?></td>
        <td class=""><?php echo $estado; ?></td>
        <td class="last">

            <form method="GET" id="exclude_form_<?php echo $id ?>" action="">
                <div style="float: left;" title="Editar <?php echo $titulo; ?>"> 
                    <a href = "listDepa?idpais=<?php echo $idpais;?>&iddepartamento=<?php echo $iddepartamento ?>" class = "btn btn-round btn-primary btn-sm " ><i class = "fa fa-pencil-square"></i></a>
                </div>
                <div style="float: left;" title="Eliminar <?php echo $titulo; ?>">
                    <button type="button" class="btn btn-round btn-danger btn-sm" onclick="eliminar(<?php echo "'quitar','$des_departamento','departamento','iddepartamento', $iddepartamento,'listDepa'" ?>);"  > 
                        <i class="fa fa-minus-square"></i> 
                    </button>
                </div>
            </form>
        </td>
    </tr>
<?php } ?>

<script>
    function eliminar(accion, nombre, table, atributo, id, pag)
    {
        if (confirm('Esta Apunto de eliminar a "' + nombre + '" de la tabla ' + table + ' \n¿Desea Continuar?'))
        {
            window.location.href = 'modificarB?accion=' + accion + '&table=' + table + '&atrib=' + atributo + '&id=' + id + '&pag=' + pag;
        } else
        {
            alert('Se canceló ésta Operacion.');
        }

    }
</script>