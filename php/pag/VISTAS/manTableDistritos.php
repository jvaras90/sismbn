<?php
$consulta = "select *,e.descripcion estado from distrito c 
inner join estado e on e.idestado = c.idestado where idprovincia=$idprovincia order by des_distrito;";
$result = $mysqlMBN->consultas($consulta);
$registros = mysqli_num_rows($result);
echo "Total de Registros: " . $registros . "<br>";
while ($row = mysqli_fetch_array($result)) {
    $iddistrito = $row['iddistrito'];
    $des_distrito = $row['des_distrito'];
    $idestado = $row['idestado'];
    $estado = $row['estado'];
    ?>
    <tr class="even pointer">
        <td class="a-center "><input type="checkbox" class="flat" name="table_records" ></td>
        <td class=""><?php echo $iddistrito; ?></td>
        <td class=""><?php echo $des_distrito; ?></td>
        <td class=""><?php echo $estado; ?></td>
        <td class="last">

            <form method="GET" id="exclude_form_<?php echo $id ?>" action="">
                <div style="float: left;" title="Editar <?php echo $titulo; ?>"> 
                    <a href = "listDist?idpais=<?php echo $_GET['idpais']; ?>&iddepartamento=<?php echo $_GET['iddepartamento']; ?>&idprovincia=<?php echo $_GET['idprovincia']; ?>&iddistrito=<?php echo $iddistrito ?>" class = "btn btn-round btn-primary btn-sm " ><i class = "fa fa-pencil-square"></i></a>
                </div>
                <div style="float: left;" title="Eliminar <?php echo $titulo; ?>">
                    <button type="button" class="btn btn-round btn-danger btn-sm" onclick="eliminar(<?php echo "'quitar','$des_distrito','distrito','iddistrito', $iddistrito,'listDist'" ?>);"  > 
                        <i class="fa fa-minus-square"></i> 
                    </button>
                </div>
            </form>
        </td>
    </tr>
<?php } ?>

<script>
    function eliminar(accion, nombre, table, atributo, id, pag)
    {
        if (confirm('Esta Apunto de eliminar a "' + nombre + '" de la tabla ' + table + ' \n¿Desea Continuar?'))
        {
            window.location.href = 'modificarB?accion=' + accion + '&table=' + table + '&atrib=' + atributo + '&id=' + id + '&pag=' + pag + '&idprovincia=' + <?php echo $_GET['idprovincia']; ?>;
        } else
        {
            alert('Se canceló ésta Operacion.');
        }

    }
</script>