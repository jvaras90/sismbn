
<!-- ############################################ MODAL CAMBIAR ESTADO ############################################ -->
<div class="modal fade" id="modalCambiarEstado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <script type="text/javascript">
        function quitarDis() {
            var motivo = $('#motivo').val();
            var idestado = $('#idestado2').val();
            if (motivo.length > 4 && idestado != 0) {
                $('#cambiarEstado').removeAttr('disabled');
            }
        }
        function agregarDis() {
            var motivo = $('#motivo').val();
            var idestado = $('#idestado2').val();
            if (motivo.length < 4 || idestado == 0) {
                $('#cambiarEstado').attr('disabled', 'disabled');
            }
        }
        function valSelectCambio() {
            var motivo = $('#motivo').val();
            var idestado = $('#idestado2').val();
            if (motivo.length < 4 || idestado == 0) {
                $('#cambiarEstado').attr('disabled', 'disabled');
            } else if (motivo.length > 4 && idestado != 0) {
                $('#cambiarEstado').removeAttr('disabled');
            }
        }
        function mostrar(id) {
            $(document).ready(function () {
                $("#result").hide("slow");
                $("#cargar_reporte").show("slow");
                $("#editar_resul").load("php/pag/VISTAS/modalCambioEstado.php?mod=<?php echo $_GET['mod']; ?>&idpersona=" + id, "&idestado=0", function () {
                    $("#editar_resul").show("slow");
                    $("#cargar_reporte").hide("slow");
                });
            });
        }
    </script> 
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" role="form" action="modificarB" method="post" name="formCambiarEstado" id="formCambiarEstado">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style="text-align: center;">Modificar</h4>
                </div>
                <div class="modal-body">
                    <center>
                        <div id="cargar_reporte" > 
                            <div class="a-center">
                                <img src="img/loading.gif"/> 
                            </div>
                            <label>Espere!!! Cargando datos...</label>
                        </div>
                    </center>
                    <div class="panel-body" id="editar_resul" >
                    </div> 
                </div>
                <div class="clearfix"></div>                                                            
                <div class="modal-footer">
                    <div style="float: right;">
                        <button type="submit" id="cambiarEstado" name="cambiarEstado" class="btn btn-primary" disabled form="formCambiarEstado" >Guardar</button>
                    </div>
                    <div style="float: right;">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    </div>
                    <input type="hidden" name="mod" value="<?php echo $_GET['mod']; ?>"/>
                    <input type="hidden" name="idpais" value="<?php echo $_GET['idpais']; ?>"/>
                    <input type="hidden" name="idsede" value="<?php echo $_GET['idsede']; ?>"/>
                    <input type="hidden" name="idgrupo" value="<?php echo $_GET['idgrupo']; ?>"/>
                    <input type="hidden" name="idcategoria" value="<?php echo $_GET['idcategoria']; ?>"/>
                    <input type="hidden" name="S" value="<?php echo $_GET['S']; ?>"/>
                    <input type="hidden" name="form" value="formCambiarEstado"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- ############################################ MODAL NUEVOS CARGOS ############################################ -->
<div class="modal fade" id="modalNuevoCargos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" role="form" action="registrarA" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Nuevo <?php echo $titulo; ?></h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Cargos</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span></span>
                                <input type="text"  name="descripcion" class="form-control" placeholder="Descripción" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <div style="float: right;">
                        <button type="submit" id="nuevo<?php echo $hoja; ?>" name="nuevo<?php echo $hoja; ?>" class="btn btn-primary">Guardar</button>
                    </div>
                    <div style="float: right;">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    </div>
                    <input type="hidden" name="form" value="nuevoCargos"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- ############################################ MODAL NUEVOS CATEGORIAS ############################################ -->
<div class="modal fade" id="modalNuevoCategorias" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" role="form" action="registrarA" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Nueva <?php echo $titulo; ?></h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Condición</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span></span>
                                <input type="text"  name="descripcion" class="form-control" placeholder="Descripción" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <div style="float: right;">
                        <button type="submit" id="nuevo<?php echo $hoja; ?>" name="nuevo<?php echo $hoja; ?>" class="btn btn-primary">Guardar</button>
                    </div>
                    <div style="float: right;">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    </div>
                    <input type="hidden" name="form" value="nuevoCategorias"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- ############################################ MODAL NUEVOS tIPOS DE USUSARIOS ############################################ -->
<div class="modal fade" id="modalNuevotipUsuarios" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" role="form" action="registrarA" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Nuevo <?php echo $titulo; ?></h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Tipo de Usuario</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span></span>
                                <input type="text"  name="descripcion" class="form-control" placeholder="Descripción" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <div style="float: right;">
                        <button type="submit" id="nuevo<?php echo $hoja; ?>" name="nuevo<?php echo $hoja; ?>" class="btn btn-primary">Guardar</button>
                    </div>
                    <div style="float: right;">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    </div>
                    <input type="hidden" name="form" value="nuevotipUsuarios"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- ############################################ MODAL NUEVOS REUNION ZONAL  ############################################ -->
<div class="modal fade" id="modalNuevoreuZonal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" role="form" action="registrarA" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Nuevo <?php echo $titulo; ?></h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <label>Gran Zona</label>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span>
                                <select class='form-control' name='idreunion_granzona2' id='idreunion_granzona2' required>
                                    <option>Seleccione</option>
                                    <?php include '../../system/selectReunionGranZona.php'; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Reunion Zonal</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span></span>
                                <input type="text"  name="descripcion" class="form-control" placeholder="Descripción" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <div style="float: right;">
                        <button type="submit" id="nuevo<?php echo $hoja; ?>" name="nuevo<?php echo $hoja; ?>" class="btn btn-primary">Guardar</button>
                    </div>
                    <div style="float: right;">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    </div>
                    <input type="hidden" name="form" value="nuevoreuZonal"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- ############################################ MODAL NUEVOS REUNIONES GRAN ZONA ############################################ -->
<div class="modal fade" id="modalNuevoreuGranZon" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" role="form" action="registrarA" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Nuevo <?php echo $titulo; ?></h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Gran Zona</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span></span>
                                <input type="text"  name="descripcion" class="form-control" placeholder="Descripción" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <div style="float: right;">
                        <button type="submit" id="nuevo<?php echo $hoja; ?>" name="nuevo<?php echo $hoja; ?>" class="btn btn-primary">Guardar</button>
                    </div>
                    <div style="float: right;">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    </div>
                    <input type="hidden" name="form" value="nuevoreuGranZon"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- ############################################ MODAL NUEVOS PROVINCIA ############################################ -->
<div class="modal fade" id="modalNuevoProvincias" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" role="form" action="registrarA" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Nueva Provincia</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <label>Pais</label>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span>
                                <select class='form-control' name='idpais2' id='idpais2' onchange="load_departamento1(this.value)" >
                                    <option value="0">Seleccione</option>
                                    <?php include '../../system/selectPais.php'; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Departamento</label>
                        <div class="form-group">
                            <div class="input-group" id="myDivDep1">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span>
                                <select class='form-control' name='iddepartamento2' id='iddepartamento2' required>
                                    <option value="0">Seleccione</option>
                                    <?php include '../../system/selectDepartamento.php'; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Provincia</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span></span>
                                <input type="text"  name="descripcion" class="form-control" placeholder="Descripción" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <div style="float: right;">
                        <button type="submit" id="nuevo<?php echo $hoja; ?>" name="nuevo<?php echo $hoja; ?>" class="btn btn-primary">Guardar</button>
                    </div>
                    <div style="float: right;">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    </div>
                    <input type="hidden" name="form" value="nuevoProvincias"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- ############################################ MODAL NUEVOS GRUPOS ############################################ -->
<div class="modal fade" id="modalNuevoGrupos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" role="form" action="registrarA" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Nuevo <?php echo $titulo; ?></h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Grupo</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span></span>
                                <input type="text"  name="descripcion" class="form-control" placeholder="Descripción" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <div style="float: right;">
                        <button type="submit" id="nuevoGrupos" name="nuevoGrupos" class="btn btn-primary">Guardar</button>
                    </div>
                    <div style="float: right;">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    </div>
                    <input type="hidden" name="form" value="nuevoGrupos"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- ############################################ MODAL NUEVOS ESTADOS ############################################ -->
<div class="modal fade" id="modalNuevoEstados" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" role="form" action="registrarA" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Nuevo <?php echo $titulo; ?></h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Estado</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span></span>
                                <input type="text"  name="descripcion" class="form-control" placeholder="Descripción" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <div style="float: right;">
                        <button type="submit" id="nuevo<?php echo $hoja; ?>" name="nuevo<?php echo $hoja; ?>" class="btn btn-primary">Guardar</button>
                    </div>
                    <div style="float: right;">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    </div>
                    <input type="hidden" name="form" value="nuevo<?php echo $hoja; ?>"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- ############################################ MODAL NUEVOS Distritos ############################################ -->
<div class="modal fade" id="modalNuevoDistritos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" role="form" action="registrarA" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Nuevo <?php echo $titulo; ?></h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <label>Pais</label>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span>
                                <select class='form-control' name='idpais2' id='idpais2' onchange="load_departamento2(this.value)" >
                                    <option value="0">Seleccione</option>
                                    <?php include '../../system/selectPais.php'; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Departamento</label>
                        <div class="form-group">
                            <div class="input-group" id="myDivDep2">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span>
                                <select class='form-control' name='iddepartamento2' id='iddepartamento2' onchange="load_provincia2(this.value)" >
                                    <option value="0">Seleccione</option>
                                    <?php include '../../system/selectDepartamento.php'; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Provincia</label>
                        <div class="form-group">
                            <div class="input-group" id="myDivProv2">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span>
                                <select class='form-control' name='idprovincia2' id='idprovincia2' required>
                                    <option value="0">Seleccione</option>
                                    <?php include '../../system/selectProvincia.php'; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Distrito</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span></span>
                                <input type="text"  name="descripcion" class="form-control" placeholder="Descripción" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <div style="float: right;">
                        <button type="submit" id="nuevo<?php echo $hoja; ?>" name="nuevo<?php echo $hoja; ?>" class="btn btn-primary">Guardar</button>
                    </div>
                    <div style="float: right;">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    </div>
                    <input type="hidden" name="form" value="nuevoDistritos"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- ############################################ MODAL NUEVOS Departamentos ############################################ -->
<div class="modal fade" id="modalNuevoDepartamentos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" role="form" action="registrarA" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Nuevo <?php echo $titulo; ?></h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <label>Pais</label>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span>
                                <select class='form-control' name='idpais2' id='idpais2' required>
                                    <option value="0">Seleccione</option>
                                    <?php include '../../system/selectPais.php'; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Departamento</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span></span>
                                <input type="text"  name="descripcion" class="form-control" placeholder="Descripción" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <div style="float: right;">
                        <button type="submit" id="nuevo<?php echo $hoja; ?>" name="nuevo<?php echo $hoja; ?>" class="btn btn-primary">Guardar</button>
                    </div>
                    <div style="float: right;">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    </div>
                    <input type="hidden" name="form" value="NuevoDepartamentos"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- ############################################ MODAL NUEVOS ESTADO CIVIL ############################################ -->
<div class="modal fade" id="modalNuevoEstadoCivil" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" role="form" action="registrarA" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Nuevo Estado Civil</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Estado Civil</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span></span>
                                <input type="text"  name="descripcion" class="form-control" placeholder="Descripción" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <div style="float: right;">
                        <button type="submit" id="nuevo<?php echo $hoja; ?>" name="nuevoEstadoCivil" class="btn btn-primary">Guardar</button>
                    </div>
                    <div style="float: right;">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    </div>
                    <input type="hidden" name="form" value="nuevoEstadoCivil"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- ############################################ MODAL NUEVOS TIPO DOCUMENTO ############################################ -->
<div class="modal fade" id="modalNuevoTipoDocumento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" role="form" action="registrarA" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Nuevo Tipo Documento</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Tipo Documento</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span></span>
                                <input type="text"  name="descripcion" class="form-control" placeholder="Descripción" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <div style="float: right;">
                        <button type="submit" id="nuevo<?php echo $hoja; ?>" name="nuevoEstadoCivil" class="btn btn-primary">Guardar</button>
                    </div>
                    <div style="float: right;">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    </div>
                    <input type="hidden" name="form" value="nuevoTipoDocumento"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- ############################################ MODAL Cambiar Condicion ############################################ -->
<div class="modal fade" id="cambiarCondicion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"> 
            <form class="form-horizontal" role="form" action="registrarA" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Datos de Miembro <?php echo $titulo; ?></h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12 col-sm-12 col-xs-12"> 
                        <div class="item form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-2">Fec. Salvación<?php //echo $iddepartamento;                                                                                                                                                            ?></label>
                            <div class="col-md-4 col-sm-4 col-xs-4" id="aPaterno">
                                <!--<input name="fecha_salvacion" id="fecha_salvacion" class="form-control col-md-3 col-xs-12" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="off" type="text"  value="<?php echo $fecha_salvacion; ?>" readonly="">-->
                                <input  type="text" id="fecha_salvacion1"  name="fecha_salvacion1"  class="form-control" data-inputmask="'mask': '9999-99-99'" value="<?php echo $fecha_salvacion; ?>">
                            </div>
                            <label class="control-label col-md-2 col-sm-2 col-xs-2">Predicó  <?php //echo $iddepartamento;                                                                                                                                                            ?></label>
                            <div class="col-md-4 col-sm-4 col-xs-4" id="aPaterno">
                                <input  name="s_referencia" id="s_referencia" class="form-control col-md-3 col-xs-12" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="off" type="text"  value="<?php echo $referencia; ?>">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-2">Gran Zona </label>
                            <div class="col-md-3 col-sm-3 col-xs-3" id="myDivProv12">
                                <select <?php
                                if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                    echo "disabled";
                                }
                                ?> class='form-control' name='idreunion_granzona' id='idreunion_granzona' onchange="load_reunion_zonal(this.value)" >
                                    <option>Seleccione</option>
                                    <?php include "../../system/selectReunionGranZona.php"; ?>
                                </select>
                            </div>
                            <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nueva Gran Zona">
                                <a href="#modalNuevoreuGranZon" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                            </div>
                            <label class="control-label col-md-2 col-sm-2 col-xs-2">Reunion Zonal </label>
                            <div class="col-md-3 col-sm-3 col-xs-3" id="myDivReuZon"  title="Asignado para las reuniones Zonales">
                                <select <?php
                                if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                    echo "disabled";
                                }
                                ?> class='form-control' name='idreunion_zonal' id='idreunion_zonal'>
                                    <?php include "../../system/selectReunionZonal.php"; ?>
                                </select>
                            </div>
                            <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nueva Reunion Zonal">
                                <a href="#modalNuevoreuZonal" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                            </div>
                        </div>  
                        <div id="selectMultiple"  class="item form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-2" title="Cargo del miembro">Cargo (s) </label>
                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <select  class="select2_multiple  form-control" multiple="multiple" name='idcargoM[]' id='idcargoM[]'>
                                    <option value="11" selected="">Hermano(a)</option>
                                    <?php include '../../system/selectCargo.php' ?>   
                                </select>
                            </div>                                                                     
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <div style="float: right;">
                        <button type="submit" id="nuevo<?php echo $hoja; ?>" name="nuevo<?php echo $hoja; ?>" class="btn btn-primary">Guardar</button>
                    </div>
                    <div style="float: right;">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    </div>
                    <input type="hidden" name="form" value="cambiarCondicion"/>
                    <input type="hidden" name="idpersona" value="<?php echo $_GET['idpersona']; ?>"/>
                </div> 
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!--     ############################################ MODAL NUEVO PERSONA ############################################ -->

<div class="modal fade" id="modalNuevaPersona" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" role="form" action="agndNewPers" method="get">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Nueva Persona</h4>
                </div>
                <div class="modal-body">                        
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <label>Condición</label>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="hidden" name="mod" value="nPersona"/>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-heart"></span></span>
                                <select class='form-control' name='idcategoria' id='idcategoria'  >
                                    <?php include "../../system/selectCategoria.php"; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    <button type="submit" id="nuevoAgente" name="nuevoAgente" class="btn btn-primary">Siguiente >> </button>
                </div>
            </form>
        </div> <!--/.modal-content -->
    </div> <!--/.modal-dialog -->
</div> <!--/.modal -->