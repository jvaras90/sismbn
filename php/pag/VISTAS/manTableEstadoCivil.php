<?php 
$consulta = "select idestado_civil,c.descripcion estadoCivil,c.idestado,e.descripcion estado from estado_civil c 
inner join estado e on e.idestado = c.idestado order by c.descripcion;";
$result = $mysqlMBN->consultas($consulta);
$registros = mysqli_num_rows($result);
echo "Total de Registros: " . $registros . "<br>";
while ($row = mysqli_fetch_array($result)) {
    $idestado_civil= $row['idestado_civil'];
    $estadoCivil = $row['estadoCivil'];
    $idestado = $row['idestado'];
    $estado = $row['estado'];
?>
<tr class="even pointer">
    <td class="a-center "><input type="checkbox" class="flat" name="table_records" ></td>
    <td class=""><?php echo $idestado_civil; ?></td>
    <td class=""><?php echo $estadoCivil; ?></td>
    <td class=""><?php echo $estado; ?></td>
    <td class="last">

        <form method="GET" id="exclude_form_<?php echo $id ?>" action="">
            <div style="float: left;" title="Editar <?php echo $titulo;?>"> 
                <a href = "listEsCi?idestado_civil=<?php echo $idestado_civil ?>" class = "btn btn-round btn-primary btn-sm " ><i class = "fa fa-pencil-square"></i></a>
            </div>
            <div style="float: left;" title="Eliminar <?php echo $titulo; ?>">
                    <button type="button" class="btn btn-round btn-danger btn-sm" onclick="eliminar(<?php echo "'quitar','$estadoCivil','estado_civil','idestado_civil', $idestado_civil,'listEsCi'" ?>);"  > 
                        <i class="fa fa-minus-square"></i> 
                    </button>
                </div>
            </form>
        </td>
    </tr>
<?php } ?>

<script>
    function eliminar(accion, nombre, table, atributo, id, pag)
    {
        if (confirm('Esta Apunto de eliminar a "' + nombre + '" de la tabla ' + table + ' \n¿Desea Continuar?'))
        {
            window.location.href = 'modificarB?accion=' + accion + '&table=' + table + '&atrib=' + atributo + '&id=' + id + '&pag=' + pag;
        } else
        {
            alert('Se canceló ésta Operacion.');
        }

    }
</script>