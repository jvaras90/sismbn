<?php
$consulta = "select * from estado order by 1";
$result = $mysqlMBN->consultas($consulta);
$registros = mysqli_num_rows($result);
echo "Total de Registros: " . $registros . "<br>";
while ($row = mysqli_fetch_array($result)) {
    $idestado = $row['idestado'];
    $descripcion = $row['descripcion'];
    ?>
    <tr class="even pointer">
        <td class="a-center "><input type="checkbox" class="flat" name="table_records" ></td>
        <td class=""><?php echo $idestado; ?></td>
        <td class=""><?php echo $descripcion; ?></td>
        <td class="last">
            <form method="GET" id="exclude_form_<?php echo $id ?>" action="">
                <div style="float: left;" title="Editar <?php echo $titulo; ?>"> 
                    <a href = "listEsta?idestado=<?php echo $idestado ?>" class = "btn btn-round btn-primary btn-sm " ><i class = "fa fa-pencil-square"></i></a>
                </div>
                <div style="float: left;" title="Eliminar <?php echo $titulo; ?>">
                    <button type="button" class="btn btn-round btn-danger btn-sm" onclick="eliminar(<?php echo "'quitar','$descripcion','estado','idestado', $idestado,'listEsta'" ?>);"  > 
                        <i class="fa fa-minus-square"></i> 
                    </button>
                </div>
            </form>
        </td>
    </tr>
<?php } ?>

<script>
    function eliminar(accion, nombre, table, atributo, id, pag)
    {
        if (confirm('Esta Apunto de eliminar a "' + nombre + '" de la tabla ' + table + ' \n¿Desea Continuar?'))
        {
            window.location.href = 'modificarB?accion=' + accion + '&table=' + table + '&atrib=' + atributo + '&id=' + id + '&pag=' + pag;
        } else
        {
            alert('Se canceló ésta Operacion.');
        }

    }
</script>