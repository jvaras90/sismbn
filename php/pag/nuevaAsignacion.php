<?php
require('../../include/header.php');
include '../../system/mensajesAlerta.php';
include_once '../../system/crearConexion.php';
clearstatcache();
$idgrupo_camp = $_GET['idgrupo_camp'];
$idgrupo_cabe = $_GET['idgrupo_cabe'];
$sexo = $_GET['S'];
$accion = $_GET['accion']
?>
<script src="js/ajax.js" type="text/javascript"></script>
<!-- switchery -->
<link rel="stylesheet" href="css/switchery/switchery.min.css" />
<script type="text/javascript">
    $(document).ready(function () {
        var lista1 = document.getElementById("S");
        var lista = document.getElementById("idpersona");
        $('#idpersona').change(function () {
<?php include '../../system/datos_autoload_personal.php' ?>;
            window.location = "newInsc?mod=nInscripcion&accion=<?php echo $accion; ?>&idpais=<?php echo $idpais; ?>&S=" + lista1.value + "&idpersona=" + lista.value;
        });
        var lista2 = document.getElementById("idgrupo");
        //cargarGrupo(lista2.value);
        var idgrupo_camp = $('#idgrupo_camp').val();
        //load_numeroGrupo(idgrupo_camp);
    });

</script>

<!-- page content -->
<div class="right_col" <?php echo $newDimensionBody; ?> role="main">
    <div class="page-title">

    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 col-sm-12 col-xs-12">

        <div class="x_panel">
            <div style="text-align: center">
                <h3><i>Asignación de Grupos</i></h3>
                <!--<h3><b>ENCUENTRO INTERNACIONAL PARA LOS JÓVENES 2017</b></h3>-->             
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 col-sm-12 col-xs-12">
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <form  class="form-horizontal form-label-left" method="get" action="newAsig" id="actualiza">
                    <div class="col-md-1 col-sm-1 col-xs-1 form-group text-center">                            
                        <input type="hidden" name="mod" value="<?php echo $_GET['mod']; ?>"/>
                        <input type="hidden" name="accion" value="buscar"/>
                    </div>
                    <table border="0" style="background-color: white;text-align: center;margin: auto">
                        <tr style="height: 2px;">
                            <td colspan="1"  style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong>Grupo</strong></td>
                            <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
                            <td colspan="1"  style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong>Clase</strong></td>
                            <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
<!--                            <td colspan="2"  style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong>Sexo</strong></td>
                            <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>-->
                            <td colspan="1" rowspan="2" style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong> <input class="btn btn-danger" type="submit" value="Buscar"></strong></td>
                            <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
                            <td colspan="1" rowspan="2" style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong><a href="#modalNuevaPersona" class="btn btn-success" data-toggle="modal"><i class="fa fa-user"></i> Nuevo</a></strong></td>
                            <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
                        </tr>
                        <tr>
                            <td style="background-color: #2774e6; padding: 5px; margin: 1px;">

                                <select name="idgrupo_camp" id="idgrupo_camp" class="form-control" onchange="load_numeroGrupo(this.value)" onload="load_numeroGrupo(this.value)">
                                    <option value="0">Seleccione</option>                                         
                                    <?php include '../../system/selectGrupoCamp.php'; ?>
                                </select> 
                            </td>
                            <td colspan="1"  style="text-align: center;padding: 1px; margin: 1px;"><strong>&nbsp;</strong></td>
                            <td style="background-color: #2774e6; padding: 5px; margin: 1px;" >
                                <div id="myDivDep1">
                                    <select name="idgrupo_cab" id="idgrupo_cab" class="form-control" >
                                        <option value="0">Seleccione</option>
                                        <?php if(isset($_GET['accion'])) {include '../../system/selectNumeroGrup.php';} ?>
                                    </select> 
                                </div>
                            </td>
                            <td colspan="1"  style="text-align: center;padding: 1px; margin: 1px;"><strong>&nbsp;</strong></td>
<!--                            <td style="text-align: center;background-color: white; padding: 5px; margin: 1px;">
                                <div class="radio-inline" title="Varón">
                                    <label class="">
                                        <input class="flat" type="radio" <?php
                                        if ($_GET['S'] == "M") {
                                            echo "checked";
                                        }
                                        ?> value="M" id="S" name="S"> M
                                    </label>
                                </div>
                            </td>
                            <td style="text-align: center;background-color: white; padding: 5px; margin: 1px;">
                                <div class="radio-inline" title="Mujer">
                                    <label>
                                        <input class="flat" type="radio" <?php
                                        if ($_GET['S'] == "F") {
                                            echo "checked";
                                        }
                                        ?> value="F" id="S" name="S"> F
                                    </label>
                                </div>
                            </td>-->
                            <td colspan="1"  style="text-align: center;padding: 1px; margin: 1px;"><strong>&nbsp;</strong></td>
                        </tr>
                    </table>
                </form>
            </div>
            <div class="clearfix"></div>
            <div class="x_content">


                <?php
                ?>
                <form class="form-horizontal form-label-left input_mask" novalidate action="registrarA" method="POST">  
                    <div class="clearfix"></div>
                    <div class="ln_solid"></div>
                    <div style="text-align: center;">
                        <h2><i><u>Información del Grupo</u></i></h2>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Nombres del participante">
                        <div class="item form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group text-center" title="Listado de personas, según el filtrado realizado">
                                <label class="control-label">Maestro </label>
                                <select class='select2_single form-control has-feedback-left' tabindex="-1" name='idmaestro' id='idmaestro' >
                                    <?php
                                    if ($accion == "buscar") {
                                        include_once '../../system/buscarMaestros.php';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Nombres del participante">
                        <div class="item form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group text-center" title="Listado de personas, según el filtrado realizado">
                                <label class="control-label">Capitán </label>
                                <select class='select2_single form-control has-feedback-left' tabindex="-1" name='idcapitan' id='idcapitan' >
                                    <?php
                                    if ($accion == "buscar") {
                                        include_once '../../system/buscarCapitan.php';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>                 
                    <div id="selectMultiple"  class="col-md-12 col-sm-6 col-xs-6 form-group text-center" title="Seleccione los cargos si es que tiene, de lo contrario dejar por defecto la opcion de participante">
                        <label class="control-label">Miembros del Grupo </label>
                        <select class="select2_multiple form-control" multiple="multiple" name='idPersonas[]' id='idPersonas[]'>
                            <!--<option value="17" selected>Participante</option>-->
                            <?php include '../../system/selectParticipantes.php' ?>   
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <div style="text-align: center;">
                        <div class="panel-footer">
                            <a title='Presione "Cancelar" para realizar una nueva busqueda' href="newInsc?mod=nInscripcion&idpais=<?php echo $idpais; ?>&S=<?php echo $sexo; ?>&idpersona=0" class="btn btn-danger"><i class="fa fa-rotate-left"></i> Regresar</a>
                            <input title='Presione "Registrar" para guardar los datos' type="button" value="Registrar" id="actualizarPersonal" name="nuevaInscripcion" class="btn btn-primary" onclick="this.disabled = true;
                                    this.value = 'Registrando...';
                                    this.form.submit()"/>
                            <input type="hidden" name="form" value="nuevaInscripcion"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php
//include_once '../VISTAS/listaModals.php';
    ?>
    <!-- ############################################ MODAL NUEVO AGENTE ############################################ -->

    <div class="modal fade" id="modalNuevaPersona" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="form-horizontal" role="form" action="persNews" method="get">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Nueva Persona</h4>
                    </div>
                    <div class="modal-body">                        
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Condición</label>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-heart"></span></span>
                                    <select class='form-control' name='idcategoria' id='idcategoria'>
                                        <?php include "../../system/selectCategoria.php"; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        <button type="submit" id="nuevoAgente" name="nuevoAgente" class="btn btn-primary">Siguiente >> </button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="clearfix"></div>

    <script>
        function agregarBoleta() {
            //capturamos los valores
            var nro_boleta = $('#nro_boleta').val();
            var monto_soles = $('#monto_soles').val();
            var monto_dolares = $('#monto_dolares').val();

            var cat = document.getElementById("table_boleta").rows.length;
            if (nro_boleta.length >= 3) {
                $('#table_boleta').append('<tr id="' + cat + '" class="impreso"><td><input type="hidden" name="nroBoleta' + nro_boleta + '" value="' + nro_boleta + '"/>' + nro_boleta + '</td><td><input type="hidden" name="montoSoles' + nro_boleta + '" value="' + monto_soles + '"/>' + monto_soles + '</td><td><input type="hidden" name="montoDolares' + monto_dolares + '" value="' + monto_dolares + '"/>' + monto_dolares + '</td><td><button type="button" class="btn btn-danger" onclick="eliminarAgente(' + cat + ');"><i class="fa fa-minus-square"></i> Quitar</button></td></tr>');
            }

            //limiamos los textbox
            $('#nro_boleta').val("");
            $('#monto_soles').val("");
            $('#monto_dolares').val("");
        }

        function eliminarAgente(id)
        {
            $('#' + id).remove();
        }
    </script>

    <!-- switchery -->
    <script src="js/switchery/switchery.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".select2_single").select2({
                placeholder: "<?php
                                        if ($accion != "buscar") {
                                            echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class='label label-danger'>Lista vacía, porfavor realice la busqueda&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fa fa-arrow-up'></i><i class='fa fa-arrow-up'></i><i class='fa fa-arrow-up'></i></label>";
                                        } else {
                                            if ($num_regs_personas > 0) {
                                                echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class='label label-success'>Seleccionar Persona</label>";
                                            } else {
                                                echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class='label label-warning'>Agregue al Participante ó realize una nueva busqueda</label>";
                                            }
                                        }
                                        ?>",
                allowClear: true
            });
            $(".select2_group").select2({});
            $(".select2_multiple").select2({
                maximumSelectionLength: <?php
                                        if ($accion != "buscar") {
                                            echo 0;
                                        } else {                                            
                                            echo 10;
                                        }
                                        ?>,
                placeholder: <?php
                                        if ($accion != "buscar") {
                                            echo "'Realize una busqueda'";
                                        } else {
                                            echo "'Seleccione Máximo 10 Participantes'";
                                        }
                                        ?>,
                allowClear: true
            });
        });
    </script>
    <?php
    require('../../include/footer.php');
    ?>

    <!-- form validation -->
    <script src="js/validator/validator.js"></script>
    <script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
                .on('blur', 'input[required], input.optional, select.required', validator.checkField)
                .on('change', 'select.required', validator.checkField)
                .on('keypress', 'input[required][pattern]', validator.keypress);

        $('.multi.required')
                .on('keyup blur', 'input', function () {
                    validator.checkField.apply($(this).siblings().last()[0]);
                });

        // bind the validation to the form submit event
        //$('#send').click('submit');//.prop('disabled', true);

        $('form').submit(function (e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
                submit = false;
            }

            if (submit)
                this.submit();
            return false;
        });

        /* FOR DEMO ONLY */
        $('#vfields').change(function () {
            $('form').toggleClass('mode2');
        }).prop('checked', false);

        $('#alerts').change(function () {
            validator.defaults.alerts = (this.checked) ? false : true;
            if (this.checked)
                $('form .alert').remove();
        }).prop('checked', false);
    </script>
