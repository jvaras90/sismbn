<?php
require('../../include/header.php');
include '../../system/mensajesAlerta.php';
include_once '../../system/crearConexion.php';
clearstatcache();

$idcategoria = $_GET['idcategoria'];

?>


<script src="js/ajax.js" type="text/javascript"></script>
<script type="text/javascript">
//    $(document).ready(function () {
//        var listaC = document.getElementById("idcategoria");
//        $('#idcategoria').change(function () {
//            window.location.href = "persNews?idcategoria=" + listaC.value;
//        });
//    });

    function reloadPageC(idC) {
        window.location.href = "persNews?idcategoria=" + idC;
    }
</script>

<script>
    $(function () {
        // Obtener la referencia a la lista
        var lista = document.getElementById("iddepartamento");
        var lista1 = document.getElementById("idprovincia");
        // Obtener el índice de la opción que se ha seleccionado
        var indiceSeleccionado = lista.selectedIndex;
        var indiceSeleccionado1 = lista1.selectedIndex;
        // Con el índice y el array "options", obtener la opción seleccionada
        var opcionSeleccionada = lista.options[indiceSeleccionado];
        var opcionSeleccionada1 = lista1.options[indiceSeleccionado1];
        // Obtener el valor y el texto de la opción seleccionada
        //var textoSeleccionado = opcionSeleccionada.text;
        var valorSeleccionado = opcionSeleccionada.value;
        var valorSeleccionado1 = opcionSeleccionada1.value;
        load_departamento(0);
        load_provincia(0);
        load_distrito(0);

    });
</script>

<!-- page content -->
<div class="right_col" <?php echo $newDimensionBody; ?> role="main">
    <form class="form-horizontal form-label-left" method="post" action="registrarA" enctype="multipart/form-data">
        <div class="page-title">
            <h1>Registros de Participantes </h1>
        </div>
        <br>
        <div class="clearfix"></div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <br>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="col-md-3 col-sm-3 col-xs-3" id="cliente">
                    </div>
                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Condición</label>
                    <div class="col-md-3 col-sm-3 col-xs-3" id="cliente">
                        <select class='form-control' name='idcategoria' id='idcategoria' onchange="reloadPageC(this.value);" >
                            <?php include_once "../../system/selectCategoria.php"; ?>
                        </select>
                    </div>
                    <?php if ($_SESSION['id_nivel'] == 1) { ?>
                        <div class="col-md-1 col-sm-1 col-xs-3" id="nuevoGrupo" title="Agregar Nueva Condición">
                            <a href="#modalNuevoCategorias" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                        </div>
                    <?php } ?>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Datos Personales</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Nombre</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="nombre">
                                    <input name="nombre" id="nombre" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="3" data-validate-words="1" placeholder="" maxlength="300" autocomplete="" type="text" required>
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Subir Imagen</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="nombre">
                                    <input class="avatar-input" id="foto_fls" name="foto_fls" type="file">
                                    <input type="hidden" name="foto_hdn" value="<?php echo $foto ?>">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">A. Paterno</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="aPaterno">
                                    <input name="aPaterno" id="aPaterno" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="2" data-validate-words="1" placeholder="" maxlength="300" autocomplete="" type="text" required >
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">A. Materno</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="aMaterno">
                                    <input name="aMaterno" id="aMaterno" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="1" data-validate-words="1" placeholder="" maxlength="300" autocomplete="" type="text" required >
                                </div>
                            </div> 
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Tipo Documento</label>
                                <div class="col-md-3 col-sm-3 col-xs-3" id="aPaterno">
                                    <select class='form-control' name='idtipo_documento' id='idtipo_documento'>
                                        <?php include_once "../../system/selectTipoDocumento.php"; ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Tipo de Documento">
                                    <a href="#modalNuevoTipoDocumento" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Número Doc.</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="dni">
                                    <input name="dni" id="dni" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="off" type="text" value="<?php echo "$nro_documento "; ?>">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Sexo</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="aPaterno">
                                    <div class="radio-inline">
                                        <label>
                                            <input class="flat" type="radio" <?php
                                            if ($sexo == "M") {
                                                echo "checked";
                                            }
                                            ?> value="M" id="S" name="S"> M
                                        </label>
                                    </div>
                                    <div class="radio-inline">
                                        <label>
                                            <input class="flat" type="radio" <?php
                                            if ($sexo == "F") {
                                                echo "checked";
                                            }
                                            ?> value="F" id="S" name="S"> F
                                        </label>
                                    </div> 
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Estado Civil</label>
                                <div class="col-md-3 col-sm-3 col-xs-3" id="aPaterno">
                                    <select class='form-control' name='idestado_civil' id='idestado_civil'>
                                        <option>Seleccione</option>
                                        <?php include_once "../../system/selectEstadoCivil.php"; ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Estado Civil">
                                    <a href="#modalNuevoEstadoCivil" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Fecha Nac.</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="aPaterno">
                                    <input type="text" id="fecha_nac1"  name="fecha_nac1"  class="form-control col-md-3 col-sm-3 col-xs-3" data-inputmask="'mask': '9999-99-99'">
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Lugar Nac.</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="aPaterno">
                                    <input name="lugar_nac" id="lugar_nac" class="form-control col-md-3 col-sm-3 col-xs-3" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="off" type="text">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Profesión</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="aPaterno">
                                    <input name="profesion" id="profesion" class="form-control col-md-3 col-sm-3 col-xs-3" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="245" autocomplete="off" type="text">
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Trabajo Actual</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="aPaterno">
                                    <input name="ocupacion" id="ocupacion" class="form-control col-md-3 col-sm-3 col-xs-3" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="245" autocomplete="off" type="text" >
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Contactabilidad</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Teléfono</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="aPaterno">
                                    <input name="telefono" id="telefono" class="form-control col-md-3 col-sm-3 col-xs-3" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="off" type="text" >
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Celular</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="aPaterno">
                                    <input name="celular" id="celular" class="form-control col-md-3 col-sm-3 col-xs-3" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="off" type="text" >
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Email</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="cliente">
                                    <input type="text" id="email" name="email" class="form-control col-md-7 col-sm-7 col-xs-7" >
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Persona de Referencia</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="cliente">
                                    <input type="text" id="referencia" name="referencia" class="form-control col-md-7 col-sm-7 col-xs-7" >
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Datos de Ubicación</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Pais  <?php //echo $iddepartamento;                                                                                                                        ?></label>
                                <div class="col-md-3 col-sm-3 col-xs-3" id="cliente">
                                    <select class='form-control' name='idpais' id='idpais' onchange="load_departamento(this.value)" >
                                        <option>Seleccione</option>
                                        <?php include_once "../../system/selectPais.php"; ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Pais">
                                    <a href="#modalNuevoPais" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Departamento  <?php //echo $iddepartamento;                                                                                                                        ?></label>
                                <div class="col-md-3 col-sm-3 col-xs-3" id="myDivDep">
                                    <select class='form-control' name='iddepartamento' id='iddepartamento' onchange="load_provincia(this.value)" >
                                        <option>Seleccione Departamento</option>
                                        <?php //include_once "../../system/selectDepartamento.php"; ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Departamento">
                                    <a href="#modalNuevoDepartamentos" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Provincia  <?php // echo $idprovincia;                                                                                                                        ?></label>
                                <div class="col-md-3 col-sm-3 col-xs-3" id="myDivProv">
                                    <select class='form-control' name='idprovincia' id='idprovincia' onchange="load_distrito(this.value)" >
                                        <option>Seleccione Provincia</option>
                                        <?php //include_once "../../system/selectProvincia.php"; ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nueva Provincia">
                                    <a href="#modalNuevoProvincias" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Distrito <?php // echo $iddistrito;                                                                                                                        ?></label>
                                <div class="col-md-3 col-sm-3 col-xs-3" id="myDivDist">
                                    <select class='form-control' name='iddistrito' id='iddistrito'>
                                        <option>Seleccione Distrito</option>
                                        <?php //include_once "../../system/selectDistrito.php"; ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Distrito">
                                    <a href="#modalNuevoDistritos" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Dirección</label>
                                <div class="col-md-10 col-sm-10 col-xs-12" id="cliente">
                                    <input type="text" id="direccion" name="direccion" required="required" placeholder="" class="form-control col-md-7 col-sm-7 col-xs-7" value="<?php echo "$direccion"; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            if ($idcategoria == 1) {
                ?>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Datos de Miembro</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="item form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Fec. Salvación</label>
                                    <div class="col-md-4 col-sm-4 col-xs-4" id="aPaterno">
                                        <input type="text" id="fecha_salvacion1"  name="fecha_salvacion1"  class="form-control col-md-3 col-sm-3 col-xs-3" data-inputmask="'mask': '9999-99-99'" required>
                                    </div>
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Predicó  </label>
                                    <div class="col-md-4 col-sm-4 col-xs-4" id="aPaterno">
                                        <input name="s_referencia" id="s_referencia" class="form-control col-md-3 col-sm-3 col-xs-3" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="off" type="text"  value="<?php echo $referencia; ?>">
                                    </div>
                                </div>
                                <div id="selectMultiple"  class="item form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2" title="Puerto de salida">Cargo </label>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <select class="select2_multiple form-control" multiple="multiple" name='idcargoM[]' id='idcargoM[]'>
                                            <?php include '../../system/selectCargo.php' ?>   
                                        </select>
                                    </div> 
                                    <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Cargo">
                                        <a href="#modalNuevoCargos" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                    </div>    
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Grupo </label>
                                    <div class="col-md-3 col-sm-3 col-xs-3" title="Asignado para las reuniones Grupales">
                                        <select class='form-control' name='idgrupo' id='idgrupo'  >
                                            <option>Seleccione</option>
                                            <?php include_once "../../system/selectGrupo.php"; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nueva Reunion Grupal">
                                        <a href="#modalNuevoGrupos" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Gran Zona </label>
                                    <div class="col-md-3 col-sm-3 col-xs-3" id="myDivProv">
                                        <select class='form-control' name='idreunion_granzona' id='idreunion_granzona' onchange="load_reunion_zonal(this.value)" >
                                            <option>Seleccione</option>
                                            <?php include_once "../../system/selectReunionGranZona.php"; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nueva Gran Zona">
                                        <a  href="#modalNuevoreuGranZon" data-toggle="modal" class = "btn btn-round btn-success btn-sm" ><i class = "fa fa-plus-square"></i></a>
                                    </div>
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Reunion Zonal </label>
                                    <div class="col-md-3 col-sm-3 col-xs-3" id="myDivReuZon" title="Asignado para las reuniones Zonales">
                                        <select class='form-control' name='idreunion_zonal' id='idreunion_zonal'>
                                            <?php include_once "../../system/selectReunionZonal.php"; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nueva Reunion Zonal">
                                        <a href="#modalNuevoreuZonal" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div style="text-align: center;">
                <a href="listPers" class="btn btn-danger"><i class="fa fa-rotate-left"></i> Regresar</a>
                <input type="button" value="Registrar" id="actualizarPersonal" name="actualizarPersonal" class="btn btn-primary" onclick="this.disabled = true;
                        this.value = 'Registrando...';
                        this.form.submit()"/>
                <input type="hidden" name="form" value="nuevaPersona"/>
            </div>
            
            
        </div>
        <div class="clearfix"></div>
    </form>
    <?php
    if ($_SESSION['id_nivel'] == 1 || $_SESSION['id_nivel'] == 2) {
        include '../VISTAS/listaModals.php';
    }
    require('../../include/footer.php');
    ?>
    <script>
        $(document).ready(function () {
            $(".select2_single").select2({
                placeholder: "Select a state",
                allowClear: true
            });
            $(".select2_group").select2({});
            $(".select2_multiple").select2({
                maximumSelectionLength: <?php echo $num_regs_cargos; ?>,
                placeholder: "Seleccione Maximo " + <?php echo $num_regs_cargos; ?> + " Cargos",
                allowClear: true
            });


        });
    </script>
    <!-- input_mask -->
    <script>
        $(document).ready(function () {
            $(":input").inputmask();
        });
    </script>
    <!-- /input mask -->