<?php
require('../../include/header.php');
include '../../system/mensajesAlerta.php';
include_once '../../system/crearConexion.php';
clearstatcache();

$idcategoria = $_GET['idcategoria'];
?>


<script src="js/ajax.js" type="text/javascript"></script>
<script type="text/javascript">
//    $(document).ready(function () {
//        var listaC = document.getElementById("idcategoria");
//        $('#idcategoria').change(function () {
//            window.location.href = "persNews?idcategoria=" + listaC.value;
//        });
//    });

    function reloadPageC(idC) {
        window.location.href = "persNews?mod=nPersona&idcategoria=" + idC;
    }

    function cargarGrupo(id) {
        if (id >= 60) {
            grupo = "SILVER";
        } else if (id >= 39) {
            grupo = "SENIOR";
        } else if (id >= 31) {
            grupo = "GRACE";
        } else if (id >= 24) {
            grupo = "FAITH";
        } else if (id >= 17) {
            grupo = "TRUTH";
        } else if (id >= 11) {
            grupo = "JUNNIOR";
        } else {
            grupo = "Niños";
        }
//        switch (id) {
//            case "6":/*Adolescente */
//                grupo = "JUNIOR";
//                break;
//            case "5":/*Joven Menor */
//                grupo = "TRUTH";
//                break;
//            case "4":/* Joven Mayor */
//                grupo = "DREAM";
//                break;
//            case "1":/* Silver */
//                grupo = "SILVER";
//                break;
//            case "2":/* Senior */
//                grupo = "SENIOR";
//                break;
//            default :
//                grupo = "";
//        }
        //alert("idGrupo: " + id + " descripcion: " + grupo + " ");
        $("#grupo").val("" + grupo + "");
    }
    function validarFormPersona(boton) {
        //var button = $('#registrarPersonalInscripcion').val();
        var idgrupo = $('#idgrupo').val();
        var idciudad = $('#idciudad').val();
        var idestado_pago = $('#idestado_pago').val();
        var idpais = $('#idpais').val();
        var idestado_civil = $('#idestado_civil').val();
        if (idgrupo == '0') {
            alert('Seleccione el grupo de la persona (Silver,Casado,Joven o Adolescente) ');
            $('#idgrupo').focus();
            return false;
        } else if (idestado_civil == '0') {
            alert('Seleccione el estado civil del participante');
            $('#idestado_civil').focus();
            return false;
        } else if (idpais == '0') {
            alert('Seleccione el Pais del participante');
            $('#idpais').focus();
            return false;
        } else if (idciudad == '0') {
            alert('Seleccione la ciudad de la persona ');
            $('#idciudad').focus();
            return false;
        } else if (idestado_pago == '0') {
            alert('Seleccione el estado en el que se encuentra el pago ');
            $('#idestado_pago').focus();
            return false;
        } else if (!$("#registrarPersonal input[name='S']:radio").is(':checked')) {
            alert('Seleccione el sexo de la persona ');
            $('#S').focus();
            return false;
        } else {
            boton.disabled = true;
            boton.value = 'Registrando...';
            boton.form.submit();
            return true;
        }
    }



</script>

<SCRIPT LANGUAGE="JavaScript">
    function NumCheck(e, field) {
        key = e.keyCode ? e.keyCode : e.which
        // backspace
        if (key == 8)
            return true
        // 0-9
        if (key > 47 && key < 58) {
            if (field.value == "")
                return true
            regexp = /.[0-9]{3}$/
            return !(regexp.test(field.value))
        }
        // .
        if (key == 46) {
            if (field.value == "")
                return false
            regexp = /^[0-9]+$/
            return regexp.test(field.value)
        }
        // other key
        return false
    }
</script>
<script>
    $(function () {
        // Obtener la referencia a la lista
        var lista = document.getElementById("iddepartamento");
        var lista1 = document.getElementById("idprovincia");
        var idpais = $('#idpais').val();
        // Obtener el índice de la opción que se ha seleccionado
        var indiceSeleccionado = lista.selectedIndex;
        var indiceSeleccionado1 = lista1.selectedIndex;
        // Con el índice y el array "options", obtener la opción seleccionada
        var opcionSeleccionada = lista.options[indiceSeleccionado];
        var opcionSeleccionada1 = lista1.options[indiceSeleccionado1];
        // Obtener el valor y el texto de la opción seleccionada
        //var textoSeleccionado = opcionSeleccionada.text;
        var valorSeleccionado = opcionSeleccionada.value;
        var valorSeleccionado1 = opcionSeleccionada1.value;
        load_departamento(idpais);
        load_provincia(0);
        load_distrito(0);

    });
</script>

<!-- page content -->
<div class="right_col" <?php echo $newDimensionBody; ?> role="main">
    <form id="registrarPersonal" class="form-horizontal form-label-left" method="post" action="registrarA" enctype="multipart/form-data">
        <div class="page-title">
            <h1>Registros de Participantes 1s </h1>
        </div>
        <br>

        <input type="hidden" name="condicionMod" id="condicionMod" value="<?php echo $_GET['mod']; ?>" >
        <div class="clearfix"></div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <br>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="col-md-3 col-sm-3 col-xs-3" id="cliente">
                    </div>
                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Condición</label>
                    <div class="col-md-3 col-sm-3 col-xs-3" id="cliente">
                        <select class='form-control' name='idcategoria' id='idcategoria' onchange="reloadPageC(this.value);" >
                            <?php include_once "../../system/selectCategoria.php"; ?>
                        </select>
                    </div>
                    <?php if ($_SESSION['id_nivel'] == 1) { ?>
                        <div class="col-md-1 col-sm-1 col-xs-3" id="nuevoGrupo" title="Agregar Nueva Condición">
                            <a href="#modalNuevoCategorias" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                        </div>
                    <?php } ?>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Datos Personales</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Nombre</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" >
                                    <input name="nombre" id="nombre" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="3" data-validate-words="1" placeholder="" maxlength="300" autocomplete="" type="text" required>
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Subir Imagen</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" >
                                    <input class="avatar-input" id="foto_fls" name="foto_fls" type="file">
                                    <input type="hidden" name="foto_hdn" value="<?php echo $foto ?>">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">A. Paterno</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" >
                                    <input name="aPaterno" id="aPaterno" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="2" data-validate-words="1" placeholder="" maxlength="300" autocomplete="" type="text" required >
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">A. Materno</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="aMaterno">
                                    <input name="aMaterno" id="aMaterno" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="1" data-validate-words="1" placeholder="" maxlength="300" autocomplete="" type="text" required >
                                </div>
                            </div> 
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Tipo Documento</label>
                                <div class="col-md-3 col-sm-3 col-xs-3" >
                                    <select class='form-control' name='idtipo_documento' id='idtipo_documento'>
                                        <?php include_once "../../system/selectTipoDocumento.php"; ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Tipo de Documento">
                                    <a href="#modalNuevoTipoDocumento" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Número Doc.</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="dni">
                                    <input name="dni" id="dni" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="off" type="text" value="<?php echo "$nro_documento"; ?>">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Sexo</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" >
                                    <div class="radio-inline">
                                        <label>
                                            <input class="flat" type="radio" <?php
                                            if ($sexo == "M") {
                                                echo "checked";
                                            }
                                            ?> value="M" id="S" name="S"> M
                                        </label>
                                    </div>
                                    <div class="radio-inline">
                                        <label>
                                            <input class="flat" type="radio" <?php
                                            if ($sexo == "F") {
                                                echo "checked";
                                            }
                                            ?> value="F" id="S" name="S"> F
                                        </label>
                                    </div> 
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Estado Civil</label>
                                <div class="col-md-3 col-sm-3 col-xs-3" >
                                    <select class='form-control' name='idestado_civil' id='idestado_civil'>
                                        <option value="0">Seleccione</option>
                                        <?php include_once "../../system/selectEstadoCivil.php"; ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Estado Civil">
                                    <a href="#modalNuevoEstadoCivil" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Fecha Nac.</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" >
                                    <input type="text" id="fecha_nac1"  name="fecha_nac1"  class="form-control col-md-3 col-sm-3 col-xs-3" data-inputmask="'mask': '9999-99-99'">
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Edad</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" >
                                    <input name="edad" id="edad" class="form-control col-md-3 col-sm-3 col-xs-3" data-validate-minmax="10,100" data-inputmask="'mask': '99'" required="" placeholder="" maxlength="30" autocomplete="" type="text" onkeydown="" onkeyup="cargarGrupo(this.value)" onkeypress="">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Lugar Nac.</label>
                                <div class="col-md-10 col-sm-10 col-xs-10" >
                                    <input name="lugar_nac" id="lugar_nac" class="form-control col-md-3 col-sm-3 col-xs-3" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="" type="text">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Profesión</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" >
                                    <input name="profesion" id="profesion" class="form-control col-md-3 col-sm-3 col-xs-3" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="245" autocomplete="" type="text">
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Trabajo Actual</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" >
                                    <input name="ocupacion" id="ocupacion" class="form-control col-md-3 col-sm-3 col-xs-3" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="245" autocomplete="" type="text" >
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Contactabilidad</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Teléfono</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" >
                                    <input name="telefono" id="telefono" class="form-control col-md-3 col-sm-3 col-xs-3" data-inputmask="'mask': '999-9999'" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="" type="text" >
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Celular</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" >
                                    <input name="celular" id="celular" class="form-control col-md-3 col-sm-3 col-xs-3" data-inputmask="'mask': '999-999-999'" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="" type="text" >
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Email</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="cliente">
                                    <input type="text" id="email" name="email" class="form-control col-md-7 col-sm-7 col-xs-7" >
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Persona de Referencia</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="cliente">
                                    <input type="text" id="referencia" name="referencia" class="form-control col-md-7 col-sm-7 col-xs-7" >
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Datos de Ubicación</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Pais  <?php //echo $iddepartamento;                                                                                                                                                                              ?></label>
                                <div class="col-md-3 col-sm-3 col-xs-3" id="cliente">
                                    <select class='form-control' name='idpais' id='idpais' onchange="load_departamento(this.value)" >
                                        <option value="0">Seleccione</option>
                                        <?php include_once "../../system/selectPais.php"; ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Pais">
                                    <a href="#modalNuevoPais" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Departamento  <?php //echo $iddepartamento;                                                                                                                                                                              ?></label>
                                <div class="col-md-3 col-sm-3 col-xs-3" id="myDivDep">
                                    <select class='form-control' name='iddepartamento' id='iddepartamento' onchange="load_provincia(this.value)" >
                                        <option value="0">Seleccione Departamento</option>
                                        <?php //include_once "../../system/selectDepartamento.php";  ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Departamento">
                                    <a href="#modalNuevoDepartamentos" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Provincia  <?php // echo $idprovincia;                                                                                                                                                                              ?></label>
                                <div class="col-md-3 col-sm-3 col-xs-3" id="myDivProv">
                                    <select class='form-control' name='idprovincia' id='idprovincia' onchange="load_distrito(this.value)" >
                                        <option value="0">Seleccione Provincia</option>
                                        <?php //include_once "../../system/selectProvincia.php";  ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nueva Provincia">
                                    <a href="#modalNuevoProvincias" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Distrito <?php // echo $iddistrito;                                                                                                                                                                              ?></label>
                                <div class="col-md-3 col-sm-3 col-xs-3" id="myDivDist">
                                    <select class='form-control' name='iddistrito' id='iddistrito'>
                                        <option value="0">Seleccione Distrito</option>
                                        <?php //include_once "../../system/selectDistrito.php";  ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Distrito">
                                    <a href="#modalNuevoDistritos" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Dirección</label>
                                <div class="col-md-10 col-sm-10 col-xs-12" id="cliente">
                                    <input type="text" id="direccion" name="direccion" required="required" placeholder="" class="form-control col-md-7 col-sm-7 col-xs-7" value="<?php echo "$direccion"; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            if ($idcategoria == 1) {
                ?>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Datos de Miembro</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="item form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Fec. Salvación</label>
                                    <div class="col-md-4 col-sm-4 col-xs-4" >
                                        <input type="text" id="fecha_salvacion1"  name="fecha_salvacion1"  class="form-control col-md-3 col-sm-3 col-xs-3" data-inputmask="'mask': '9999-99-99'" required>
                                    </div>
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Predicó  </label>
                                    <div class="col-md-4 col-sm-4 col-xs-4" >
                                        <input name="s_referencia" id="s_referencia" class="form-control col-md-3 col-sm-3 col-xs-3" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="off" type="text"  value="<?php echo $referencia; ?>">
                                    </div>
                                </div>
                                <div id="selectMultiple"  class="item form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2" title="Puerto de salida">Cargo </label>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <select class="select2_multiple form-control" multiple="multiple" name='idcargoM[]' id='idcargoM[]'>
                                            <option value="17" selected="">Participante</option>
                                            <?php include '../../system/selectCargo.php' ?>   
                                        </select>
                                    </div> 
                                    <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Cargo">
                                        <a href="#modalNuevoCargos" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                    </div>    
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Grupo </label>
                                    <div class="col-md-3 col-sm-3 col-xs-3" title="Asignado para las reuniones Grupales">
                                        <select class='form-control' name='idgrupo' id='idgrupo'  onload="cargarGrupo(this.value)" >
                                            <option value="0">Seleccione</option>
                                            <?php include_once "../../system/selectGrupo.php"; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nueva Reunion Grupal">
                                        <a href="#modalNuevoGrupos" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Gran Zona </label>
                                    <div class="col-md-3 col-sm-3 col-xs-3" id="myDivProv">
                                        <select class='form-control' name='idreunion_granzona' id='idreunion_granzona' onchange="load_reunion_zonal(this.value)" >
                                            <option value="0">Seleccione</option>
                                            <?php include_once "../../system/selectReunionGranZona.php"; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nueva Gran Zona">
                                        <a  href="#modalNuevoreuGranZon" data-toggle="modal" class = "btn btn-round btn-success btn-sm" ><i class = "fa fa-plus-square"></i></a>
                                    </div>
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Reunion Zonal </label>
                                    <div class="col-md-3 col-sm-3 col-xs-3" id="myDivReuZon" title="Asignado para las reuniones Zonales">
                                        <select class='form-control' name='idreunion_zonal' id='idreunion_zonal'>
                                            <?php include_once "../../system/selectReunionZonal.php"; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nueva Reunion Zonal">
                                        <a href="#modalNuevoreuZonal" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
            <?php } ?>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Datos de Inscripción</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                            <div style="text-align: center;">
                                <label class="control-label  has-feedback-left" title="Active esta opcion si el participante es Voluntario">
                                    Voluntario <input type="checkbox" id="volunt" name="volunt" class="js-switch form-control" /> 
                                </label>
                            </div>
                        </div>
                        <?php if ($idcategoria == 2) { ?>
                            <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback"  id="myDivReuZon" title="Seleccione la Zona del participante">
                                <select class="form-control  has-feedback-left" id="idreunion_zonal" name="idreunion_zonal" >
                                    <option value="0">Zona</option>
                                    <?php include_once "../../system/selectReunionZonal.php"; ?>
                                </select>
                                <span class="fa fa-bullseye form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <div id="selectMultiple"  class="col-md-6 col-sm-6 col-xs-6 form-group text-center" title="Seleccione los cargos si es que tiene, de lo contrario dejar por defecto la opcion de participante">
                                <select class="select2_multiple form-control" multiple="multiple" name='idcargoM[]' id='idcargoM[]'>
                                    <option value="17" selected>Participante</option>
                                    <?php include '../../system/selectCargo.php' ?>   
                                </select>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Seleccione el Estado del participante">
                                <select class="form-control  has-feedback-left" id="idgrupo" name="idgrupo" onload="cargarGrupo(this.value)" >
                                    <option value="0">Estado</option>
                                    <?php include_once "../../system/selectGrupo.php"; ?>
                                </select>
                                <span class="fa fa-shield form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        <?php } ?>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Éste es el grupo al que pertenece el participante">
                            <input class="form-control has-feedback-left" id="grupo" name="grupo" placeholder="Grupo" type="text" value="" readonly>
                            <span class="fa fa-users form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" id="myDivCiudad" title="Seleccione la Ciudad">
                            <select class="form-control  has-feedback-left" id="idciudad" name="idciudad" >
                                <option value="0">Lugar</option>
                                <?php include_once "../../system/selectCiudad.php"; ?>
                            </select>
                            <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Seleccione la talla del polo">
                            <select class="form-control  has-feedback-left" id="idtalla_polo" name="idtalla_polo" >
                                <option value="0">Talla Polo</option>
                                <?php include_once "../../system/selectTallaPolo.php"; ?>
                            </select>
                            <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Seleccione el estado en el que se encuentra el Abono de la Iniscipción">
                            <select class="form-control  has-feedback-left" id="idestado_pago" name="idestado_pago" >
                                <option value="0">Estado Pago</option>
                                <?php include_once "../../system/selectEstadoPago.php"; ?>
                            </select>
                            <span class="fa fa-money form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="clearfix"></div>

                        <!----------------------------------------DIV DETALLE PAGO------------------------------------------------------------->
                        <div class="ln_solid"></div>
                        <div style="text-align: center;">
                            <h2><i><u>Información de Abono</u></i></h2>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3 form-group has-feedback" title="Ingrese el número de la boleta">
                            <input class="form-control has-feedback-left" id="nro_boleta" name="nro_boleta" data-validate-minmax="0,1000000000000000" placeholder="Nro. Boleta" type="text" required="">
                            <span class="fa fa-slack form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3 form-group has-feedback" title="Ingrese el monto en soles">
                            <input class="form-control has-feedback-left" id="monto_soles" name="monto_soles" data-validate-minmax="0,1000000000000000" placeholder="Monto Soles" type="text"  onkeypress="return NumCheck(event, this)">
                            <span class="fa form-control-feedback left" aria-hidden="true">S/.</span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3 form-group has-feedback" title="Ingrese el monto en dólares">
                            <input class="form-control has-feedback-left" id="monto_dolares" name="monto_dolares" data-validate-minmax="0,1000000000000000" placeholder="Monto Dólares" type="text"  onkeypress="return NumCheck(event, this)">
                            <span class="fa fa-dollar form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3 form-group has-feedback" title="Presione para agregar los datos a la tabla">
                            <button type="button" class="btn btn-primary" onclick="agregarBoleta()" >Agregar</button>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback" title="Detalle de Abono">

                            <table id="table_boleta" class="table table-striped responsive-utilities jambo_table bulk_action">
                                <thead style="text-align: center;" >
                                    <tr class="headings">
    <!--                                    <th style="text-align: center;" >
    
                                        </th>-->
                                        <th style="text-align: center;"  class="column-title">Nro. Boleta</th>
                                        <th style="text-align: center;" class="column-title">Monto Soles (<i class="fa">S/.</i>)</th>
                                        <th style="text-align: center;" class="column-title">Monto Dólares (<i class="fa fa-dollar">)</i></th>
                                        <th style="text-align: center;" class="column-title">Gestión</th>
                                        <!--<th class="column-title no-link last"><span class="nobr">Accion</span>-->
                                        </th>
                                        <th class="bulk-actions" colspan="7">
                                            <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody style="text-align: center;"  >
    <!--                                <tr class="even pointer">
                                        <td class="a-center"><input type="checkbox" class="flat" name="table_records" ></td>
                                        <td class=""><?php echo $iddistrito; ?></td>
                                        <td class=""><?php echo $des_distrito; ?></td>
                                        <td class=""><?php echo $estado; ?></td>
                                        <td class="last">
    
                                            <form method="GET" id="exclude_form_<?php echo $id ?>" action="">
                                                <div style="float: left;" title="Eliminar <?php echo $titulo; ?>">
                                                    <button type="button" class="btn btn-round btn-danger btn-sm" onclick="eliminar(<?php echo "'quitar','$des_distrito','distrito','iddistrito', $iddistrito,'listDist'" ?>);"  > 
                                                        <i class="fa fa-minus-square"></i> 
                                                    </button>
                                                </div>
                                            </form>
                                        </td>
                                    </tr>  -->
    <!--                                <tr >
                                        <td class="a-center "></td>
                                        <td class=""><?php echo "total boletas"; ?></td>
                                        <td class=""><?php echo "total soles"; ?></td>
                                        <td class=""><?php echo "total dolares"; ?></td>
                                        <td class="last">
    
                                        </td>
                                    </tr>-->
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ln_solid"></div>
                        <!---------------------------------------------------------------------------------------------------------------------->

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback" title="Ingrese alguna observacion del participante">
                            <textarea style="resize: none;" class="form-control has-feedback-left" id="observacion" name="observacion" placeholder="Observación"></textarea>
                            <span class="fa fa-comments form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                </div>
            </div>
            <div style="text-align: center;">
                <a href="listPers?mod=lPersona" class="btn btn-danger"><i class="fa fa-rotate-left"></i> Regresar</a>
<!--                <input type="button" value="Registrar" id="actualizarPersonal" name="actualizarPersonal" class="btn btn-primary" onclick="this.disabled = true;
                        this.value = 'Registrando...';
                        this.form.submit()"/>-->
                <input type="button" value="Registrar" id="registrarPersonalInscripcion" name="registrarPersonalInscripcion" class="btn btn-primary" onclick="validarFormPersona(this)">
                <!--<button onclick="validar()">Registrar</button>-->
                <input type="hidden" name="form" value="nuevaPersona"/>
            </div>


        </div>
        <div class="clearfix"></div>
    </form>
    <?php
    if ($_SESSION['id_nivel'] == 1 || $_SESSION['id_nivel'] == 2) {
        include '../VISTAS/listaModals.php';
    }
    ?>
    <script>
        $(document).ready(function () {
            $(".select2_single").select2({
                placeholder: "Select a state",
                allowClear: true
            });
            $(".select2_group").select2({});
            $(".select2_multiple").select2({
                maximumSelectionLength: <?php echo $num_regs_cargos; ?>,
                placeholder: "Seleccione Maximo " + <?php echo $num_regs_cargos; ?> + " Cargos",
                allowClear: true
            });

        });
    </script>
    <!-- input_mask -->
    <script>
        $(document).ready(function () {
            $(":input").inputmask();
        });
    </script>
    <!-- /input mask -->
    <script>
        function agregarBoleta() {
            //capturamos los valores
            var nro_boleta = $('#nro_boleta').val();
            var monto_soles = $('#monto_soles').val();
            var monto_dolares = $('#monto_dolares').val();

            var cat = document.getElementById("table_boleta").rows.length;
            if (nro_boleta.length >= 3) {
                $('#table_boleta').append('<tr id="' + cat + '" class="impreso"><td><input type="hidden" name="nroBoleta' + nro_boleta + '" value="' + nro_boleta + '"/>' + nro_boleta + '</td><td><input type="hidden" name="montoSoles' + nro_boleta + '" value="' + monto_soles + '"/>' + monto_soles + '</td><td><input type="hidden" name="montoDolares' + monto_dolares + '" value="' + monto_dolares + '"/>' + monto_dolares + '</td><td><button type="button" class="btn btn-danger" onclick="eliminarAgente(' + cat + ');"><i class="fa fa-minus-square"></i> Quitar</button></td></tr>');
            }

            //limiamos los textbox
            $('#nro_boleta').val("");
            $('#monto_soles').val("");
            $('#monto_dolares').val("");
        }

        function eliminarAgente(id)
        {
            $('#' + id).remove();
        }
    </script>

    <!-- switchery -->
    <script src="js/switchery/switchery.min.js"></script>
    <?php require('../../include/footer.php'); ?>