<?php
require('../../include/header.php');
include '../../system/mensajesAlerta.php';
include_once '../../system/crearConexion.php';
clearstatcache();
$idgrupo_camp = $_GET['idgrupo_camp'];
//$sexo = $_GET['S'];
$accion = $_GET['accion'];
$idgrupo_cabe = $_GET['idgrupo_cabe'];
if (isset($_GET['idgrupo_cabe']) && $_GET['idgrupo_cabe'] != '0') {
    $sqry = "select  gb.idgrupo_camp, gc.descripcion grupo,idmaestro,idcapitan,capacidad,idpersonas,gb.idestado,sexo
from grupo_cab gb 
inner join grupo_camp gc on gc.idgrupo_camp = gb.idgrupo_camp
where gb.idgrupo_cab=$idgrupo_cabe";
    $res = $mysqlMBN->consultas($sqry);
    while ($fila = mysqli_fetch_array($res)) {
        $idgrupo_camp = $fila['idgrupo_camp'];
        $grupo = $fila['grupo'];
        $idmaestro = $fila['idmaestro'];
        $idcapitan = $fila['idcapitan'];
        $capacidad = $fila['capacidad'];
        $idpersonas = $fila['idpersonas'];
        $idestado = $fila['idestado'];
        $sexo = $fila['sexo'];
        $maxCap = $capacidad - 2;
    }
}
?>
<script src="js/ajax.js" type="text/javascript"></script>
<!-- switchery -->
<link rel="stylesheet" href="css/switchery/switchery.min.css" />
<script type="text/javascript">
    $(document).ready(function () {
        var lista1 = document.getElementById("S");
        var lista = document.getElementById("idpersona");
        $('#idpersona').change(function () {
<?php include '../../system/datos_autoload_personal.php' ?>;
            window.location = "newInsc?mod=nInscripcion&accion=<?php echo $accion; ?>&idpais=<?php echo $idpais; ?>&S=" + lista1.value + "&idpersona=" + lista.value;
        });
        var lista2 = document.getElementById("idgrupo");
        //cargarGrupo(lista2.value);
        var idgrupo_camp = $('#idgrupo_camp').val();
        //load_numeroGrupo(idgrupo_camp);
    });
    function validaFormAsig(boton) {
        var idgrupo_camp = $('#idgrupo_camp').val();
        var idgrupo_cabe = $('#idgrupo_cabe').val();
        if (idgrupo_camp == '0') {
            alert('Seleccione el grupo  ');
            $('#idgrupo_camp').focus();
            return false;
        } else if (idgrupo_cabe == '0') {
            alert('Seleccione la clase del grupo');
            $('#idgrupo_cabe').focus();
            return false;
        } else {
            boton.disabled = true;
            boton.value = 'Buscando...';
            boton.form.submit();
            return true;
        }
    }
</script>

<!-- page content -->
<div class="right_col" <?php echo $newDimensionBody; ?> role="main">
    <div class="page-title">

    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 col-sm-12 col-xs-12">

        <div class="x_panel">
            <div style="text-align: center">
                <h3><u><i>Asignación de Grupos</u></i></h3>
                <!--<h3><b>ENCUENTRO INTERNACIONAL PARA LOS JÓVENES 2017</b></h3>-->             
            </div>
            <div class="clearfix"></div>
            
            <div class="col-md-12 col-sm-12 col-xs-12">
                <form  class="form-horizontal form-label-left" method="get" action="newAsig" id="validaFormAsig">
                    <div class="col-md-1 col-sm-1 col-xs-1 form-group text-center">                            
                        <input type="hidden" name="mod" value="<?php echo $_GET['mod']; ?>"/>
                        <input type="hidden" name="accion" value="buscar"/>
                    </div>
                    <table border="0" style="background-color: white;text-align: center;margin: auto">
                        <tr style="height: 2px;">
                            <td colspan="1"  style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong>Grupo</strong></td>
                            <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
                            <td colspan="1"  style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong>Clase</strong></td>
                            <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
<!--                            <td colspan="2"  style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong>Sexo</strong></td>
                            <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>-->
                            <td colspan="1" rowspan="2" style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong> <input type="button" value="Buscar" id="regNewAsig" name="regNewAsig" class="btn btn-danger" onclick="validaFormAsig(this)"></strong></td>
                            <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
                            <td colspan="1" rowspan="2" style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong><a href="#modalNuevaPersona" class="btn btn-success" data-toggle="modal"><i class="fa fa-users"></i> Nuevo</a></strong></td>
                            <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
                        </tr>
                        <tr>
                            <td style="background-color: #2774e6; padding: 5px; margin: 1px;">

                                <select name="idgrupo_camp" id="idgrupo_camp" class="form-control" onchange="load_numeroGrupo(this.value)" onload="load_numeroGrupo(this.value)">
                                    <option value="0">Seleccione</option>                                         
                                    <?php include '../../system/selectGrupoCamp.php'; ?>
                                </select> 
                            </td>
                            <td colspan="1"  style="text-align: center;padding: 1px; margin: 1px;"><strong>&nbsp;</strong></td>
                            <td style="background-color: #2774e6; padding: 5px; margin: 1px;" >
                                <div id="myDivDep1">
                                    <select name="idgrupo_cabe" id="idgrupo_cabe" class="form-control" >
                                        <option value="0">Seleccione</option>
                                        <?php
                                        if (isset($_GET['accion'])) {
                                            include '../../system/selectNumeroGrup.php';
                                        }
                                        ?>
                                    </select> 
                                </div>
                            </td>
                            <td colspan="1"  style="text-align: center;padding: 1px; margin: 1px;"><strong>&nbsp;</strong></td>
                        </tr>
                    </table>
                </form>
            </div>
            <div class="clearfix"></div>
            <div class="x_content">
                <?php ?>
                <form class="form-horizontal form-label-left input_mask" novalidate action="registrarA" method="POST">  
                    <div class="clearfix"></div>
                    <div class="ln_solid"></div>
                    <div style="text-align: center;">
                        <h2><i><u>Información del Grupo</u></i></h2>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Nombres del participante">
                        <div class="item form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group text-center" title="Listado de personas, según el filtrado realizado">
                                <label class="control-label">Maestro </label>
                                <select class='select2_single form-control has-feedback-left' tabindex="-1" name='idmaestro' id='idmaestro' >
                                    <?php
                                    if ($accion == "buscar") {
                                        include_once '../../system/buscarMaestros.php';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Nombres del participante">
                        <div class="item form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group text-center" title="Listado de personas, según el filtrado realizado">
                                <label class="control-label">Capitán </label>
                                <select class='select2_single form-control has-feedback-left' tabindex="-1" name='idcapitan' id='idcapitan' >
                                    <?php
                                    if ($accion == "buscar") {
                                        include_once '../../system/buscarCapitan.php';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>                 
                    <div id="selectMultiple"  class="col-md-12 col-sm-6 col-xs-6 form-group text-center" title="Seleccione los cargos si es que tiene, de lo contrario dejar por defecto la opcion de participante">
                        <label class="control-label">Miembros del Grupo </label>
                        <select class="select2_multiple form-control" multiple="multiple" name='idPersonas[]' id='idPersonas[]'>
                            <!--<option value="17" selected>Participante</option>-->
                            <?php include '../../system/selectParticipantes.php' ?>   
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <div style="text-align: center;">
                        <div class="panel-footer">
                            <a title='Presione "Cancelar" para realizar una nueva busqueda' href="newInsc?mod=nInscripcion&idpais=<?php echo $idpais; ?>&S=<?php echo $sexo; ?>&idpersona=0" class="btn btn-danger"><i class="fa fa-rotate-left"></i> Regresar</a>
                            <input title='Presione "Registrar" para guardar los datos' type="button" value="Registrar" id="actualizarPersonal" name="nuevaInscripcion" class="btn btn-primary" onclick="this.disabled = true;
                                    this.value = 'Registrando...';
                                    this.form.submit()"/>

<!--                            <input type="button" value="Registrar" id="regNewAsig" name="regNewAsig" class="btn btn-primary" onclick="validaFormAsig(this)">-->
                            <input type="hidden" name="form" value="nuevaInscripcion"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php
//include_once '../VISTAS/listaModals.php';
    ?>
    <!-- ############################################ MODAL NUEVO AGENTE ############################################ -->

    <div class="modal fade" id="modalNuevaPersona" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="form-horizontal" role="form" action="registrarA" method="POST">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Nuevo Grupo</h4>
                    </div>
                    <div class="modal-body">   
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Grupo</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-tasks"></span></span>
                                    <select class='form-control' name='idgrupo_camp' id='idgrupo_camp'>
                                        <option value="0">Seleccione Grupo</option>
                                        <?php include "../../system/selectGrupoCamp.php"; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Descripción</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span></span>
                                    <input type="text"  name="descripcion" class="form-control" placeholder="Descripción" required>
                                </div>
                            </div>
                        </div><div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Capacidad Máxima</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span></span>
                                    <input type="text"  name="capacidad" class="form-control" placeholder="Capacidad" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Sexo</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-bookmark"></span></span>
                                    <select class='form-control' name='sexo' id='sexo'>
                                        <option value="M">Hombres</option> 
                                        <option value="F">Mujeres</option> 
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        <button type="submit" id="nuevoAgente" name="nuevoAgente" class="btn btn-primary">Siguiente >> </button>
                        <input type="hidden" name="form" value="nuevoGrupoCamp">
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="clearfix"></div>



    <!-- switchery -->
    <script src="js/switchery/switchery.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".select2_single").select2({
                placeholder: "<?php
                                        if ($accion != "buscar") {
                                            echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class='label label-danger'>Lista vacía, porfavor realice la busqueda&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fa fa-arrow-up'></i><i class='fa fa-arrow-up'></i><i class='fa fa-arrow-up'></i></label>";
                                        } else {
                                            if ($num_regs_personas > 0) {
                                                echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class='label label-success'>Seleccionar Persona</label>";
                                            } else {
                                                echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class='label label-warning'>Agregue al Participante ó realize una nueva busqueda</label>";
                                            }
                                        }
                                        ?>",
                allowClear: true
            });
            $(".select2_group").select2({});
            $(".select2_multiple").select2({
                maximumSelectionLength: <?php
                                        if ($accion != "buscar") {
                                            echo 0;
                                        } else {
                                            echo $maxCap;
                                        }
                                        ?>,
                placeholder: <?php
                                        if ($accion != "buscar") {
                                            echo "'Realize una busqueda'";
                                        } else {
                                            echo "'Seleccione Máximo $maxCap Participantes'";
                                        }
                                        ?>,
                allowClear: true
            });
        });
    </script>
    <?php
    require('../../include/footer.php');
    ?>

