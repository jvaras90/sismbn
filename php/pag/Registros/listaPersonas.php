<?php
require('../../include/header.php');
include '../../system/mensajesAlerta.php';
include_once '../../system/crearConexion.php';
clearstatcache();
$titulo = "Cargos";
$hoja = "Cargos";
$idpais = $_GET['idpais'];
if (isset($_GET['idciudad'])) {
    $idciudad = $_GET['idciudad'];
} else {
    $idciudad = 0;
}
$idgrupo = $_GET['idgrupo'];
?>
<!-- page content -->
<script>
    $(function () {
        //var idpais =  $("#idpais").val();
        load_ciudad1(<?php echo "$idpais,$idciudad"; ?>);
    });
</script>
<script src="js/ajax.js" type="text/javascript"></script>
<!-- switchery -->
<div class="right_col" <?php echo $newDimensionBody; ?> role="main">
    <div class="page-title">
        <div class="title_left">
            <h3>Listado de los participantes Inscritos</h3>
            <?php
            ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <form action="" method="GET">
            <div class="x_panel">
                <div class="x_title">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <table style="background-color: white;text-align: center;margin: auto">
                            <input type="hidden" id="mod" name="mod" value="<?php echo $_GET['mod']; ?>">
                            <tr style="height: 2px;">
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong>Pais</strong></td>
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong>Ciudad</strong></td>
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong>Grupo</strong></td>
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
                                <td colspan="2"  style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong>Sexo</strong></td>
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
                                <td colspan="1" rowspan="2" style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong> <input class="btn btn-danger" type="submit" value="Filtrar"></strong></td>
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
                                <td colspan="1" rowspan="2" style="height: 2px;text-align: center;padding: 1px; margin: 1px;"><strong><a href="#modalNuevaPersona" class="btn btn-success" data-toggle="modal"><i class="fa fa-user"></i> Nuevo</a></strong></td>
                                <td colspan="1"  style="height: 2px;text-align: center;padding: 10px; margin: 10px;"><strong>&nbsp;</strong></td>
                            </tr>
                            <tr>
                                <td style="background-color: #2774e6; padding: 5px; margin: 1px;">
                                    <select name="idpais" id="idpais" class="form-control" onchange="load_ciudad1(this.value)">
                                        <option value="0">Pais</option>                                         
                                        <?php include '../../system/selectPais.php'; ?>
                                    </select> 
                                </td>
                                <td colspan="1"  style="text-align: center;padding: 1px; margin: 1px;"><strong>&nbsp;</strong></td>
                                <td style="background-color: #2774e6; padding: 5px; margin: 1px;">
                                    <div id="myDivCiudad">
                                        <select name="idciudad" id="idciudad" class="form-control">
                                            <option value="0">Ciudad</option>                                         
                                            <?php include '../../system/selectCiudad.php'; ?>
                                        </select> 
                                    </div>
                                </td>
                                <td colspan="1"  style="text-align: center;padding: 1px; margin: 1px;"><strong>&nbsp;</strong></td>
                                <td style="background-color: #2774e6; padding: 5px; margin: 1px;">
                                    <select name="idgrupo" id="idgrupo" class="form-control">
                                        <option value="99">Grupo</option>
                                        <?php include '../../system/selectGrupo.php'; ?>
                                    </select> 
                                </td>
                                <td colspan="1"  style="text-align: center;padding: 1px; margin: 1px;"><strong>&nbsp;</strong></td>
                                <td style="text-align: center;background-color: white; padding: 5px; margin: 1px;">
                                    <div class="radio-inline" title="Varón">
                                        <label class="">
                                            <input class="flat" type="radio" <?php
                                            if ($_GET['S'] == "M") {
                                                echo "checked";
                                            }
                                            ?> value="M" id="S" name="S"> M
                                        </label>
                                    </div>
                                </td>
                                <td style="text-align: center;background-color: white; padding: 5px; margin: 1px;">
                                    <div class="radio-inline" title="Mujer">
                                        <label>
                                            <input class="flat" type="radio" <?php
                                            if ($_GET['S'] == "F") {
                                                echo "checked";
                                            }
                                            ?> value="F" id="S" name="S"> F
                                        </label>
                                    </div>
                                </td>
                                <td colspan="1"  style="text-align: center;padding: 1px; margin: 1px;"><strong>&nbsp;</strong></td>

                            </tr>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table  id="example"  class="table table-striped   ">
<!--                        <thead style="background-color: rgb(39, 116, 230)">
                            <tr class="headings">

                                <th class="column-title"></th>
                                <th class="column-title no-link last"><span class="nobr">Accion</span>
                                </th>
                            </tr>
                        </thead>-->
                        <tbody>
                            <?php include '../VISTAS/manTablePersonas.php'; ?>                                    
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
    </div>
    <!--     ############################################ MODAL NUEVO AGENTE ############################################ -->

    <div class="modal fade" id="modalNuevaPersona" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="form-horizontal" role="form" action="persNews" method="get">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Nueva Persona</h4>
                    </div>
                    <div class="modal-body">                        
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Condición</label>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="hidden" name="mod" value="nPersona"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-heart"></span></span>
                                    <select class='form-control' name='idcategoria' id='idcategoria'  >
                                        <?php include_once "../../system/selectCategoria.php"; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        <button type="submit" id="nuevoAgente" name="nuevoAgente" class="btn btn-primary">Siguiente >> </button>
                    </div>
                </form>
            </div> <!--/.modal-content -->
        </div> <!--/.modal-dialog -->
    </div> <!--/.modal -->

    <div class="clearfix"></div>
    <?php
    require('../../include/footer.php');
    ?>
