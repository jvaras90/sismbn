<?php
require('../../include/header.php');
include '../../system/mensajesAlerta.php';
include_once '../../system/crearConexion.php';
clearstatcache();
$titulo = "Cargos";
$hoja = "Cargos";
$idgrupo = $_GET['idgrupo'];
?>
<!-- page content -->
<div class="right_col" <?php echo $newDimensionBody; ?> role="main">
    <div class="page-title">
        <div class="title_left">
            <h3>Gestión de Mantenimiento</h3>
            <?php
            ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <form action="" method="GET">
            <div class="x_panel">
                <div class="x_title">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-1 col-sm-1 col-xs-1">
                            <label class="control-label">Grupos: </label>
                            <input type="hidden" name="mod" value="<?php echo $_GET['mod']; ?>">
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <select class="form-control" id="idgrupo" name="idgrupo">
                                <option value="99"> Todos</option>
                                <?php include '../../system/selectGrupo.php'; ?>
                            </select>
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-1">
                            <label class="control-label">Sexo:</label>
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-1">
                            <div class="radio-inline" title="Varón">
                                <label class="">
                                    <input class="flat" type="radio" <?php
                                    if ($_GET['S'] == "M") {
                                        echo "checked";
                                    }
                                    ?> value="M" id="S" name="S"> M
                                </label>
                            </div>
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-1">
                            <div class="radio-inline" title="Mujer">
                                <label>
                                    <input class="flat" type="radio" <?php
                                    if ($_GET['S'] == "F") {
                                        echo "checked";
                                    }
                                    ?> value="F" id="S" name="S"> F
                                </label>
                            </div> 
                        </div>
                        <!--                        <div class="col-md-4 col-sm-4 col-xs-4">
                                                    <div class="col-md-4 col-sm-4 col-xs-4">
                        
                                                    </div>
                                                    <div class="col-md-8 col-sm-8 col-xs-8">
                        
                        
                                                    </div>
                                                </div>-->

                        <div class="col-md-2 col-sm-2 col-xs-2">
                            <input class="btn btn-danger" type="submit" value="Filtrar">
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                            <a href="#modalNuevaPersona" class="btn btn-success" data-toggle="modal"><i class="fa fa-user"></i> Nuevo</a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table  id="example"  class="table table-striped   ">
                        <thead style="background-color: rgb(39, 116, 230)">
                            <tr class="headings">

                                <th class="column-title"></th>
                                <!--<th class="column-title no-link last"><span class="nobr">Accion</span>-->
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php include '../VISTAS/manTablePersonas.php'; ?>                                    
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
    </div>
    <!--     ############################################ MODAL NUEVO AGENTE ############################################ -->

    <div class="modal fade" id="modalNuevaPersona" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="form-horizontal" role="form" action="persNews" method="get">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Nueva Persona</h4>
                    </div>
                    <div class="modal-body">                        
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Condición</label>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="hidden" name="mod" value="nPersona"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-heart"></span></span>
                                    <select class='form-control' name='idcategoria' id='idcategoria'  >
                                        <?php include_once "../../system/selectCategoria.php"; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        <button type="submit" id="nuevoAgente" name="nuevoAgente" class="btn btn-primary">Siguiente >> </button>
                    </div>
                </form>
            </div> <!--/.modal-content -->
        </div> <!--/.modal-dialog -->
    </div> <!--/.modal -->

    <div class="clearfix"></div>
    <?php
    require('../../include/footer.php');
    ?>
