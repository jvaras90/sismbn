<?php
require('../../include/header.php');
include '../../system/mensajesAlerta.php';
include_once '../../system/crearConexion.php';
clearstatcache();
$idpersona = $_GET['idpersona'];
$idpais = $_GET['idpais'];
$sexo = $_GET['S'];
$accion = $_GET['accion']
?>
<!-- switchery -->
<link rel="stylesheet" href="css/switchery/switchery.min.css" />
<script type="text/javascript">
    $(document).ready(function () {
        var lista1 = document.getElementById("S");
        var lista = document.getElementById("idpersona");
        $('#idpersona').change(function () {
<?php include '../../system/datos_autoload_personal.php' ?>;
            window.location = "newInsc?mod=nInscripcion&accion=<?php echo $accion; ?>&idpais=<?php echo $idpais; ?>&S=" + lista1.value + "&idpersona=" + lista.value;
        });
        var lista2 = document.getElementById("idgrupo");
        cargarGrupo(lista2.value);
    });

    function cargarGrupo(id) {
        switch (id) {
            case "6":/*Adolescente */
                grupo = "JUNNIOR";
                break;
            case "5":/*Joven Menor */
                grupo = "TRUTH";
                break;
            case "4":/* Joven Mayor */
                grupo = "DREAM";
                break;
            case "1":/* Silver */
                grupo = "SILVER";
                break;
            case "2":/* Senior */
                grupo = "SENIOR";
                break;
            default :
                grupo = "";
        }
//        alert("idGrupo: " + id + " descripcion: " + grupo + " ");
        $("#grupo").val("" + grupo + "");
    }
</script>

<!-- page content -->
<div class="right_col" <?php echo $newDimensionBody; ?> role="main">
    <div class="page-title">

    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 col-sm-12 col-xs-12">

        <div class="x_panel">
            <div class="x_title">
                <div style="text-align: center">
                    <h3><i>INSCRIPCIÓN DE PARTICIPANTES</i></h3>
                    <h3><b>ENCUENTRO INTERNACIONAL PARA LOS JÓVENES 2017</b></h3>             
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="item form-group">
                    <form  class="form-horizontal form-label-left" action="" method="GET">
                        <div class="col-md-1 col-sm-1 col-xs-1 form-group text-center">                            
                            <input type="hidden" name="mod" value="<?php echo $_GET['mod']; ?>"/>
                            <input type="hidden" name="accion" value="buscar"/>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3 form-group text-center" title="Seleccione el Pais del participante">
                            <label class="control-label">Pais </label>
                            <select style="float: left;" class='form-control' name='idpais' id='idpais' >
                                <option>Seleccione</option>
                                <?php include_once "../../system/selectPais.php"; ?>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3 form-group text-center" title="Seleccione de Sexo del participante">                            
                            <label class="control-label">Sexo </label>
                            <select style="float: left;" class='form-control' name='S' id='S' >
                                <option <?php
                                if ($_GET['S'] == "M") {
                                    echo "selected";
                                }
                                ?> value="M">M</option>
                                <option <?php
                                if ($_GET['S'] == "F") {
                                    echo "selected";
                                }
                                ?> value="F">F</option>
                            </select>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2 form-group" title="Iniciar la Busqueda">
                            <label class="control-label">&nbsp;&nbsp;&nbsp;</label>
                            <div style="text-align: center;" >
                                <input type="button" value="Buscar" id="actualizarPersonal" name="nuevaInscripcion" class=" btn btn-primary" onclick="this.disabled = true;
                                        this.value = 'Buscando...';
                                        this.form.submit()"/>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2 form-group" title="Agregar un nuevo participante">
                            <label class="control-label"> &nbsp;</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <a href="#modalNuevaPersona" class="btn btn-success" data-toggle="modal"><i class="fa fa-user"></i> Nuevo</a>
                            </div>
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-1 form-group text-center">
                            &nbsp;
                        </div>

                    </form>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group text-center" title="Listado de personas, según el filtrado realizado">
                        <label class="control-label">Nombres </label>
                        <select class='select2_single form-control has-feedback-left' tabindex="-1" name='idpersona' id='idpersona' >
                            <?php
                            if ($accion == "buscar") {
                                include_once '../../system/selectPersona.php';
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <?php
                ?>
                <form class="form-horizontal form-label-left input_mask" novalidate action="registrarA" method="POST">  
                    <div class="clearfix"></div>
                    <div class="ln_solid"></div>
                    <div style="text-align: center;">
                        <h2><i><u>Información del Participante</u></i></h2>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                        <div style="text-align: center;">
                            <label class="control-label  has-feedback-left" title="Active esta opcion si el participante es Voluntario">
                                Voluntario <input type="checkbox" id="volunt" name="volunt" class="js-switch form-control" /> 
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Nombres del participante">
                        <input class="form-control has-feedback-left" id="nombres" name="nombres" placeholder="Nombres" type="text" value="<?php echo $nombre; ?>">
                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Apellidos del participante">
                        <input class="form-control has-feedback-left" id="apellidos" name="apellidos" placeholder="Apellidos" type="text" value="<?php echo $apellidos; ?>">
                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                    </div>
                    <input type="hidden" name="idpersona" id="idpersona" value="<?php echo $idpersona; ?>" >
                    <input type="hidden" name="idpais" id="idpais" value="<?php echo $idpais; ?>" >
                    <input type="hidden" name="S" id="S" value="<?php echo $sexo; ?>" >
                    <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Ingrese la Edad">
                        <input class="form-control has-feedback-left" id="edad" name="edad"placeholder="Edad" type="text" value="<?php echo $edad; ?>">
                        <span class="fa fa-circle-o-notch form-control-feedback left" aria-hidden="true"></span>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Ingrese el Nro. de Documento">
                        <input class="form-control has-feedback-left" id="nro_documento" name="nro_documento" placeholder="Nro. Documento" type="text" value="<?php echo $nro_documento; ?>">
                        <span class="fa fa-barcode form-control-feedback left" aria-hidden="true"></span>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Ingrese el número de telefono o celular">
                        <input class="form-control has-feedback-left" id="telefono" name="telefono" placeholder="Teléfono" type="text" value="<?php echo $telefono; ?>">
                        <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                    </div>
                    <div id="selectMultiple"  class="col-md-6 col-sm-6 col-xs-6 form-group text-center" title="Seleccione los cargos si es que tiene, de lo contrario dejar por defecto la opcion de participante">
                        <select class="select2_multiple form-control" multiple="multiple" name='idcargoM[]' id='idcargoM[]'>
                            <option value="17" selected>Participante</option>
                            <?php include '../../system/selectCargo.php' ?>   
                        </select>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Seleccione el Estado del participante">
                        <select class="form-control  has-feedback-left" id="idgrupo" name="idgrupo" onload="cargarGrupo(this.value)" onchange="cargarGrupo(this.value)">
                            <option value="0">Estado</option>
                            <?php include_once "../../system/selectGrupo.php"; ?>
                        </select>
                        <span class="fa fa-shield form-control-feedback left" aria-hidden="true"></span>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Éste es el grupo al que pertenece el participante">
                        <input class="form-control has-feedback-left" id="grupo" name="grupo" placeholder="Grupo" type="text" value="" readonly>
                        <span class="fa fa-users form-control-feedback left" aria-hidden="true"></span>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Seleccione la condición del participante">
                        <select class="form-control  has-feedback-left" id="idcategoria1" name="idcategoria1" >
                            <option value="0">Condición</option>
                            <?php include_once "../../system/selectCategoria.php"; ?>
                        </select>
                        <span class="fa fa-heart-o form-control-feedback left" aria-hidden="true"></span>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Seleccione la Zona del participante">
                        <select class="form-control  has-feedback-left" id="idreunion_zonal" name="idreunion_zonal" >
                            <option value="0">Zona</option>
                            <?php include_once "../../system/selectReunionZonal.php"; ?>
                        </select>
                        <span class="fa fa-bullseye form-control-feedback left" aria-hidden="true"></span>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Seleccione la Ciudad">
                        <select class="form-control  has-feedback-left" id="idciudad" name="idciudad" >
                            <option value="0">Lugar</option>
                            <?php include_once "../../system/selectCiudad.php"; ?>
                        </select>
                        <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Seleccione la talla del polo">
                        <select class="form-control  has-feedback-left" id="idtalla_polo" name="idtalla_polo" >
                            <option value="0">Talla Polo</option>
                            <?php include_once "../../system/selectTallaPolo.php"; ?>
                        </select>
                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Seleccione el estado en el que se encuentra el Abono de la Iniscipción">
                        <select class="form-control  has-feedback-left" id="idestado_pago" name="idestado_pago" >
                            <option value="0">Estado Pago</option>
                            <?php include_once "../../system/selectEstadoPago.php"; ?>
                        </select>
                        <span class="fa fa-money form-control-feedback left" aria-hidden="true"></span>
                    </div>
                    <div class="clearfix"></div>

                    <!----------------------------------------DIV DETALLE PAGO------------------------------------------------------------->
                    <div class="ln_solid"></div>
                    <div style="text-align: center;">
                        <h2><i><u>Información de Abono</u></i></h2>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3 form-group has-feedback" title="Ingrese el número de la boleta">
                        <input class="form-control has-feedback-left" id="nro_boleta" name="nro_boleta" data-validate-minmax="0,1000000000000000" placeholder="Nro. Boleta" type="text" required="">
                        <span class="fa fa-slack form-control-feedback left" aria-hidden="true"></span>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3 form-group has-feedback" title="Ingrese el monto en soles">
                        <input class="form-control has-feedback-left" id="monto_soles" name="monto_soles" data-validate-minmax="0,1000000000000000" placeholder="Monto Soles" type="text" >
                        <span class="fa form-control-feedback left" aria-hidden="true">S/.</span>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3 form-group has-feedback" title="Ingrese el monto en dólares">
                        <input class="form-control has-feedback-left" id="monto_dolares" name="monto_dolares" data-validate-minmax="0,1000000000000000" placeholder="Monto Dólares" type="text" >
                        <span class="fa fa-dollar form-control-feedback left" aria-hidden="true"></span>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3 form-group has-feedback" title="Presione para agregar los datos a la tabla">
                        <button type="button" class="btn btn-primary" onclick="agregarBoleta()" >Agregar</button>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback" title="Detalle de Abono">

                        <table id="table_boleta" class="table table-striped responsive-utilities jambo_table bulk_action">
                            <thead style="text-align: center;" >
                                <tr class="headings">
<!--                                    <th style="text-align: center;" >

                                    </th>-->
                                    <th style="text-align: center;"  class="column-title">Nro. Boleta</th>
                                    <th style="text-align: center;" class="column-title">Monto Soles (<i class="fa">S/.</i>)</th>
                                    <th style="text-align: center;" class="column-title">Monto Dólares (<i class="fa fa-dollar">)</i></th>
                                    <th style="text-align: center;" class="column-title">Gestión</th>
                                    <!--<th class="column-title no-link last"><span class="nobr">Accion</span>-->
                                    </th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                </tr>
                            </thead>
                            <tbody style="text-align: center;"  >
<!--                                <tr class="even pointer">
                                    <td class="a-center"><input type="checkbox" class="flat" name="table_records" ></td>
                                    <td class=""><?php echo $iddistrito; ?></td>
                                    <td class=""><?php echo $des_distrito; ?></td>
                                    <td class=""><?php echo $estado; ?></td>
                                    <td class="last">

                                        <form method="GET" id="exclude_form_<?php echo $id ?>" action="">
                                            <div style="float: left;" title="Eliminar <?php echo $titulo; ?>">
                                                <button type="button" class="btn btn-round btn-danger btn-sm" onclick="eliminar(<?php echo "'quitar','$des_distrito','distrito','iddistrito', $iddistrito,'listDist'" ?>);"  > 
                                                    <i class="fa fa-minus-square"></i> 
                                                </button>
                                            </div>
                                        </form>
                                    </td>
                                </tr>  -->
<!--                                <tr >
                                    <td class="a-center "></td>
                                    <td class=""><?php echo "total boletas"; ?></td>
                                    <td class=""><?php echo "total soles"; ?></td>
                                    <td class=""><?php echo "total dolares"; ?></td>
                                    <td class="last">

                                    </td>
                                </tr>-->
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                    <div class="ln_solid"></div>
                    <!---------------------------------------------------------------------------------------------------------------------->

                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback" title="Ingrese alguna observacion del participante">
                        <textarea style="resize: none;" class="form-control has-feedback-left" id="observacion" name="observacion" placeholder="Observación"></textarea>
                        <span class="fa fa-comments form-control-feedback left" aria-hidden="true"></span>
                    </div>
                    <div class="clearfix"></div>
                    <div style="text-align: center;">
                        <div class="panel-footer">
                            <a title='Presione "Cancelar" para realizar una nueva busqueda' href="newInsc?mod=nInscripcion&idpais=<?php echo $idpais; ?>&S=<?php echo $sexo; ?>&idpersona=0" class="btn btn-danger"><i class="fa fa-rotate-left"></i> Regresar</a>
                            <input title='Presione "Registrar" para guardar los datos' type="button" value="Registrar" id="actualizarPersonal" name="nuevaInscripcion" class="btn btn-primary" onclick="this.disabled = true;
                                    this.value = 'Registrando...';
                                    this.form.submit()"/>
                            <input type="hidden" name="form" value="nuevaInscripcion"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php
//include_once '../VISTAS/listaModals.php';
    ?>
    <!-- ############################################ MODAL NUEVO AGENTE ############################################ -->

    <div class="modal fade" id="modalNuevaPersona" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="form-horizontal" role="form" action="persNews" method="get">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Nueva Persona</h4>
                    </div>
                    <div class="modal-body">                        
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Condición</label>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-heart"></span></span>
                                    <select class='form-control' name='idcategoria' id='idcategoria'>
                                        <?php include "../../system/selectCategoria.php"; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        <button type="submit" id="nuevoAgente" name="nuevoAgente" class="btn btn-primary">Siguiente >> </button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="clearfix"></div>

    <script>
        function agregarBoleta() {
            //capturamos los valores
            var nro_boleta = $('#nro_boleta').val();
            var monto_soles = $('#monto_soles').val();
            var monto_dolares = $('#monto_dolares').val();

            var cat = document.getElementById("table_boleta").rows.length;
            if (nro_boleta.length >= 3) {
                $('#table_boleta').append('<tr id="' + cat + '" class="impreso"><td><input type="hidden" name="nroBoleta' + nro_boleta + '" value="' + nro_boleta + '"/>' + nro_boleta + '</td><td><input type="hidden" name="montoSoles' + nro_boleta + '" value="' + monto_soles + '"/>' + monto_soles + '</td><td><input type="hidden" name="montoDolares' + monto_dolares + '" value="' + monto_dolares + '"/>' + monto_dolares + '</td><td><button type="button" class="btn btn-danger" onclick="eliminarAgente(' + cat + ');"><i class="fa fa-minus-square"></i> Quitar</button></td></tr>');
            }

            //limiamos los textbox
            $('#nro_boleta').val("");
            $('#monto_soles').val("");
            $('#monto_dolares').val("");
        }

        function eliminarAgente(id)
        {
            $('#' + id).remove();
        }
    </script>

    <!-- switchery -->
    <script src="js/switchery/switchery.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".select2_single").select2({
                placeholder: "<?php
                                        if ($accion != "buscar") {
                                            echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class='label label-danger'>Lista vacía, porfavor realice la busqueda&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fa fa-arrow-up'></i><i class='fa fa-arrow-up'></i><i class='fa fa-arrow-up'></i></label>";
                                        } else {
                                            if ($num_regs_personas > 0) {
                                                echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class='label label-success'>Seleccionar Persona</label>";
                                            } else {
                                                echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class='label label-warning'>Agregue al Participante ó realize una nueva busqueda</label>";
                                            }
                                        }
                                        ?>",
                allowClear: true
            });
            $(".select2_group").select2({});
            $(".select2_multiple").select2({
                maximumSelectionLength: <?php echo $num_regs_cargos; ?>,
                placeholder: "Seleccione Máximo " + <?php echo $num_regs_cargos; ?> + " CARGOS",
                allowClear: true
            });
        });
    </script>
    <?php
    require('../../include/footer.php');
    ?>

    <!-- form validation -->
    <script src="js/validator/validator.js"></script>
    <script>
    // initialize the validator function
            validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
            $('form')
                    .on('blur', 'input[required], input.optional, select.required', validator.checkField)
                    .on('change', 'select.required', validator.checkField)
                    .on('keypress', 'input[required][pattern]', validator.keypress);

            $('.multi.required')
                    .on('keyup blur', 'input', function () {
                        validator.checkField.apply($(this).siblings().last()[0]);
                    });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

            $('form').submit(function (e) {
                e.preventDefault();
                var submit = true;
                // evaluate the form using generic validaing
                if (!validator.checkAll($(this))) {
                    submit = false;
                }

                if (submit)
                    this.submit();
                return false;
            });

            /* FOR DEMO ONLY */
            $('#vfields').change(function () {
                $('form').toggleClass('mode2');
            }).prop('checked', false);

            $('#alerts').change(function () {
                validator.defaults.alerts = (this.checked) ? false : true;
                if (this.checked)
                    $('form .alert').remove();
            }).prop('checked', false);
    </script>
