<?php
require('../../include/header.php');
include '../../system/mensajesAlerta.php';
include_once '../../system/crearConexion.php';
clearstatcache();
$idgrupo = $_GET['idgrupo'];
$mod = $_GET['mod'];
?>
<script src="js/ajax.js" type="text/javascript"></script>

<script>
    jQuery.datepicker.regional['eu'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'yy-mm-dd',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''};
    $.datepicker.setDefaults($.datepicker.regional['eu']);
    $(function () {
        $("#fecha_pago").datepicker();
        $("#fecha_salvacion").datepicker();
        $("#fecHasta").datepicker();
        $("#gesDesde").datepicker();
        $("#gesHasta").datepicker();
    });
</script>
<script type="text/javascript">

    function reloadPageC(idC) {
        window.location.href = "persNews?mod=nPersona&idcategoria=" + idC;
    }

    function cargarGrupo(id) {
        if (id >= 60) {
            grupo = "SILVER";
        } else if (id >= 39) {
            grupo = "SENIOR";
        } else if (id >= 31) {
            grupo = "GRACE";
        } else if (id >= 24) {
            grupo = "FAITH";
        } else if (id >= 17) {
            grupo = "TRUTH";
        } else if (id >= 11) {
            grupo = "JUNNIOR";
        } else {
            grupo = "Niños";
        }
//        switch (id) {
//            case "6":/*Adolescente */
//                grupo = "JUNIOR";
//                break;
//            case "5":/*Joven Menor */
//                grupo = "TRUTH";
//                break;
//            case "4":/* Joven Mayor */
//                grupo = "DREAM";
//                break;
//            case "1":/* Silver */
//                grupo = "SILVER";
//                break;
//            case "2":/* Senior */
//                grupo = "SENIOR";
//                break;
//            default :
//                grupo = "";
//        }
        //alert("idGrupo: " + id + " descripcion: " + grupo + " ");
        $("#grupo").val("" + grupo + "");
    }
    function validarFormPersona(boton) {
        //var button = $('#registrarPersonalInscripcion').val();
        var idgrupo = $('#idgrupo').val();
        var idciudad = $('#idciudad').val();
        var idestado_pago = $('#idestado_pago').val();
        var idpais = $('#idpais').val();
        var idestado_civil = $('#idestado_civil').val();
        if (idgrupo == '0') {
            alert('Seleccione el grupo de la persona (Silver,Casado,Joven o Adolescente) ');
            $('#idgrupo').focus();
            return false;
        } else if (idestado_civil == '0') {
            alert('Seleccione el estado civil del participante');
            $('#idestado_civil').focus();
            return false;
        } else if (idpais == '0') {
            alert('Seleccione el Pais del participante');
            $('#idpais').focus();
            return false;
        } else if (idciudad == '0') {
            alert('Seleccione la ciudad de la persona ');
            $('#idciudad').focus();
            return false;
        } else if (idestado_pago == '0') {
            alert('Seleccione el estado en el que se encuentra el pago ');
            $('#idestado_pago').focus();
            return false;
        } else if (!$("#registrarPersonal input[name='S']:radio").is(':checked')) {
            alert('Seleccione el sexo de la persona ');
            $('#S').focus();
            return false;
        } else {
            boton.disabled = true;
            boton.value = 'Registrando...';
            boton.form.submit();
            return true;
        }
    }
</script>
<!-- switchery -->
<link rel="stylesheet" href="css/switchery/switchery.min.css" />
<!-- page content -->
<div class="right_col" <?php echo $newDimensionBody; ?> role="main">
    <div class="clearfix"></div>

    <?php if ($mod == 'lInscripcion') { ?>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="col-md-2">
                        <h2>Lista de Participantes<small>registrados</small> </h2>
                    </div>
                    <div class="col-md-8">

                    </div>
                    <div class="col-md-2 hidden">
                        <a href="#modalNuevo<?php echo $hoja; ?>" class="btn btn-success" data-toggle="modal"><i class="fa fa-plus-square-o"></i> Nuevo</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table  id="example"  class="table table-striped responsive-utilities jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
    <!--                            <th>
                                    <input type="checkbox" id="check-all" class="flat">
                                </th>-->
                                <th class="column-title">Participante</th>
                                <th class="column-title">DNI</th>
                                <th class="column-title">Edad</th>
                                <th class="column-title">Sexo</th>
                                <th class="column-title">Pais</th>
                                <th class="column-title">Lugar</th>
                                <th class="column-title">Condición</th>
                                <th class="column-title">Grupo</th>
                                <th class="column-title">Voluntario</th>
                                <th class="column-title">Estado de Pago</th>
                                <th class="column-title">Edit</th>
                                <!--<th class="column-title">Gestión</th>-->
                                <!--<th class="column-title no-link last"><span class="nobr">Accion</span>-->
                                </th>
                                <th class="bulk-actions" colspan="7">
                                    <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sql = "select  idinscripciones,p.idpersona,concat(p.nombres,' ',p.apaterno,' ',p.amaterno) nombre, i.edad , i.dni, i.idestado_pago,i.idciudad, ep.descripcion estado_pago,
i.sexo, i.voluntario , i.idcategoria, cat.descripcion categoria , i.grupo, p.idpais, des_pais , p.iddistrito , ci.descripcion ciudad
from incripciones_2016 i inner join persona p on p.idpersona = i.idpersona
inner join estado_pago ep on ep.idestado_pago = i.idestado_pago
inner join categoria cat on cat.idcategoria = i.idcategoria
inner join pais pa on pa.idpais = p.idpais
inner join ciudad ci on ci.idciudad = i.idciudad
;";
                            $result = $mysqlMBN->consultas($sql);
                            $registros = mysqli_num_rows($result);
                            echo "Total de Registros: " . $registros . "<br>";
                            while ($row = mysqli_fetch_array($result)) {
                                $idinscripciones = $row['idinscripciones'];
                                $idpersona = $row['idpersona'];
                                $nombre = $row['nombre'];
                                $edad = $row['edad'];
                                $dni = $row['dni'];
                                $idestado_pago = $row['idestado_pago'];
                                $estado_pago = $row['estado_pago'];
                                $sexo = $row['sexo'];
                                $voluntario = $row['voluntario'];
                                $idcategoria = $row['idcategoria'];
                                $cagetoria = $row['categoria'];
                                $grupo = $row['grupo'];
                                $iddistrito = $row['iddistrito'];
                                $distrito = $row['ciudad'];
                                $idpais = $row['idpais'];
                                $pais = $row['des_pais'];
                                $grupo = $row['grupo'];
                                ?>
                                <tr class="even pointer">
                                    <td class=""><?php echo $nombre; ?></td>
                                    <td class=""><?php echo $dni; ?></td>
                                    <td class=""><?php echo $edad; ?></td>
                                    <td class=""><?php echo $sexo; ?></td>
                                    <td class=""><?php echo $pais; ?></td>
                                    <td class=""><?php echo $distrito; ?></td>
                                    <td class=""><?php echo $cagetoria; ?></td>
                                    <td class=""><?php echo $grupo; ?></td>
                                    <td class=""><?php
                                        if ($voluntario == 1) {
                                            echo "Si";
                                        } else {
                                            echo "No";
                                        }
                                        ?>
                                    </td>
                                    <td class=""><?php echo $estado_pago; ?></td>
                                    <td class="last">
                                        <div style="float: left;" title="Editar <?php echo $titulo; ?>"> 
                                            <a href = "listInc?mod=mInscripcion&idinscripcion=<?php echo $idinscripciones ?>" class = "btn btn-round btn-primary btn-sm " ><i class = "fa fa-pencil-square"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>                                    
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php
    } else if ($mod == 'mInscripcion') {
        $idinscripcion = $_GET['idinscripcion'];
        $sqlI = "select i.idinscripciones, p.idpersona,nombres ,
apaterno,amaterno ,p.foto,p.idtipo_documento,td.descripcion documento,
i.edad,i.idgrupo,gru.desc_camp grupo_camp,p.idpais , pa.des_pais,
i.idciudad, ci.descripcion ciudad,i.dni,i.cargo,i.sexo,i.idreunion_zonal, rz.descripcion reunion_zonal,
p.idcategoria, cat.descripcion condicion,i.celular,i.idtalla_polo , tp.descripcion talla_polo , i.grupo, i.voluntario,
i.idestado_pago , ep.descripcion estado_pago,i.observacion
from incripciones_2016 i 
inner join persona p on p.idpersona = i.idpersona 
inner join tipo_documento td on td.idtipo_documento = p.idtipo_documento
inner join categoria cat on cat.idcategoria = p.idcategoria
inner join grupos gru on gru.idgrupos = i.idgrupo
inner join ciudad  ci on ci.idciudad = i.idciudad
inner join pais pa on pa.idpais = p.idpais
inner join reunion_zonal rz on rz.idreunion_zonal = i.idreunion_zonal
inner join talla_polo tp on tp.idtalla_polo = i.idtalla_polo
inner join estado_pago ep on ep.idestado_pago = i.idestado_pago
where i.idinscripciones=$idinscripcion;";
        //echo $sqlI;
        $result = $mysqlMBN->consultas($sqlI);
        $registros = mysqli_num_rows($result);
        //echo "Total de Registros: " . $registros . "<br>";
        while ($row = mysqli_fetch_array($result)) {
            //$idinscripciones = $row['idinscripciones'];
            $idpersona = $row['idpersona'];
            $nombres = $row['nombres'];
            $aPaterno = $row['apaterno'];
            $aMaterno = $row['amaterno'];
            $foto = $row['foto'];
            $documento = $row['documento'];
            $edad = $row['edad'];
            $idgrupo = $row['idgrupo'];
            $grupo_camp = $row['grupo_camp'];
            $idpais = $row['idpais'];
            $pais = $row['des_pais'];
            $idciudad = $row['idciudad'];
            $ciudad = $row['ciudad'];
            $dni = $row['dni'];
            $cargo = $row['cargo'];
            $sexo = $row['sexo'];
            $idreunion_zona1 = $row['idreunion_zonal'];
            $reunion_zonal = $row['reunion_zonal'];
            $idcategoria = $row['idcategoria'];
            $condicion = $row['condicion'];
            $celular = $row['celular'];
            $idtalla_polo = $row['idtalla_polo'];
            $talla_polo = $row['talla_polo'];
            $grupo = $row['grupo'];
            $voluntario = $row['voluntario'];
            $idestado_pago = $row['idestado_pago'];
            $estado_pago = $row['estado_pago'];
            $observacion = $row['observacion'];
        }
        ?>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="col-md-2">
                        <h2>Detalle de la inscripción<small>para actualizar</small> </h2>
                    </div>
                    <div class="col-md-8">

                    </div>
                    <div class="col-md-2 hidden">
                        <a href="#modalNuevo<?php echo $hoja; ?>" class="btn btn-success" data-toggle="modal"><i class="fa fa-plus-square-o"></i> Nuevo</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form class="form-horizontal form-label-left input_mask" novalidate action="modificarB" method="POST">  
                        <div style="text-align: center;">
                            <h2><i><u>Información del Participante</u></i></h2>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                            <div style="text-align: center;">
                                <label class="control-label  has-feedback-left" title="Active esta opcion si el participante es Voluntario">
                                    Voluntario <input type="checkbox" id="volunt" name="volunt" class="js-switch form-control" <?php
                                    if ($voluntario == 1) {
                                        echo "checked";
                                    }
                                    ?> /> 
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Nombres del participante">
                            <input class="form-control has-feedback-left" id="nombres" name="nombres" placeholder="Nombres" type="text" value="<?php echo $nombres; ?>" >
                            <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Sexo del participante">
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Sexo</label>

                                <div class="radio-inline">
                                    <label>
                                        <input class="flat" type="radio" <?php
                                        if ($sexo == "M") {
                                            echo "checked";
                                        }
                                        ?> value="M" id="S" name="S"> M
                                    </label>
                                </div>
                                <div class="radio-inline">
                                    <label>
                                        <input class="flat" type="radio" <?php
                                        if ($sexo == "F") {
                                            echo "checked";
                                        }
                                        ?> value="F" id="S" name="S"> F
                                    </label>
                                </div> 
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Apellidos del participante">
                            <input class="form-control has-feedback-left" id="aPaterno" name="aPaterno" placeholder="Apellido Paterno" type="text" value="<?php echo $aPaterno; ?>" >
                            <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Apellidos del participante">
                            <input class="form-control has-feedback-left" id="aMaterno" name="aMaterno" placeholder="Apellido Materno" type="text" value="<?php echo $aMaterno; ?>" >
                            <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <input type="hidden" name="condicionMod" id="condicionMod" value="<?php echo $_GET['mod']; ?>" >
                        <input type="hidden" name="idpersona" id="idpersona" value="<?php echo $idpersona; ?>" >
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Ingrese la Edad">
                            <input class="form-control has-feedback-left" id="edad" name="edad"placeholder="Edad" type="text" value="<?php echo $edad; ?>" onkeyup="cargarGrupo(this.value)">
                            <span class="fa fa-circle-o-notch form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Ingrese el Nro. de Documento">
                            <input class="form-control has-feedback-left" id="nro_documento" name="nro_documento" placeholder="Nro. Documento" type="text" value="<?php echo $dni; ?>">
                            <span class="fa fa-barcode form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Ingrese el número de telefono o celular">
                            <input class="form-control has-feedback-left" id="telefono" name="telefono" placeholder="Teléfono" type="text" value="<?php echo $celular; ?>">
                            <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div id="selectMultiple"  class="col-md-6 col-sm-6 col-xs-6 form-group text-center" title="Seleccione los cargos si es que tiene, de lo contrario dejar por defecto la opcion de participante">
                            <div class="item form-group">
                                <select class="select2_multiple form-control" multiple="multiple" name='idcargoM[]' id='idcargoM[]'>
                                    <!--<option value="17" selected>Participante</option>-->
                                    <?php include '../../system/selectCargo.php' ?>   
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Seleccione el Estado del participante">
                            <select class="form-control  has-feedback-left" id="idgrupo" name="idgrupo" onload="cargarGrupo(this.value)" onchange="cargarGrupo(this.value)">
                                <option value="0">Estado</option>
                                <?php include_once "../../system/selectGrupo.php"; ?>
                            </select>
                            <span class="fa fa-shield form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Éste es el grupo al que pertenece el participante">
                            <input class="form-control has-feedback-left" id="grupo" name="grupo" placeholder="Grupo" type="text" value="<?php echo $grupo; ?>" readonly>
                            <span class="fa fa-users form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Seleccione la condición del participante">
                            <select class="form-control  has-feedback-left" id="idcategoria1" name="idcategoria1" >
                                <option value="0">Condición</option>
                                <?php include_once "../../system/selectCategoria.php"; ?>
                            </select>
                            <span class="fa fa-heart-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Seleccione el pais">
                            <select class="form-control  has-feedback-left" id="idpais" name="idpais" onchange="load_departamento(this.value)">
                                <option value="0">Pais</option>
                                <?php include_once "../../system/selectPais.php"; ?>
                            </select>
                            <span class="fa fa-flag form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" id="myDivReuZon" title="Seleccione la Zona del participante">
                            <select class="form-control  has-feedback-left" id="idreunion_zonal" name="idreunion_zonal" >
                                <option value="0">Zona</option>
                                <?php include_once "../../system/selectReunionZonal.php"; ?>
                            </select>
                            <span class="fa fa-bullseye form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback"  id="myDivCiudad"  title="Seleccione la Ciudad">
                            <select class="form-control  has-feedback-left" id="idciudad" name="idciudad" >
                                <option value="0">Lugar</option>
                                <?php include_once "../../system/selectCiudad.php"; ?>
                            </select>
                            <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Seleccione la talla del polo">
                            <select class="form-control  has-feedback-left" id="idtalla_polo" name="idtalla_polo" >
                                <option value="0">Talla Polo</option>
                                <?php include_once "../../system/selectTallaPolo.php"; ?>
                            </select>
                            <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Seleccione el estado en el que se encuentra el Abono de la Iniscipción">
                            <select class="form-control  has-feedback-left" id="idestado_pago" name="idestado_pago" >
                                <option value="0">Estado Pago</option>
                                <?php include_once "../../system/selectEstadoPago.php"; ?>
                            </select>
                            <span class="fa fa-money form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="clearfix"></div>

                        <!----------------------------------------DIV DETALLE PAGO------------------------------------------------------------->
                        <div class="ln_solid"></div>
                        <div style="text-align: center;">
                            <h2><i><u>Información de Donación</u></i></h2>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3 form-group has-feedback" title="Ingrese la fecha en que se realizó la donación">
                            <input class="form-control has-feedback-left" id="fecha_pago" name="fecha_pago" placeholder="Fecha Donación" type="text" required="" readonly="">
                            <span class="fa fa-calendar form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2 form-group has-feedback" title="Ingrese el número de la boleta">
                            <input class="form-control has-feedback-left" id="nro_boleta" name="nro_boleta" data-validate-minmax="0,1000000000000000" placeholder="Nro. Boleta" type="text" required="">
                            <span class="fa fa-slack form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2 form-group has-feedback" title="Ingrese el monto en soles">
                            <input class="form-control has-feedback-left" id="monto_soles" name="monto_soles" data-validate-minmax="0,1000000000000000" placeholder="Soles" type="text" >
                            <span class="fa form-control-feedback left" aria-hidden="true">S/.</span>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2 form-group has-feedback" title="Ingrese el monto en dólares">
                            <input class="form-control has-feedback-left" id="monto_dolares" name="monto_dolares" data-validate-minmax="0,1000000000000000" placeholder="Dólares" type="text" >
                            <span class="fa fa-dollar form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2 form-group has-feedback" title="Presione para agregar los datos a la tabla">
                            <button type="button" class="btn btn-primary" onclick="agregarBoleta()" >Agregar</button>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback" title="Detalle de Abono">

                            <table id="table_boleta" class="table table-striped responsive-utilities jambo_table bulk_action">
                                <thead style="text-align: center;" >
                                    <tr class="headings">
    <!--                                    <th style="text-align: center;" >

                                        </th>-->
                                        <th style="text-align: center;"  class="column-title">Fecha Donación</th>
                                        <th style="text-align: center;"  class="column-title">Nro. Boleta</th>
                                        <th style="text-align: center;" class="column-title">Monto Soles (<i class="fa">S/.</i>)</th>
                                        <th style="text-align: center;" class="column-title">Monto Dólares (<i class="fa fa-dollar">)</i></th>
                                        <th style="text-align: center;" class="column-title">Gestión</th>
                                        <!--<th class="column-title no-link last"><span class="nobr">Accion</span>-->
                                        </th>
                                        <th class="bulk-actions" colspan="7">
                                            <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody style="text-align: center;"  >
                                    <?php
                                    $x = 1;
                                    $sqlBol = "select * from boleta_2016 where idinscripcion = $idinscripcion";
                                    $resB = $mysqlMBN->consultas($sqlBol);
                                    while ($rowB = mysqli_fetch_array($resB)) {
                                        $idboleta = $rowB['idboleta'];
                                        $fecha_pago = $rowB['fecha_pago'];
                                        $nroBoleta = $rowB['nro_boleta'];
                                        $montoSoles = $rowB['monto_soles'];
                                        $montoDolares = $rowB['monto_dolares'];
                                        ?>
                                        <tr id="<?php echo $x; ?>" class="impreso">
                                            <td><input type="hidden" name="fechaPago<?php echo $nroBoleta; ?>" value="<?php echo $fecha_pago; ?>"/><?php echo $fecha_pago; ?></td>
                                            <td><input type="hidden" name="nroBoleta<?php echo $nroBoleta; ?>" value="<?php echo $nroBoleta; ?>"/><?php echo $nroBoleta; ?></td>
                                            <td><input type="hidden" name="montoSoles<?php echo $nroBoleta; ?>" value="<?php echo $montoSoles; ?>"/>S/. <?php echo $montoSoles; ?></td>
                                            <td><input type="hidden" name="montoDolares<?php echo $nroBoleta; ?>" value="<?php echo $montoDolares; ?>"/>$. <?php echo $montoDolares; ?></td>
                                            <td><button type="button" class="btn btn-danger" onclick="eliminarAgente(<?php echo $x; ?>);"><i class="fa fa-minus-square"></i> Quitar</button></td>
                                        </tr>
                                        <?php
                                        $x++;
                                    }
                                    ?>
    <!--                                <tr class="even pointer">
    <td class="a-center"><input type="checkbox" class="flat" name="table_records" ></td>
    <td class=""><?php echo $iddistrito; ?></td>
    <td class=""><?php echo $des_distrito; ?></td>
    <td class=""><?php echo $estado; ?></td>
    <td class="last">

    <form method="GET" id="exclude_form_<?php echo $id ?>" action="">
    <div style="float: left;" title="Eliminar <?php echo $titulo; ?>">
    <button type="button" class="btn btn-round btn-danger btn-sm" onclick="eliminar(<?php echo "'quitar','$des_distrito','distrito','iddistrito', $iddistrito,'listDist'" ?>);"  > 
    <i class="fa fa-minus-square"></i> 
    </button>
    </div>
    </form>
    </td>
    </tr>  -->
    <!--                                <tr >
    <td class="a-center "></td>
    <td class=""><?php echo "total boletas"; ?></td>
    <td class=""><?php echo "total soles"; ?></td>
    <td class=""><?php echo "total dolares"; ?></td>
    <td class="last">

    </td>
    </tr>-->
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ln_solid"></div>
                        <!---------------------------------------------------------------------------------------------------------------------->

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback" title="Ingrese alguna observacion del participante">
                            <textarea style="resize: none;" class="form-control has-feedback-left" id="observacion" name="observacion" placeholder="Observación"><?php echo $observacion; ?></textarea>
                            <span class="fa fa-comments form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="clearfix"></div>
                        <div style="text-align: center;">
                            <div class="panel-footer">
                                <a title='Presione "Cancelar" para realizar una nueva busqueda' href="listPers?mod=lPersonas&idpais=<?php echo $idpais; ?>" class="btn btn-danger"><i class="fa fa-rotate-left"></i> Regresar</a>
                                <input title='Presione "Registrar" para guardar los datos' type="button" value="Registrar" id="actualizarPersonal" name="nuevaInscripcion" class="btn btn-primary" onclick="this.disabled = true;
                                            this.value = 'Registrando...';
                                            this.form.submit()"/>
                                <input type="hidden" name="form" value="modificarInscripcion"/>
                                <input type="hidden" name="idinscripcion" value="<?php echo $_GET['idinscripcion']; ?>"/>
                                <input type="hidden" name="mod" value="<?php echo $_GET['mod']; ?>"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="clearfix"></div>

    <script>
        function agregarBoleta() {
            //capturamos los valores
            var fecha_pago = $('#fecha_pago').val();
            var nro_boleta = $('#nro_boleta').val();
            var monto_soles = $('#monto_soles').val();
            var monto_dolares = $('#monto_dolares').val();

            var cat = document.getElementById("table_boleta").rows.length;
            if (nro_boleta.length >= 3) {
                $('#table_boleta').append('<tr id="' + cat + '" class="impreso"><td><input type="hidden" name="fechaPago' + fecha_pago + '" value="' + fecha_pago + '"/>' + fecha_pago + '</td><td><input type="hidden" name="nroBoleta' + nro_boleta + '" value="' + nro_boleta + '"/>' + nro_boleta + '</td><td><input type="hidden" name="montoSoles' + nro_boleta + '" value="' + monto_soles + '"/>S/. ' + monto_soles + '</td><td><input type="hidden" name="montoDolares' + monto_dolares + '" value="' + monto_dolares + '"/>$. ' + monto_dolares + '</td><td><button type="button" class="btn btn-danger" onclick="eliminarAgente(' + cat + ');"><i class="fa fa-minus-square"></i> Quitar</button></td></tr>');
            }

            //limiamos los textbox
            $('#nro_boleta').val("");
            $('#monto_soles').val("");
            $('#monto_dolares').val("");
        }

        function eliminarAgente(id)
        {
            $('#' + id).remove();
        }
    </script>

    <!-- switchery -->
    <script src="js/switchery/switchery.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".select2_group").select2({});
            $(".select2_multiple").select2({
                maximumSelectionLength: <?php echo $num_regs_cargos; ?>,
                placeholder: "Seleccione Máximo " + <?php echo $num_regs_cargos; ?> + " CARGOS",
                allowClear: true
            });
        });
    </script>
    <?php
    require('../../include/footer.php');
    ?>

