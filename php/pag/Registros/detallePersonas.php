 
<?php
require('../../include/header.php');
include '../../system/mensajesAlerta.php';
include_once '../../system/crearConexion.php';
clearstatcache();

$idpersona = $_GET['idpersona'];
$mod = $_GET['mod'];
$mod_a = $_GET['mod_a'];
$sql = " select p.idpersona,p.nombres, p.apaterno, p.amaterno, p.idtipo_documento, p.nro_documento, p.sexo, p.referencia,
 p.telefono_fijo, p.celular, p.lugar_nacimiento, p.fec_nac,p.email,p.profesion,p.ocupacion, p.idcategoria,p.idgrupos,
 p.iddistrito, di.idprovincia, pv.iddepartamento,de.idpais,
 p.direccion,  des_departamento,des_distrito,des_pais,des_provincia,
 p.fecha_registro,p.foto, p.idestado_civil , cat.descripcion categoria , 
 p.idsede,se.descripcion sede,
 YEAR(CURDATE())-YEAR(p.fec_nac) + IF(DATE_FORMAT(CURDATE(),'%m-%d') > DATE_FORMAT(p.fec_nac,'%m-%d'), 0, -1) edad 
 from persona p 
 inner join categoria cat on cat.idcategoria = p.idcategoria 
 inner join distrito di on di.iddistrito=p.iddistrito
 inner join provincia pv on pv.idprovincia = di.idprovincia
 inner join departamento de on de.iddepartamento = pv.iddepartamento
 inner join pais pa on pa.idpais = de.idpais
 inner join sede se on se.idsede = p.idsede
 where idpersona =$idpersona ;";
$res = $mysqlMBN->consultas($sql);
while ($row = mysqli_fetch_array($res)) {
    $nombres = $row['nombres'];
    $apaterno = $row['apaterno'];
    $amaterno = $row['amaterno'];
    $nombre = "$nombres $apaterno $amaterno";
    $idtipo_documento = $row['idtipo_documento'];
    $nro_documento = $row['nro_documento'];
    $sexo = $row['sexo'];
    $telefono_fijo = $row['telefono_fijo'];
    $celular = $row['celular'];
    $lugar_nacimiento = $row['lugar_nacimiento'];
    $fec_nac = $row['fec_nac'];
    $edad = $row['edad'];
    $email = $row['email'];
    $profesion = $row['profesion'];
    $ocupacion = $row['ocupacion'];
    $idcategoria = $row['idcategoria'];
    $referencia = $row['referencia'];
    $categoria = $row['categoria'];
    $iddistrito = $row['iddistrito'];
    $idprovincia = $row['idprovincia'];
    $iddepartamento = $row['iddepartamento'];
    $idpais = $row['idpais'];
    $direccion = $row['direccion'];
    $fecha_registro = $row['fecha_registro'];
    $idestado_civil = $row['idestado_civil'];
    $idgrupo = $row['idgrupos'];
    $des_departamento = $row['des_departamento'];
    $des_distrito = $row['des_distrito'];
    $des_pais = $row['des_pais'];
    $des_provincia = $row['des_provincia'];
    $idsede = $row['idsede'];
    $sede = $row['sede'];
    $lugar = "$des_departamento,$des_provincia,$des_distrito";
////    $=$row[''];
    //   $=$row[''];
//    $=$row[''];
    $foto = $row['foto'];
}
?>

<script src="js/ajax.js" type="text/javascript"></script>
<script src="js/bootstrap-datepicker.min.js" type="text/javascript"></script>


<script type="text/javascript">
    function validarFormPersona(boton) {
        //var button = $('#registrarPersonalInscripcion').val();
        var idgrupo = $('#idgrupo').val();
        var iddepartamento = $('#iddepartamento').val();
        var idprovincia = $('#idprovincia').val();
        var iddistrito = $('#iddistrito').val();
        var idpais = $('#idpais').val();
        var idsede = $('#idsede').val();
        var idcargo = $('#idcargo').val();
        var idestado_civil = $('#idestado_civil').val();
        if (idgrupo == '0') {
            alert('Seleccione el grupo de la persona (Silver,Casado,Joven o Adolescente) ');
            $('#idgrupo').focus();
            return false;
        } else if (idestado_civil == '0') {
            alert('Seleccione el estado civil del participante');
            $('#idestado_civil').focus();
            return false;
        } else if (idpais == '0') {
            alert('Seleccione el Pais del participante');
            $('#idpais').focus();
            return false;
        } else if (iddepartamento == '0') {
            alert('Seleccione el Departamento de la persona ');
            $('#iddepartamento').focus();
            return false;
        } else if (idprovincia == '0') {
            alert('Seleccione la Provincia de la persona ');
            $('#idprovincia').focus();
            return false;
        } else if (iddistrito == '0') {
            alert('Seleccione el Distrito de la persona ');
            $('#iddistrito').focus();
            return false;
        } else if (idsede == '0') {
            alert('Seleccione la SEDE a la que pertenece persona ');
            $('#idsede').focus();
            return false;
        } else if (idcargo == '0') {
            alert('Seleccione el Cargo de la persona ');
            $('#iddistrito').focus();
            return false;
        } else if (!$("#registrarPersonal input[name='S']:radio").is(':checked')) {
            alert('Seleccione el sexo de la persona ');
            $('#S').focus();
            return false;
        } else {
            boton.disabled = true;
            boton.value = 'Actualizando...';
            boton.form.submit();
            return true;
        }
    }
</script>

<script>
    $(function () {
        // Obtener la referencia a la lista
        var lista = document.getElementById("iddepartamento");
        var lista1 = document.getElementById("idprovincia");
        // Obtener el índice de la opción que se ha seleccionado
        var indiceSeleccionado = lista.selectedIndex;
        var indiceSeleccionado1 = lista1.selectedIndex;
        // Con el índice y el array "options", obtener la opción seleccionada
        var opcionSeleccionada = lista.options[indiceSeleccionado];
        var opcionSeleccionada1 = lista1.options[indiceSeleccionado1];
        // Obtener el valor y el texto de la opción seleccionada
        //var textoSeleccionado = opcionSeleccionada.text;
        var valorSeleccionado = opcionSeleccionada.value;
        var valorSeleccionado1 = opcionSeleccionada1.value;
        //load_provincia(valorSeleccionado);
        //load_distrito(valorSeleccionado1); 

    });
</script>
<!-- page content -->
<div class="right_col" role="main">
    <div class="col-md-12">        
        <?php //echo $sql;   ?>
    </div>


    <div class="col-md-8 col-sm-12 col-xs-7">
        <form class="form-horizontal form-label-left" id="registrarPersonal" name="registrarPersonal" method="post" action="modificarB">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Datos Personales</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Nombre</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="nombre">
                                    <input <?php
                                    if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                        echo "disabled";
                                    }
                                    ?>  name="nombre" id="nombre" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="300" autocomplete="off" type="text" value="<?php echo "$nombres"; ?>">
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Condición</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="cliente">
                                    <input name="condicion" id="condicion" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="300" autocomplete="off" type="text" value="<?php echo "$categoria"; ?>" readonly="">
                                </div>
                                <?php
                                if ($_SESSION['id_nivel'] == 1 || $_SESSION['id_nivel'] == 2) {
                                    if ($idcategoria == 2) {
                                        ?>
                                        <div class="col-md-7 col-sm-7 col-xs-7" >

                                        </div>
                                        <div class="col-md-5 col-sm-5 col-xs-5" id="nuevoGrupo" title="Agregar Nueva Condición" style="text-align: center;"> 

                                            <!--                                            <a href="../../php/doc/ayuda.php" target="_blank" onclick="window.open(this.href, this.target, 'width=1100,height=700,top=0,left=700,resizable=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes');
                                                                                                        return false;" class="label label-danger">Cambiar Condición</a>-->
                                            <a href="#cambiarCondicion" data-toggle="modal" class="label label-danger">Cambiar Condición</a>

                                        </div>

                                        <?php
                                    }
                                }
                                ?>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">A. Paterno</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="aPaterno">
                                    <input <?php
                                    if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                        echo "disabled";
                                    }
                                    ?> name="aPaterno" id="aPaterno" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="300" autocomplete="off" type="text"  value="<?php echo $apaterno; ?>">
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">A. Materno</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="aMaterno">
                                    <input <?php
                                    if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                        echo "disabled";
                                    }
                                    ?> name="aMaterno" id="aMaterno" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="300" autocomplete="off" type="text"  value="<?php echo $amaterno; ?>">
                                </div>
                            </div> 
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Tipo Documento</label>
                                <div class="col-md-3 col-sm-3 col-xs-3" id="aPaterno">
                                    <select class='form-control' name='idtipo_documento' id='idtipo_documento' <?php
                                    if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                        echo "disabled";
                                    }
                                    ?>>
                                                <?php include_once "../../system/selectTipoDocumento.php"; ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Tipo de Documento">
                                    <a href="#modalNuevoTipoDocumento" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Número Doc.</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="dni">
                                    <input <?php
                                    if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                        echo "disabled";
                                    }
                                    ?> name="dni" id="dni" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="off" type="text" value="<?php echo "$nro_documento"; ?>">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Sexo</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="aPaterno">
                                    <div class="radio-inline">
                                        <label>
                                            <input class="flat" type="radio" <?php
                                            if ($sexo == "M") {
                                                echo "checked";
                                            }
                                            ?> value="M" id="S" name="S"> M
                                        </label>
                                    </div>
                                    <div class="radio-inline">
                                        <label>
                                            <input class="flat" type="radio" <?php
                                            if ($sexo == "F") {
                                                echo "checked";
                                            }
                                            ?> value="F" id="S" name="S"> F
                                        </label>
                                    </div> 
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Estado Civil</label>
                                <div class="col-md-3 col-sm-3 col-xs-3" id="aPaterno">
                                    <select class='form-control' name='idestado_civil' id='idestado_civil' <?php
                                    if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                        echo "disabled";
                                    }
                                    ?>>
                                        <option>Seleccione</option>
                                        <?php include_once "../../system/selectEstadoCivil.php"; ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Estado Civil">
                                    <a href="#modalNuevoEstadoCivil" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Fecha Nac.</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="aPaterno">
                                    <!--<input type="date" id="sdate" name="sdate" rowid="1" role="textbox" class="editable inline-edit-cell ui-widget-content ui-corner-all" style="width: 96%;" value="<?php echo $fec_nac; ?>">-->
                                    <input <?php
                                    if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                        echo "disabled";
                                    }
                                    ?> type="text" id="fecha_nac1"  name="fecha_nac1"  class="form-control" data-inputmask="'mask': '9999-99-99'" value="<?php echo $fec_nac; ?>">
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Edad</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="aPaterno">
                                    <input <?php
                                    if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                        echo "disabled";
                                    }
                                    ?> name="edad" id="edad" class="form-control col-md-3 col-xs-12" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="off" type="text"  value="<?php echo $edad; ?>" readonly="">
                                </div>
                            </div>

                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Lugar Nac.</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="aPaterno">
                                    <input <?php
                                    if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                        echo "disabled";
                                    }
                                    ?> name="lugar_nac" id="lugar_nac" class="form-control col-md-3 col-xs-12" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="off" type="text" value="<?php echo $lugar_nacimiento; ?>">
                                </div>

                                <?php if ($idcategoria == 2) { ?>

                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Grupo </label>
                                    <div class="col-md-3 col-sm-3 col-xs-3" title="Asignado para las reuniones Grupales">
                                        <select class='form-control' name='idgrupo' id='idgrupo' >
                                            <option value="0">Seleccione</option>
                                            <?php include_once "../../system/selectGrupo.php"; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nueva Reunion Grupal">
                                        <a href="#modalNuevoGrupos" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Profesión</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="aPaterno">
                                    <input <?php
                                    if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                        echo "disabled";
                                    }
                                    ?> name="profesion" id="profesion" class="form-control col-md-3 col-xs-12" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="off" type="text" value="<?php echo $profesion; ?>">
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Trabajo Actual</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="aPaterno">
                                    <input <?php
                                    if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                        echo "disabled";
                                    }
                                    ?> name="ocupacion" id="ocupacion" class="form-control col-md-3 col-xs-12" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="off" type="text" value="<?php echo $ocupacion; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Contactabilidad</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Teléfono</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="aPaterno">
                                    <input <?php
                                    if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                        echo "disabled";
                                    }
                                    ?> name="telefono" id="telefono" class="form-control col-md-3 col-xs-12" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="off" type="text"  value="<?php echo $telefono_fijo; ?>">
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Celular</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="aPaterno">
                                    <input <?php
                                    if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                        echo "disabled";
                                    }
                                    ?> name="celular" id="celular" class="form-control col-md-3 col-xs-12" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="off" type="text"  value="<?php echo $celular; ?>">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Email</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="cliente">
                                    <input <?php
                                    if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                        echo "disabled";
                                    }
                                    ?> type="text" id="email" name="email" class="form-control col-md-7 col-sm-7 col-xs-7" value="<?php echo "$email"; ?>">
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Hno. de Referencia</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="cliente">
                                    <input <?php
                                    if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                        echo "disabled";
                                    }
                                    ?> type="text" id="referencia" name="referencia" class="form-control col-md-7 col-sm-7 col-xs-7" value="<?php echo $referencia; ?>" >
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Datos de Ubicación</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Pais  <?php //echo $iddepartamento;                                                                                                                                                                       ?></label>
                                <div class="col-md-3 col-sm-3 col-xs-3" id="cliente">
                                    <select class='form-control' name='idpais' id='idpais' onchange="load_departamento(this.value)" <?php
                                    if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                        echo "disabled";
                                    }
                                    ?> >
                                                <?php include_once "../../system/selectPais.php"; ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Pais">
                                    <a href="#modalNuevoPais" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Departamento  <?php //echo $iddepartamento;                                                                                                                                                                        ?></label>
                                <div class="col-md-3 col-sm-3 col-xs-3" id="myDivDep">
                                    <select class='form-control' name='iddepartamento' id='iddepartamento' onchange="load_provincia(this.value)"  <?php
                                    if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                        echo "disabled";
                                    }
                                    ?>>
                                                <?php include_once "../../system/selectDepartamento.php"; ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Departamento">
                                    <a href="#modalNuevoDepartamentos" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Provincia  <?php // echo $idprovincia;                                                                                                                                                                      ?></label>
                                <div class="col-md-3 col-sm-3 col-xs-3" id="myDivProv">
                                    <select class='form-control' name='idprovincia' id='idprovincia' onchange="load_distrito(this.value)"  <?php
                                    if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                        echo "disabled";
                                    }
                                    ?>>
                                                <?php include_once "../../system/selectProvincia.php"; ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nueva Provincia">
                                    <a href="#modalNuevoProvincias" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Distrito <?php // echo $iddistrito;                                                                                                                                                                      ?></label>
                                <div class="col-md-3 col-sm-3 col-xs-3" id="myDivDist">
                                    <select class='form-control' name='iddistrito' id='iddistrito' <?php
                                    if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                        echo "disabled";
                                    }
                                    ?>>
                                                <?php include_once "../../system/selectDistrito.php"; ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Distrito">
                                    <a href="#modalNuevoDistritos" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Dirección</label>
                                <div class="col-md-10 col-sm-6 col-xs-12" id="">
                                    <input <?php
                                    if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                        echo "disabled";
                                    }
                                    ?> type="text" id="direccion" name="direccion" required="required" placeholder="" class="form-control col-md-7 col-sm-7 col-xs-7" value="<?php echo "$direccion"; ?>">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2"> SEDE <?php // echo $iddistrito;                                                                                                                                                                      ?></label>
                                <div class="col-md-3 col-sm-3 col-xs-3" id="myDivSede">
                                    <select class='form-control' name='idsede' id='idsede' <?php
                                    if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                        echo "disabled";
                                    }
                                    ?>>
                                        <option value="0">Seleccione</option>
                                        <?php include_once "../../system/selectSede.php"; ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nueva Sede">
                                    <a href="#modalNuevaSede" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            if ($idcategoria == 1) {
                $sqry = "select * from detalle_salvacion where idpersona = $idpersona;";
                $res1 = $mysqlMBN->consultas($sqry);
                $row = mysqli_fetch_array($res1);
                $idcargos = $row['idcargo'];
                if ($idcargos != '') {
                    $sqry2 = "select * from cargo where idcargo in ($idcargos);";
                    $res2 = $mysqlMBN->consultas($sqry2);
                    while ($row2 = mysqli_fetch_array($res2)) {
                        $des_total .= $row2['descripcion'] . ",";
                    }
                    $des_cargo = substr($des_total, 0, -1);

                    $sqlDS = "select * from detalle_salvacion where idpersona = " . $_GET['idpersona'];
                    $resDS = $mysqlMBN->consultas($sqlDS);
                    while ($row1 = mysqli_fetch_array($resDS)) {
                        $fecha_salvacion = $row1['fecha_salvacion'];
                        $referencia = $row1['referencia'];
                        $idcargo = $row1['idcargo'];
                        $idreunion_granzona1 = $row1['idreunion_granzona'];
                        $idreunion_zona1 = $row1['idreunion_zonal'];
                    }
                }
                ?>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Datos de Miembro</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="item form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Fec. Salvación<?php //echo $iddepartamento;                                                                                                                                                                        ?></label>
                                    <div class="col-md-4 col-sm-4 col-xs-4" id="aPaterno">
                                        <!--<input name="fecha_salvacion" id="fecha_salvacion" class="form-control col-md-3 col-xs-12" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="off" type="text"  value="<?php echo $fecha_salvacion; ?>" readonly="">-->
                                        <input <?php
                                        if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                            echo "disabled";
                                        }
                                        ?> type="text" id="fecha_salvacion1"  name="fecha_salvacion1"  class="form-control" data-inputmask="'mask': '9999-99-99'" value="<?php echo $fecha_salvacion; ?>">
                                    </div>
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Predicó  <?php //echo $iddepartamento;                                                                                                                                                                        ?></label>
                                    <div class="col-md-4 col-sm-4 col-xs-4" id="aPaterno">
                                        <input <?php
                                        if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                            echo "disabled";
                                        }
                                        ?> name="s_referencia" id="s_referencia" class="form-control col-md-3 col-xs-12" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="off" type="text"  value="<?php echo $referencia; ?>">
                                    </div>
                                </div>
                                <div id="selectMultiple"  class="item form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2" title="Puerto de salida">Cargo (s) </label>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <select <?php
                                        if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                            echo "disabled";
                                        }
                                        ?> class="select2_multiple form-control" multiple="multiple" name='idcargoM[]' id='idcargoM[]'>
                                            <?php include '../../system/selectCargo.php' ?>   
                                        </select>
                                    </div> 
                                    <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Cargo">
                                        <a href="#modalNuevoCargos" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Gran Zona </label>
                                    <div class="col-md-3 col-sm-3 col-xs-3" id="myDivProv">
                                        <select <?php
                                        if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                            echo "disabled";
                                        }
                                        ?> class='form-control' name='idreunion_granzona' id='idreunion_granzona' onchange="load_reunion_zonal(this.value)" >
                                            <option>Seleccione</option>
                                            <?php include_once "../../system/selectReunionGranZona.php"; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nueva Gran Zona">
                                        <a href="#modalNuevoreuGranZon" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                    </div>
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Reunion Zonal </label>
                                    <div class="col-md-3 col-sm-3 col-xs-3" id="myDivReuZon"  title="Asignado para las reuniones Zonales">
                                        <select <?php
                                        if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                            echo "disabled";
                                        }
                                        ?> class='form-control' name='idreunion_zonal' id='idreunion_zonal'>
                                            <?php include_once "../../system/selectReunionZonal.php"; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nueva Reunion Zonal">
                                        <a href="#modalNuevoreuZonal" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Grupo </label>
                                    <div class="col-md-3 col-sm-3 col-xs-3" title="Asignado para las Reuniones Grupales">
                                        <select <?php
                                        if ($_SESSION['id_nivel'] == 4 || $_SESSION['id_nivel'] == 3) {
                                            echo "disabled";
                                        }
                                        ?> class='form-control' name='idgrupo' id='idgrupo'  >
                                            <option>Seleccione</option>
                                            <?php include_once "../../system/selectGrupo.php"; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nueva Reunion Grupal">
                                        <a href="#modalNuevoGrupos" data-toggle="modal" class = "btn btn-round btn-success btn-sm " ><i class = "fa fa-plus-square"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div style="text-align: center;">
                <!-- listAgndPer?mod=lPersonas&idpais=89&idsede=$idsede&idgrupo=2&idcategoria=2&S=M  -->
                <a href="listAgndPer?mod=<?php echo $mod_a; ?>&idpais=<?php echo $idpais; ?>&idsede=<?php echo $idsede; ?>&idgrupo=<?php echo $idgrupo; ?>&idcategoria=<?php echo $idcategoria; ?>&S=<?php echo $sexo; ?>" class="btn btn-danger"><i class="fa fa-rotate-left"></i> Regresar</a>
                <?php if ($_SESSION['id_nivel'] == 1 || $_SESSION['id_nivel'] == 2) { ?> 
                    <input type="button" value="Actualizar" id="actualizarPersonal" name="actualizarPersonal" class="btn btn-primary" onclick="validarFormPersona(this)"/>
                <?php } ?>
                <input type="hidden" name="form" value="modificarPersona"/>
                <input type="hidden" name="idcategoria" value="<?php echo $idcategoria; ?>"/>
                <input type="hidden" name="mod" value="<?php echo $_GET['mod']; ?>"/>
                <input type="hidden" name="idpersona" value="<?php echo $_GET['idpersona']; ?>"/>
            </div>
        </form>
    </div>
    <br><br><br>
    <div class="col-md-4 col-sm-5 col-xs-4">
        <div class="x_panel">
            <div class="x_title">
                <h2>Actualizar Datos</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-12 col-sm-12 col-xs-12 profile_left">
                    <?php
                    if (isset($_GET['msj'])) {
                        echo "<div class='label label-warning'>$mensaje</div>";
                    }
                    ?>   
                    <div class="profile_img">

                        <!-- end of image cropping -->
                        <div id="crop-avatar">
                            <!-- Current avatar -->
                            <div  data-original-title="<?php
                            if ($_SESSION['id_nivel'] == '1' || isset($_GET['perfil'])) {
                                echo "Cambiar Imagen";
                            }
                            ?>" class="avatar-view" title="">
                                <img src="images/Fotos/<?php echo $foto; ?>" alt="Avatar">
                            </div>
                            <?php if ($_SESSION['id_nivel'] == '1' || $_SESSION['id_nivel'] == '2' || isset($_GET['perfil'])) { ?>
                                <!-- Cropping modal -->
                                <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <form class="avatar-form" action="cambImagen" enctype="multipart/form-data" method="post">
                                                <div class="modal-header">
                                                    <button class="close" data-dismiss="modal" type="button">×</button>
                                                    <h4 class="modal-title" id="avatar-modal-label">Cambiar Foto de Perfil</h4>
                                                    <input class="hidden" type="text" id="idPersonal" name="idPersonal" value="<?php echo $idpersona; ?>"/>
                                                    <input type="hidden" name="mod" value="<?php echo $_GET['mod']; ?>"/>
                                                    <input class="hidden" type="text" id="dniPersonal" name="dniPersonal" value="<?php echo $idpersona; ?>"/>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="avatar-body">
                                                        <!-- Upload image and data -->
                                                        <div class="avatar-upload">
                                                            <input class="avatar-src" name="avatar_src" type="hidden">
                                                            <input class="avatar-data" name="avatar_data" type="hidden">
                                                            <label for="foto">Subir Imagen</label>
                                                            <input class="avatar-input" id="foto" name="foto_fls" type="file">
                                                            <input type="hidden" name="foto_hdn" value="<?php echo $foto ?>">
                                                        </div>

                                                        <!-- Crop and preview -->
                                                        <div class="row">
                                                            <div class="col-md-9">
                                                                <div class="avatar-wrapper"></div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="avatar-preview preview-lg"></div>
                                                                <div class="avatar-preview preview-md"></div>
                                                                <div class="avatar-preview preview-sm"></div>
                                                            </div>
                                                        </div>

                                                        <div class="row avatar-btns">
                                                            <div class="col-md-9">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <button class="btn btn-primary btn-block avatar-save" type="submit">Guardar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.modal -->
                            <?php } ?>
                            <!-- Loading state -->
                            <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                        </div>
                        <!-- end of image cropping -->

                    </div>
                    <h3><?php echo $nombre; ?></h3>

                    <ul class="list-unstyled user_data">
                        <li><i class="fa fa-map-marker user-profile-icon"></i> <?php echo $lugar; ?>
                        </li>

                        <li>
                            <i class="fa fa-briefcase user-profile-icon"></i> Cargo: <?php echo $des_cargo; ?>
                        </li>

                        <!--                        <li class="m-top-xs">
                                                    <i class="fa fa-external-link user-profile-icon"></i>
                                                    <a href="https://www.facebook.com/alucardix98" target="_blank">https://www.facebook.com/alucardix98</a>
                                                </li>-->
                    </ul>
                    <?php
                    if ($_SESSION['id_nivel'] != 4) {
                        $sql1 = "select * from usuario where idpersona = $idpersona";
                        $res = $mysqlMBN->consultas($sql1);
                        $num_ref = mysqli_num_rows($res);
                        while ($rowU = mysqli_fetch_array($res)) {
                            $user = $rowU['user'];
                            $pass = $rowU['pass2'];
                        }
                        if ($num_ref > 0) {
                            ?>
                            <form class="form-horizontal form-label-left" method="post" action="modificarB">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="item form-group">
                                        <label class="control-label col-md-4 col-sm-3 col-xs-12">Usuario</label>
                                        <div class="col-md-8 col-sm-6 col-xs-12" id="nombre">
                                            <input name="nombre" id="nombre" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="off" type="text" value="<?php echo "$user"; ?>">
                                        </div>
                                    </div>
                                    <div class="item form-group">                     
                                        <label class="control-label col-md-4 col-sm-3 col-xs-12">Password</label>
                                        <div class="col-md-8 col-sm-6 col-xs-12" id="dni">
                                            <input name="dni" id="dni" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="off" type="password" value="<?php echo "$pass "; ?>">
                                        </div>
                                    </div>
                                </div>  
                            </form>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <?php
    if ($_SESSION['id_nivel'] != 4) {
        include '../VISTAS/listaModals.php';
    }

    require('../../include/footer.php');
    ?>

    <!-- input_mask -->
    <script>
        $(document).ready(function () {
            $(":input").inputmask();
        });
    </script>
    <!-- /input mask -->
    <script>
        $(document).ready(function () {
            $(".select2_single").select2({
                placeholder: "Select a state",
                allowClear: true
            });
            $(".select2_group").select2({});
            $(".select2_multiple").select2({
                maximumSelectionLength: <?php echo $num_regs_cargos; ?>,
                placeholder: "Seleccione Maximo " + <?php echo $num_regs_cargos; ?> + " Cargos",
                allowClear: true
            });
        });
    </script>