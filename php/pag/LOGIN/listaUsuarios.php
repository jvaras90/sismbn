<?php
require('../../include/header.php');
include '../../system/mensajesAlerta.php';
include_once '../../system/crearConexion.php';
clearstatcache();
$titulo = "Usuarios";
$hoja = "Usuarios";
$mod = $_GET['mod'];
?>
<!-- page content -->
<div class="right_col" role="main">

    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Gestión de Mantenimiento</h3>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <!-- EL FORM NUEVO -->
            <?PHP
            if ($mod=='nUsuario') {
                /* SQL para la busqueda de datos */
                $sqlB = "select idpersona,concat(nombres,' ',apaterno,' ',amaterno) nombre from persona ;";
                $res = $mysqlMBN->consultas($sqlB);
                ?>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Nuevo Usuario: </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-12 col-sm-9 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_content">
                                        <form class="form-horizontal form-label-left" method="post" action="registrarA" id="actualiza">
                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Nombre<span class="required">*</span>
                                                </label>
                                                <div class="col-md-8 col-sm-6 col-xs-12" id="cliente">
                                                    <select class='select2_single form-control' tabindex="-1" name='idpersona' id='idpersona' >
                                                        <?php include_once '../../system/selectPersona.php'; ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-4 col-xs-12" >Tipo de Usuario <span class="required">*</span></label>
                                                <div class="col-md-8 col-sm-6 col-xs-12">
                                                    <select class='form-control' name='idtipo_usuario' id='idtipo_usuario' >
                                                        <option value="<?PHP
                                                        if ($_SESSION['id_nivel'] == '1') {
                                                            echo "1";
                                                        } else {
                                                            echo 0;
                                                        }
                                                        ?>">Seleccione</option>
                                                                <?php include_once '../../system/selectTipoUser.php'; ?>
                                                    </select>
                                                </div>
                                            </div>  
                                            <div class="item form-group">
                                                <label for="user" class="control-label col-md-3">Usuario <span class="required">*</span></label>
                                                <div class="col-md-8 col-sm-6 col-xs-12">
                                                    <input id="user" type="text" name="user" class="form-control col-md-7 col-xs-12" required="">
                                                </div>
                                            </div>
                                            <div class="item form-group">
                                                <label for="password" class="control-label col-md-3">Password <span class="required">*</span></label>
                                                <div class="col-md-8 col-sm-6 col-xs-12">
                                                    <input id="password" type="password" name="password" class="form-control col-md-7 col-xs-12">
                                                </div>
                                            </div>
                                            <div class="item form-group">
                                                <label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">Repeat Password <span class="required">*</span></label>
                                                <div class="col-md-8 col-sm-6 col-xs-12">
                                                    <input id="repassword" type="password" name="repassword" data-validate-linked="password" class="form-control col-md-7 col-xs-12">
                                                </div>
                                            </div>
                                            <div class="panel-footer">
                                                <div style="text-align: center;">
                                                    <a href="userLista?mod=lUsuario" class="btn btn-danger">Cancelar</a>
                                                    <input type="button" value="Guardar" id="actualizar" name="actualizar" class="btn btn-primary" onclick="this.disabled = true;
                                                            this.value = 'Guardando...';
                                                            this.form.submit()"/>
                                                    <input type="hidden" name="form" value="nuevoUsuario"/>
                                                </div>
                                            </div>                                          
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <!-- EL FORM MODIFICAR -->
            <?php
            if ($mod == "mUsuario") {
                $idusuario = $_GET['idusuario'];
                $sqlMail = "select u.* , concat (nombres, ' ' ,apaterno, ' ' ,amaterno) nombre 
                from usuario u 
                inner join persona p on p.idpersona = u.idpersona
                where idusuario = $idusuario;";
                $resMail = $mysqlMBN->consultas($sqlMail);
                while ($rowMail = mysqli_fetch_array($resMail)) {
                    $nombre = $rowMail['nombre'];
                    $user = $rowMail['user'];
                    $pass = $rowMail['pass2'];
                    $idtipo_usuario = $rowMail['idtipo_usuario'];
                    $idestado = $rowMail['idestado'];
                }
                ?>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Actualización del Usuario: <b><?php echo $user ?></b></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-12 col-sm-9 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_content">
                                        <form class="form-horizontal form-label-left" method="post" action="modificarB" id="actualiza">
                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Usuario<span class="required">*</span>
                                                </label>
                                                <div class="col-md-8 col-sm-6 col-xs-12" id="cliente">
                                                    <input type="text" id="descripcion" name="descripcion" class="form-control col-md-7 col-xs-12" value="<?php echo $nombre; ?>" readonly="" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-4 col-xs-12" >Tipo de Usuario <span class="required">*</span></label>
                                                <div class="col-md-8 col-sm-6 col-xs-12">
                                                    <select class='form-control' name='idtipo_usuario' id='idtipo_usuario' >
                                                        <option value="<?PHP
                                                        if ($_SESSION['id_nivel'] == '1') {
                                                            echo "1";
                                                        } else {
                                                            echo 0;
                                                        }
                                                        ?>">Seleccione</option>
                                                                <?php include_once '../../system/selectTipoUser.php'; ?>
                                                    </select>
                                                </div>
                                            </div>  
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-4 col-xs-12" >Estado <span class="required">*</span></label>
                                                <div class="col-md-8 col-sm-6 col-xs-12">
                                                    <select class='form-control' name='idestado' id='idestado' >
                                                        <option value="0">Seleccione</option>
                                                        <?php include_once '../../system/selectEstado.php'; ?>
                                                    </select>
                                                </div>
                                            </div> 

                                            <div class="item form-group">
                                                <label for="password" class="control-label col-md-3">Old Password</label>
                                                <div class="col-md-8 col-sm-6 col-xs-12">
                                                    <input id="o_password" type="password" name="o_password" class="form-control col-md-7 col-xs-12" required="required">
                                                </div>
                                            </div>
                                            <div title="LLENAR SOLAMENTE SI QUIERE CAMBIAR LA CLAVE">
                                                <div class="item form-group">
                                                    <label for="password" class="control-label col-md-3">Password</label>
                                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                                        <input id="password" type="password" name="password" class="form-control col-md-7 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="item form-group">
                                                    <label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">Repeat Password</label>
                                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                                        <input id="repassword" type="password" name="repassword" data-validate-linked="password" class="form-control col-md-7 col-xs-12">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer">
                                                <div style="text-align: center;">
                                                    <a href="userLista?mod=lUsuario" class="btn btn-danger">Cancelar</a>
                                                    <input type="button" value="Actualizar" id="actualizar" name="actualizar" class="btn btn-primary" onclick="this.disabled = true;
                                                            this.value = 'Actualizando...';
                                                            this.form.submit()"/>
                                                    <input type="hidden" name="form" value="modificarUsuarios"/>
                                                    <input type="hidden" name="idusuario" value="<?php echo $idusuario; ?>"/>
                                                </div>
                                            </div>                                          
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>


            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <div class="col-md-2">
                            <h2>Lista de <?php echo $titulo; ?><small> </small> </h2>
                        </div>
                        <div class="col-md-8">

                        </div>
                        <div class="col-md-2" title="Nuevo Usuario">
                            <a href="userLista?mod=nUsuario" class="btn btn-success" ><i class="fa fa-user"></i> Nuevo</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table  id="example"  class="table table-striped responsive-utilities jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>
                                        <input type="checkbox" id="check-all" class="flat">
                                    </th>
                                    <th class="column-title">Id Usuario</th>
                                    <th class="column-title">Nombre</th>
                                    <th class="column-title">DNI</th>
                                    <th class="column-title">User</th>
                                    <?php if ($_SESSION['id_nivel'] == 1) { ?>
                                        <th class="column-title">Clave</th>
                                    <?php } ?>
                                    <th class="column-title">Tipo User</th>
                                    <th class="column-title">Estado</th>
                                    <th class="column-title">Gestión</th>
                                    <!--<th class="column-title no-link last"><span class="nobr">Accion</span>-->
                                    </th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php include '../VISTAS/logTableUsuarios.php'; ?>                                    
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <script>
        $(document).ready(function () {
            $(".select2_single").select2({
                placeholder: "Select a state",
                allowClear: true
            });
        });
    </script>
    <?php
    require('../../include/footer.php');
    ?>