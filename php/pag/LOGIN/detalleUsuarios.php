<?php
require_once('../../include/header.php');
require_once('../../system/crearConexionSMS.php');
clearstatcache();
if ($_SESSION['idpersonal'] == "") {
    //si no existe, va a la página de autenticacion
    header("Location: inicio");
    //salimos de este script 
    exit();
}
//las alertar
include '../../system/mensajesAlerta.php';
//los datos del personal
include '../VISTAS/logDetUser.php';
?>
<!-- PARA CAPTURAR EL CHECKBOX-->        
<div class="right_col" role="main">

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Reporte Usuario <small>Reporte de Actividades</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                        <?php
                        if (isset($_GET['msj'])) {
                            echo "<div class='label label-warning'>$mensaje</div>";
                        }
                        ?>   
                        <div class="profile_img">

                            <!-- end of image cropping -->
                            <div id="crop-avatar">
                                <!-- Current avatar -->
                                <div data-original-title="<?php
                                if ($_SESSION['id_nivel'] == '1' || isset($_GET['perfil'])) {
                                    echo "Cambiar Imagen";
                                }
                                ?>" class="avatar-view" title="">
                                    <img src="images/Fotos/<?php echo $foto; ?>" alt="Avatar">
                                </div>
                                <?php if ($_SESSION['id_nivel'] == '1' || $_SESSION['id_nivel'] == '4' || isset($_GET['perfil'])) { ?>
                                    <!-- Cropping modal -->
                                    <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <form class="avatar-form" action="cambImagen" enctype="multipart/form-data" method="post">
                                                    <div class="modal-header">
                                                        <button class="close" data-dismiss="modal" type="button">×</button>
                                                        <h4 class="modal-title" id="avatar-modal-label">Cambiar Foto de Perfil</h4>
                                                        <input class="hidden" type="text" id="idPersonal" name="idPersonal" value="<?php echo $idPersonal; ?>"/>
                                                        <input class="hidden" type="text" id="dniPersonal" name="dniPersonal" value="<?php echo $dni; ?>"/>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="avatar-body">
                                                            <!-- Upload image and data -->
                                                            <div class="avatar-upload">
                                                                <input class="avatar-src" name="avatar_src" type="hidden">
                                                                <input class="avatar-data" name="avatar_data" type="hidden">
                                                                <label for="foto">Subir Imagen</label>
                                                                <input class="avatar-input" id="foto" name="foto_fls" type="file">
                                                                <input type="hidden" name="foto_hdn" value="<?php echo $foto ?>">
                                                            </div>

                                                            <!-- Crop and preview -->
                                                            <div class="row">
                                                                <div class="col-md-9">
                                                                    <div class="avatar-wrapper"></div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div class="avatar-preview preview-lg"></div>
                                                                    <div class="avatar-preview preview-md"></div>
                                                                    <div class="avatar-preview preview-sm"></div>
                                                                </div>
                                                            </div>

                                                            <div class="row avatar-btns">
                                                                <div class="col-md-9">
                                                                    <div class="btn-group">
                                                                        <button class="btn btn-primary" data-method="rotate" data-option="-90" type="button" title="Rotate -90 degrees">Rotate Left</button>
                                                                        <button class="btn btn-primary" data-method="rotate" data-option="-15" type="button">-15deg</button>
                                                                        <button class="btn btn-primary" data-method="rotate" data-option="-30" type="button">-30deg</button>
                                                                        <button class="btn btn-primary" data-method="rotate" data-option="-45" type="button">-45deg</button>
                                                                    </div>
                                                                    <div class="btn-group">
                                                                        <button class="btn btn-primary" data-method="rotate" data-option="90" type="button" title="Rotate 90 degrees">Rotate Right</button>
                                                                        <button class="btn btn-primary" data-method="rotate" data-option="15" type="button">15deg</button>
                                                                        <button class="btn btn-primary" data-method="rotate" data-option="30" type="button">30deg</button>
                                                                        <button class="btn btn-primary" data-method="rotate" data-option="45" type="button">45deg</button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <button class="btn btn-primary btn-block avatar-save" type="submit">Guardar</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.modal -->
                                <?php } ?>
                                <!-- Loading state -->
                                <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                            </div>
                            <!-- end of image cropping -->

                        </div>
                        <h3><?php echo $fllName; ?></h3>

                        <ul class="list-unstyled user_data">
                            <li><i class="fa fa-map-marker user-profile-icon"></i> Lince, Lima, Peru
                            </li>

                            <li>
                                <i class="fa fa-briefcase user-profile-icon"></i> Negocio: <?php echo $negocio; ?>
                            </li>

                            <li class="m-top-xs">
                                <i class="fa fa-external-link user-profile-icon"></i>
                                <a href="#" target="_blank"><?php echo $mail; ?></a>
                            </li>
                        </ul>
                        <?php
                        if (!isset($_GET['int'])) {
                            if ($_SESSION['id_nivel'] == '1' || $_SESSION['id_nivel'] == '4' || isset($_GET['perfil'])) {
                                ?>
                                <a href="userDeta?int=1&idpersonal=<?php echo $idPersonal; ?>" class="btn btn-success"><i class="fa fa-edit m-right-xs"></i>Editar Perfil</a>                                
                                <?php
                            }
                        } else {
                            echo "";
                        }
                        ?>
                    </div>
                    <?php if (isset($_GET['int'])) { //si presionan el boton Editar Perfil?>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <form class="form-horizontal form-label-left" method="post" action="modificarB">
                                <div class="item form-group">
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">Nombre <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12" id="nombre">
                                        <input name="nombre" id="nombre" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="off" type="text" value="<?php echo "$nombres"; ?>">
                                    </div>
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">DNI <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12" id="dni">
                                        <input name="dni" id="dni" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="off" type="text"  <?php
                                        if ($_SESSION['id_nivel'] != 4) {
                                            echo "readonly";
                                        }
                                        ?> value="<?php echo "$dni "; ?>">
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">A. Paterno <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12" id="aPaterno">
                                        <input name="aPaterno" id="aPaterno" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="off" type="text"  value="<?php echo $aparterno; ?>">
                                    </div>
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">A. Materno <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12" id="aMaterno">
                                        <input name="aMaterno" id="aMaterno" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="off" type="text"  value="<?php echo $amaterno; ?>">
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">Email <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12" id="cliente">
                                        <input type="text" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12" <?php
                                        if ($_SESSION['id_nivel'] != 4) {
                                            echo "readonly";
                                        }
                                        ?> value="<?php echo "$mail"; ?>">
                                    </div>
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">Usuario <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12" id="cliente">
                                        <input type="text" id="usuario" name="usuario" required="required" placeholder="" class="form-control col-md-7 col-xs-12" <?php
                                        if ($_SESSION['id_nivel'] != 4) {
                                            echo "readonly";
                                        }
                                        ?>  value="<?php echo "$user"; ?>">
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">Nivel <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <select class='form-control' name='nivel' id='nivel'>
                                            <?php include_once "../../system/selectTipoNivel.php"; ?>
                                        </select>
                                    </div>
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">Negocio <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <select class='form-control' name='negocio' id='negocio'>
                                            <?php include_once "../../system/selectNegocio.php"; ?>
                                        </select>
                                    </div>
                                </div>
                                <?php if ($_SESSION['id_nivel'] == 4) { ?>
                                    <div class="ln_solid">  </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-2 col-sm-3 col-xs-12">
                                            Anexos <span class="required">*</span>
                                        </label>
                                        <table class="col-md-10 col-sm-6 col-xs-12" >
                                            <?php
                                            $sqlA = "select * from anexos a inner join categoria c on a.idcategoria = c.idcategoria where a.idpersonal=" . $_GET['idpersonal'] . " order by descripcion";
                                            $res = $mysqlSMS->consultas($sqlA);
                                            $num_regs = mysqli_num_rows($res);
                                            while ($row = mysqli_fetch_array($res)) {
                                                $idanexo = $row['idanexos'];
                                                $categoria = $row['descripcion'];
                                                $idcategoria = $row['idcategoria'];
                                                $anexo = $row['anexo'];
                                                $prioridad = $row['prioridad'];
                                                ?>
                                                <tr>
                                                    <td class="col-md-3">
                                                        <label class="control-label col-md-12 col-sm-3 col-xs-12">
                                                            <?php echo $categoria; ?> <input class="radio-inline" type="radio" value="<?php echo $idcategoria; ?>" id="anexo_cat"name="anexo_cat"   <?php
                                                            if ($prioridad == 1) {
                                                                echo "checked";
                                                            }
                                                            ?> title="Prioridad">
                                                        </label>
                                                    </td>
                                                    <td class="col-md-3">
                                                        <input type="text" id="anexo<?php echo $idcategoria; ?>" name="anexo<?php echo $idcategoria; ?>" class="form-control col-md-7 col-xs-12"value="<?php echo "$anexo"; ?>" readonly>
                                                    </td>
                                                    <td class="col-md-1">
                                                        <a href="#modalAnexoDesc<?php echo $idanexo; ?>" class="btn btn-success" data-toggle="modal" title="Editar Anexo"><i class="fa fa-pencil"></i></a>
                                                        <form method="POST" id="exclude_form_<?php echo $id ?>" action="">                                                

                                                        </form>
                                                        <!-- ############################################ MODAL EDITAR  ANEXO ############################################ -->
                                                        <div class="modal fade" id="modalAnexoDesc<?php echo $idanexo; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <form class="form-horizontal" role="form" action="modificarB" method="post">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                            <h4 class="modal-title">Modificar el Anexo <?php echo " $categoria de $nombres"; ?></h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="hidden">
                                                                                <div class="form-group">
                                                                                    <label>Id Anexo</label>
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span></span>
                                                                                        <input type="text"  name="idAnexo" id="idAnexo" class="form-control"  value="<?php echo $idanexo; ?>" readonly>
                                                                                    </div>
                                                                                </div>
                                                                            </div>                           
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label>Anexo</label>
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-bookmark"></span></span>
                                                                                        <input type="text"  name="anexo" class="form-control" placeholder="Anexo" value="<?php echo $anexo; ?>">
                                                                                    </div>
                                                                                </div>
                                                                            </div>                          
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                                            <button type="submit" id="modificarAnexo" name="modificarAnexo" class="btn btn-primary">Guardar</button>
                                                                            <input type="hidden" name="form" value="modificarAnexo"/>
                                                                            <input type="hidden" name="idpersonal" value="<?php echo $_GET['idpersonal']; ?>"/>
                                                                        </div>
                                                                    </form>
                                                                </div><!-- /.modal-content -->
                                                            </div><!-- /.modal-dialog -->
                                                        </div><!-- /.modal -->
                                                        <!-- ############################################ MODAL NUEVA DIRECCION IP ############################################ -->
                                                    </td>
                                                    <td class="col-md-1">
                                                        <button title="Eliminar Anexo" type="button" class="btn btn-danger" onclick="eliminarAnexo(<?php echo "'$idanexo','quitarAnexo','$categoria','" . $_GET['idpersonal'] . "'"; ?>);"  > 
                                                            <i class="fa fa-minus-square"></i> 
                                                        </button>
                                                    </td>
                                                <?php }if ($num_regs < 5) { ?>                                                    
                                                    <td class="col-md-5">
                                                        <a href="#modalAnexo" class="btn btn-info"  data-toggle="modal"  title="Agregar Anexo"><i class="fa fa-plus-square"></i></a>                                            
                                                    </td>
                                                <?php } else { ?>
                                                    <td class="col-md-5"> </td>
                                                <?php } ?> 
                                            </tr>
                                            <?php //} ?>                                       
                                        </table>
                                    </div> 
                                    <div class="item form-group">
                                        <label class="control-label col-md-2 col-sm-3 col-xs-12">
                                            Direcciones Ip<span class="required">*</span>
                                        </label>
                                        <table class="col-md-10 col-sm-6 col-xs-12">
                                            <?php
                                            $sqlIp = "select id_ip ,di.descripcion as desc_ip, c.descripcion as categoria , prioridad,di.idcategoria from direcciones_ip di 
                                        inner join categoria c on di.idcategoria = c.idcategoria where di.idpersonal=" . $_GET['idpersonal'] . " order by categoria";
                                            $resIP = $mysqlSMS->consultas($sqlIp);
                                            $num_regs = mysqli_num_rows($resIP); //cantidad de categorias de ip asignadas a un personal
                                            while ($row = mysqli_fetch_array($resIP)) {
                                                $id_ip = $row['id_ip'];
                                                $categoria = $row['categoria'];
                                                $desc_ip = $row['desc_ip'];
                                                $idcategoria = $row['idcategoria'];
                                                $prioridad = $row['prioridad'];
                                                ?>
                                                <tr>
                                                    <td class="col-md-3">
                                                        <label class="control-label col-md-12 col-sm-3 col-xs-12">
                                                            <?php echo $categoria; ?> <input class="radio-inline" type="radio" value="<?php echo $idcategoria; ?>" id="direccionIP_cat" name="direccionIP_cat"   <?php
                                                            if ($prioridad == 1) {
                                                                echo "checked";
                                                            }
                                                            ?> title="Prioridad">
                                                        </label>
                                                    </td>
                                                    </td>
                                                    <td class="col-md-3">
                                                        <input type="text" id="desc_ip<?php echo $idcategoria; ?>" name="ip<?php echo $idcategoria; ?>" class="form-control col-md-7 col-xs-12"value="<?php echo "$desc_ip"; ?>" readonly>
                                                    </td>
                                                    <td class="col-md-1">
                                                        <a href="#modalIpDesc<?php echo $id_ip; ?>" class="btn btn-success" data-toggle="modal"  title="Editar IP"><i class="fa fa-pencil"></i></a>

                                                        <!-- ############################################ MODAL EDITAR IP ############################################ -->
                                                        <div class="modal fade" id="modalIpDesc<?php echo $id_ip; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <form class="form-horizontal" role="form" action="modificarB" method="post">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                            <h4 class="modal-title">Modificar el Ip <?php echo " $categoria de $nombres"; ?></h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="hidden">
                                                                                <div class="form-group">
                                                                                    <label>Id IP</label>
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span></span>
                                                                                        <input type="text"  name="idIP" id="idIP" class="form-control"  value="<?php echo $id_ip; ?>" readonly>
                                                                                    </div>
                                                                                </div>
                                                                            </div>                           
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label>Ip</label>
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-bookmark"></span></span>
                                                                                        <input type="text"  name="desc_ip" class="form-control" placeholder="Ip" value="<?php echo $desc_ip; ?>">
                                                                                    </div>
                                                                                </div>
                                                                            </div>                          
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                                            <button type="submit" id="nuevoIP" name="nuevoIP" class="btn btn-primary">Guardar</button>
                                                                            <input type="hidden" name="form" value="modificarIP"/>
                                                                            <input type="hidden" name="idpersonal" value="<?php echo $_GET['idpersonal']; ?>"/>
                                                                        </div>
                                                                    </form>
                                                                </div><!-- /.modal-content -->
                                                            </div><!-- /.modal-dialog -->
                                                        </div><!-- /.modal -->
                                                        <!-- ############################################ MODAL NUEVA DIRECCION IP ############################################ -->
                                                    </td>
                                                    <td class="col-md-1">
                                                        <button title="Eliminar IP" type="button" class="btn btn-danger" onclick="eliminarIP(<?php echo "'$id_ip','quitarIP','$categoria','" . $_GET['idpersonal'] . "'"; ?>);"  > 
                                                            <i class="fa fa-minus-square"></i> 
                                                        </button>
                                                    </td>
                                                <?php } if ($num_regs < 5) { ?>
                                                    <td class="col-md-5">
                                                        <a href="#modalDireccionIP" class="btn btn-info"  data-toggle="modal" title="Agregar Direccion Ip"><i class="fa fa-plus-square"></i></a>
                                                    </td>
                                                <?php } else { ?>
                                                    <td class="col-md-5"> </td>
                                                <?php } ?> 
                                            </tr>
                                            <?php //} ?>   
                                        </table>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="ln_solid">  </div>
                                <?php } ?>
                                <?php if ($_SESSION['id_nivel'] != 4) { ?>
                                    <div class="item form-group">
                                        <label for="password" class="control-label col-md-2 col-sm-3 col-xs-12">Password</label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <input id="password" type="password" name="password" class="form-control col-md-7 col-xs-12" required="required">
                                        </div>
                                        <label for="password2" class="control-label col-md-2 col-sm-3 col-xs-12">Repeat Password</label>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <input id="repassword" type="password" name="repassword" data-validate-linked="password" class="form-control col-md-7 col-xs-12" required="required">
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="ln_solid"></div>
                                <?php } ?>
                                <div class="form-group">
                                    <div style="text-align: center;">
                                        <?php if ($_SESSION['id_nivel'] == '4') { ?>
                                            <a href="userDeta?idpersonal=<?php echo $idPersonal ?>&perfil=1" class="btn btn-primary"><i class="fa fa-user"></i> Perfil</a>
                                            <a href="userLista?int=1" class="btn btn-danger"><i class="fa fa-rotate-left"></i> Regresar</a>
                                        <?php } else { ?>
                                            <a href="userDeta?idpersonal=<?php echo $idPersonal ?>&perfil=1" class="btn btn-danger"><i class="fa fa-rotate-left"></i> Regresar</a>
                                        <?php } ?>
                                        <input type="button" value="Actualizar" id="actualizarPersonal" name="actualizarPersonal" class="btn btn-primary" onclick="this.disabled = true;
                                                    this.value = 'Actualizando...';
                                                    this.form.submit()"/>
                                        <input type="hidden" name="form" value="modificarUsuario"/>
                                        <input type="hidden" name="idpersonal" value="<?php echo $_GET['idpersonal']; ?>"/>
                                    </div>
                                </div>

                            </form>
                        </div>
                        <div class="hidden">
                        <?php } else { //cuando esta en el editar personal por defecto        ?>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php } ?>
                            <div class="profile_title">
                                <div class="col-md-6">
                                    <h2>Reporte: Envios de IVR por Mes</h2>
                                </div>
                                <div class="col-md-6">
                                    <div id="reportrange" class="pull-right" style="margin-top: 5px; background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #E6E9ED">
                                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                        <span></span> <b class="caret"></b>
                                    </div>
                                </div>
                            </div>
                            <!-- start of user-activity-graph -->
                            <div class="x_panel">
                                <div class="x_content">
                                    <!--<canvas id="canvas_bar"></canvas>-->
                                </div>
                                <div class="x_content" name="rtdiv1" id="rtdiv1">
                                    <div id="graph_bar" style="width:100%; height:280px;"></div>
                                </div>
                            </div>  
                            <input type="text" class="hidden" id="idpersonal1" name="idpersonal1" value="<?php echo $idPersonal; ?>">

                        </div>
                        <!--  FIN DEL FORMULARIO ACTUALIZAR PERSONAL-->
                        <div class="clearfix"></div>
                        <!-- DIV PARA  EL TAB PANEL -->
                        <div class="center-block">
                            <div class="col-md-10">
                                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#tab_content_act_rec" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Actividad General Reciente</a>
                                        </li>
                                        <li role="presentation" class=""><a href="#tab_content_protecto_curso" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Proyectos en Curso</a>
                                        </li>
                                        <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Perfil</a>
                                        </li>
                                    </ul>
                                    <div id="myTabContent" class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content_act_rec" aria-labelledby="home-tab">
                                            <!-- start recent activity -->
                                            <!-- end recent activity -->
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="tab_content_protecto_curso" aria-labelledby="profile-tab">
                                            <!-- start user projects -->
                                            <!-- end user projects -->
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                                            <p> <?php echo "$fllName Pertenece al negocio de $negocio"; ?> </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $fec_ini = $_GET['fec_ini'];
        $fec_fin = $_GET['fec_fin'];
        if (isset($_GET['fec_ini']) && isset($_GET['fec_fin'])) {
            $sql = "select sum(total_enviados)as Total_enviado , DATE_FORMAT(fecha_envio,'%M') as Mes
            from resumen_ivr 
            where dni_creo_campana = $dni and fecha_envio between '$fec_ini' and '$fec_fin'
            group by mes
            order by fecha_envio";
        } else {
            $sql = "select sum(total_enviados)as Total_enviado , DATE_FORMAT(fecha_envio,'%M') as Mes
            from resumen_ivr 
            where dni_creo_campana = $dni
            group by mes
            order by fecha_envio";
        }
        //$result1 = $mysqlSMS->consultas($sql);
        $num_regs = mysqli_num_rows($result1);
        //echo $num_regs;
        //var_dump($result1);
        echo "<br>";
        while ($row = mysqli_fetch_array($result1)) {
            $total = $row['Total_enviado'];
            $nom_mes = $row['Mes'];
            //$cadena_mes .="\"$nom_mes\",";
            //$cadena_total .="$total,";
            $cadena_Barra .= "{\"period\": \"$nom_mes\", \"Hours worked\": $total },";
            //$i++;
            //echo "<input class='' id='nombre_mes' type='text' value=\"$nom_mes \"/> <br>";
        }


        //echo $cadena_mes . "<br>";
        //echo $cadena_total;
        //echo $cadena_Barra;
        ?>
        <input class="hidden" id="num_regs" type="text" value="<?php echo "$num_regs"; ?>"/>
        <a href=""></a>
        <!--<script src="js/custom.js" type="text/javascript"></script>-->

        <script>
            var segundos = 1;
            function eliminarAnexo(idAnexo, accion, catAnexo, idpersonal)
            {
                if (confirm('Esta Apunto de eliminar el anexo ' + catAnexo + ' \n¿Desea Continuar?'))
                {
                    $.post("modificarB", {idanexo: "" + idAnexo + "", form: "" + accion + ""})
                    var timeSlide = 1000 * segundos;
                    setTimeout(function () {
                        window.location.href = "userDeta?int=1&idpersonal=" + idpersonal + "&msj=29";
                    }, (timeSlide + 500));
                }
                else
                {
                    alert('Se canceló ésta Operacion.');
                }
            }
            function eliminarIP(idIP, accion, catAnexo, idpersonal)
            {
                if (confirm('Esta Apunto de eliminar el IP ' + catAnexo + ' \n¿Desea Continuar?'))
                {
                    $.post("modificarB", {idIP: "" + idIP + "", form: "" + accion + ""})
                    var timeSlide = 1000 * segundos;
                    setTimeout(function () {
                        window.location.href = "userDeta?int=1&idpersonal=" + idpersonal + "&msj=30";
                    }, (timeSlide + 500));
                }
                else
                {
                    alert('Se canceló ésta Operacion.');
                }
            }
        </script>

        <script>
            $(document).ready(function () {
                $('input.flat').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                });
            });

            $(function () {
                //var num_regs = document.getElementById("num_regs").value;
                //var nom_mes = document.getElementById("nombre_mes").value;

                //alert(num_regs);
                var day_data = [<?php echo $cadena_Barra; ?>
                    //{"period": "nom_mes","Hours worked" :80 }
                    //{"period": "Jan", "Hours worked": 80},
                    //for(i=0;i<num_regs;i++){
                    //     {"period" : ""}
                    // }
                ];
                Morris.Bar({
                    element: 'graph_bar',
                    data: day_data,
                    xkey: 'period', hideHover: 'auto',
                    barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                    ykeys: ['Hours worked', 'sorned'],
                    labels: ['IVR\'s Enviados',
                        '<?php
        echo "<a href=\"detalleIvrEnviados.php?idpersonal=$idPersonal&mes=$nom_mes\">Detalle</a>"
        ?>'],
                    xLabelAngle: 60
                });
            });
        </script>

        <!-- datepicker -->
        <script type="text/javascript">
            $(document).ready(function () {

                var cb = function (start, end, label) {
                    console.log(start.toISOString(), end.toISOString(), label);
                    $('#reportrange span').html(start.format('MMMM  YYYY') + ' - ' + end.format('MMMM  YYYY'));
                    //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
                }

                var optionSet1 = {
                    startDate: moment().subtract(365, 'days'),
                    endDate: moment(),
                    minDate: '01/01/2000',
                    maxDate: '12/31/2050',
                    dateLimit: {
                        days: 366
                    },
                    showDropdowns: true,
                    showWeekNumbers: true,
                    timePicker: false,
                    timePickerIncrement: 1,
                    timePicker12Hour: true,
                    ranges: {
                        //'Today': [moment(), moment()],
                        //'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        //'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        //'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        //'This Month': [moment().startOf('month'), moment().endOf('month')],
                        //'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    opens: 'left',
                    buttonClasses: ['btn btn-default'],
                    applyClass: 'btn-small btn-primary',
                    cancelClass: 'btn-small btn-danger',
                    format: 'MM/DD/YYYY',
                    separator: ' to ',
                    locale: {
                        applyLabel: 'Ok',
                        cancelLabel: 'Limpiar',
                        fromLabel: 'Desde', toLabel: 'Hasta',
                        customRangeLabel: 'Rango',
                        daysOfWeek: ['Dom', 'Lu', 'Ma', 'Mi', 'Jue', 'Vi', 'Sa'],
                        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                        firstDay: 1
                    }
                };
                $('#reportrange span').html(moment().subtract(365, 'days').format('MMMM  YYYY') + ' - ' + moment().format('MMMM  YYYY'));
                $('#reportrange').daterangepicker(optionSet1, cb);
                $('#reportrange').on('show.daterangepicker', function () {
                    console.log("show event fired");
                });
                $('#reportrange').on('hide.daterangepicker', function () {
                    console.log("hide event fired");
                });
                $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
                    console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
                    location.href = "detalleUsuarios.php?idpersonal=<?php echo $idPersonal; ?>&fec_ini=" + picker.startDate.format('YYYY-MM-DD') + "&fec_fin=" + picker.endDate.format('YYYY-MM-DD');
                });
                $('#reportrange').on('cancel.daterangepicker', function (ev, picker) {
                    console.log("cancel event fired");
                });
                $('#options1').click(function () {
                    $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
                });
                $('#options2').click(function () {
                    $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
                });
                $('#destroy').click(function () {
                    $('#reportrange').data('daterangepicker').remove();
                });
            });
        </script>
        <!-- /datepicker -->
        <div class="clearfix"></div>

        <!-- ############################################ MODAL NUEVO ANEXO ############################################ -->

        <div class="modal fade" id="modalAnexo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form class="form-horizontal" role="form" action="registrarA" method="post">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Nuevo Anexo</h4>
                        </div>
                        <div class="modal-body">
                            <div class="hidden">
                                <div class="form-group">
                                    <label>Id Personal</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span></span>
                                        <input type="text"  name="idpersonal" id="idpersonal" class="form-control"  value="<?php echo $idPersonal; ?>" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label>Categoria</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-th-list"></span></span>
                                        <select class='form-control' name='idcategoria' id='idcategoria' required>
                                            <?php
                                            include_once '../../system/selectCategoria.php';
                                            $ssql2 = "select idcategoria,descripcion from categoria where idestado != 2 and  idcategoria in(select idcategoria from categoria where clasificacion=1) and idcategoria not in (select idcategoria from anexos where idpersonal=$idPersonal) order by descripcion;";
                                            $resultado2 = $mysqlSMS->consultas($ssql2);
                                            select_categoria($resultado2);
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>                             
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Anexo</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-bookmark"></span></span>
                                        <input type="text"  name="anexo" class="form-control" placeholder="Anexo" required>
                                    </div>
                                </div>
                            </div>                          
                        </div>
                        <div class="clearfix"></div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" id="nuevoUsuario" name="nuevoIP" class="btn btn-primary">Guardar</button>
                            <input type="hidden" name="form" value="nuevoAnexoPersonal"/>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!-- ############################################ MODAL NUEVA DIRECCION IP ############################################ -->

        <div class="modal fade" id="modalDireccionIP" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form class="form-horizontal" role="form" action="registrarA" method="post">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Nueva Dirección IP</h4>
                        </div>
                        <div class="modal-body">
                            <div class="hidden">
                                <div class="form-group">
                                    <label>Id Personal</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span></span>
                                        <input type="text"  name="idpersonal" id="idpersonal" class="form-control"  value="<?php echo $idPersonal; ?>" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label>Categoria</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-th-list"></span></span>
                                        <select class='form-control' name='idcategoria' id='idcategoria' required>
                                            <?php
                                            include_once '../../system/selectCategoria.php';
                                            $ssql1 = "select idcategoria,descripcion from categoria where idestado != 2 and  idcategoria in(select idcategoria from categoria where clasificacion=2) and idcategoria not in (select idcategoria from direcciones_ip where idpersonal=$idPersonal) order by descripcion;";
                                            $resultado = $mysqlSMS->consultas($ssql1);
                                            select_categoria($resultado);
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>                             
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>IP</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-bookmark"></span></span>
                                        <input type="text"  name="direccionIP" class="form-control" placeholder="Dirección Ip" required>
                                    </div>
                                </div>
                            </div>                          
                        </div>
                        <div class="clearfix"></div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" id="nuevoUsuario" name="nuevoIP" class="btn btn-primary">Guardar</button>
                            <input type="hidden" name="form" value="nuevoIpPersonal"/>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <?php require('../../include/footer.php'); ?>