<?php
require_once('../../include/header.php');
require_once('../../system/crearConexionSMS.php');
clearstatcache();
if ($_SESSION['dni'] == "") {
    //si no existe, va a la página de autenticacion
    header("Location: inicio");
    //salimos de este script 
    exit();
}
//las alertar
include '../../system/mensajesAlerta.php';
?>
<script type="text/javascript">
    $(document).ready(function () {
        var lista = document.getElementById("idnegocio");
        $('#idnegocio').change(function () {
            window.location = "listaAnex2?int=1&anex=1&id_neg=" + lista.value;
        });
    });
</script>
<!-- page content -->
<div class="right_col" role="main">

    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Gestión de Mantenimiento de Conextos </h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <div class="col-md-8">
                            <h2>Listado de Cuentas</h2>
                        </div>
                        <div class="col-md-1">
                        </div>
                        <div class="col-md-2">
                            <a href="#modalNuevoContexto" class="btn btn-success" data-toggle="modal"><i class="fa fa-user-md"></i> Nuevo</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table  id="example"  class="table table-striped responsive-utilities jambo_table bulk_action">
                            <thead>
                                <tr class="headings">

                                    <th class="column-title">Conexto</th>
                                    <th class="column-title">Tipo</th>
                                    <th class="column-title">Estado</th>
                                    <th class="column-title">Managment</th>
                                    <!--<th class="column-title no-link last"><span class="nobr">Accion</span>-->
                                    </th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php include '../VISTAS/logTableConextos.php'; ?>                                    
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php
            if (isset($_GET['idcontexto'])) {
                $idcontexto = $_GET['idcontexto'];
                $sqlContext = "select c.idcontexto ,c.descripcion contexto,c.type,c.host,c.qualify,c.secret,c.idestado,e.descripcion estado
                from contexto c inner join estado e on e.idestado = c.idestado where idcontexto=$idcontexto";
                $resContext = $mysqlLOG->consultas($sqlContext);
                while ($rowContext = mysqli_fetch_array($resContext)) {
                    $contexto1 = $rowContext['contexto'];
                    $type1 = $rowContext['type'];
                    $host1 = $rowContext['host'];
                    $qualify1 = $rowContext['qualify'];
                    $secret1 = $rowContext['secret'];
                    $idestado1 = $rowContext['idestado'];
                    $estado1 = $rowContext['estado'];
                }
                ?>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <div class="col-md-2">
                                <h2>Actualizacion del Conexto: <b style="font-size: 20px;"><?php echo $anexo; ?></b></h2>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-12 col-sm-9 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_content">
                                        <form class="form-horizontal form-label-left" method="post" action="modificarB" id="actualiza">
                                            <div class="item form-group" title="Contexto de la Cuenta" >
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Contexto <span class="required">*</span>
                                                </label>
                                                <div class="col-md-8 col-sm-6 col-xs-12" id="cliente">
                                                    <input type="text" id="contexto" name="contexto" required="" class="form-control col-md-7 col-xs-12" value="<?php echo $contexto1; ?>">
                                                </div>
                                            </div>
                                            <div class="item form-group" title="Type de la Cuenta" >
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Type <span class="required">*</span>
                                                </label>
                                                <div class="col-md-8 col-sm-6 col-xs-12" id="cliente">
                                                    <input type="text" id="type" name="type" required="" class="form-control col-md-7 col-xs-12" value="<?php echo $type1; ?>">
                                                </div>
                                            </div>
                                            <div class="item form-group" title="Host de la Cuenta" >
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Host <span class="required">*</span>
                                                </label>
                                                <div class="col-md-8 col-sm-6 col-xs-12" id="cliente">
                                                    <input type="text" id="host" name="host" required="" class="form-control col-md-7 col-xs-12" value="<?php echo $host1; ?>">
                                                </div>
                                            </div>
                                            <div class="item form-group" title="Qualify de la Cuenta" >
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Qualify <span class="required">*</span>
                                                </label>
                                                <div class="col-md-8 col-sm-6 col-xs-12" id="cliente">
                                                    <input type="text" id="qualify" name="qualify" required="" class="form-control col-md-7 col-xs-12" value="<?php echo $qualify1; ?>">
                                                </div>
                                            </div>
                                            <div class="item form-group" title="Secret de la Cuenta" >
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Secret <span class="required">*</span>
                                                </label>
                                                <div class="col-md-8 col-sm-6 col-xs-12" id="cliente">
                                                    <input type="password" id="secret" name="secret" required="" class="form-control col-md-7 col-xs-12" value="<?php echo $secret1; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group" title="Estado del Anexo">
                                                <label class="control-label col-md-3 col-sm-4 col-xs-12">Estado <span class="required">*</span></label>
                                                <div class="col-md-9 col-sm-6 col-xs-12">
                                                    <select class='form-control' name='idestado' id='idestado' >
                                                        <?php include '../../system/selectEstado.php'; ?>
                                                    </select>
                                                </div>
                                            </div> 
                                            <div class="clearfix"></div>
                                            <div class="panel-footer">
                                                <div style="text-align: center;">
                                                    <a href="listaContext?int=1&anex=1" class="btn btn-danger">Cancelar</a>
                                                    <input type="button" value="Actualizar" id="actualizar" name="actualizar" class="btn btn-primary" onclick="this.disabled = true;
                                                                this.value = 'Actualizando...';
                                                                this.form.submit()"/>
                                                    <input type="hidden" name="form" value="modificarContexto"/>
                                                    <input type="hidden" name="idcontexto" value="<?php echo $idcontexto; ?>"/>
                                                </div>
                                            </div>                                          
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <?php include '../VISTAS/listaModals.php'; ?>
    <script>
        $(document).ready(function () {
            $('input.flat').iCheck({
                checkboxClass: 'icheckbox_flat-green', radioClass: 'iradio_flat-green'
            });
        });

        var asInitVals = new Array();
        $(document).ready(function () {
            var oTable = $('#example').dataTable({
                "oLanguage": {
                    "sSearch": "Buscar:"},
                "aoColumnDefs": [
                    {
                        'bSortable': false,
                        'aTargets': [0]
                    } //disables sorting for column one
                ],
                'iDisplayLength': <?php echo 10; ?>,
                "sPaginationType": "full_numbers",
                "dom": 'T<"clear">lfrtip',
                //"tableTools": {                 // "sSwfPath": "<?php // echo base_url('../.../js/datatables/tools/swf/copy_csv_xls_pdf.swf');                                                                                          ?>"
                //}
            });
            $("tfoot input").keyup(function () {
                /* Filter on the column based on the index of this element's parent <th> */
                oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
            });
            $("tfoot input").each(function (i) {
                asInitVals[i] = this.value;
            });
            $("tfoot input").focus(function () {
                if (this.className == "search_init") {
                    this.className = "";
                    this.value = "";
                }
            });
            $("tfoot input").blur(function (i) {
                if (this.value == "") {
                    this.className = "search_init";
                    this.value = asInitVals[$("tfoot input").index(this)];
                }
            });
        });
    </script>
    <?php
    require('../../include/footer.php');
    