<?php
require_once('../../include/header.php');
require_once('../../system/crearConexionSMS.php');
clearstatcache();
if ($_SESSION['dni'] == "") {
    //si no existe, va a la página de autenticacion
    header("Location: inicio");
    //salimos de este script 
    exit();
}
//las alertar
include '../../system/mensajesAlerta.php';
?>
<script type="text/javascript">
    $(document).ready(function () {
        var lista = document.getElementById("idnegocio");
        $('#idnegocio').change(function () {
            window.location = "listaAnex2?int=1&anex=1&id_neg=" + lista.value;
        });
    });
</script>
<!-- page content -->
<div class="right_col" role="main">

    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Gestión de Mantenimiento de Anexos </h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <div class="col-md-2">
                            <h2>Negocio</h2>
                        </div>
                        <div class="col-md-8">
                            <?php if (!isset($_GET['idanexo'])) { ?>
                                <select class='form-control' name='idnegocio' id='idnegocio' >
                                    <option value="69">Todos - [Conextos]</option>
                                    <?php include '../../system/selectNegocio.php'; ?>
                                </select>
                            <?php } ?>
                        </div>
                        <div class="col-md-2">
                            <a href="#modalNuevoAnexoLog" class="btn btn-success" data-toggle="modal"><i class="fa fa-phone"></i> New</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table  id="example"  class="table table-striped responsive-utilities jambo_table bulk_action">
                            <thead>
                                <tr class="headings">

                                    <th class="column-title">Anexo</th>
                                    <th class="column-title">Caller ID</th>
                                    <th class="column-title">Contexto</th>
                                    <th class="column-title">Estado</th>
                                    <th class="column-title">Managment</th>
                                    <!--<th class="column-title no-link last"><span class="nobr">Accion</span>-->
                                    </th>
                                    <th class="bulk-actions" colspan="7">
                                        <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php include '../VISTAS/logTableAnexos.php'; ?>                                    
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php
            if (isset($_GET['idanexo'])) {
                $idanexo = $_GET['idanexo'];
                $sqlAnexo = "select * from anexo where idanexo=$idanexo";
                $resAnexo = $mysqlLOG->consultas($sqlAnexo);
                while ($rowAnex = mysqli_fetch_array($resAnexo)) {
                    $anexo = $rowAnex['anexo'];
                    $callerid = $rowAnex['callerid'];
                    $idnegocio1 = $rowAnex['idnegocio'];
                    $idestado = $rowAnex['idestado'];
                }
                ?>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <div class="col-md-2">
                                <h2>Actualizacion del Anexo: <b style="font-size: 20px;"><?php echo $anexo; ?></b></h2>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-12 col-sm-9 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_content">
                                        <form class="form-horizontal form-label-left" method="post" action="modificarB" id="actualiza">
                                            <div class="hidden" title="Número de anexo" >
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Anexo <span class="required">*</span>
                                                </label>
                                                <div class="col-md-8 col-sm-6 col-xs-12" id="cliente">
                                                    <input type="text" id="anexo" name="anexo" readonly="" class="form-control col-md-7 col-xs-12" value="<?php echo $anexo; ?>">
                                                </div>
                                            </div>
                                            <div class="item form-group" title="Nombre del Identificador">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12"  >Caller ID <span class="required">*</span>
                                                </label>
                                                <div class="col-md-8 col-sm-6 col-xs-12" id="cliente">
                                                    <input type="text" id="callerid" name="callerid" required class="form-control col-md-7 col-xs-12" value="<?php echo $callerid; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group" title="Negocio del Anexo">
                                                <label class="control-label col-md-3 col-sm-4 col-xs-12">Negocio <span class="required">*</span></label>
                                                <div class="col-md-9 col-sm-6 col-xs-12">
                                                    <select class='form-control' name='idnegocio1' id='idnegocio1' >
                                                        <?php include '../../system/selectNegocio.php'; ?>
                                                    </select>
                                                </div>
                                            </div> 
                                            <div class="form-group" title="Estado del Anexo">
                                                <label class="control-label col-md-3 col-sm-4 col-xs-12">Estado <span class="required">*</span></label>
                                                <div class="col-md-9 col-sm-6 col-xs-12">
                                                    <select class='form-control' name='idestado' id='idestado' >
                                                        <?php include '../../system/selectEstado.php'; ?>
                                                    </select>
                                                </div>
                                            </div> 
                                            <div class="clearfix"></div>
                                            <div class="panel-footer">
                                                <div style="text-align: center;">
                                                    <a href="listaAnex2?int=1&anex=1&id_neg=<?php echo $_GET['id_neg']; ?>" class="btn btn-danger">Cancelar</a>
                                                    <input type="button" value="Actualizar" id="actualizar" name="actualizar" class="btn btn-primary" onclick="this.disabled = true;
                                                                this.value = 'Actualizando...';
                                                                this.form.submit()"/>
                                                    <input type="hidden" name="form" value="modificarAnexoLOGS"/>
                                                    <input type="hidden" name="idanexo" value="<?php echo $idanexo; ?>"/>
                                                    <input type="hidden" name="id_neg" value="<?php echo $_GET['id_neg']; ?>"/>
                                                </div>
                                            </div>                                          
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <?php include '../VISTAS/listaModals.php'; ?>
    <script>
        $(document).ready(function () {
            $('input.flat').iCheck({
                checkboxClass: 'icheckbox_flat-green', radioClass: 'iradio_flat-green'
            });
        });

        var asInitVals = new Array();
        $(document).ready(function () {
            var oTable = $('#example').dataTable({
                "oLanguage": {
                    "sSearch": "Buscar:"},
                "aoColumnDefs": [
                    {
                        'bSortable': false,
                        'aTargets': [0]
                    } //disables sorting for column one
                ],
                'iDisplayLength': <?php echo 10; ?>,
                "sPaginationType": "full_numbers",
                "dom": 'T<"clear">lfrtip',
                //"tableTools": {                 // "sSwfPath": "<?php // echo base_url('../.../js/datatables/tools/swf/copy_csv_xls_pdf.swf');                                                                                       ?>"
                //}
            });
            $("tfoot input").keyup(function () {
                /* Filter on the column based on the index of this element's parent <th> */
                oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
            });
            $("tfoot input").each(function (i) {
                asInitVals[i] = this.value;
            });
            $("tfoot input").focus(function () {
                if (this.className == "search_init") {
                    this.className = "";
                    this.value = "";
                }
            });
            $("tfoot input").blur(function (i) {
                if (this.value == "") {
                    this.className = "search_init";
                    this.value = asInitVals[$("tfoot input").index(this)];
                }
            });
        });
    </script>
    <?php
    require('../../include/footer.php');
    