<?php
require_once('../../include/header.php');
require_once('../../system/crearConexionSMS.php');
clearstatcache();
if ($_SESSION['dni'] == "") {
    //si no existe, va a la página de autenticacion
    header("Location: inicio");
    //salimos de este script 
    exit();
}
//las alertar
include '../../system/mensajesAlerta.php';
?>
<!-- page content -->
<div class="right_col" role="main">

    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Gestión de Mantenimiento Negocios</h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">

        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('input.flat').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });

        var asInitVals = new Array();
        $(document).ready(function () {
            var oTable = $('#example').dataTable({
                "oLanguage": {
                    "sSearch": "Buscar:"
                },
                "aoColumnDefs": [
                    {
                        'bSortable': false,
                        'aTargets': [0]
                    } //disables sorting for column one
                ],
                'iDisplayLength': <?php echo 10; ?>,
                "sPaginationType": "full_numbers",
                "dom": 'T<"clear">lfrtip',
                //"tableTools": {
                // "sSwfPath": "<?php // echo base_url('../.../js/datatables/tools/swf/copy_csv_xls_pdf.swf');                                                                 ?>"
                //}
            });
            $("tfoot input").keyup(function () {
                /* Filter on the column based on the index of this element's parent <th> */
                oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
            });
            $("tfoot input").each(function (i) {
                asInitVals[i] = this.value;
            });
            $("tfoot input").focus(function () {
                if (this.className == "search_init") {
                    this.className = "";
                    this.value = "";
                }
            });
            $("tfoot input").blur(function (i) {
                if (this.value == "") {
                    this.className = "search_init";
                    this.value = asInitVals[$("tfoot input").index(this)];
                }
            });
        });
    </script>
    <?php
    require('../../include/footer.php');
    