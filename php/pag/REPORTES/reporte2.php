<?php
require('../../include/header.php');
include '../../system/mensajesAlerta.php';
include_once '../../system/crearConexion.php';
clearstatcache();
$titulo = "Cargos";
$hoja = "Cargos";
$idpais = $_GET['idpais'];
$idgrupo = $_GET['idgrupo'];
?>

<script type="text/javascript">
    $(document).ready(function () {
        var lista = document.getElementById("idpais");
        $('#idpais').change(function () {
            window.location = "repor1?idpais=" + lista.value;
        });
    });
</script>

<!-- page content -->
<div class="right_col" <?php echo $newDimensionBody; ?> role="main">
    <div class="clearfix"></div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="col-md-12 col-sm-12 col-xs-12"> 
                <div style="text-align: center">
                    <b><u><h2 style="font-size: 25px;">Reportes de Estado de Pago</h2></u></b>
                </div>

                <div class="ln_solid"></div>
            </div>
            <div class="x_content">
                <div class="col-md-12 col-sm-12 col-xs-12"> <!-- Tabla de Miembros -->
                    <div style="text-align: left">
                        <h2 style="font-size: 22px;">Reporte de Miembros</h2>
                    </div>
                    <table   id=""  class="table table-striped table-responsive">
                        <thead bgcolor="black" style="color: white;">
                            <tr >
                                <th colspan="1" style=" text-align: center;" class="column-title">Descripción</th>
                                <th colspan="2" style="text-align: center;" class="column-title">Miembros</th> 
                                <th colspan="2" style="text-align: center;" class="column-title">Cancelado</th>                    
                                <th colspan="2" style="text-align: center;" class="column-title">No Cancelado</th>                    
                                <th colspan="2" style="text-align: center;" class="column-title">A Cuenta</th>
                            </tr>

                        </thead>
                        <tbody>
                            <?php
                            $criterio = "";
                            if ($_GET['idpais'] != 0) {
                                $criterio .= "and  p.idpais=$idpais ";
                            }
                            $sql = "select 
(case 
when p.idpais = 89 and i.idciudad =19 then ' Lima' 
when p.idpais = 89 and i.idciudad !=19 then ' Provincia' 
when p.idpais = 123 then 'Bolivia'
when p.idpais = 103 then 'Ecuador'
when p.idpais = 82 then 'Colombia'
when p.idpais = 36 then 'Costa Rica'
when p.idpais = 124 then 'Panama'
when p.idpais = 138 then 'Republica Dominicana'
when p.idpais = 42 then 'México'
when p.idpais = 16 then 'Haiti'
end )as Descripcion,
count(case when i.idcategoria=1 then 'Pago' end) Miembros,
(select count(x.idpersona) from incripciones_2016 x inner join persona pe on pe.idpersona = x.idpersona where pe.idpais=89 and x.idcategoria=1) P_Miembros,
count(case when i.idcategoria=1 and i.idestado_pago=2 then 'Pago' end) Cancelado,
(select count(x.idpersona) from incripciones_2016 x inner join persona pe on pe.idpersona = x.idpersona where pe.idpais=89 and x.idcategoria=1 and x.idestado_pago=2) P_cancelados,
count(case when i.idcategoria=1 and i.idestado_pago=3 then 'Pago' end) No_Cancelado,
(select count(x.idpersona) from incripciones_2016 x inner join persona pe on pe.idpersona = x.idpersona where pe.idpais=89 and x.idcategoria=1 and x.idestado_pago=3) P_No_Cancelado,
count(case when i.idcategoria=1 and i.idestado_pago=1 then 'Pago' end) A_CTA,
(select count(x.idpersona) from incripciones_2016 x inner join persona pe on pe.idpersona = x.idpersona where pe.idpais=89 and x.idcategoria=1 and x.idestado_pago=1) P_A_CTA
from incripciones_2016 i 
inner join persona p on p.idpersona = i.idpersona
group by Descripcion;
";
                            $result = $mysqlMBN->consultas($sql);
                            $x = 1;
                            $z = 1;
                            while ($row = mysqli_fetch_array($result)) {
                                $Descripcion = $row['Descripcion'];
                                $Miembros = $row['Miembros'];
                                $Cancelado = $row['Cancelado'];
                                $No_Cancelado = $row['No_Cancelado'];
                                $A_CTA = $row['A_CTA'];
                                $miem_per = $row['P_Miembros'];
                                $canc_per = $row['P_cancelados'];
                                $ncan_per = $row['P_No_Cancelado'];
                                $acta_per = $row['P_A_CTA'];
                                $s1 += $Miembros;
                                $s2 += $Cancelado;
                                $s3 += $No_Cancelado;
                                $s4 += $A_CTA;
                                ?>
                                <tr class="even pointer" style="text-align: center; "><!--Resultados-->
                                    <td><?php echo $Descripcion; ?></td>
                                    <td colspan="<?php
                                    if ($x >= 3) {
                                        echo 2;
                                    } else {
                                        echo 1;
                                    }
                                    ?>"><?php echo $Miembros; ?></td>
                                        <?php if ($x == 1) { ?>
                                        <td rowspan="2" bgcolor='white'><?php echo "<br>" . $miem_per; ?></td>
                                    <?php } ?>
                                    <td colspan="<?php
                                    if ($x >= 3) {
                                        echo 2;
                                    } else {
                                        echo 1;
                                    }
                                    ?>"><?php echo $Cancelado; ?></td>
                                        <?php if ($x == 1) { ?>
                                        <td rowspan="2" bgcolor='white'><?php echo "<br>" . $canc_per; ?></td>
                                    <?php } ?>
                                    <td colspan="<?php
                                    if ($x >= 3) {
                                        echo 2;
                                    } else {
                                        echo 1;
                                    }
                                    ?>"><?php echo $No_Cancelado; ?></td>
                                        <?php if ($x == 1) { ?>
                                        <td rowspan="2" bgcolor='white'><?php echo "<br>" . $ncan_per; ?></td>
                                    <?php } ?>
                                    <td colspan="<?php
                                    if ($x >= 3) {
                                        echo 2;
                                    } else {
                                        echo 1;
                                    }
                                    ?>"><?php echo $A_CTA; ?></td>
                                        <?php if ($x == 1) { ?>
                                        <td rowspan="2" bgcolor='white'><?php echo "<br>" . $acta_per; ?></td>
                                    <?php } ?>

                                </tr>
                                <?php
                                $x++;
                            }
                            ?>  
                            <tr class="even pointer" style="text-align: center; ">
                                <td>Total</td>
                                <td colspan="2"><?php echo $s1; ?></td>
                                <td colspan="2"><?php echo $s2; ?></td>
                                <td colspan="2"><?php echo $s3; ?></td>
                                <td colspan="2"><?php echo $s4; ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="ln_solid"></div>
                </div> <!-- End Tabla de Miembros -->

                <div class="col-md-12 col-sm-12 col-xs-12"> <!-- Tabla de Invitados -->
                    <div style="text-align: left">
                        <h2 style="font-size: 22px;">Reporte de Invitados</h2>
                    </div>
                    <table   id=""  class="table table-striped table-responsive">
                        <thead bgcolor="black" style="color: white;">
                            <tr >
                                <th colspan="1" style=" text-align: center;" class="column-title">Descripción</th>
                                <th colspan="2" style="text-align: center;" class="column-title">Invitados</th> 
                                <th colspan="2" style="text-align: center;" class="column-title">Cancelado</th>                    
                                <th colspan="2" style="text-align: center;" class="column-title">No Cancelado</th>                    
                                <th colspan="2" style="text-align: center;" class="column-title">A Cuenta</th>
                            </tr>

                        </thead>
                        <tbody>
                            <?php
                            $criterio = "";
                            if ($_GET['idpais'] != 0) {
                                $criterio .= "and  p.idpais=$idpais ";
                            }
                            $sql = "select 
(case 
when p.idpais = 89 and i.idciudad =19 then ' Lima' 
when p.idpais = 89 and i.idciudad !=19 then ' Provincia' 
when p.idpais = 123 then 'Bolivia'
when p.idpais = 103 then 'Ecuador'
when p.idpais = 82 then 'Colombia'
when p.idpais = 36 then 'Costa Rica'
when p.idpais = 124 then 'Panama'
when p.idpais = 138 then 'Republica Dominicana'
when p.idpais = 42 then 'México'
when p.idpais = 16 then 'Haiti'
end )as Descripcion,
count(case when i.idcategoria=2 then 'Pago' end) Miembros,
(select count(x.idpersona) from incripciones_2016 x inner join persona pe on pe.idpersona = x.idpersona where pe.idpais=89 and x.idcategoria=2) P_Miembros,
count(case when i.idcategoria=2 and i.idestado_pago=2 then 'Pago' end) Cancelado,
(select count(x.idpersona) from incripciones_2016 x inner join persona pe on pe.idpersona = x.idpersona where pe.idpais=89 and x.idcategoria=2 and x.idestado_pago=2) P_cancelados,
count(case when i.idcategoria=2 and i.idestado_pago=3 then 'Pago' end) No_Cancelado,
(select count(x.idpersona) from incripciones_2016 x inner join persona pe on pe.idpersona = x.idpersona where pe.idpais=89 and x.idcategoria=2 and x.idestado_pago=3) P_No_Cancelado,
count(case when i.idcategoria=2 and i.idestado_pago=1 then 'Pago' end) A_CTA,
(select count(x.idpersona) from incripciones_2016 x inner join persona pe on pe.idpersona = x.idpersona where pe.idpais=89 and x.idcategoria=2 and x.idestado_pago=1) P_A_CTA
from incripciones_2016 i 
inner join persona p on p.idpersona = i.idpersona
group by Descripcion;
";
                            $result1 = $mysqlMBN->consultas($sql);
                            $x = 1;
                            $z = 1;
                            while ($row1 = mysqli_fetch_array($result1)) {
                                $Descripcion = $row1['Descripcion'];
                                $Miembros_i = $row1['Miembros'];
                                $Cancelado_i = $row1['Cancelado'];
                                $No_Cancelado_i = $row1['No_Cancelado'];
                                $A_CTA_i = $row1['A_CTA'];
                                $miem_per = $row1['P_Miembros'];
                                $canc_per = $row1['P_cancelados'];
                                $ncan_per = $row1['P_No_Cancelado'];
                                $acta_per = $row1['P_A_CTA'];
                                $s5 += $Miembros_i;
                                $s6 += $Cancelado_i;
                                $s7 += $No_Cancelado_i;
                                $s8 += $A_CTA_i;
                                ?>
                                <tr class="even pointer" style="text-align: center; "><!--Resultados-->
                                    <td><?php echo $Descripcion; ?></td>
                                    <td colspan="<?php
                                    if ($x >= 3) {
                                        echo 2;
                                    } else {
                                        echo 1;
                                    }
                                    ?>"><?php echo $Miembros_i; ?></td>
                                        <?php if ($x == 1) { ?>
                                        <td rowspan="2" bgcolor='white'><?php echo "<br>" . $miem_per; ?></td>
                                    <?php } ?>
                                    <td colspan="<?php
                                    if ($x >= 3) {
                                        echo 2;
                                    } else {
                                        echo 1;
                                    }
                                    ?>"><?php echo $Cancelado_i; ?></td>
                                        <?php if ($x == 1) { ?>
                                        <td rowspan="2" bgcolor='white'><?php echo "<br>" . $canc_per; ?></td>
                                    <?php } ?>
                                    <td colspan="<?php
                                    if ($x >= 3) {
                                        echo 2;
                                    } else {
                                        echo 1;
                                    }
                                    ?>"><?php echo $No_Cancelado_i; ?></td>
                                        <?php if ($x == 1) { ?>
                                        <td rowspan="2" bgcolor='white'><?php echo "<br>" . $ncan_per; ?></td>
                                    <?php } ?>
                                    <td colspan="<?php
                                    if ($x >= 3) {
                                        echo 2;
                                    } else {
                                        echo 1;
                                    }
                                    ?>"><?php echo $A_CTA_i; ?></td>
                                        <?php if ($x == 1) { ?>
                                        <td rowspan="2" bgcolor='white'><?php echo "<br>" . $acta_per; ?></td>
                                    <?php } ?>

                                </tr>
                                <?php
                                $x++;
                            }
                            ?>  
                            <tr class="even pointer" style="text-align: center; ">
                                <td>Total</td>
                                <td colspan="2"><?php echo $s5; ?></td>
                                <td colspan="2"><?php echo $s6; ?></td>
                                <td colspan="2"><?php echo $s7; ?></td>
                                <td colspan="2"><?php echo $s8; ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="ln_solid"></div>
                </div>  <!-- End Tabla de Invitados -->

                <div class="col-md-12 col-sm-12 col-xs-12"> <!-- Reporte General-->
                    <div style="text-align: left">
                        <h2 style="font-size: 22px;">Reporte General</h2>
                    </div>
                    <table   id=""  class="table table-striped table-responsive">
                        <thead bgcolor="black" style="color: white;">
                            <tr >
                                <th colspan="1" style=" text-align: center;" class="column-title">Paises</th>
                                <th colspan="1" style="text-align: center;" class="column-title">Total Participantes</th> 
                                <th colspan="1" style="text-align: center;" class="column-title">Cancelado</th>                    
                                <th colspan="1" style="text-align: center;" class="column-title">No Cancelado</th>                    
                                <th colspan="1" style="text-align: center;" class="column-title">A Cuenta</th>
                            </tr>

                        </thead>
                        <tbody>
                            <?php
                            $sql3 = "select (case 
when p.idpais = 89 then ' Peru' 
when p.idpais = 123 then 'Bolivia'
when p.idpais = 103 then 'Ecuador'
when p.idpais = 82 then 'Colombia'
when p.idpais = 36 then 'Costa Rica'
when p.idpais = 124 then 'Panama'
when p.idpais = 138 then 'Republica Dominicana'
when p.idpais = 42 then 'México'
when p.idpais = 16 then 'Haiti'
end )as Paises, 
count(*) Total_participantes,
count(case when i.idestado_pago= 2 then 'pago' end) Cancelaron,
count(case when i.idestado_pago= 3 then 'pago' end) No_Cancelaron,
count(case when i.idestado_pago= 1 then 'pago' end) A_CTA
from incripciones_2016 i 
inner join persona p on p.idpersona = i.idpersona
inner join pais pa on pa.idpais = p.idpais
group by p.idpais
order by 1;";
                            $result2 = $mysqlMBN->consultas($sql3);
                            $x = 1; // posicion
                            $z = 1;
                            while ($row2 = mysqli_fetch_array($result2)) {
                                $paises = $row2['Paises'];
                                $totalP = $row2['Total_participantes'];
                                $cancelaron = $row2['Cancelaron'];
                                $no_cancelaron = $row2['No_Cancelaron'];
                                $a_cta = $row2['A_CTA'];
                                $s9 +=$totalP;
                                $s10 +=$cancelaron;
                                $s11 +=$no_cancelaron;
                                $s12 +=$a_cta;
                                ?>
                                <tr class="even pointer" style="text-align: center; "><!--Resultados-->
                                    <td><?php echo $paises; ?></td>                                    
                                    <td><?php echo $totalP; ?></td>
                                    <td><?php echo $cancelaron; ?></td>
                                    <td><?php echo $no_cancelaron; ?></td>
                                    <td><?php echo $a_cta; ?></td>
                                </tr>
                            <?php } ?>
                            <tr class="even pointer" style="text-align: center; ">
                                <td>Total</td>
                                <td colspan="1"><?php echo $s9; ?></td>
                                <td colspan="1"><?php echo $s10; ?></td>
                                <td colspan="1"><?php echo $s11; ?></td>
                                <td colspan="1"><?php echo $s12; ?></td>
                            </tr>
                        </tbody>

                    </table>
                    <div class="ln_solid"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <?php
    require('../../include/footer.php');
    ?>
