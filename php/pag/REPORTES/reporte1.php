<?php
require('../../include/header.php');
include '../../system/mensajesAlerta.php';
include_once '../../system/crearConexion.php';
clearstatcache();
$titulo = "Cargos";
$hoja = "Cargos";
$idpais = $_GET['idpais'];
$idciudad = $_GET['idciudad'];
?>

<script type="text/javascript">
    $(document).ready(function () {
//        var lista = document.getElementById("idpais");
//        $('#idpais').change(function () {
//            window.location = "repor1?idpais=" + lista.value;
//        });
        load_ciudad1(<?php echo "$idpais,$idciudad"; ?>);
    });
</script>

<script src="js/ajax.js" type="text/javascript"></script>
<!-- page content -->
<div class="right_col" <?php echo $newDimensionBody; ?> role="main">
    <div class="page-title">
        <div class="title_left">
            <h3>Listado de los participantes Inscritos</h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <form action="" method="GET">
            <div class="x_panel">
                <div class="x_title">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div style="text-align: center;" class="col-md-1 col-sm-1 col-xs-1" title="Seleccione el Pais">
                            <label class="control-label">Seleccione el Pais</label>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3" title="Seleccione el Pais">
                            <select class='form-control' name='idpais' id='idpais'  onchange="load_ciudad(this.value)">
                                <option value="0">Todos</option>
                                <?php include_once '../../system/selectPais.php'; ?>
                            </select>
                        </div>
                        <div style="text-align: center;" class="col-md-1 col-sm-1 col-xs-1" title="Seleccione la ciudad">
                            <label class="control-label">Seleccione Ciudad</label>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3" title="Seleccione la ciudad" id="myDivCiudad">
                            <select class='form-control' name='idciudad' id='idciudad' >
                                <option value="0">Todos</option>
                                <?php //include_once '../../system/selectCiudad.php'; ?>
                            </select>
                        </div >
                        <div class="col-md-2 col-sm-2 col-xs-2" >
                            <input type="submit" value="Filtrar" class="btn btn-danger" />
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table   id=""  class="table table-striped table-responsive">
                        <thead bgcolor="#3498DB" style="color: white;">
                            <tr >
                                <th rowspan="3" style=" text-align: center;" class="column-title">Grupo</th>
                                <th colspan="6" bgcolor="#F8A51B" style="text-align: center;" class="column-title">A Cuenta</th> 
                                <th colspan="6" bgcolor="#17a086" style="text-align: center;" class="column-title">Cancelado</th>                    
                                <th colspan="6" bgcolor="#bf3a2b" style="text-align: center;" class="column-title">No Cancelado</th>                    
                                <th colspan="6" style="text-align: center;" class="column-title">Resumen General</th>
                            </tr>
                            <tr >
                                <?php for ($x = 1; $x <= 4; $x++) { ?>
                                    <th bgcolor="<?php
                                    if ($x == 1) {
                                        echo "#F8A51B";
                                    } else if ($x == 2) {
                                        echo "#17a086";
                                    } else if ($x == 3) {
                                        echo "#bf3a2b";
                                    }
                                    ?>" style="text-align: center;" colspan="2" class="column-title">Miembro</th>
                                    <th bgcolor="<?php
                                    if ($x == 1) {
                                        echo "#F8A51B";
                                    } else if ($x == 2) {
                                        echo "#17a086";
                                    } else if ($x == 3) {
                                        echo "#bf3a2b";
                                    }
                                    ?>" style="text-align: center;" colspan="2" class="column-title">Invitado</th>
                                    <th bgcolor="<?php
                                    if ($x == 1) {
                                        echo "#F8A51B";
                                    } else if ($x == 2) {
                                        echo "#17a086";
                                    } else if ($x == 3) {
                                        echo "#bf3a2b";
                                    }
                                    ?>" style="text-align: center;" colspan="2" class="column-title">Total</th>
                                    <?php } ?>
                            </tr>
                            <tr >  
                                <?php for ($x = 1; $x <= 12; $x++) { ?>
                                    <th bgcolor="<?php
                                    if ($x == 1 || $x == 2 || $x == 3) {
                                        echo "#F8A51B";
                                    } else if ($x == 4 || $x == 5 || $x == 6) {
                                        echo "#17a086";
                                    } else if ($x == 7 || $x == 8 || $x == 9) {
                                        echo "#bf3a2b";
                                    }
                                    ?>"style="text-align: center;" class="column-title">V</th>
                                    <th bgcolor="<?php
                                    if ($x == 1 || $x == 2 || $x == 3) {
                                        echo "#F8A51B";
                                    } else if ($x == 4 || $x == 5 || $x == 6) {
                                        echo "#17a086";
                                    } else if ($x == 7 || $x == 8 || $x == 9) {
                                        echo "#bf3a2b";
                                    }
                                    ?>"style="text-align: center;" class="column-title">M</th>
                                    <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $criterio = "";
                            if ($_GET['idpais'] != 0) {
                                $criterio .= " and  p.idpais=$idpais ";
                            }
                            if ($_GET['idciudad'] != 0) {
                                $criterio .= " and  i.idciudad=$idciudad ";
                            }
                            $sql = "select 
(case 
when grupo = 'JUNNIOR' then '1' 
when grupo = 'TRUTH' then '2' 
when grupo = 'FAITH' then '3' 
when grupo = 'GRACE' then '4' 
when grupo = 'SENIOR' then '5' 
when grupo = 'SILVER' then '6' 
when grupo = '' then '7' 
end )as '0',/* estadoPago_Condicion_Sexo */
grupo as '1', 
count(case when idestado_pago=1 and i.idcategoria=1 and i.sexo = 'M' then 'Pago' end) '2',
count(case when idestado_pago=1 and i.idcategoria=1 and i.sexo = 'F' then 'Pago' end) '3',
count(case when idestado_pago=1 and i.idcategoria=2 and i.sexo = 'M' then 'Pago' end) '4',
count(case when idestado_pago=1 and i.idcategoria=2 and i.sexo = 'F' then 'Pago' end) '5',
count(case when i.idestado_pago=1 and i.sexo='M' then 'Miembro' end) '6' ,
count(case when i.idestado_pago=1 and i.sexo='F' then 'Miembro' end) '7' ,
count(case when idestado_pago=2 and i.idcategoria=1 and i.sexo = 'M' then 'Pago' end) '8',
count(case when idestado_pago=2 and i.idcategoria=1 and i.sexo = 'F' then 'Pago' end) '9',
count(case when idestado_pago=2 and i.idcategoria=2 and i.sexo = 'M' then 'Pago' end) '10',
count(case when idestado_pago=2 and i.idcategoria=2 and i.sexo = 'F' then 'Pago' end) '11',
count(case when i.idestado_pago=2 and i.sexo='M' then 'Miembro' end) '12' ,
count(case when i.idestado_pago=2 and i.sexo='F' then 'Miembro' end) '13' ,
count(case when idestado_pago=3 and i.idcategoria=1 and i.sexo = 'M' then 'Pago' end) '14',
count(case when idestado_pago=3 and i.idcategoria=1 and i.sexo = 'F' then 'Pago' end) '15',
count(case when idestado_pago=3 and i.idcategoria=2 and i.sexo = 'M' then 'Pago' end) '16',
count(case when idestado_pago=3 and i.idcategoria=2 and i.sexo = 'F' then 'Pago' end) '17',
count(case when i.idestado_pago=3 and i.sexo='M' then 'Miembro' end) '18' ,
count(case when i.idestado_pago=3 and i.sexo='F' then 'Miembro' end) '19' ,
count(case when i.idcategoria=1 and i.sexo='M' then 'Miembro' end) '20',
count(case when i.idcategoria=1 and i.sexo='F' then 'Miembro' end) '21',
count(case when i.idcategoria=2 and i.sexo='M' then 'Miembro' end) '22',
count(case when i.idcategoria=2 and i.sexo='F' then 'Miembro' end) '23',
count(case when i.sexo='M' then 'Miembro' end) '24',
count(case when i.sexo='F' then 'Miembro' end) '25'
from incripciones_2016 i 
inner join persona p on p.idpersona = i.idpersona
where '1'='1' $criterio                                    
group by grupo ORDER BY 1;";
                            $result = $mysqlMBN->consultas($sql);
                            $x = 1;
                            $z = 1;
                            while ($row = mysqli_fetch_array($result)) {
                                $s1 += $row['1'];
                                $s2 += $row['2'];
                                $s3 += $row['3'];
                                $s4 += $row['4'];
                                $s5 += $row['5'];
                                $s6 += $row['6'];
                                $s7 += $row['7'];
                                $s8 += $row['8'];
                                $s9 += $row['9'];
                                $s10 += $row['10'];
                                $s11 += $row['11'];
                                $s12 += $row['12'];
                                $s13 += $row['13'];
                                $s14 += $row['14'];
                                $s15 += $row['15'];
                                $s16 += $row['16'];
                                $s17 += $row['17'];
                                $s18 += $row['18'];
                                $s19 += $row['19'];
                                $s20 += $row['20'];
                                $s21 += $row['21'];
                                $s22 += $row['22'];
                                $s23 += $row['23'];
                                $s24 += $row['24'];
                                $s25 += $row['25'];
                                ?>
                                <tr class="even pointer" style="text-align: center; "><!--Resultados-->
                                    <?php
                                    for ($y = 1; $y <= 25; $y++) {
                                        ?>
                                        <td <?php
                                        if ($y == 1) {
                                            echo "bgcolor='#3498DB' style='color:white;font-weight:bold;'";
                                        } else if ($y >= 2 && $y <= 5) {
                                            echo "bgcolor='#fdfbca'";
                                        } else if ($y >= 6 && $y <= 7) {
                                            echo "bgcolor='#F8A51B'  style='color:white;font-weight:bold;' ";
                                        } else if ($y >= 8 && $y <= 11) {
                                            echo "bgcolor='#c0dfd0' ";
                                        } else if ($y >= 12 && $y <= 13) {
                                            echo "bgcolor='#17a086'  style='color:white;font-weight:bold;' ";
                                        } else if ($y >= 14 && $y <= 17) {
                                            echo "bgcolor='#fad2d0' ";
                                        } else if ($y >= 18 && $y <= 19) {
                                            echo "bgcolor='#bf3a2b'  style='color:white;font-weight:bold;' ";
                                        } else if ($y >= 24 && $y <= 25) {
                                            echo "bgcolor='#3498DB'  style='color:white;font-weight:bold;' ";
                                        }
                                        ?> 
                                            ><?php echo $row[$y]; ?>
                                        </td>
                                    <?php } ?>
                                </tr>
                                <?php
                                $x++;
                            } //include '../VISTAS/manTable' . $hoja . '.php';  
                            ?>   
                            <tr style="font-weight:bold;font-size: 14px;">
                                <td bgcolor='#3498DB' style="text-align: center;color: white;"><?php echo "Totales"; ?></td>
                                <td bgcolor='#fdfbca' style="text-align: center;"><?php echo $s2 ?></td>
                                <td bgcolor='#fdfbca' style="text-align: center;"><?php echo $s3; ?></td>
                                <td bgcolor='#fdfbca' style="text-align: center;"><?php echo $s4; ?></td>
                                <td bgcolor='#fdfbca' style="text-align: center;"><?php echo $s5; ?></td>
                                <td bgcolor='#F8A51B' style="color:white;text-align: center;"><?php echo $s6; ?></td>
                                <td bgcolor='#F8A51B' style="color:white;text-align: center;"><?php echo $s7; ?></td>
                                <td bgcolor='#c0dfd0' style="text-align: center;"><?php echo $s8; ?></td>
                                <td bgcolor='#c0dfd0' style="text-align: center;"><?php echo $s9; ?></td>
                                <td bgcolor='#c0dfd0' style="text-align: center;"><?php echo $s10; ?></td>
                                <td bgcolor='#c0dfd0' style="text-align: center;"><?php echo $s11; ?></td>
                                <td bgcolor='#17a086' style="color:white;text-align: center;"><?php echo $s12; ?></td>
                                <td bgcolor='#17a086' style="color:white;text-align: center;"><?php echo $s13; ?></td>
                                <td bgcolor='#fad2d0' style="text-align: center;"><?php echo $s14; ?></td>
                                <td bgcolor='#fad2d0' style="text-align: center;"><?php echo $s15; ?></td>
                                <td bgcolor='#fad2d0' style="text-align: center;"><?php echo $s16; ?></td>                              
                                <td bgcolor='#fad2d0' style="text-align: center;"><?php echo $s17; ?></td>
                                <td bgcolor='#bf3a2b' style="color:white;text-align: center;"><?php echo $s18; ?></td>
                                <td bgcolor='#bf3a2b' style="color:white;text-align: center;"><?php echo $s19; ?></td>
                                <td bgcolor='' style="text-align: center;"><?php echo $s20; ?></td>
                                <td bgcolor='' style="text-align: center;"><?php echo $s21; ?></td>
                                <td bgcolor='' style="text-align: center;"><?php echo $s22; ?></td>
                                <td bgcolor='' style="text-align: center;"><?php echo $s23; ?></td>
                                <td bgcolor='#3498DB' style="color:white;text-align: center;"><?php echo $s24; ?></td>
                                <td bgcolor='#3498DB' style="color:white;text-align: center;"><?php echo $s25; ?></td>
                            </tr>
                            <tr style="text-align: center;font-weight:bold;font-size: 14px;color: white;">
                                <td bgcolor='white' colspan="5">&nbsp;</td>
                                <td bgcolor='#F8A51B' colspan="2"><?php echo $s6 + $s7; ?></td>
                                <td bgcolor='white' colspan="4">&nbsp;</td>
                                <td bgcolor='#17a086' colspan="2"><?php echo $s12 + $s13; ?></td>
                                <td bgcolor='white' colspan="4">&nbsp;</td>
                                <td bgcolor='#bf3a2b' colspan="2"><?php echo $s18 + $s19; ?></td>
                                <td bgcolor='white' colspan="4">&nbsp;</td>
                                <td bgcolor='#3498DB' colspan="2"><?php echo $s24 + $s25; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </form>
    </div>

    <div class="clearfix"></div>
    <?php
    require('../../include/footer.php');
    ?>
