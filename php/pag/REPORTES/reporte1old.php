<?php
require('../../include/header.php');
include '../../system/mensajesAlerta.php';
include_once '../../system/crearConexion.php';
clearstatcache();
$titulo = "Cargos";
$hoja = "Cargos";
$idpais = $_GET['idpais'];
$idgrupo = $_GET['idgrupo'];
?>

<script type="text/javascript">
    $(document).ready(function () {
        var lista = document.getElementById("idpais");
        $('#idpais').change(function () {
            window.location = "repor1?idpais=" + lista.value;
        });
    });
</script>
<!-- page content -->
<div class="right_col" <?php echo $newDimensionBody; ?> role="main">
    <div class="page-title">
        <div class="title_left">
            <h3>Listado de los participantes Inscritos</h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <form action="" method="GET">
            <div class="x_panel">
                <div class="x_title">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-2 col-sm-5 col-xs-5" title="Seleccione el Pais">
                            <label class="control-label">Seleccione el Pais</label>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-5" title="Seleccione el Pais">
                            <select class='form-control' name='idpais' id='idpais' >
                                <option value="0">Todos</option>
                                <?php include_once '../../system/selectPais.php'; ?>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table   id=""  class="table table-striped table-responsive">
                        <thead bgcolor="#3498DB" style="color: white;">
                            <tr >
                                <th rowspan="3" style="text-align: center;" class="column-title">Grupo</th>
                                <th colspan="4" style="text-align: center;" class="column-title">A CTA</th>
                                <th colspan="4" style="text-align: center;" class="column-title">Cancelado</th>
                                <th colspan="4" style="text-align: center;" class="column-title">No Cancelado</th>
                                <th colspan="4" style="text-align: center;" class="column-title">Subtotal</th>
                                <th rowspan="3" style="text-align: center;" class="column-title">Total</th>
                            </tr>
                            <tr >
                                <?php for ($x = 1; $x <= 4; $x++) { ?>
                                    <th style="text-align: center;" colspan="2" class="column-title">Miembro</th>
                                    <th style="text-align: center;" colspan="2" class="column-title">Invitado</th>
                                <?php } ?>
                            </tr>
                            <tr >  
                                <?php for ($x = 1; $x <= 8; $x++) { ?>
                                    <th style="text-align: center;" class="column-title">V</th>
                                    <th style="text-align: center;" class="column-title">M</th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $criterio = "";
                            if ($_GET['idpais'] != 0) {
                                $criterio .= "and  p.idpais=$idpais ";
                            }
                            $sql = "select grupo as '1', /* estadoPago_Condicion_Sexo */
count(case when idestado_pago=1 and i.idcategoria=1 and i.sexo = 'M' then 'Pago' end) '2',
count(case when idestado_pago=1 and i.idcategoria=1 and i.sexo = 'F' then 'Pago' end) '3',
count(case when idestado_pago=1 and i.idcategoria=2 and i.sexo = 'M' then 'Pago' end) '4',
count(case when idestado_pago=1 and i.idcategoria=2 and i.sexo = 'F' then 'Pago' end) '5',
count(case when idestado_pago=2 and i.idcategoria=1 and i.sexo = 'M' then 'Pago' end) '6',
count(case when idestado_pago=2 and i.idcategoria=1 and i.sexo = 'F' then 'Pago' end) '7',
count(case when idestado_pago=2 and i.idcategoria=2 and i.sexo = 'M' then 'Pago' end) '8',
count(case when idestado_pago=2 and i.idcategoria=2 and i.sexo = 'F' then 'Pago' end) '9',
count(case when idestado_pago=3 and i.idcategoria=1 and i.sexo = 'M' then 'Pago' end) '10',
count(case when idestado_pago=3 and i.idcategoria=1 and i.sexo = 'F' then 'Pago' end) '11',
count(case when idestado_pago=3 and i.idcategoria=2 and i.sexo = 'M' then 'Pago' end) '12',
count(case when idestado_pago=3 and i.idcategoria=2 and i.sexo = 'F' then 'Pago' end) '13',
count(case when i.idcategoria=1 and i.sexo='M' then 'Miembro' end) '14' ,
count(case when i.idcategoria=1 and i.sexo='F' then 'Miembro' end) '15' ,
count(case when i.idcategoria=2 and i.sexo='M' then 'Miembro' end) '16' ,
count(case when i.idcategoria=2 and i.sexo='F' then 'Miembro' end) '17' ,
count(i.idpersona) '18'
from incripciones_2016 i 
inner join persona p on p.idpersona = i.idpersona
where '1'='1' $criterio                                    
group by grupo;";
                            $result = $mysqlMBN->consultas($sql);
                            $x = 1;
                            $z = 1;
                            while ($row = mysqli_fetch_array($result)) {                                
                                        $s1 += $row['1'];
                                        $s2 += $row['2'];
                                        $s3 += $row['3'];
                                        $s4 += $row['4'];
                                        $s5 += $row['5'];
                                        $s6 += $row['6'];
                                        $s7 += $row['7'];
                                        $s8 += $row['8'];
                                        $s9 += $row['9'];
                                        $s10 += $row['10'];
                                        $s11 += $row['11'];
                                        $s12 += $row['12'];
                                        $s13 += $row['13'];
                                        $s14 += $row['14'];
                                        $s15 += $row['15'];
                                        $s16 += $row['16'];
                                        $s17 += $row['17'];
                                        $s18 += $row['18'];
                                ?>
                                <tr class="even pointer">
                                    <?php
                                    for ($y = 1; $y <= 18; $y++) {
                                        ?>
                                        <td style="text-align: center;"><?php echo $row[$y]; ?></td>
                                    <?php } ?>
                                </tr>
                                <?php
                                $x++;
                            } //include '../VISTAS/manTable' . $hoja . '.php';  
                            ?>   
                            <tr>
                                <td style="text-align: center;"><?php echo "Totales"; ?></td>
                                <td style="text-align: center;"><?php echo $s2 ?></td>
                                <td style="text-align: center;"><?php echo $s3; ?></td>
                                <td style="text-align: center;"><?php echo $s4; ?></td>
                                <td style="text-align: center;"><?php echo $s5; ?></td>
                                <td style="text-align: center;"><?php echo $s6; ?></td>
                                <td style="text-align: center;"><?php echo $s7; ?></td>
                                <td style="text-align: center;"><?php echo $s8; ?></td>
                                <td style="text-align: center;"><?php echo $s9; ?></td>
                                <td style="text-align: center;"><?php echo $s10; ?></td>
                                <td style="text-align: center;"><?php echo $s11; ?></td>
                                <td style="text-align: center;"><?php echo $s12; ?></td>
                                <td style="text-align: center;"><?php echo $s13; ?></td>
                                <td style="text-align: center;"><?php echo $s14; ?></td>
                                <td style="text-align: center;"><?php echo $s15; ?></td>
                                <td style="text-align: center;"><?php echo $s16; ?></td>                                
                                <td style="text-align: center;"><?php echo $s17; ?></td>
                                <td style="text-align: center;"><?php echo $s18; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </form>
    </div>

    <div class="clearfix"></div>
    <?php
    require('../../include/footer.php');
    ?>
