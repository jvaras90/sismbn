<?php
require('../../include/header.php');
include '../../system/mensajesAlerta.php';
include_once '../../system/crearConexion.php';
clearstatcache();
$titulo = "Cargos";
$hoja = "Cargos";
$idpais = $_GET['idpais'];
$idciudad = $_GET['idciudad'];

$fecDesde = $_GET['fecDesde'];
$fecHasta = $_GET['fecHasta'];
$now = date('Y-m-d');
?>
<script>
    jQuery.datepicker.regional['eu'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'yy-mm-dd',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''};
    $.datepicker.setDefaults($.datepicker.regional['eu']);
    $(function () {
        $("#dia").datepicker();
        $("#fecDesde").datepicker();
        $("#fecHasta").datepicker();
        $("#gesDesde").datepicker();
        $("#gesHasta").datepicker();
    });
</script>
<!-- Fin FIltros Fija -->

<script type="text/javascript">
//    $(document).ready(function () {
////        var lista = document.getElementById("idpais");
////        $('#idpais').change(function () {
////            window.location = "repor1?idpais=" + lista.value;
////        });
//        load_ciudad1(<?php echo "$idpais,$idciudad"; ?>);
//    });
</script>

<script src="js/ajax.js" type="text/javascript"></script>
<!-- page content -->
<div class="right_col" <?php echo $newDimensionBody; ?> role="main">
    <div class="page-title">
        <div class="title_left">
            <h3>Listado de Pagos Realizados</h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 col-sm-12 col-xs-12">

        <div class="x_panel">
            <div class="x_title">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <form class="form-horizontal form-label-left" method="get" action="" enctype="multipart/form-data">
                        <div class="form-group">
                            <label  style="text-align: center;" class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Seleccione Pais</label>
                            <div class="col-md-2 col-sm-3 col-xs-12">
                                <select class='form-control' name='idpais' id='idpais' >
                                    <option>Pais</option>
                                    <?php include '../../system/selectPais.php'; ?>
                                </select>
                            </div>
                            <label  style="text-align: center;" class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Seleccione Reunion Zonal</label>
                            <div class="col-md-2 col-sm-3 col-xs-12">
                                <select class='form-control' name='idreunion_zonal' id='idreunion_zonal' >
                                    <option>Reunion Zonal</option>
                                    <?php include '../../system/selectReunionZonal.php'; ?>
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-3 col-xs-12">
                                <button id="generar" name="generar" type="submit" class="btn btn-primary">Generar</button>
                                <input type="text" class="hidden" name="form" value="generar"/>
                            </div>
                        </div>
                        <div class="clearfix" ></div>                        
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table   id="example"  class="table table-striped table-responsive">
                    <thead bgcolor="#3498DB" style="color: white;">
                        <tr >
                            <th rowspan="1" style=" text-align: center;" class="column-title"> <input type="checkbox" id="check-all" class="flat"></th>
                            <th colspan="1" style="text-align: center;" class="column-title">Nombres</th> 
                            <th colspan="1" style="text-align: center;" class="column-title">Pais</th>                    
                            <th colspan="1" style="text-align: center;" class="column-title">Ciudad</th>                   
                            <th colspan="1" style="text-align: center;" class="column-title">Numero de Boleta</th>                  
                            <th colspan="1" style="text-align: center;" class="column-title">Monto Soles</th>                  
                            <th colspan="1" style="text-align: center;" class="column-title">Monto Dolares</th>                  
                            <th colspan="1" style="text-align: center;" class="column-title">Fecha Donación</th>                  
                            <th colspan="1" style="text-align: center;" class="column-title">Gestión</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $criterio = "";
                        if (isset($_GET['form']) && $_GET['form'] == 'generar') {
                            $criterio = " and (fecha_pago between '$fecDesde' and '$fecHasta'  )";
                        } else {
                            $criterio = " and fecha_pago = '$now' ";
                        }

                        $sql = "select i.idinscripciones , i.idpersona , concat(nombres,' ',apaterno,' ',amaterno) nombre,  des_pais pais,ci.descripcion ciudad,
b.*
from incripciones_2016 i  inner join persona p  on p.idpersona = i.idpersona 
inner join boleta_2016 b  on b.idinscripcion = i.idinscripciones
inner join pais pa on pa.idpais = p.idpais
inner join ciudad ci on ci.idciudad = i.idciudad
where '1' = '1' $criterio
order by fecha_pago desc
;";

                        $result = $mysqlMBN->consultas($sql);
                        while ($row = mysqli_fetch_array($result)) {
                            $idinscripciones = $row['idinscripciones'];
                            $idpersona = $row['idpersona'];
                            $nombre = $row['nombre'];
                            $pais = $row['pais'];
                            $ciudad = $row['ciudad'];
                            $nro_boleta = $row['nro_boleta'];
                            $monto_soles = $row['monto_soles'];
                            $monto_dolares = $row['monto_dolares'];
                            $fecha_pago = $row['fecha_pago'];
                            $tot_soles +=$monto_soles;
                            $tot_dolares += $monto_dolares;
                            ?>
                            <tr class="even pointer" style="text-align: center; "><!--Resultados-->
                                <?php
                                //for ($y = 1; $y <= 25; $y++) {
                                ?>
                                <td ><input type="checkbox" class="flat"> </td>
                                <td ><?php echo $nombre; ?> </td>
                                <td ><?php echo $pais; ?> </td>
                                <td ><?php echo $ciudad; ?> </td>
                                <td ><?php echo $nro_boleta; ?> </td>
                                <td ><?php echo "S/. " . $monto_soles; ?> </td>
                                <td ><?php echo "$/. " . $monto_dolares; ?> </td>
                                <td ><?php echo $fecha_pago; ?> </td>
                                <td >
                                    <a href="listInc?mod=mInscripcion&idinscripcion=<?php echo $idinscripciones ?>" class="btn btn-success btn-sm"><i class="fa fa-list"></i> Ver Inscripción</a></td>
                                <?php //}  ?>
                            </tr>
                            <?php
                            $x++;
                        } //include '../VISTAS/manTable' . $hoja . '.php';  
                        ?>   
                        <tr style="font-weight:bold;font-size: 14px;">
                            <td  style="text-align: center;">&nbsp;</td>
                            <td  style="text-align: center;">&nbsp;</td>
                            <td  style="text-align: center;">&nbsp;</td>
                            <td  style="text-align: center;">&nbsp;</td>
                            <td  style="text-align: center;">Totales</td>
                            <td  style="text-align: center;"><?php echo "S/. " . $tot_soles; ?></td>
                            <td  style="text-align: center;"><?php echo "$/. " . $tot_dolares; ?></td>
                            <td  style="text-align: center;">&nbsp;</td>
                            <td  style="text-align: center;">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>

    </div>

    <div class="clearfix"></div>
    <?php
    require('../../include/footer.php');
    ?>
