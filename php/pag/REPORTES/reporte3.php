<?php
require('../../include/header.php');
include '../../system/mensajesAlerta.php';
include_once '../../system/crearConexion.php';
clearstatcache();
$titulo = "Cargos";
$hoja = "Cargos";
$idpais = $_GET['idpais'];
$idgrupo = $_GET['idgrupo'];
?>

<script type="text/javascript">
    $(document).ready(function () {
        var lista = document.getElementById("idpais");
        $('#idpais').change(function () {
            window.location = "repor1?idpais=" + lista.value;
        });
    });
</script>

<!-- page content -->
<div class="right_col" <?php echo $newDimensionBody; ?> role="main">
    <div class="clearfix"></div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="col-md-12 col-sm-12 col-xs-12"> 
                <div style="text-align: center">
                    <b><u><h2 style="font-size: 25px;">Reportes de Varones y Mujeres</h2></u></b>
                </div>

                <div class="ln_solid"></div>
            </div>
            <div class="x_content">
                <div class="col-md-12 col-sm-12 col-xs-12"> <!-- Tabla de Miembros -->
                    <div style="text-align: left">
                        <h2 style="font-size: 22px;">Reporte de Miembros</h2>
                    </div>
                    <table   id=""  class="table table-striped table-responsive">
                        <thead bgcolor="black" style="color: white;">
                            <tr >
                                <th colspan="1" style=" text-align: center;" class="column-title"rowspan="2" >Descripción</th>
                                <th colspan="2" style="text-align: center;" class="column-title"rowspan="2" >Hombres</th> 
                                <th colspan="2" style="text-align: center;" class="column-title"rowspan="2" >Mujeres</th> 
                                <th colspan="2" style="text-align: center;" class="column-title">Cancelado</th>                    
                                <th colspan="2" style="text-align: center;" class="column-title">No Cancelado</th>                    
                                <th colspan="2" style="text-align: center;" class="column-title">A Cuenta</th>
                            </tr>
                            <tr>
                                <?php for($x=0;$x<=2;$x++){ ?>
                                <th colspan="1" style="text-align: center;" class="column-title">Varon</th>  
                                <th colspan="1" style="text-align: center;" class="column-title">Mujer</th>   
                                <?php } ?>
                            </tr>

                        </thead>
                        <tbody>
                            <?php
                            $criterio = "";
                            if ($_GET['idpais'] != 0) {
                                $criterio .= "and  p.idpais=$idpais ";
                            }
                            $sql1 = "select 
(case 
when p.idpais = 89 and i.idciudad =19 then ' Lima' 
when p.idpais = 89 and i.idciudad !=19 then ' Provincia' 
when p.idpais = 123 then 'Bolivia'
when p.idpais = 103 then 'Ecuador'
when p.idpais = 82 then 'Colombia'
when p.idpais = 36 then 'Costa Rica'
when p.idpais = 124 then 'Panama'
when p.idpais = 138 then 'Republica Dominicana'
when p.idpais = 42 then 'México'
when p.idpais = 16 then 'Haiti'
end )as Descripcion,
count(case when i.idcategoria=1 and i.sexo='M' then 'Pago' end) Hombres,
(select count(x.idpersona) from incripciones_2016 x inner join persona pe on pe.idpersona = x.idpersona where pe.idpais=89 and x.idcategoria=1 and x.sexo='M' ) P_Miembros_v,
count(case when i.idcategoria=1 and i.sexo='F' then 'Pago' end) Mujeres,
(select count(x.idpersona) from incripciones_2016 x inner join persona pe on pe.idpersona = x.idpersona where pe.idpais=89 and x.idcategoria=1 and x.sexo='F') P_Miembros_m,
count(case when i.idcategoria=1 and i.idestado_pago=2 and i.sexo='M' then 'Pago' end) C_Varon,
count(case when i.idcategoria=1 and i.idestado_pago=2 and i.sexo='F' then 'Pago' end) C_Mujer,
count(case when i.idcategoria=1 and i.idestado_pago=3 and i.sexo='M' then 'Pago' end) NC_Varon,
count(case when i.idcategoria=1 and i.idestado_pago=3 and i.sexo='F' then 'Pago' end) NC_Mujer,
count(case when i.idcategoria=1 and i.idestado_pago=1 and i.sexo='M' then 'Pago' end) AC_Varon,
count(case when i.idcategoria=1 and i.idestado_pago=1 and i.sexo='F' then 'Pago' end) AC_Mujer
from incripciones_2016 i 
inner join persona p on p.idpersona = i.idpersona
group by Descripcion;
";
                            $result1 = $mysqlMBN->consultas($sql1);
                            $x = 1;
                            $z = 1;
                            while ($row1 = mysqli_fetch_array($result1)) {
                                $Descripcion = $row1['Descripcion'];
                                $hombres = $row1['Hombres'];
                                $P_Miembros_v = $row1['P_Miembros_v'];
                                $Mujeres= $row1['Mujeres'];
                                $P_Miembros_m = $row1['P_Miembros_m'];
                                $C_Varon = $row1['C_Varon'];
                                $C_Mujer = $row1['C_Mujer'];
                                $NC_Varon = $row1['NC_Varon'];
                                $NC_Mujer = $row1['NC_Mujer'];
                                $AC_Varon = $row1['AC_Varon'];
                                $AC_Mujer = $row1['AC_Mujer'];
                                
                                $s1 += $hombres;
                                $s2 += $Mujeres;
                                $s3 += $C_Varon;
                                $s4 += $C_Mujer;
                                $s5 += $NC_Varon;
                                $s6 += $NC_Mujer;
                                $s7 += $AC_Varon;
                                $s8 += $AC_Mujer;
                                ?>
                                <tr class="even pointer" style="text-align: center; "><!--Resultados-->
                                    <td><?php echo $Descripcion; ?></td>
                                    <td colspan="<?php
                                    if ($x >= 3) {
                                        echo 2;
                                    } else {
                                        echo 1;
                                    }
                                    ?>"><?php echo $hombres; ?></td>
                                        <?php if ($x == 1) { ?>
                                        <td rowspan="2" bgcolor='white'><?php echo "<br>" . $P_Miembros_v; ?></td>
                                    <?php } ?>
                                    <td colspan="<?php
                                    if ($x >= 3) {
                                        echo 2;
                                    } else {
                                        echo 1;
                                    }
                                    ?>"><?php echo $Mujeres; ?></td>
                                        <?php if ($x == 1) { ?>
                                        <td rowspan="2" bgcolor='white'><?php echo "<br>" . $P_Miembros_m; ?></td>
                                    <?php } ?>
                                    <td colspan="1"><?php echo $C_Varon; ?></td>                                        
                                    <td colspan="1"><?php echo $C_Mujer; ?></td>                                    
                                    <td colspan="1"><?php echo $NC_Varon; ?></td>                                   
                                    <td colspan="1"><?php echo $NC_Mujer; ?></td>                                   
                                    <td colspan="1"><?php echo $AC_Varon; ?></td>                                   
                                    <td colspan="1"><?php echo $AC_Mujer; ?></td>                                    
                                </tr>
                                <?php
                                $x++;
                            }
                            ?>  
                            <tr class="even pointer" style="text-align: center; ">
                                <td>Total</td>
                                <td colspan="2"><?php echo $s1; ?></td>
                                <td colspan="2"><?php echo $s2; ?></td>
                                <td colspan="1"><?php echo $s3; ?></td>
                                <td colspan="1"><?php echo $s4; ?></td>
                                <td colspan="1"><?php echo $s5; ?></td>
                                <td colspan="1"><?php echo $s6; ?></td>
                                <td colspan="1"><?php echo $s7; ?></td>
                                <td colspan="1"><?php echo $s8; ?></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="4" style="text-align: center; font-size: 18px;"><?php $tot = $s1 + $s2 ;echo "<b>$tot</b>";?></td>
                                <td colspan="6"></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="ln_solid"></div>
                </div> <!-- End Tabla de Miembros -->

                
                <div class="col-md-12 col-sm-12 col-xs-12"> <!-- Tabla de Invitados -->
                    <div style="text-align: left">
                        <h2 style="font-size: 22px;">Reporte de Invitados</h2>
                    </div>
                    <table   id=""  class="table table-striped table-responsive">
                        <thead bgcolor="black" style="color: white;">
                            <tr >
                                <th colspan="1" style=" text-align: center;" class="column-title"rowspan="2" >Descripción</th>
                                <th colspan="2" style="text-align: center;" class="column-title"rowspan="2" >Hombres</th> 
                                <th colspan="2" style="text-align: center;" class="column-title"rowspan="2" >Mujeres</th> 
                                <th colspan="2" style="text-align: center;" class="column-title">Cancelado</th>                    
                                <th colspan="2" style="text-align: center;" class="column-title">No Cancelado</th>                    
                                <th colspan="2" style="text-align: center;" class="column-title">A Cuenta</th>
                            </tr>
                            <tr>
                                <?php for($x=0;$x<=2;$x++){ ?>
                                <th colspan="1" style="text-align: center;" class="column-title">Varon</th>  
                                <th colspan="1" style="text-align: center;" class="column-title">Mujer</th>   
                                <?php } ?>
                            </tr>

                        </thead>
                        <tbody>
                            <?php
                            $criterio = "";
                            if ($_GET['idpais'] != 0) {
                                $criterio .= "and  p.idpais=$idpais ";
                            }
                            $sql1 = "select 
(case 
when p.idpais = 89 and i.idciudad =19 then ' Lima' 
when p.idpais = 89 and i.idciudad !=19 then ' Provincia' 
when p.idpais = 123 then 'Bolivia'
when p.idpais = 103 then 'Ecuador'
when p.idpais = 82 then 'Colombia'
when p.idpais = 36 then 'Costa Rica'
when p.idpais = 124 then 'Panama'
when p.idpais = 138 then 'Republica Dominicana'
when p.idpais = 42 then 'México'
when p.idpais = 16 then 'Haiti'
end )as Descripcion,
count(case when i.idcategoria=2 and i.sexo='M' then 'Pago' end) Hombres,
(select count(x.idpersona) from incripciones_2016 x inner join persona pe on pe.idpersona = x.idpersona where pe.idpais=89 and x.idcategoria=2 and x.sexo='M' ) P_Miembros_v,
count(case when i.idcategoria=2 and i.sexo='F' then 'Pago' end) Mujeres,
(select count(x.idpersona) from incripciones_2016 x inner join persona pe on pe.idpersona = x.idpersona where pe.idpais=89 and x.idcategoria=2 and x.sexo='F') P_Miembros_m,
count(case when i.idcategoria=2 and i.idestado_pago=2 and i.sexo='M' then 'Pago' end) C_Varon,
count(case when i.idcategoria=2 and i.idestado_pago=2 and i.sexo='F' then 'Pago' end) C_Mujer,
count(case when i.idcategoria=2 and i.idestado_pago=3 and i.sexo='M' then 'Pago' end) NC_Varon,
count(case when i.idcategoria=2 and i.idestado_pago=3 and i.sexo='F' then 'Pago' end) NC_Mujer,
count(case when i.idcategoria=2 and i.idestado_pago=1 and i.sexo='M' then 'Pago' end) AC_Varon,
count(case when i.idcategoria=2 and i.idestado_pago=1 and i.sexo='F' then 'Pago' end) AC_Mujer
from incripciones_2016 i 
inner join persona p on p.idpersona = i.idpersona
group by Descripcion;
";
                            $result1 = $mysqlMBN->consultas($sql1);
                            $x = 1;
                            $z = 1;
                            while ($row1 = mysqli_fetch_array($result1)) {
                                $Descripcion = $row1['Descripcion'];
                                $hombres = $row1['Hombres'];
                                $P_Miembros_v = $row1['P_Miembros_v'];
                                $Mujeres= $row1['Mujeres'];
                                $P_Miembros_m = $row1['P_Miembros_m'];
                                $C_Varon = $row1['C_Varon'];
                                $C_Mujer = $row1['C_Mujer'];
                                $NC_Varon = $row1['NC_Varon'];
                                $NC_Mujer = $row1['NC_Mujer'];
                                $AC_Varon = $row1['AC_Varon'];
                                $AC_Mujer = $row1['AC_Mujer'];
                                
                                $s11 += $hombres;
                                $s12 += $Mujeres;
                                $s13 += $C_Varon;
                                $s14 += $C_Mujer;
                                $s15 += $NC_Varon;
                                $s16 += $NC_Mujer;
                                $s17 += $AC_Varon;
                                $s18 += $AC_Mujer;
                                ?>
                                <tr class="even pointer" style="text-align: center; "><!--Resultados-->
                                    <td><?php echo $Descripcion; ?></td>
                                    <td colspan="<?php
                                    if ($x >= 3) {
                                        echo 2;
                                    } else {
                                        echo 1;
                                    }
                                    ?>"><?php echo $hombres; ?></td>
                                        <?php if ($x == 1) { ?>
                                        <td rowspan="2" bgcolor='white'><?php echo "<br>" . $P_Miembros_v; ?></td>
                                    <?php } ?>
                                    <td colspan="<?php
                                    if ($x >= 3) {
                                        echo 2;
                                    } else {
                                        echo 1;
                                    }
                                    ?>"><?php echo $Mujeres; ?></td>
                                        <?php if ($x == 1) { ?>
                                        <td rowspan="2" bgcolor='white'><?php echo "<br>" . $P_Miembros_m; ?></td>
                                    <?php } ?>
                                    <td colspan="1"><?php echo $C_Varon; ?></td>                                        
                                    <td colspan="1"><?php echo $C_Mujer; ?></td>                                    
                                    <td colspan="1"><?php echo $NC_Varon; ?></td>                                   
                                    <td colspan="1"><?php echo $NC_Mujer; ?></td>                                   
                                    <td colspan="1"><?php echo $AC_Varon; ?></td>                                   
                                    <td colspan="1"><?php echo $AC_Mujer; ?></td>                                    
                                </tr>
                                <?php
                                $x++;
                            }
                            ?>  
                            <tr class="even pointer" style="text-align: center; ">
                                <td>Total</td>
                                <td colspan="2"><?php echo $s11; ?></td>
                                <td colspan="2"><?php echo $s12; ?></td>
                                <td colspan="1"><?php echo $s13; ?></td>
                                <td colspan="1"><?php echo $s14; ?></td>
                                <td colspan="1"><?php echo $s15; ?></td>
                                <td colspan="1"><?php echo $s16; ?></td>
                                <td colspan="1"><?php echo $s17; ?></td>
                                <td colspan="1"><?php echo $s18; ?></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="4" style="text-align: center; font-size: 18px;"><?php $tot2 = $s11 + $s12 ;echo "<b>$tot2</b>";?></td>
                                <td colspan="6"></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="ln_solid"></div>
                </div> <!-- End Tabla de Invitados -->
                

                <div class="col-md-12 col-sm-12 col-xs-12"> <!-- Reporte General-->
                    <div style="text-align: left">
                        <h2 style="font-size: 22px;">Reporte General</h2>
                    </div>
                    <table   id=""  class="table table-striped table-responsive">
                        <thead bgcolor="black" style="color: white;">
                            <tr >
                                <th colspan="1" style=" text-align: center;" class="column-title">Paises</th>
                                <th colspan="1" style="text-align: center;" class="column-title">Hombres</th> 
                                <th colspan="1" style="text-align: center;" class="column-title">Mujeres</th>                    
                                <th colspan="1" style="text-align: center;" class="column-title">Total</th>                    
                                <!--<th colspan="1" style="text-align: center;" class="column-title">A Cuenta</th>-->
                            </tr>

                        </thead>
                        <tbody>
                            <?php
                            $sql3 = "select (case 
when p.idpais = 89 then ' Peru' 
when p.idpais = 123 then 'Bolivia'
when p.idpais = 103 then 'Ecuador'
when p.idpais = 82 then 'Colombia'
when p.idpais = 36 then 'Costa Rica'
when p.idpais = 124 then 'Panama'
when p.idpais = 138 then 'Republica Dominicana'
when p.idpais = 42 then 'México'
when p.idpais = 16 then 'Haiti'
end )as Paises,
count(case when i.sexo= 'M' then 'pago' end) Hombres,
count(case when i.sexo= 'F' then 'pago' end) Mujeres, 
count(*) Total
from incripciones_2016 i 
inner join persona p on p.idpersona = i.idpersona
inner join pais pa on pa.idpais = p.idpais
group by p.idpais
order by 1;";
                            $result2 = $mysqlMBN->consultas($sql3);
                            $x = 1; // posicion
                            $z = 1;
                            while ($row2 = mysqli_fetch_array($result2)) {
                                $paises = $row2['Paises'];
                                $Hombres = $row2['Hombres'];
                                $Mujeres = $row2['Mujeres'];
                                $Total = $row2['Total'];                                
                                $s20 +=$Hombres;
                                $s21 +=$Mujeres;
                                $s22 +=$Total;
                                ?>
                                <tr class="even pointer" style="text-align: center; "><!--Resultados-->
                                    <td><?php echo $paises; ?></td>                                    
                                    <td><?php echo $Hombres; ?></td>
                                    <td><?php echo $Mujeres; ?></td>
                                    <td><?php echo $Total; ?></td>
                                </tr>
                            <?php } ?>
                            <tr class="even pointer" style="text-align: center; ">
                                <td>Total</td>
                                <td colspan="1"><?php echo $s20; ?></td>
                                <td colspan="1"><?php echo $s21; ?></td>
                                <td colspan="1"><?php echo $s22; ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="ln_solid"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <?php
    require('../../include/footer.php');
    ?>
