<?php
require('../../include/header.php');
include '../../system/mensajesAlerta.php';
include_once '../../system/crearConexion.php';
clearstatcache();
$titulo = "Cargos";
$hoja = "Cargos";
$idpais = $_GET['idpais'];
$idgrupo = $_GET['idgrupo'];
?>

<script type="text/javascript">
    $(document).ready(function () {
        var lista = document.getElementById("idpais");
        $('#idpais').change(function () {
            window.location = "repor1?idpais=" + lista.value;
        });
    });
</script>

<!-- page content -->
<div class="right_col" <?php echo $newDimensionBody; ?> role="main">
    <div class="page-title">
        <div class="title_left">
            <h3>Listado de los participantes Inscritos</h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <form action="" method="GET">
            <div class="x_panel">
                <div class="x_title">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-2 col-sm-5 col-xs-5" title="Seleccione el Pais">
                            <label class="control-label">Seleccione el Pais</label>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-5" title="Seleccione el Pais">
                            <select class='form-control' name='idpais' id='idpais' >
                                <option value="0">Todos</option>
                                <?php include_once '../../system/selectPais.php'; ?>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table   id=""  class="table table-striped table-responsive">
                        <thead bgcolor="#3498DB" style="color: white;">
                            <tr >
                                <th rowspan="3" style="text-align: center;" class="column-title">Grupo</th>
                                <th colspan="4" bgcolor="#f39c11" style="text-align: center;" class="column-title">A CTA</th>
                                <th colspan="4" bgcolor="#17a086" style="text-align: center;" class="column-title">Cancelado</th>
                                <th colspan="4" bgcolor="#bf3a2b" style="text-align: center;" class="column-title">No Cancelado</th>
                                <th rowspan="3" style="text-align: center;" class="column-title">Miembro</th>
                                <th rowspan="3" style="text-align: center;" class="column-title">Invitado</th>
                                <th rowspan="3" style="text-align: center;" class="column-title">Total</th>
                            </tr>
                            <tr >
                                <?php for ($x = 1; $x <= 3; $x++) { ?>
                                    <th bgcolor="<?php
                                    if ($x == 1) {
                                        echo "#f2b610";
                                    } else if ($x == 2) {
                                        echo "#1fb794";
                                    } else if ($x == 3) {
                                        echo "#d33f2e";
                                    }
                                    ?>" style="text-align: center;" colspan="2" class="column-title">Miembro</th>
                                    <th bgcolor="<?php
                                    if ($x == 1) {
                                        echo "#f2b610";
                                    } else if ($x == 2) {
                                        echo "#1fb794";
                                    } else if ($x == 3) {
                                        echo "#d33f2e";
                                    }
                                    ?>" style="text-align: center;" colspan="2" class="column-title">Invitado</th>
                                    <?php } ?>
                            </tr>
                            <tr >  
                                <?php for ($x = 1; $x <= 6; $x++) { ?>
                                    <th bgcolor="<?php
                                    if ($x == 1 || $x == 2) {
                                        echo "#efbf10";
                                    } else if ($x == 3 || $x == 4) {
                                        echo "#2ad3ac";
                                    } else if ($x == 5 || $x == 6) {
                                        echo "#ef4937";
                                    }
                                    ?>"style="text-align: center;" class="column-title">V</th>
                                    <th bgcolor="<?php
                                    if ($x == 1 || $x == 2) {
                                        echo "#efbf10";
                                    } else if ($x == 3 || $x == 4) {
                                        echo "#2ad3ac";
                                    } else if ($x == 5 || $x == 6) {
                                        echo "#ef4937";
                                    }
                                    ?>"style="text-align: center;" class="column-title">M</th>
                                    <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $criterio = "";
                            if ($_GET['idpais'] != 0) {
                                $criterio .= "and  p.idpais=$idpais ";
                            }
                            $sql = "select grupo as '1', /* estadoPago_Condicion_Sexo */
count(case when idestado_pago=1 and i.idcategoria=1 and i.sexo = 'M' then 'Pago' end) '2',
count(case when idestado_pago=1 and i.idcategoria=1 and i.sexo = 'F' then 'Pago' end) '3',
count(case when idestado_pago=1 and i.idcategoria=2 and i.sexo = 'M' then 'Pago' end) '4',
count(case when idestado_pago=1 and i.idcategoria=2 and i.sexo = 'F' then 'Pago' end) '5',
count(case when idestado_pago=2 and i.idcategoria=1 and i.sexo = 'M' then 'Pago' end) '6',
count(case when idestado_pago=2 and i.idcategoria=1 and i.sexo = 'F' then 'Pago' end) '7',
count(case when idestado_pago=2 and i.idcategoria=2 and i.sexo = 'M' then 'Pago' end) '8',
count(case when idestado_pago=2 and i.idcategoria=2 and i.sexo = 'F' then 'Pago' end) '9',
count(case when idestado_pago=3 and i.idcategoria=1 and i.sexo = 'M' then 'Pago' end) '10',
count(case when idestado_pago=3 and i.idcategoria=1 and i.sexo = 'F' then 'Pago' end) '11',
count(case when idestado_pago=3 and i.idcategoria=2 and i.sexo = 'M' then 'Pago' end) '12',
count(case when idestado_pago=3 and i.idcategoria=2 and i.sexo = 'F' then 'Pago' end) '13',
count(case when i.idcategoria=1 then 'Miembro' end) '14' ,
count(case when i.idcategoria=2 then 'Miembro' end) '15' ,
count(i.idpersona) '16'
from incripciones_2016 i 
inner join persona p on p.idpersona = i.idpersona
where '1'='1' $criterio                                    
group by grupo;";
                            $result = $mysqlMBN->consultas($sql);
                            $x = 1;
                            $z = 1;
                            while ($row = mysqli_fetch_array($result)) {
                                $s1 += $row['1'];
                                $s2 += $row['2'];
                                $s3 += $row['3'];
                                $s4 += $row['4'];
                                $s5 += $row['5'];
                                $s6 += $row['6'];
                                $s7 += $row['7'];
                                $s8 += $row['8'];
                                $s9 += $row['9'];
                                $s10 += $row['10'];
                                $s11 += $row['11'];
                                $s12 += $row['12'];
                                $s13 += $row['13'];
                                $s14 += $row['14'];
                                $s15 += $row['15'];
                                $s16 += $row['16'];
                                ?>
                                <tr class="even pointer"><!--Resultados-->
                                    <?php
                                    for ($y = 1; $y <= 16; $y++) {
                                        ?>
                                        <td <?php
                                        if ($y == 1) {
                                            echo "bgcolor='#aed4e2' ";
                                        } else if ($y == 2 || $y == 3 || $y == 4 || $y == 5) {
                                            echo "bgcolor='#fdfbca' ";
                                        } else if ($y == 6 || $y == 7 || $y == 8 || $y == 9) {
                                            echo "bgcolor='#c0dfd0' ";
                                        } else if ($y == 10 || $y == 11 || $y == 12 || $y == 13) {
                                            echo "bgcolor='#fad2d0' ";
                                        }
                                        ?> 
                                            style="text-align: center;"><?php echo $row[$y]; ?>
                                        </td>
                                    <?php } ?>
                                </tr>
                                <?php
                                $x++;
                            } //include '../VISTAS/manTable' . $hoja . '.php';  
                            ?>   
                            <tr style="font-weight:bold;font-size: 15px;">
                                <td bgcolor='#aed4e2' style="text-align: center;"><?php echo "Totales"; ?></td>
                                <td bgcolor='#fdfbca' style="text-align: center;"><?php echo $s2 ?></td>
                                <td bgcolor='#fdfbca' style="text-align: center;"><?php echo $s3; ?></td>
                                <td bgcolor='#fdfbca' style="text-align: center;"><?php echo $s4; ?></td>
                                <td bgcolor='#fdfbca' style="text-align: center;"><?php echo $s5; ?></td>
                                <td bgcolor='#c0dfd0' style="text-align: center;"><?php echo $s6; ?></td>
                                <td bgcolor='#c0dfd0' style="text-align: center;"><?php echo $s7; ?></td>
                                <td bgcolor='#c0dfd0' style="text-align: center;"><?php echo $s8; ?></td>
                                <td bgcolor='#c0dfd0' style="text-align: center;"><?php echo $s9; ?></td>
                                <td bgcolor='#fad2d0' style="text-align: center;"><?php echo $s10; ?></td>
                                <td bgcolor='#fad2d0' style="text-align: center;"><?php echo $s11; ?></td>
                                <td bgcolor='#fad2d0' style="text-align: center;"><?php echo $s12; ?></td>
                                <td bgcolor='#fad2d0' style="text-align: center;"><?php echo $s13; ?></td>
                                <td style="text-align: center;"><?php echo $s14; ?></td>
                                <td style="text-align: center;"><?php echo $s15; ?></td>
                                <td style="text-align: center;"><?php echo $s16; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </form>
    </div>

    <div class="clearfix"></div>
    <?php
    require('../../include/footer.php');
    ?>
