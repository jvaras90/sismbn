<?php
//require('include/header.php');

include '../../include/informacion.php';
session_start();
//if ($_SESSION['idpersonal'] == "") {
//    //si no existe, va a la página de autenticacion
//    header("Location: index.php");
//    //salimos de este script
//    exit();
//}
include '../../system/mensajesAlerta.php';
include '../../system/crearConexion.php';
clearstatcache();

$color = "#2774E6";
$userAgent = $_SERVER[HTTP_USER_AGENT];
$userAgent1 = strtolower($userAgent);
//echo $userAgent . "<br>";
//echo $userAgent1 . "<br>";
if (strpos($userAgent1, "android") !== false || strpos($userAgent1, "iphone") !== false) {
    //print ("<P>OS Client: Android $_SERVER[REMOTE_ADDR]");
    $newDimensionNav = "width: 1000px;";
    $newDimensionBody = 'style="' . $newDimensionNav . '"';
} else {
    $newDimensionNav = "";
    $newDimensionBody = "";
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Inscripciones World Camp 2017</title>
        <!--<link href="http://misionbuenasnuevas.info/wp-content/uploads/2016/02/16x16-mbn.png" rel="shortcut icon" />-->
        <meta name="msapplication-TileImage" content="http://iyf.org.pe/wp-content/uploads/2016/03/iyf.ico.png">
        <meta name="description" content="Es una asociación que forma a los jóvenes como líderes del futuro en un ambiente globalizado y actualmente está presente en 80 países. Durante 10 años, teniendo como base el cristianismo y por medio de programas edificativos, IYF quiere ayudar a todos los jóvenes del mundo a tener un cambio en sus vidas.">
        <link rel="icon" href="http://iyf.org.pe/wp-content/uploads/2016/03/iyf.ico.png" sizes="32x32">
        <link rel="icon" href="http://iyf.org.pe/wp-content/uploads/2016/03/iyf.ico.png" sizes="192x192">
        <link rel="apple-touch-icon-precomposed" href="http://iyf.org.pe/wp-content/uploads/2016/03/iyf.ico.png">
        <meta name="msapplication-TileImage" content="http://iyf.org.pe/wp-content/uploads/2016/03/iyf.ico.png">

        <!-- Bootstrap core CSS -->

        <link href="css/bootstrap.min.css" rel="stylesheet">

        <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">

        <!-- editor -->
        <!--<link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">-->
        <link href="css/editor/external/google-code-prettify/prettify.css" rel="stylesheet">
        <link href="css/editor/index.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="css/custom.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.1.css" />
        <link href="css/icheck/flat/green.css" rel="stylesheet" />
        <link href="css/floatexamples.css" rel="stylesheet" type="text/css" />    
        <link rel="stylesheet" type="text/css" href="css/progressbar/bootstrap-progressbar-3.3.0.css">

        <!-- select2 -->
        <link href="css/select/select2.min.css" rel="stylesheet">
        <link href="css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">


        <!-- switchery -->
        <link rel="stylesheet" href="css/switchery/switchery.min.css" />

        <script src="js/jquery.min.js"></script>
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js">></script>-->

        <script type="text/javascript" src="js/llena_inputs.js"></script>
        <script type="text/javascript" src="js/llena_canales.js"></script>
        <!--<script src="js/nprogress.js"></script>
        <script src="js/autocomplete/jquery.autocomplete.js"></script>
            <script>
            NProgress.start();
        </script>-->

        <!-- editor -->
        <!--[if lt IE 9]>
            <script src="../assets/js/ie8-responsive-file-warning.js"></script>
            <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->

        <!--jeditable-->
        <script type="text/javascript" src="js/jquery.jeditable.js"></script>
        <script type="text/javascript" src="js/js.js"></script>
        <!--<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" />-->


        <!--fin jeditable-->
        <!--<script src="js/jquery.min.js"></script>-->

    </head>

<!--<script src="js/realtime4.js"></script>-->
    <body style="<?php echo $newDimensionNav; ?>;background-color: <?php echo $color; ?>" class="nav-md">
        <div style="background-color: #EDEDED;">
            <div class="right_col" role="main">
                <br>
                <div style="text-align: right;">
                    <a href="http://iyf.org.pe/" target="_blank" class="btn btn-primary" ><i class="fa fa-home"></i> Volver</a>
                </div><br>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><i class="fa fa-th-large"> Encuentro Internacional de Jóvenes para la Familia 2017</i>  <small></small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">                                
                                <div class="x_content">
                                    <div class="" role="tabpanel" data-example-id="togglable-tabs" >
                                        <div class="col-xs-2">
                                            <ul id="myTab" class="nav nav-tabs tabs-left" role="tablist">
                                                <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Inicio</a>
                                                </li>                                            
                                                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab6" data-toggle="tab" aria-expanded="false">Registros</a>
                                                </li>                                            
                                                <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Participantes</a>
                                                </li>
                                                <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab3" data-toggle="tab"  aria-expanded="false">Contactanos</a>
                                                </li> 
                                                <!--                                            <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab4" data-toggle="tab"  aria-expanded="false">Perfil</a>
                                                                                            </li>-->
                                                <?php if ($_SESSION['id_nivel'] == 1) { ?>
                                                    <!--                                                <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab5" data-toggle="tab" aria-expanded="false">Reportes</a>
                                                                                                    </li>-->
                                                <?php } ?>
                                            </ul>
                                            <!--<a href="http://sismbn.iyf.org.pe/index.php">link</a>-->
                                        </div>
                                        <div class="col-xs-10">
                                            <div id="myTabContent" class="tab-content">
                                                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                                    <?php include 'inicio.php'; ?>
                                                </div>                                
                                                <div role="tabpanel" class="tab-pane fade " id="tab_content2" aria-labelledby="profile-tab">
                                                    <?php include 'newRegister.php'; ?>
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                                                    <?php include 'listaParticipantes.php'; ?>
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">
                                                    <?php include 'contactenos.php'; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <!-- footer content -->
                <div style="height: 600px;">

                </div>
                <footer>
                    <div class="">
                        <strong>Copyright &copy; <?php echo $año; ?> <a href="<?php echo $webEmpresa; ?>"><?php echo $empresa; ?></a>.</strong> All rights reserved.

                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->

            </div>
            <!-- /page content -->
        </div>

    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>


    <script src="js/select/select2.full.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>

    <script src="js/custom.js"></script>
    <!-- form validation -->
    <script src="js/validator/validator.js"></script>

    <link href="css/custom.css" rel="stylesheet">

    <!-- input mask -->
    <script src="js/input_mask/jquery.inputmask.js"></script>

    <script src="js/datatables/js/jquery.dataTables.js"></script>
    <script src="js/datatables/tools/js/dataTables.tableTools.js"></script>
    <!-- form wizard -->
    <script type="text/javascript" src="js/wizard/jquery.smartWizard.js"></script>

    <!-- switchery -->
    <script src="js/switchery/switchery.min.js"></script>
    <!-- image cropping -->
    <script src="js/cropping/cropper.min.js"></script>
    <script src="js/cropping/main.js"></script>
    <!-- daterangepicker -->
    <script type="text/javascript" src="js/moment.min.js"></script>
    <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>
    <!-- moris js -->
    <script src="js/moris/raphael-min.js"></script>
    <script src="js/moris/morris.js"></script>
    <!-- sparkline -->
    <script src="js/sparkline/jquery.sparkline.min.js"></script>
    <!-- easypie -->
    <script src="js/easypie/jquery.easypiechart.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function ()
        {
            $("input[name=S]").change(function () {
                alert($(this).val());
            });

        });
    </script>

    <script>
        $(document).ready(function () {
            $('input.flat').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });

        var asInitVals = new Array();
        $(document).ready(function () {
            var oTable = $('#example').dataTable({
                "oLanguage": {
                    "sSearch": "Buscar:"
                },
                "aoColumnDefs": [
                    {
                        'bSortable': false,
                        'aTargets': [0]
                    } //disables sorting for column one
                ],
                'iDisplayLength': <?php echo 10; ?>,
                "sPaginationType": "full_numbers",
                "dom": 'T<"clear">lfrtip',
                //"tableTools": {
                // "sSwfPath": "<?php // echo base_url('../.../js/datatables/tools/swf/copy_csv_xls_pdf.swf');                                                                                        ?>"
                //}
            });
            $("tfoot input").keyup(function () {
                /* Filter on the column based on the index of this element's parent <th> */
                oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
            });
            $("tfoot input").each(function (i) {
                asInitVals[i] = this.value;
            });
            $("tfoot input").focus(function () {
                if (this.className == "search_init") {
                    this.className = "";
                    this.value = "";
                }
            });
            $("tfoot input").blur(function (i) {
                if (this.value == "") {
                    this.className = "search_init";
                    this.value = asInitVals[$("tfoot input").index(this)];
                }
            });
        });
    </script>

</body>

</html>
