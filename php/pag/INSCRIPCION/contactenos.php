<div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_title">

            <h2>Comuniquese con nosotros</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <label><i class="fa fa-phone"></i> (01) 651-8773</label> <br>
            <label><i class="fa fa-map-marker"></i> Av. Naciones Unidas #1650</label><br>
            <label><i class="fa fa-globe"></i> Urb. Chacra Ríos Norte, Lima</label><br>
            <label><i class="fa fa-at"></i> info@iyf.org.pe </label><br>
            
            <label><a href=http://iyf.org.pe/ target="_blank">Web Oficial</a></label>
            <br><br>
            <a href="https://web.facebook.com/iyfperu/" class="btn btn-primary btn-sm btn-round" target="_blank"><i class="fa fa-facebook"></i></a>
            <a href="https://twitter.com/IYFPERU" class="btn btn-info btn-sm btn-round" target="_blank"><i class="fa fa-twitter"></i></a>
            <a href="https://www.youtube.com/channel/UCcAWB2frMLoZSPt2VCUB9vg" class="btn btn-danger btn-sm btn-round" target="_blank"><i class="fa fa-youtube"></i></a>
            <label>IYF Perú</label>
            <br>
        </div>
    </div>
</div>