<?php
//require('include/header.php');

include '../../include/informacion.php';
session_start();
//if ($_SESSION['idpersonal'] == "") {
//    //si no existe, va a la página de autenticacion
//    header("Location: index.php");
//    //salimos de este script
//    exit();
//}
include '../../system/mensajesAlerta.php';
include '../../system/crearConexion.php';
clearstatcache();

$color = "#2774E6";
$userAgent = $_SERVER[HTTP_USER_AGENT];
$userAgent1 = strtolower($userAgent);
//echo $userAgent . "<br>";
//echo $userAgent1 . "<br>";
if (strpos($userAgent1, "android") !== false || strpos($userAgent1, "iphone") !== false) {
    //print ("<P>OS Client: Android $_SERVER[REMOTE_ADDR]");
    $newDimensionNav = "width: 1000px;";
    $newDimensionBody = 'style="' . $newDimensionNav . '"';
} else {
    $newDimensionNav = "";
    $newDimensionBody = "";
}

$idpais = $_GET['idpais'];
$dni = $_GET['dni'];
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Inscripciones World Camp 2017</title>
        <!--<link href="http://misionbuenasnuevas.info/wp-content/uploads/2016/02/16x16-mbn.png" rel="shortcut icon" />-->
        <meta name="msapplication-TileImage" content="http://iyf.org.pe/wp-content/uploads/2016/03/iyf.ico.png">
        <meta name="description" content="Es una asociación que forma a los jóvenes como líderes del futuro en un ambiente globalizado y actualmente está presente en 80 países. Durante 10 años, teniendo como base el cristianismo y por medio de programas edificativos, IYF quiere ayudar a todos los jóvenes del mundo a tener un cambio en sus vidas.">
        <link rel="icon" href="http://iyf.org.pe/wp-content/uploads/2016/03/iyf.ico.png" sizes="32x32">
        <link rel="icon" href="http://iyf.org.pe/wp-content/uploads/2016/03/iyf.ico.png" sizes="192x192">
        <link rel="apple-touch-icon-precomposed" href="http://iyf.org.pe/wp-content/uploads/2016/03/iyf.ico.png">
        <meta name="msapplication-TileImage" content="http://iyf.org.pe/wp-content/uploads/2016/03/iyf.ico.png">

        <!-- Bootstrap core CSS -->

        <link href="css/bootstrap.min.css" rel="stylesheet">

        <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">

        <!-- editor -->
        <!--<link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">-->
        <link href="css/editor/external/google-code-prettify/prettify.css" rel="stylesheet">
        <link href="css/editor/index.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="css/custom.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.1.css" />
        <link href="css/icheck/flat/green.css" rel="stylesheet" />
        <link href="css/floatexamples.css" rel="stylesheet" type="text/css" />    
        <link rel="stylesheet" type="text/css" href="css/progressbar/bootstrap-progressbar-3.3.0.css">

        <!-- select2 -->
        <link href="css/select/select2.min.css" rel="stylesheet">
        <link href="css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">


        <!-- switchery -->
        <link rel="stylesheet" href="css/switchery/switchery.min.css" />

        <script src="js/jquery.min.js"></script>
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js">></script>-->

        <script type="text/javascript" src="js/llena_inputs.js"></script>
        <script type="text/javascript" src="js/llena_canales.js"></script>
        <!--<script src="js/nprogress.js"></script>
        <script src="js/autocomplete/jquery.autocomplete.js"></script>
            <script>
            NProgress.start();
        </script>-->

        <!-- editor -->
        <!--[if lt IE 9]>
            <script src="../assets/js/ie8-responsive-file-warning.js"></script>
            <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->

        <!--jeditable-->
        <script type="text/javascript" src="js/jquery.jeditable.js"></script>
        <script type="text/javascript" src="js/js.js"></script>
        <!--<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" />-->

        <script>
            function validaFormAsig(boton) {
                var idpais = $('#idpais').val();
                var dni = $('#dni').val();
                if (idpais == '0') {
                    alert('Porfavor, seleccione el pais  ');
                    $('#idpais').focus();
                    return false;
                } else if (dni == '') {
                    alert('Ingrese el numero de documento');
                    $('#dni').focus();
                    return false;
                } else {
                    boton.disabled = true;
                    boton.value = 'Buscando...';
                    boton.form.submit();
                    return true;
                }
            }
        </script>
        <!--fin jeditable-->
        <!--<script src="js/jquery.min.js"></script>-->

    </head>

<!--<script src="js/realtime4.js"></script>-->
    <?php ?>
    <body style="<?php echo $newDimensionNav; ?>;background-color: <?php echo $color; ?>" class="nav-md">
        <div style="background-color: #EDEDED;">
            <div class="right_col" role="main">
                <br>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <form  class="form-horizontal form-label-left" method="GET" action="" id="validaFormAsig">
                                <div style="text-align: center;">
                                    <input type="hidden" name="mod" id="mod" value="bPersona" >
                                    <div class="col-md-5 col-sm-5 col-xs-5">
                                        <label class="control-label">Seleccione Pais</label>
                                        <select class="form-control" name="idpais" id="idpais">
                                            <option value="0">Pais</option>
                                            <option value="89">Peru</option>
                                            <option value="82">Colombia</option>
                                            <option value="103">Ecuador</option>
                                            <option value="123">Bolivia</option>
                                            <option value="42">Mexico</option>
                                            <option value="124">Panamá</option>
                                            <option value="16">Haití</option>
                                            <option value="36">Costa Rica</option>
                                            <option value="138">Republica Dominicana</option>
                                        </select>
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5">
                                        <label class="control-label">Ingrese el Número de Documento de Identidad</label>
                                        <input class="form-control" type="text"name="dni" id="dni" placeholder="nro documento" value="<?php echo $dni; ?>">
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                        <input type="button" value="Buscar" id="regNewAsig" name="regNewAsig" class="btn btn-danger" onclick="validaFormAsig(this)">
                                        <!--<input type="submit" class="btn btn-danger" value="Buscar">-->
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">                                
                                <div class="x_content">
                                    <?php if (isset($_GET['mod'])) { ?>
                                        <?php
                                        $sql = "select i.idinscripciones, i.idpersona ,p.nombres, concat(apaterno,' ',amaterno) apellidos,i.edad, i.dni,i.celular,cargo,i.idgrupo,
                                    i.grupo,i.idcategoria,i.idreunion_zonal,p.idpais,i.idciudad,i.idtalla_polo,i.idestado_pago,voluntario
                                    from incripciones_2016 i 
                                    inner join persona p on p.idpersona = i.idpersona where p.idpais=$idpais and  i.dni ='$dni';";
                                        $res = $mysqlMBN->consultas($sql);
                                        $num_reg = mysqli_num_rows($res);
                                        while ($row = mysqli_fetch_array($res)) {
                                            $idinscripcion = $row['idinscripciones'];
                                            $nombre = $row['nombres'];
                                            $apellidos = $row['apellidos'];
                                            $idpersona = $row['idpersona'];
                                            $sexo = $row['sexo'];
                                            $edad = $row['edad'];
                                            $nro_documento = $row['dni'];
                                            $telefono = $row['celular'];
                                            $idgrupo = $row['idgrupo'];
                                            $idcargoMulti = $row['cargo'];
                                            $grupo = $row['grupo'];
                                            $idcategoria = $row['idcategoria'];
                                            $idreunion_zona1 = $row['idreunion_zonal'];
                                            $voluntario = $row['voluntario'];
                                            $idpais = $row['idpais'];
                                            $idciudad = $row['idciudad'];
                                            $idtalla_polo = $row['eidtalla_polo'];
                                            $idestado_pago = $row['idestado_pago'];
                                        }

                                        if ($num_reg >= 1) {
                                            ?>

                                            <form class="form-horizontal form-label-left input_mask" novalidate action="registrarA" method="POST">  
                                                <div class="clearfix"></div>
                                                <div class="ln_solid"></div>
                                                <div style="text-align: center;">
                                                    <h2><i><u>Información del Participante</u></i></h2>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                                    <div style="text-align: center;">
                                                        <label class="control-label  has-feedback-left" title="Active esta opcion si el participante es Voluntario">
                                                            Voluntario <input type="checkbox" id="volunt" name="volunt" class="js-switch form-control" <?php
                                                            if ($voluntario == 1) {
                                                                echo "checked";
                                                            }
                                                            ?> /> 
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Nombres del participante">
                                                    <input class="form-control has-feedback-left" id="nombres" name="nombres" placeholder="Nombres" type="text" value="<?php echo $nombre; ?>" readonly>
                                                    <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Apellidos del participante">
                                                    <input class="form-control has-feedback-left" id="apellidos" name="apellidos" placeholder="Apellidos" type="text" value="<?php echo $apellidos; ?>" readonly>
                                                    <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                                <input type="hidden" name="idpersona" id="idpersona" value="<?php echo $idpersona; ?>" >
                                                <input type="hidden" name="idpais" id="idpais" value="<?php echo $idpais; ?>" >
                                                <input type="hidden" name="S" id="S" value="<?php echo $sexo; ?>" >
                                                <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Ingrese la Edad">
                                                    <input class="form-control has-feedback-left" id="edad" name="edad"placeholder="Edad" type="text" value="<?php echo $edad; ?>" readonly>
                                                    <span class="fa fa-circle-o-notch form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Ingrese el Nro. de Documento">
                                                    <input class="form-control has-feedback-left" id="nro_documento" name="nro_documento" placeholder="Nro. Documento" type="text" value="<?php echo $nro_documento; ?>" readonly>
                                                    <span class="fa fa-barcode form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Ingrese el número de telefono o celular">
                                                    <input class="form-control has-feedback-left" id="telefono" name="telefono" placeholder="Teléfono" type="text" value="<?php echo $telefono; ?>"  readonly>
                                                    <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                                <div id="selectMultiple"  class="col-md-6 col-sm-6 col-xs-6 form-group text-center" title="Seleccione los cargos si es que tiene, de lo contrario dejar por defecto la opcion de participante">
                                                    <select class="select2_multiple form-control" multiple="multiple" name='idcargoM[]' id='idcargoM[]' disabled>
                                                        <option value="17" selected>Participante</option>
                                                        <?php include '../../system/selectCargo.php' ?>   
                                                    </select>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Seleccione el Estado del participante">
                                                    <select class="form-control  has-feedback-left" id="idgrupo" name="idgrupo" onload="cargarGrupo(this.value)" onchange="cargarGrupo(this.value)" disabled>
                                                        <option value="0">Estado</option>
                                                        <?php include_once "../../system/selectGrupo.php"; ?>
                                                    </select>
                                                    <span class="fa fa-shield form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Éste es el grupo al que pertenece el participante">
                                                    <input class="form-control has-feedback-left" id="grupo" name="grupo" placeholder="Grupo" type="text" value="<?php echo $grupo; ?>" readonly>
                                                    <span class="fa fa-users form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Seleccione la condición del participante">
                                                    <select class="form-control  has-feedback-left" id="idcategoria1" name="idcategoria1"  disabled>
                                                        <option value="0">Condición</option>
                                                        <?php include_once "../../system/selectCategoria.php"; ?>
                                                    </select>
                                                    <span class="fa fa-heart-o form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Seleccione la Zona del participante">
                                                    <select class="form-control  has-feedback-left" id="idreunion_zonal" name="idreunion_zonal"  disabled>
                                                        <option value="0">Zona</option>
                                                        <?php include_once "../../system/selectReunionZonal.php"; ?>
                                                    </select>
                                                    <span class="fa fa-bullseye form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Seleccione la Ciudad">
                                                    <select class="form-control  has-feedback-left" id="idciudad" name="idciudad"  disabled>
                                                        <option value="0">Lugar</option>
                                                        <?php include_once "../../system/selectCiudad.php"; ?>
                                                    </select>
                                                    <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Seleccione la talla del polo">
                                                    <select class="form-control  has-feedback-left" id="idtalla_polo" name="idtalla_polo"  disabled>
                                                        <option value="0">Talla Polo</option>
                                                        <?php include_once "../../system/selectTallaPolo.php"; ?>
                                                    </select>
                                                    <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Seleccione el estado en el que se encuentra el Abono de la Iniscipción">
                                                    <select class="form-control  has-feedback-left" id="idestado_pago" name="idestado_pago" disabled >
                                                        <option value="0">Estado Pago</option>
                                                        <?php include_once "../../system/selectEstadoPago.php"; ?>
                                                    </select>
                                                    <span class="fa fa-money form-control-feedback left" aria-hidden="true"></span>
                                                </div>

                                                <?php
                                                $sql2 = "select ag.idgrupo_cabe,ag.idpersona,gb.descripcion grupo,gb.idmaestro,
                                                        gb.idcapitan, gb.idgrupo_camp,gc.descripcion grupo_desc
                                                    from det_asig_grupos ag 
                                                    inner join grupo_cab gb on ag.idgrupo_cabe = gb.idgrupo_cab
                                                    inner join grupo_camp gc on gc.idgrupo_camp = gb.idgrupo_camp
                                                    where ag.idpersona = $idpersona limit 1 ;";
                                                //echo $sql2;
                                                $res2 = $mysqlMBN->consultas($sql2);
                                                //$num_r = mysqli_num_rows($res2);
                                                while ($row = mysqli_fetch_array($res2)) {
                                                    $idgrupo_cabe = $row['idgrupo_cabe'];
                                                    $grupo1 = $row['grupo'];
                                                    $idmaestro = $row['idmaestro'];
                                                    $idcapitan = $row['idcapitan'];
                                                    $idgrupo_camp = $row['idgrupo_camp'];
                                                    $grupo_desc = $row['grupo_desc'];
                                                }


                                                //echo "Grupo : $grupo_desc $grupo1";
                                                ?>
                                                <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Grupo Asignado">
                                                    <input class="form-control has-feedback-left" id="telefono" name="grupo_desc" placeholder="Teléfono" type="text" value="<?php echo "Grupo : $grupo_desc $grupo1"; ?>"  readonly>
                                                    <span class="fa fa-users form-control-feedback left" aria-hidden="true"></span>
                                                </div>

                                                <?php ?>
                                                <div class="clearfix"></div>

                                                <!----------------------------------------DIV DETALLE PAGO------------------------------------------------------------->
                                                <div class="ln_solid"></div>
                                                <div style="text-align: center;">
                                                    <h2><i><u>Información de Abono</u></i></h2>
                                                </div>

                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback" title="Detalle de Abono">

                                                    <table id="table_boleta" class="table table-striped responsive-utilities jambo_table bulk_action">
                                                        <thead style="text-align: center;" >
                                                            <tr class="headings">
                            <!--                                    <th style="text-align: center;" >
                        
                                                                </th>-->
                                                                <th style="text-align: center;"  class="column-title">Fecha Donación</th>
                                                                <th style="text-align: center;"  class="column-title">Nro. Boleta</th>
                                                                <th style="text-align: center;" class="column-title">Monto Soles (<i class="fa">S/.</i>)</th>
                                                                <th style="text-align: center;" class="column-title">Monto Dólares (<i class="fa fa-dollar">)</i></th>
                                                                <!--<th class="column-title no-link last"><span class="nobr">Accion</span>-->
                                                                </th>
                                                                <th class="bulk-actions" colspan="7">
                                                                    <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody style="text-align: center;"  >
                                                            <?php
                                                            $x = 1;
                                                            $sqlBol = "select * from boleta_2016 where idinscripcion = $idinscripcion";
                                                            $resB = $mysqlMBN->consultas($sqlBol);
                                                            while ($rowB = mysqli_fetch_array($resB)) {
                                                                $idboleta = $rowB['idboleta'];
                                                                $fecha_pago = $rowB['fecha_pago'];
                                                                $nroBoleta = $rowB['nro_boleta'];
                                                                $montoSoles = $rowB['monto_soles'];
                                                                $montoDolares = $rowB['monto_dolares'];
                                                                ?>
                                                                <tr id="<?php echo $x; ?>" class="impreso">
                                                                    <td><input type="hidden" name="fechaPago<?php echo $nroBoleta; ?>" value="<?php echo $fecha_pago; ?>"/><?php echo $fecha_pago; ?></td>
                                                                    <td><input type="hidden" name="nroBoleta<?php echo $nroBoleta; ?>" value="<?php echo $nroBoleta; ?>"/><?php echo $nroBoleta; ?></td>
                                                                    <td><input type="hidden" name="montoSoles<?php echo $nroBoleta; ?>" value="<?php echo $montoSoles; ?>"/>S/. <?php echo $montoSoles; ?></td>
                                                                    <td><input type="hidden" name="montoDolares<?php echo $nroBoleta; ?>" value="<?php echo $montoDolares; ?>"/>$. <?php echo $montoDolares; ?></td>
                                                                </tr>
                                                                <?php
                                                                $x++;
                                                            }
                                                            ?>
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <div class="clearfix"></div>
                                                <div class="ln_solid"></div>
                                                <!---------------------------------------------------------------------------------------------------------------------->

                                            </form>
                                            <form class="form-horizontal form-label-left input_mask" novalidate action="registrarA" method="POST">  
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback" title="Ingrese alguna observacion del participante">
                                                    <textarea style="resize: none;" class="form-control has-feedback-left" id="observacion" name="observacion" placeholder="Observación"></textarea>
                                                    <span class="fa fa-comments form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div style="text-align: center;">
                                                    <div class="panel-footer">
                                                        <a title='Presione "Cancelar" para realizar una nueva busqueda' href="consultas" class="btn btn-danger"><i class="fa fa-close"></i> Cancelar</a>
                                                        <input title='Presione "Registrar" para guardar los datos' type="button" value="Registrar" id="actualizarPersonal" name="nuevaInscripcion" class="btn btn-primary" onclick="this.disabled = true;
                                                                        this.value = 'Registrando...';
                                                                        this.form.submit()"/>
                                                        <input type="hidden" name="form" value="nuevaInscripcion"/>
                                                    </div>
                                                </div>
                                            </form>
                                        <?php } else { ?>
                                            <div style=" text-align: center;">
                                                <h2>Número de Documento no encontrado</h2> porfavor póngase en contácto con nosotros vía whatsapp : <br>
                                                <b>945019336 ó 933976250 </b><br>
                                                o enviando un correo a iyfperu@gmail.com<br>
                                                Gracias.<br>
                                            </div> 
                                            <?php
                                        }
                                    } else {
                                        echo "Realize una nueba busqueda..";
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">


                </div>

                <!-- footer content -->
                <div style="height: 600px;">

                </div>
                <footer>
                    <div class="">
                        <strong>Copyright &copy; <?php echo $año; ?> <a href="<?php echo $webEmpresa; ?>"><?php echo $empresa; ?></a>.</strong> All rights reserved.

                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->

            </div>
            <!-- /page content -->
        </div>

    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>


    <script src="js/select/select2.full.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>

    <script src="js/custom.js"></script>
    <!-- form validation -->
    <script src="js/validator/validator.js"></script>

    <link href="css/custom.css" rel="stylesheet">

    <!-- input mask -->
    <script src="js/input_mask/jquery.inputmask.js"></script>

    <script src="js/datatables/js/jquery.dataTables.js"></script>
    <script src="js/datatables/tools/js/dataTables.tableTools.js"></script>
    <!-- form wizard -->
    <script type="text/javascript" src="js/wizard/jquery.smartWizard.js"></script>

    <!-- switchery -->
    <script src="js/switchery/switchery.min.js"></script>
    <!-- image cropping -->
    <script src="js/cropping/cropper.min.js"></script>
    <script src="js/cropping/main.js"></script>
    <!-- daterangepicker -->
    <script type="text/javascript" src="js/moment.min.js"></script>
    <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>
    <!-- moris js -->
    <script src="js/moris/raphael-min.js"></script>
    <script src="js/moris/morris.js"></script>
    <!-- sparkline -->
    <script src="js/sparkline/jquery.sparkline.min.js"></script>
    <!-- easypie -->
    <script src="js/easypie/jquery.easypiechart.min.js"></script>

    <script type="text/javascript">
                                                            $(document).ready(function ()
                                                            {
                                                                $("input[name=S]").change(function () {
                                                                    alert($(this).val());
                                                                });

                                                            });
    </script>


    <script>
        function agregarBoleta() {
            //capturamos los valores
            var nro_boleta = $('#nro_boleta').val();
            var monto_soles = $('#monto_soles').val();
            var monto_dolares = $('#monto_dolares').val();

            var cat = document.getElementById("table_boleta").rows.length;
            if (nro_boleta.length >= 3) {
                $('#table_boleta').append('<tr id="' + cat + '" class="impreso"><td><input type="hidden" name="nroBoleta' + nro_boleta + '" value="' + nro_boleta + '"/>' + nro_boleta + '</td><td><input type="hidden" name="montoSoles' + nro_boleta + '" value="' + monto_soles + '"/>' + monto_soles + '</td><td><input type="hidden" name="montoDolares' + monto_dolares + '" value="' + monto_dolares + '"/>' + monto_dolares + '</td><td><button type="button" class="btn btn-danger" onclick="eliminarAgente(' + cat + ');"><i class="fa fa-minus-square"></i> Quitar</button></td></tr>');
            }

            //limiamos los textbox
            $('#nro_boleta').val("");
            $('#monto_soles').val("");
            $('#monto_dolares').val("");
        }

        function eliminarAgente(id)
        {
            $('#' + id).remove();
        }
    </script>

    <!-- switchery -->
    <script src="js/switchery/switchery.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".select2_single").select2({
                placeholder: "<?php
                                    if ($accion != "buscar") {
                                        echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class='label label-danger'>Lista vacía, porfavor realice la busqueda&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fa fa-arrow-up'></i><i class='fa fa-arrow-up'></i><i class='fa fa-arrow-up'></i></label>";
                                    } else {
                                        if ($num_regs_personas > 0) {
                                            echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class='label label-success'>Seleccionar Persona</label>";
                                        } else {
                                            echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class='label label-warning'>Agregue al Participante ó realize una nueva busqueda</label>";
                                        }
                                    }
                                    ?>",
                allowClear: true
            });
            $(".select2_group").select2({});
            $(".select2_multiple").select2({
                maximumSelectionLength: <?php echo $num_regs_cargos; ?>,
                placeholder: "Seleccione Máximo " + <?php echo $num_regs_cargos; ?> + " CARGOS",
                allowClear: true
            });
        });
    </script>
    <?php
    require('../../include/footer.php');
    ?>

    <!-- form validation -->
    <script src="js/validator/validator.js"></script>
    <script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
                .on('blur', 'input[required], input.optional, select.required', validator.checkField)
                .on('change', 'select.required', validator.checkField)
                .on('keypress', 'input[required][pattern]', validator.keypress);

        $('.multi.required')
                .on('keyup blur', 'input', function () {
                    validator.checkField.apply($(this).siblings().last()[0]);
                });

        // bind the validation to the form submit event
        //$('#send').click('submit');//.prop('disabled', true);

        $('form').submit(function (e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
                submit = false;
            }

            if (submit)
                this.submit();
            return false;
        });

        /* FOR DEMO ONLY */
        $('#vfields').change(function () {
            $('form').toggleClass('mode2');
        }).prop('checked', false);

        $('#alerts').change(function () {
            validator.defaults.alerts = (this.checked) ? false : true;
            if (this.checked)
                $('form .alert').remove();
        }).prop('checked', false);
    </script>


</body>

</html>
