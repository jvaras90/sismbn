<script type="text/javascript">
    $(document).ready(function () {
        $('#formNewInscrip').keypress(function (e) {
            if (e.which === 37) {
                return false;
            } else if (e.which === 38) {
                return false;
            } else if (e.which === 39) {
                return false;
            } else if (e.which === 40) {
                return false;
            } else {
                return true;
            }
            document.onkeydown = disabled();

        });
    });
</script>
<script type="text/javascript">
    function cargarGrupo(id) {
        if (id >= 60) {
            grupo = "SILVER";
        } else if (id >= 39) {
            grupo = "SENIOR";
        } else if (id >= 31) {
            grupo = "GRACE";
        } else if (id >= 24) {
            grupo = "FAITH";
        } else if (id >= 17) {
            grupo = "TRUTH";
        } else if (id >= 11) {
            grupo = "JUNIOR";
        } else {
            grupo = "Niños";
        }
//        switch (id) {
//            case "6":/*Adolescente */
//                grupo = "JUNIOR";
//                break;
//            case "5":/*Joven Menor */
//                grupo = "TRUTH";
//                break;
//            case "4":/* Joven Mayor */
//                grupo = "DREAM";
//                break;
//            case "1":/* Silver */
//                grupo = "SILVER";
//                break;
//            case "2":/* Senior */
//                grupo = "SENIOR";
//                break;
//            default :
//                grupo = "";
//        }
        //alert("idGrupo: " + id + " descripcion: " + grupo + " ");
        $("#grupo").val("" + grupo + "");
    }
</script>
<!-- switchery -->
<link rel="stylesheet" href="css/switchery/switchery.min.css" />
<script src="js/ajax.js" type="text/javascript"></script>
<script src="js/js.js" type="text/javascript"></script>
<div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_title">
            <h2><i>INSCRIPCIÓN DE PARTICIPANTES</i></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <!-- Smart Wizard -->
            <!--<p>En esta vista se encuentran las instrucciones para poder crear correctamente una campaña.</p>-->
            <!--<h2>En esta vista se encuentran las instrucciones para poder crear correctamente la <b>Campaña</b> </h2> -->
            <br>
            <form id="formNewInscrip" class="form-horizontal form-label-left" novalidate method="post" action="registrarA" enctype="multipart/form-data" onkeydown="tecla1(event);">
                <div id="wizard1" class="form_wizard wizard_horizontal">
                    <ul class="wizard_steps">
                        <li>
                            <a href="#step-1">
                                <span class="step_no">1</span>
                                <span class="step_descr">
                                    Paso 1<br />
                                    <i class="fa fa-user"></i><small> Datos Personales</small>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#step-2">
                                <span class="step_no">2</span>
                                <span class="step_descr">
                                    Paso 2<br />
                                    <i class="fa fa-phone"></i><small> Contactabilidad</small>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#step-3">
                                <span class="step_no">3</span>
                                <span class="step_descr">
                                    Paso 3<br />
                                    <i class="fa fa-map-marker"></i><small> Datos de Domicilio</small>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#step-4">
                                <span class="step_no">4</span>
                                <span class="step_descr">
                                    Paso 4<br />
                                    <i class="fa fa-list"></i><small> Adicionales</small>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#step-5">
                                <span class="step_no">5</span>
                                <span class="step_descr">
                                    Paso 5<br />
                                    <i class="fa fa-money"></i><small> Inf. de Abono</small>
                                </span>
                            </a>
                        </li>
                    </ul>
                    <div id="step-1">
                        <h2 class="StepTitle">Ingrese su información rigurosamente, puede leer las instrucciones en cada etiqueta</h2><br>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="item form-group" title="">
                                <div title="Seleccione su condición ante la comunidad de IYF"> 
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">Condición</label>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <select class='form-control' name='idcategoria' id='idcategoria'>
                                            <option value="2">Invitado</option>
                                            <option value="1">Miembro</option>
                                        </select>
                                    </div>
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">&nbsp;</label>
                                </div>
                            </div>
                            <div class="item form-group" title="">
                                <div title="Ingrese sus nombres"> 
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Nombre</label>
                                    <div class="col-md-4 col-sm-4 col-xs-4" >
                                        <input name="nombre" id="nombre" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="3" data-validate-words="1" placeholder="Ingrese su nombre" maxlength="300" autocomplete="" type="text" required="required">
                                    </div>
                                </div>
                                <div title="Ingrese una foto, de preferencia postal"> 
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Subir Imagen</label>
                                    <div class="col-md-4 col-sm-4 col-xs-4" >
                                        <input class="avatar-input" id="foto_fls" name="foto_fls" type="file">
                                        <input type="hidden" name="foto_hdn" value="<?php echo $foto ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="item form-group" title="">
                                <div title="Ingrese su Apellido Paterno"> 
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">A. Paterno</label>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <input name="aPaterno" id="aPaterno" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="2" data-validate-words="1" placeholder="Ingrese Apellido Paterno" maxlength="300" autocomplete="" type="text" required="required" >
                                    </div>
                                </div>
                                <div title="Ingrese su Apellido Materno"> 
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">A. Materno</label>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <input name="aMaterno" id="aMaterno" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="1" data-validate-words="1" placeholder="Ingrese Apellido Materno" maxlength="300" autocomplete="" type="text" required="required" >
                                    </div>
                                </div>
                            </div> 
                            <div class="item form-group">
                                <div title="Seleccione el tipo de documento"> 
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Tipo Documento</label>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <select class='form-control' name='idtipo_documento' id='idtipo_documento'>
                                            <?php include_once "../../system/selectTipoDocumento.php"; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-1 col-sm-1 col-xs-1 " id="nuevoGrupo" title="Agregar Nuevo Tipo de Documento">
                                        <a href="#modalNuevoTipoDocumento" data-toggle="modal" class = "btn btn-round btn-success btn-sm hidden" ><i class = "fa fa-plus-square"></i></a>
                                    </div>
                                </div>
                                <div title="Ingrese su número de documento (DNI)"> 
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Número Doc.</label>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <input name="dni" id="dni" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="1" placeholder="Ingrese su número de documento" maxlength="30" autocomplete="" type="text" value="<?php echo $nro_documento; ?>"required="required">
                                    </div>
                                </div>
                            </div>
                            <div class="item form-group">
                                <div title="Seleccione su Sexo"> 
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Sexo</label>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <div class="radio-inline">
                                            <label>
                                                <input class="flat" type="radio" <?php
                                                if ($sexo == "M") {
                                                    echo "checked";
                                                }
                                                ?> value="M" id="S" name="S"> M
                                            </label>
                                        </div>
                                        <div class="radio-inline">
                                            <label>
                                                <input class="flat" type="radio" <?php
                                                if ($sexo == "F") {
                                                    echo "checked";
                                                }
                                                ?> value="F" id="S" name="S"> F
                                            </label>
                                        </div> 
                                    </div>
                                </div>
                                <div title="Seleccione su estado civil"> 
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Estado Civil</label>
                                    <div class="col-md-3 col-sm-3 col-xs-3" >
                                        <select class='form-control' name='idestado_civil' id='idestado_civil'>
                                            <option value="0" >Seleccione</option>
                                            <?php include_once "../../system/selectEstadoCivil.php"; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Estado Civil">
                                        <a href="#modalNuevoEstadoCivil" data-toggle="modal" class = "btn btn-round btn-success btn-sm hidden" ><i class = "fa fa-plus-square"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item form-group">
                                <div title="Ingrese su fecha de nacimiento Año - Mes - Dia | ejemplo:  1990-01-01"> 
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Fecha Nac.</label>
                                    <div class="col-md-4 col-sm-4 col-xs-4" >
                                        <input type="text" id="fecha_nac1"  name="fecha_nac1"  class="form-control col-md-3 col-sm-3 col-xs-3" data-inputmask="'mask': '9999-99-99'" required="required">
                                    </div> 
                                </div>
                                <div title="Ingrese su edad"> 
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Edad</label>
                                    <div class="col-md-4 col-sm-4 col-xs-4" >
                                        <input name="edad" id="edad" class="form-control col-md-3 col-sm-3 col-xs-3" data-validate-length-range="1" data-validate-words="1" placeholder="Edad" maxlength="30" autocomplete="" type="text" required="required" onkeyup="cargarGrupo(this.value)">
                                    </div>
                                </div>
                            </div>
                            <div class="item form-group">
                                <div title="Ingrese la ciudad en donde ud. nació"> 
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Lugar Nac.</label>
                                    <div class="col-md-4 col-sm-4 col-xs-4" >
                                        <input name="lugar_nac" id="lugar_nac" class="form-control col-md-3 col-sm-3 col-xs-3" data-validate-length-range="1" data-validate-words="1" placeholder="Ingrese el lugar donde nació" maxlength="30" autocomplete="" type="text" required="required">
                                    </div>
                                </div>
                            </div>
                            <div class="item form-group">
                                <div title="Ingrese la profesión o la carrera que esta cursando"> 
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Profesión</label>
                                    <div class="col-md-4 col-sm-4 col-xs-4" >
                                        <input name="profesion" id="profesion" class="form-control col-md-3 col-sm-3 col-xs-3" data-validate-length-range="6" data-validate-words="2" placeholder="Ingrese la profesión u ocupación que ejerce." maxlength="245" autocomplete="" type="text">
                                    </div>
                                </div>
                                <div title="Ingrese su trabajo actual u ocupación que realice"> 
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Trabajo Actual</label>
                                    <div class="col-md-4 col-sm-4 col-xs-4" >
                                        <input name="ocupacion" id="ocupacion" class="form-control col-md-3 col-sm-3 col-xs-3" data-validate-length-range="6" data-validate-words="2" placeholder="Ingrese su trabajo actual" maxlength="245" autocomplete="" type="text" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div><br>  
                    </div>
                    <div id="step-2">
                        <h2 class="StepTitle">Estos datos nos ayudarán a poder contactarnos con ud.</h2>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="item form-group">
                                <div title="Ingrese su número de telefono fijo">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Teléfono</label>
                                    <div class="col-md-4 col-sm-4 col-xs-4" >
                                        <input name="telefono" id="telefono" class="form-control col-md-3 col-sm-3 col-xs-3" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="" type="text" >
                                    </div>
                                </div>
                                <div title="Ingrese su número móvil">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Celular</label>
                                    <div class="col-md-4 col-sm-4 col-xs-4" >
                                        <input name="celular" id="celular" class="form-control col-md-3 col-sm-3 col-xs-3" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="30" autocomplete="" type="text" >
                                    </div>
                                </div>
                            </div>
                            <div class="item form-group">
                                <div title="Ingrese su correo electroonico de frecuencia">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Email</label>
                                    <div class="col-md-4 col-sm-4 col-xs-4" id="cliente">
                                        <input type="text" id="email" name="email" class="form-control col-md-7 col-sm-7 col-xs-7" >
                                    </div>
                                </div>
                                <div title="Ingrese por medio de quien se enteró de éste evento">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Persona de Referencia</label>
                                    <div class="col-md-4 col-sm-4 col-xs-4" id="cliente">
                                        <input type="text" id="referencia" name="referencia" class="form-control col-md-7 col-sm-7 col-xs-7" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div><br>  
                    </div>
                    <div id="step-3">
                        <h2 class="StepTitle">Seleccione de forma ordenada el País, Departamento, Provincia, Distrito y finalemnte ingrese su dirección.</h2><br>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Pais  <?php //echo $iddepartamento;                                                                                                                                                                   ?></label>
                                <div class="col-md-3 col-sm-3 col-xs-3" id="cliente">
                                    <select class='form-control' name='idpais' id='idpais' onchange="load_departamento(this.value)" >
                                        <option value="0">Seleccione</option>
                                        <?php include_once "../../system/selectPais.php"; ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Pais">
                                    <a href="#modalNuevoPais" data-toggle="modal" class = "btn btn-round btn-success btn-sm hidden" ><i class = "fa fa-plus-square"></i></a>
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Departamento  <?php //echo $iddepartamento;                                                                                                                                                                   ?></label>
                                <div class="col-md-3 col-sm-3 col-xs-3" id="myDivDep">
                                    <select class='form-control' name='iddepartamento' id='iddepartamento' onchange="load_provincia(this.value)" >
                                        <option>Seleccione Departamento</option>
                                        <?php //include_once "../../system/selectDepartamento.php"; ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Departamento">
                                    <a href="#modalNuevoDepartamentos" data-toggle="modal" class = "btn btn-round btn-success btn-sm hidden" ><i class = "fa fa-plus-square"></i></a>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Provincia  <?php // echo $idprovincia;                                                                                                                                                                   ?></label>
                                <div class="col-md-3 col-sm-3 col-xs-3" id="myDivProv">
                                    <select class='form-control' name='idprovincia' id='idprovincia' onchange="load_distrito(this.value)" >
                                        <option>Seleccione Provincia</option>
                                        <?php //include_once "../../system/selectProvincia.php"; ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nueva Provincia">
                                    <a href="#modalNuevoProvincias" data-toggle="modal" class = "btn btn-round btn-success btn-sm hidden" ><i class = "fa fa-plus-square"></i></a>
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Distrito <?php // echo $iddistrito;                                                                                                                                                                   ?></label>
                                <div class="col-md-3 col-sm-3 col-xs-3" id="myDivDist">
                                    <select class='form-control' name='iddistrito' id='iddistrito'>
                                        <option>Seleccione Distrito</option>
                                        <?php //include_once "../../system/selectDistrito.php"; ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1" id="nuevoGrupo" title="Agregar Nuevo Distrito">
                                    <a href="#modalNuevoDistritos" data-toggle="modal" class = "btn btn-round btn-success btn-sm hidden" ><i class = "fa fa-plus-square"></i></a>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Dirección</label>
                                <div class="col-md-10 col-sm-10 col-xs-12" id="cliente">
                                    <input type="text" id="direccion" name="direccion" required="required" placeholder="" class="form-control col-md-7 col-sm-7 col-xs-7" value="<?php echo "$direccion"; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div><br>  
                    </div>
                    <div id="step-4">
                        <h2 class="StepTitle">Información adicional para el World Camp</h2> <br>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                <div style="text-align: center;">
                                    <label class="control-label  has-feedback-left" title="Active esta opcion si deseas participar como voluntario para este gran evento">
                                        Voluntario <input type="checkbox" id="volunt" name="volunt" class="js-switch form-control" /> 
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Éste es el grupo al que pertenece el participante">
                                <input class="form-control has-feedback-left" id="grupo" name="grupo" placeholder="Grupo" type="text" value="" readonly>
                                <span class="fa fa-users form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Seleccione la talla del polo">
                                <select class="form-control  has-feedback-left" id="idtalla_polo" name="idtalla_polo" >
                                    <option value="0">Talla Polo</option>
                                    <?php include_once "../../system/selectTallaPolo.php"; ?>
                                </select>
                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" id="myDivCiudad" title="Seleccione la Ciudad">
                                <select class="form-control  has-feedback-left" id="idciudad" name="idciudad" >
                                    <option value="0">Lugar</option>
                                    <?php include_once "../../system/selectCiudad.php"; ?>
                                </select>
                                <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback" title="Seleccione el estado en el que se encuentra el Abono de la Iniscipción">
                                <select class="form-control  has-feedback-left" id="idestado_pago" name="idestado_pago" >
                                    <option value="0">No Cancelado</option>
                                    <?php //include_once "../../system/selectEstadoPago.php"; ?>
                                </select>
                                <span class="fa fa-money form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback" title="Ingrese alguna observacion del participante">
                                <textarea style="resize: none;" class="form-control has-feedback-left" id="observacion" name="observacion" placeholder="Observaciones o Sugerencias"></textarea>
                                <span class="fa fa-comments form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="clearfix"></div><br>                    
                    </div>
                    <div id="step-5">
                        <input type="hidden" name="form" value="newIncripcionOnline"/>

                        <h2 class="StepTitle">Aqui aparece la información correspondiente para que pueda realizar el abono de la inscripción.</h2>
                        <div class="ln_solid"></div>
                        <div style="text-align: center;">
                            <h1>  Numero de cuenta BCP</h1>
                            <h2 style="font-size: 20px;"> 191 - 30536787 - 037</h2>                            
                            <h2 style="font-size: 20px;"> Donación : $ 70.00 o un equivalente de S/.240.00 </h2>
                            <h2 style="font-size: 20px;"> Representannte: David Cercado Ruiz </h2>
                        </div>
                        </p>
                        <p>
                            Después de realizar el abono en un agente o una ventanilla del BCP, por favor envíenos la foto del boucher a los siguientes correos :
                        </p>
                        <ul>
                            <li>rossyiyf@gmail.com</li>
                            <li>peruiyf@gmail.com</li>
                        </ul>
                        <div style="text-align: right;">
                            <div class="product_social">
                                <b><span style="font-size: 17px;text-align: left">Visítenos en:</span></b>
                                <ul class="list-inline">
                                    <li title=""><a href="https://web.facebook.com/iyfperu/" target="_blank"><i class="btn btn-primary btn-xs fa fa-facebook-square"></i></a>
                                    </li>
                                    <li title=""><a href="https://twitter.com/IYFPERU" target="_blank"><i class="btn btn-info btn-xs  fa fa-twitter-square"></i></a>
                                    </li>
                                    <li title=""><a href="http://iyf.org.pe/contactenos/" target="_blank"><i class="btn btn-warning btn-xs  fa fa-envelope-square"></i></a>
                                    </li>
                                    <li title=""><a href="https://www.youtube.com/channel/UCcAWB2frMLoZSPt2VCUB9vg" target="_blank"><i class="btn btn-danger btn-xs  fa fa-youtube-square"></i></a>
                                    </li>
                                    <li title=""><a  target="_blank"><i class="btn btn-warning btn-xs  fa fa-rss-square"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="clearfix"></div><br> 
                        </div>
                    </div>
                </div>
                <!-- End SmartWizard Content -->

                <!-- End SmartWizard Content -->
            </form>
        </div>
    </div>
</div>
<!-- form wizard -->
<script type="text/javascript" src="js/wizard/jquery.smartWizard.js"></script>
<script type="text/javascript">
                                        $(document).ready(function () {
                                            // Smart Wizard     
                                            $('#wizard1').smartWizard();
                                            function onFinishCallback() {
                                                $('#wizard1').smartWizard('showMessage', 'Finish Clicked');
                                                //alert('Finish Clicked');
                                            }
                                        });
                                        $(document).ready(function () {
                                            // Smart Wizard 
                                            $('#wizard_verticle').smartWizard({
                                                transitionEffect: 'slide'
                                            });
                                        });
</script>   
<script>
    $(document).ready(function () {
        $(".select2_single").select2({
            placeholder: "Select a state",
            allowClear: true
        });
        $(".select2_group").select2({});
        $(".select2_multiple").select2({
            maximumSelectionLength: <?php echo 10; ?>,
            placeholder: "Seleccione Maximo " + <?php echo 10; ?> + " Cargos",
            allowClear: true
        });


    });
</script>
<!-- input_mask -->
<script>
    $(document).ready(function () {
        $(":input").inputmask();
    });

    function validar() {
        //var nombre = document.getElementById("nombres").value();
        var nombre = $('#nombre').val();
        if (nombre == "") {
            alert("Falta nombre");
        }
    }
</script>
<!-- /input mask -->
<!-- switchery -->
<script src="js/switchery/switchery.min.js"></script>


<!-- form validation -->
<script src="js/validator/validator.js"></script>
<script>
// initialize the validator function
    validator.message['date'] = 'not a real date';

// validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
            .on('keyup blur', 'input', function () {
                validator.checkField.apply($(this).siblings().last()[0]);
            });

// bind the validation to the form submit event
//$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function (e) {
        e.preventDefault();
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
            submit = false;
        }

        if (submit)
            this.submit();
        return false;
    });

    /* FOR DEMO ONLY */
    $('#vfields').change(function () {
        $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function () {
        validator.defaults.alerts = (this.checked) ? false : true;
        if (this.checked)
            $('form .alert').remove();
    }).prop('checked', false);
</script>
