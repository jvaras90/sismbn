<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <div class="col-md-2">
                    <h2>Lista de Participantes<small>registrados</small> </h2>
                </div>
                <div class="col-md-8">

                </div>
                <div class="col-md-2 hidden">
                    <a href="#modalNuevo<?php echo $hoja; ?>" class="btn btn-success" data-toggle="modal"><i class="fa fa-plus-square-o"></i> Nuevo</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table  id="example"  class="table table-striped responsive-utilities jambo_table bulk_action">
                    <thead>
                        <tr class="headings">
<!--                            <th>
                                <input type="checkbox" id="check-all" class="flat">
                            </th>-->
                            <th class="column-title">Participante</th>
                            <!--<th class="column-title">DNI</th>-->
                            <th class="column-title">Edad</th>
                            <!--<th class="column-title">Sexo</th>-->
                            <th class="column-title">Pais</th>
                            <th class="column-title">Lugar</th>
                            <th class="column-title">Condición</th>
                            <th class="column-title">Grupo</th>
                            <th class="column-title">Voluntario</th>
                            <!--<th class="column-title">Estado de Pago</th>-->
                            <!--<th class="column-title">Gestión</th>-->
                            <!--<th class="column-title no-link last"><span class="nobr">Accion</span>-->
                            </th>
                            <th class="bulk-actions" colspan="7">
                                <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "select p.idpersona,concat(p.nombres,' ',p.apaterno,' ',p.amaterno) nombre, i.edad , i.dni, i.idestado_pago,i.idciudad, ep.descripcion estado_pago,
i.sexo, i.voluntario , i.idcategoria, cat.descripcion categoria , i.grupo, p.idpais, des_pais , p.iddistrito , des_distrito
from incripciones_2016 i inner join persona p on p.idpersona = i.idpersona
inner join estado_pago ep on ep.idestado_pago = i.idestado_pago
inner join categoria cat on cat.idcategoria = i.idcategoria
inner join distrito di on di.iddistrito = p.iddistrito
inner join pais pa on pa.idpais = p.idpais
order by i.fecha_registro desc ;";
                        $result = $mysqlMBN->consultas($sql);
                        $registros = mysqli_num_rows($result);
                        echo "Total de Registros: " . $registros . "<br>";
                        while ($row = mysqli_fetch_array($result)) {
                            $idpersona = $row['idpersona'];
                            $nombre = $row['nombre'];
                            $edad = $row['edad'];
                            $dni = $row['dni'];
                            $idestado_pago = $row['idestado_pago'];
                            $estado_pago = $row['estado_pago'];
                            $sexo = $row['sexo'];
                            $voluntario = $row['voluntario'];
                            $idcategoria = $row['idcategoria'];
                            $cagetoria = $row['categoria'];
                            $grupo = $row['grupo'];
                            $iddistrito = $row['iddistrito'];
                            $distrito = $row['des_distrito'];
                            $idpais = $row['idpais'];
                            $pais = $row['des_pais'];
                            $grupo = $row['grupo'];
                            ?>
                            <tr class="even pointer">
                                <td class=""><?php echo $nombre; ?></td>
                                <!--<td class=""><?php echo $dni; ?></td>-->
                                <td class=""><?php echo $edad; ?></td>
                                <!--<td class=""><?php echo $sexo; ?></td>-->
                                <td class=""><?php echo $pais; ?></td>
                                <td class=""><?php echo $distrito; ?></td>
                                <td class=""><?php echo $cagetoria; ?></td>
                                <td class=""><?php echo $grupo; ?></td>
                                <td class=""><?php
                                    if ($voluntario == 1) {
                                        echo "Si";
                                    } else {
                                        echo "No";
                                    }
                                    ?>
                                </td>
                                <!--<td class=""><?php echo $estado_pago; ?></td>-->
                            </tr>
                        <?php } ?>                                    
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <?php
    include '../VISTAS/listaModals.php';
    ?>
</div>