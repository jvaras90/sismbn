<?php

include '../../php/system/crearConexion.php';
session_start();

//datos iniciales por si no existe el $_POST
$nombreListado = "random";
//$consulta = "select p.idpersona,concat(nombres,' ',apaterno,' ',amaterno) as nombre,p.foto,p.telefono_fijo,p.celular,p.lugar_nacimiento, p.email, p.profesion,p.ocupacion, p.idtipo_documento,td.descripcion documento , p.nro_documento,p.idgrupos,gru.desc_camp grupo,p.idpais , p.fec_nac, pa.des_pais pais, p.iddepartamento , dep.des_departamento , ds.idcargo,p.sexo,ds.idreunion_zonal,rz.descripcion reunion_zonal, p.idcategoria, cat.descripcion condicion,p.direccion,p.idestado_civil,ec.descripcion estado_civil, YEAR(CURDATE())-YEAR(p.fec_nac) + IF(DATE_FORMAT(CURDATE(),'%m-%d') > DATE_FORMAT(p.fec_nac,'%m-%d'), 0, -1) edad from persona p inner join tipo_documento td on td.idtipo_documento = p.idtipo_documento inner join categoria cat on cat.idcategoria = p.idcategoria inner join grupos gru on gru.idgrupos = p.idgrupos inner join pais pa on pa.idpais = p.idpais inner join departamento dep on dep.iddepartamento = p.iddepartamento inner join estado_civil ec on ec.idestado_civil = p.idestado_civil left join detalle_salvacion ds on ds.idpersona = p.idpersona left join reunion_zonal rz on rz.idreunion_zonal = ds.idreunion_zonal where '1'='1' and p.sexo= 'F' and p.idgrupos = 1 and p.idpais = 89 and p.iddepartamento = 1610 limit 10 ;";
if (isset($_GET['sql'])) {
    $sql = $_GET['sql'];
    $parte = $_GET['parte'];
    $mod = $_GET['mod'];
    $limite = $_GET['limite'];

    if ($mod == 'lPersonas') {
        $idgrupo = $_GET['idgrupo'];
        $sexo = $_GET['sexo'];
        $sqli = "select * from grupos where idgrupos = $idgrupo";
        $resi = $mysqlMBN->consultas($sqli);
        $row = mysqli_fetch_array($resi);
        $grupo = $row['desc_camp'];
        if ($sexo == 'M') {
            $s = 'Varones';
        } else {
            $s = 'Mujeres';
        }

        $nombreListado = "$grupo $s ";
    } else if ($mod == 'lMinistros') {
        $idcargo = $_GET['idcargo'];
        $sqli = "select * from cargo where idcargo = $idcargo";
        $resi = $mysqlMBN->consultas($sqli);
        $row = mysqli_fetch_array($resi);
        $descripcion = $row['descripcion']; 
        $nombreListado = "$descripcion";
    } else if ($mod == 'lReunZonal') {
        $idreunion_zonal = $_GET['idreunion_zonal'];
        $sqli = "select * from reunion_zonal where idreunion_zonal = $idreunion_zonal";
        $resi = $mysqlMBN->consultas($sqli);
        $row = mysqli_fetch_array($resi);
        $descripcion = $row['descripcion'];
        $nombreListado = "Reunion Zonal: $descripcion";
    }

    $cadena = str_replace("_3", "%", $sql);
    $consulta = "select p.idpersona,concat(nombres,' ',apaterno,' ',amaterno) as nombre,p.foto,p.telefono_fijo,p.celular,p.lugar_nacimiento,
p.email,p.profesion,p.ocupacion,p.idtipo_documento,td.descripcion documento,p.nro_documento,p.idgrupos,gru.desc_camp grupo,p.idpais,p.fec_nac,
pa.des_pais pais,p.iddepartamento,dep.des_departamento , ds.idcargo,p.sexo,ds.idreunion_zonal,rz.descripcion reunion_zonal,
p.idcategoria,cat.descripcion condicion,p.direccion,p.idestado_civil,ec.descripcion estado_civil,
YEAR(CURDATE())-YEAR(p.fec_nac) + IF(DATE_FORMAT(CURDATE(),'%m-%d') > DATE_FORMAT(p.fec_nac,'%m-%d'),0,-1) edad 
from persona p 
inner join tipo_documento td on td.idtipo_documento=p.idtipo_documento 
inner join categoria cat on cat.idcategoria=p.idcategoria 
inner join grupos gru on gru.idgrupos=p.idgrupos 
inner join pais pa on pa.idpais=p.idpais 
inner join departamento dep on dep.iddepartamento=p.iddepartamento 
inner join estado_civil ec on ec.idestado_civil=p.idestado_civil 
left join detalle_salvacion ds on ds.idpersona=p.idpersona 
left join reunion_zonal rz on rz.idreunion_zonal=ds.idreunion_zonal 
$cadena 
order by nombre 
limit $limite 
";

  //  echo $consulta;
}
$nombreArchivo = "Listado de $nombreListado";
//============================================================+
// File name   : example_061.php
// Begin       : 2010-05-24
// Last Update : 2014-01-25
//
// Description : Example 061 for TCPDF class
//               XHTML + CSS
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: XHTML + CSS
 * @author Nicola Asuni
 * @since 2010-05-25
 */
// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Jean Harold Varas Ramirez');
$pdf->SetTitle('' . $nombreArchivo . '');
$pdf->SetSubject('Mision Buenas Nuevas');
$pdf->SetKeywords('Mision, PDF, Buenas, Nueva, Listado');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, "$nombreArchivo");

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
//$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
//$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
// set image scale factor
//$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
    require_once(dirname(__FILE__) . '/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------
// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage();

/* NOTE:
 * *********************************************************
 * You can load external XHTML using :
 *
 * $html = file_get_contents('/path/to/your/file.html');
 *
 * External CSS files will be automatically loaded.
 * Sometimes you need to fix the path of the external CSS.
 * *********************************************************
 */

// define some HTML content with style

$html = '
<!-- EXAMPLE OF CSS STYLE -->
<style>
	h1 {
		color: navy;
		font-family: times;
		font-size: 24pt;
		text-decoration: underline;
                text-align:center;
	} 
	table.first {
                width: 480px;
		color: #003300;
		font-family: helvetica;
		font-size: 8pt;
		border-left: 1px solid black;
		border-right: 1px solid black;
		border-top: 1px solid black;
		border-bottom: 1px solid black;
		background-color: white;
                text-align:center;
                margin:auto;
	}
	td {
		border: 1px solid black;
		background-color: white;
	} 
	.lowercase {
		text-transform: lowercase;
	}
	.uppercase {
		text-transform: uppercase;
	}
	.capitalize {
		text-transform: capitalize;
	}    
</style> 
  <br>  ';

$result = $mysqlMBN->consultas($consulta);
$registros = mysqli_num_rows($result);
//echo "Total de Registros: " . $registros . "<br>";
while ($row = mysqli_fetch_array($result)) {
    $des_total1 = "";
    $idpersona = $row['idpersona'];
    $nombres = $row['nombre'];
    $foto = $row['foto'];
    $telefono_fijo = $row['telefono_fijo'];
    $celular = $row['celular'];
    $documento = $row['documento'];
    $nro_documento = $row['nro_documento'];
    $idgrupos = $row['idgrupos'];
    $grupo = $row['grupo'];
    $idpais = $row['idpais'];
    $pais = $row['pais'];
    $iddepartamento = $row['iddepartamento'];
    $des_departamento = $row['des_departamento'];
    $idcargo = $row['idcargo'];
    $sexo = $row['sexo'];
    $idreunion_zonal = $row['idreunion_zonal'];
    $reunion_zonal = $row['reunion_zonal'];
    $idcategoria = $row['idcategoria'];
    $condicion = $row['condicion'];
    $fec_nac = $row['fec_nac'];
    if ($fec_nac == '0000-00-00') {
        $edad = "S/N";
    } else {
        $edad = $row['edad'];
    }
    $direccion = $row['direccion'];
    $idestado_civil = $row['idestado_civil'];
    $estado_civil = $row['estado_civil'];
    $lugar_nacimiento = $row['lugar_nacimiento'];
    $email = $row['email'];
    $profesion = $row['profesion'];
    $ocupacion = $row['ocupacion'];
    $cargo = $row['idcargo'];
//    $ = $row['idgrupo'];
//    $ = $row['idgrupo'];
//    $ = $row['idgrupo'];
//    $ = $row['idgrupo'];
//    $ = $row['idgrupo'];
//    
//    
    $sqry2 = "select * from cargo where idcargo in ($cargo);";
    //echo $sqry2."<br>";
    $res2 = $mysqlMBN->consultasLibre($sqry2);
    while ($row2 = mysqli_fetch_array($res2)) {
        $des_total1 .= $row2['descripcion'] . ",";
    }
    $des_cargo1 = substr($des_total1, 0, -1);

    $html .='
<br>
  <table class="first" border="0" cellspacing="0" cellpadding="0">
        <tbody>
                                    <tr>  
                                     <td  rowspan="8"  style="width:80px;height: 80px;"   >   
                                         <img src="../../images/Fotos/' . $foto . '" style="width:80px;height: 80px; top:0px;">
                                         
                                        </td>                                
                                        <td colspan="8"><h2 class="brief"><i> ' . $nombres . ' [' . $condicion . '] </i></h2></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Fecha Nac: </strong></td>
                                        <td>  ' . $fec_nac . ' </td>
                                        <td><strong>Edad </strong></td>
                                        <td>  ' . $edad . ' </td>
                                        <td><strong> ' . $documento . ': </strong></td>
                                        <td>  ' . $nro_documento . ' </td>
                                        <td><strong>Estado: </strong></td>
                                        <td>  ' . $estado_civil . ' </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Grupo: </strong></td>
                                        <td>  ' . $grupo . ' </td>
                                        <td><strong>Sexo: </strong></td>
                                        <td>  ' . $sexo . ' </td>
                                        <td><strong>Tel&eacute;fono: </strong></td>
                                        <td>  ' . $telefono_fijo . ' </td>
                                        <td><strong>Celular: </strong></td>
                                        <td>  ' . $celular . ' </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Lugar Nac: </strong></td>
                                        <td colspan="3">' . $lugar_nacimiento . ' </td>
                                        <td><strong>R. Zonal: </strong></td>
                                        <td colspan="3">' . $reunion_zonal . ' </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Ocupación: </strong></td>
                                        <td colspan="3">' . $profesion . ' </td>
                                        <td><strong>Trab. Actual: </strong></td>
                                        <td colspan="3">' . $ocupacion . ' </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Cargo: </strong></td>
                                        <td colspan="3">' . $des_cargo1 . ' </td>
                                        <td><strong>E-mail: </strong></td>
                                        <td colspan="3">' . $email . '</td>  
                                    </tr> 
                                    <tr>
                                        <td><strong>Ciudad: </strong></td>
                                        <td colspan="8"> ' . $des_departamento . '</td> 
                                    </tr> 
                                    <tr>
                                        <td><strong>Dirección: </strong></td>
                                        <td colspan="8">' . $direccion . ' </td>                                            
                                    </tr>  
                                    </tbody>
                                    
</table><br>
';
}


// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------
//Close and output PDF document
$pdf->Output('' . $nombreArchivo . $parte . '.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
