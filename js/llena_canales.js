    $(function(){
       /* Ponemos evento blur a la escucha sobre id canal en id canales. */
       $('#canales').on('blur','#canal',function(){
          /* Obtenemos el valor del campo */
          var valor = this.value;
          /* Si la longitud del valor es mayor a 2 caracteres.. */
          if(valor.length>=1){
     
             /* Cambiamos el estado.. */
             $('#estado').html('Cargando datos de servidor...');
     
             /* Hacemos la consulta ajax */
             var consulta = $.ajax({
                type:'POST',
                url:'php/system/cargaDatosCanal.php',
                data:{canal:valor},
                dataType:'JSON'
             });
     
             /* En caso de que se haya retornado bien.. */
             consulta.done(function(data){
                if(!data){
                   $('#estado').html('Ha ocurrido un error: '+data);
                   return false;
                } else {
                   if(data.central!==undefined){$('#canales #central').val(data.central);}
                   if(data.cabecera!==undefined){$('#canales #cabecera').val(data.cabecera);}
                   if(data.nombre!==undefined){$('#canales #nombre').val(data.nombre);}
                   if(data.idcanal!==undefined){$('#canales #idcanal').val(data.idcanal);}
                   $('#estado').html('Datos cargados..');
                   return true;
                }
             });
     
             /* Si la consulta ha fallado.. */
             consulta.fail(function(){
                $('#estado').html('Ha habido un error contactando el servidor.');
                return false;
             });
     
          } else {
             /* Mostrar error */
             $('#estado').html('El nombre tener una longitud mayor a 2 caracteres...');
             return false;
          }
       });
    });