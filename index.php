<?php
session_start();
clearstatcache();
include 'php/include/informacion.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title><?php echo $tittle_login; ?></title>
        <link href="http://misionbuenasnuevas.info/wp-content/uploads/2016/02/16x16-mbn.png" rel="shortcut icon" />
        <link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/functions.ajax.js"></script>
        <style>
            /*            #login_userbttn{
                            background:#e5e5e5;
                            display:inline-block;
                            margin:0px;
                            padding:1px 8px 1px;
                            color:#333;
                            text-decoration:none;
                            text-shadow: 0 1px 1px #FFF;
                            border:1px solid #ccc;
                        }*/
            .boton{
                text-align:center;
                text-decoration: none;
                font-family: 'Helvetica Neue', Helvetica, sans-serif;
                display:inline-block;
                color: #FFF;
                background: #7F8C8D;
                padding: 6px 12px;
                white-space: nowrap;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                margin: 10px 5px;
                -webkit-transition: all 0.2s ease-in-out;
                -ms-transition: all 0.2s ease-in-out;
                -moz-transition: all 0.2s ease-in-out;
                -o-transition: all 0.2s ease-in-out;
                transition: all 0.2s ease-in-out;
            }

            .azul{
                background-color: #3498DB;
            }

            .azul:hover{
                background: #2980B9;
            }

            .verde{
                background: #2ECC71;
            }

            .verde:hover{
                background: #27AE60;
            }

            .rojo{
                background: #E74C3C;
            }

            .rojo:hover{
                background: #C0392B;
            }

            .naranja{
                background: #E67E22;
            }

            .naranja:hover{
                background: #D35400;
            }
        </style>
        <script>
            function txt2Focus() {
                document.getElementById("login_userpass").focus();
            }

            function botonClick() {
                document.getElementById("login_userbttn").focus();
//                var bottom = document.getElementById("login_userpass").val;
//                if ( bottom == true) {
//                    $('#login_userbttn').click();
//                }
            }
            function validateEnter(e) {
                var key = e.keyCode || e.which;
                if (key == 13) {
                    return true;
                } else {
                    return false;
                }
            }
        </script>
    </head>
    <body>
        <div id="allContent">
            <table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%"  style="background:<?php echo $fondo ?>;">
                <tr>
                    <td align="center" valign="middle" height="100%" width="100%">
                        <div >                            
                            <p><img style="height: 200px; width: 450px;" src="images/MBNlogo.jpg"></p>
                        </div>
                        <div id="alertBoxes" >                            
                        </div>
                        <span class="loginBlock">
                            <span class="inner"  style="background:<?php echo $fondo ?>;">
                                <?php
                                $ipvisitante = $_SERVER["REMOTE_ADDR"];
                                if (isset($_SESSION['username']) && isset($_SESSION['idusuario'])) {
                                    ?>
                                    <script>
                                        window.location.href = "inicio";
                                    </script>
                                    <?php
                                    //header("Location: inicio");
                                    //die();
                                } else {
                                    ?>
                                    <form method="post" action="">
                                        <table cellpadding="0" cellspacing="0" border="0"  style="background:<?php echo $fondo ?>;">
                                            <tr>
                                                <td>Usuario:</td>
                                                <td>
                                                    <input type="text" name="login_username" id="login_username" onkeyup="if (validateEnter(event) == true) {
                                                                    txt2Focus();
                                                                }"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Contrase&ntilde;a:</td>
                                                <td>
                                                    <input type="password" name="login_userpass" id="login_userpass" onkeyup="if (validateEnter(event) == true) {
                                                                    botonClick();
                                                                }"  />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td  align="right">
                                                    <a href="salir">Logg Off</a>
                                                </td>
                                                <td  align="right">
                                                    <span class="timer" id="timer"></span>
                                                    <!--<button id="login_userbttn">Login</button>-->
                                                    <input 
                                                        id="login_userbttn" type="button" value="Login" name="nuevaCampana" class="boton azul" onclick="this.disabled = true;
                                                                    this.value = 'Logeando...';//this.form.submit()"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </form>
                                <?php } ?>
                            </span>
                        </span>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>