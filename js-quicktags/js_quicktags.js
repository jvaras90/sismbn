// JS QuickTags version 1.3.1
//
// Copyright (c) 2002-2008 Alex King
// http://alexking.org/projects/js-quicktags
//
// Thanks to Greg Heo <greg@node79.com> for his changes 
// to support multiple toolbars per page.
//
// Licensed under the LGPL license
// http://www.gnu.org/copyleft/lesser.html
//
// **********************************************************************
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
// **********************************************************************
//
// This JavaScript will insert the tags below at the cursor position in IE and 
// Gecko-based browsers (Mozilla, Camino, Firefox, Netscape). For browsers that 
// do not support inserting at the cursor position (older versions of Safari, 
// OmniWeb) it appends the tags to the end of the content.
//
// Pass the ID of the <textarea> element to the edToolbar and function.
//
// Example:
//
//  <script type="text/javascript">edToolbar('canvas');</script>
//  <textarea id="canvas" rows="20" cols="50"></textarea>
//

var dictionaryUrl = 'http://www.ninjawords.com/';

// other options include:
//
// var dictionaryUrl = 'http://www.answers.com/';
// var dictionaryUrl = 'http://www.dictionary.com/';

var edButtons = new Array();
var edLinks = new Array();
var edOpenTags = new Array();

function edButton(id, display, tagStart, tagEnd, access, open) {
    this.id = id;		// used to name the toolbar button
    this.display = display;	// label on button
    this.tagStart = tagStart; 	// open tag
    this.tagEnd = tagEnd;	// close tag
    this.access = access;	// set to -1 if tag does not need to be closed
    this.open = open;		// set to -1 if tag does not need to be closed
}

edButtons.push(
        new edButton(
                'ed_bold'
                , 'B'
                , '<b>'
                , '</b>'
                , 'b'
                )
        );

edButtons.push(
        new edButton(
                'ed_italic'
                , 'I'
                , '<i>'
                , '</i>'
                , 'i'
                )
        );

edButtons.push(
        new edButton(
                'ed_subrayado'
                , 'U'
                , '<u>'
                , '</u>'
                , 'u'
                )
        );

edButtons.push(
        new edButton(
                'ed_izq'
                , '-  '
                , '<div style="text-align: left;">'
                , '\n\n</div>\n'
                , 'izquierda'
                )
        );

edButtons.push(
        new edButton(
                'ed_centro'
                , ' - '
                , '<div style="text-align: center;">\n'
                , '\n\n</div>\n'
                , 'centro'
                )
        );

edButtons.push(
        new edButton(
                'ed_der'
                , '  -'
                , '<div style="text-align: right;">'
                , '\n\n</div>\n'
                , 'derecha'
                )
        );

edButtons.push(
        new edButton(
                'ed_style'
                , 'STYLE'
                , ' style=""'
                , ''
                , 'style'
                )
        );

edButtons.push(
        new edButton(
                'ed_img'
                , 'IMG'
                , ''
                , ''
                , 'm'
                , -1
                )
        ); // special case
edButtons.push(
        new edButton(
                'ed_div'
                , 'DIV'
                , '<div>'
                , '\n</div>\n'
                , 'div'
                )
        );

edButtons.push(
        new edButton(
                'ed_ul'
                , 'UL'
                , '<ul>'
                , '</ul>'
                , 'u'
                )
        );

edButtons.push(
        new edButton(
                'ed_ol'
                , 'OL'
                , '<ol>'
                , '</ol>'
                , 'o'
                )
        );

edButtons.push(
        new edButton(
                'ed_li'
                , 'LI'
                , '\t<li>'
                , '</li>\n'
                , 'l'
                )
        );

var extendedStart = edButtons.length;

// below here are the extended buttons
edButtons.push(
        new edButton(
                'ed_small'
                , 'SM'
                , '<p style="font-size: 10px;">\n'
                , '\n</p>\n'
                , '1'
                )
        );

edButtons.push(
        new edButton(
                'ed_normal'
                , 'NM'
                , '<p style="font-size: 20px;">\n'
                , '\n</p>\n'
                , '1'
                )
        );

edButtons.push(
        new edButton(
                'ed_large'
                , 'LM'
                , '<p style="font-size: 30px;">\n'
                , '\n</p>\n'
                , '1'
                )
        );


edButtons.push(
        new edButton(
                'ed_h1'
                , 'H1'
                , '<h1>'
                , '</h1>'
                , '1'
                )
        );

edButtons.push(
        new edButton(
                'ed_h2'
                , 'H2'
                , '<h2>'
                , '</h2>'
                , '2'
                )
        );

edButtons.push(
        new edButton(
                'ed_h3'
                , 'H3'
                , '<h3>'
                , '</h3>'
                , '3'
                )
        );

edButtons.push(
        new edButton(
                'ed_h4'
                , 'H4'
                , '<h4>'
                , '</h4>'
                , '4'
                )
        );

edButtons.push(
        new edButton(
                'ed_p'
                , 'P'
                , '<p>'
                , '</p>'
                , 'p'
                )
        );

edButtons.push(
        new edButton(
                'ed_table'
                , 'TABLE'
                , '<table>\n<tbody>'
                , '</tbody>\n</table>\n'
                )
        );

edButtons.push(
        new edButton(
                'ed_tr'
                , 'TR'
                , '\t<tr>\n'
                , '\n\t</tr>\n'
                )
        );

edButtons.push(
        new edButton(
                'ed_td'
                , 'TD'
                , '\t\t<td>'
                , '</td>\n'
                )
        );


edButtons.push(
        new edButton(
                'ed_variable'
                , 'Variable'
                , ''
                , ''
                , 'f'
                )
        );

edButtons.push(
        new edButton(
                'ed_tabla'
                , 'Table'
                , ''
                , ''
                , 't'
                )
        );

edButtons.push(
        new edButton(
                'ed_comentario'
                , 'Comment'
                , '<!--'
                , '-->'
                , 'comentario'
                )
        );
edButtons.push(
        new edButton(
                'ed_espacio'
                , 'Br'
                , ' <br>'
                , ''
                , 'br'
                )
        );
edButtons.push(
        new edButton(
                'ed_clear'
                , 'Clear'
                , '\n<div style="clear: both;"></div>\n\n'
                , ''
                , 'clear'
                )
        );

function edLink(display, URL, newWin) {
    this.display = display;
    this.URL = URL;
    if (!newWin) {
        newWin = 0;
    }
    this.newWin = newWin;
}


edLinks[edLinks.length] = new edLink('alexking.org'
        , 'http://www.alexking.org/'
        );

function edShowButton(which, button, i) {
    if (button.access) {
        var accesskey = ' accesskey = "' + button.access + '"'
    }
    else {
        var accesskey = '';
    }
    switch (button.id) {
        case 'ed_img':
            document.write('<input type="button" id="' + button.id + '_' + which + '" ' + accesskey + ' class="ed_button" onclick="edInsertImage(\'' + which + '\');" value="' + button.display + '" />');
            break;
        case 'ed_link':
            document.write('<input type="button" id="' + button.id + '_' + which + '" ' + accesskey + ' class="ed_button" onclick="edInsertLink(\'' + which + '\', ' + i + ');" value="' + button.display + '" />');
            break;
        case 'ed_ext_link':
            document.write('<input type="button" id="' + button.id + '_' + which + '" ' + accesskey + ' class="ed_button" onclick="edInsertExtLink(\'' + which + '\', ' + i + ');" value="' + button.display + '" />');
            break;
        case 'ed_variable':
            document.write('<input type="button" id="' + button.id + '_' + which + '" ' + accesskey + ' class="ed_button" onclick="edInsertVariable(\'' + which + '\');" value="' + button.display + '" />');
            break;
        case 'ed_tabla':
            document.write('<input type="button" id="' + button.id + '_' + which + '" ' + accesskey + ' class="ed_button" onclick="edInsertTabla(\'' + which + '\');" value="' + button.display + '" />');
            break;
        default:
            document.write('<input type="button" id="' + button.id + '_' + which + '" ' + accesskey + ' class="ed_button" onclick="edInsertTag(\'' + which + '\', ' + i + ');" value="' + button.display + '"  />');
            break;
    }
}

function edShowLinks() {
    var tempStr = '<select onchange="edQuickLink(this.options[this.selectedIndex].value, this);"><option value="-1" selected>(Quick Links)</option>';
    for (i = 0; i < edLinks.length; i++) {
        tempStr += '<option value="' + i + '">' + edLinks[i].display + '</option>';
    }
    tempStr += '</select>';
    document.write(tempStr);
}

function edAddTag(which, button) {
    if (edButtons[button].tagEnd != '') {
        edOpenTags[which][edOpenTags[which].length] = button;
        document.getElementById(edButtons[button].id + '_' + which).value = '/' + document.getElementById(edButtons[button].id + '_' + which).value;
    }
}

function edRemoveTag(which, button) {
    for (i = 0; i < edOpenTags[which].length; i++) {
        if (edOpenTags[which][i] == button) {
            edOpenTags[which].splice(i, 1);
            document.getElementById(edButtons[button].id + '_' + which).value = document.getElementById(edButtons[button].id + '_' + which).value.replace('/', '');
        }
    }
}

function edCheckOpenTags(which, button) {
    var tag = 0;
    for (i = 0; i < edOpenTags[which].length; i++) {
        if (edOpenTags[which][i] == button) {
            tag++;
        }
    }
    if (tag > 0) {
        return true; // tag found
    }
    else {
        return false; // tag not found
    }
}

function edCloseAllTags(which) {
    var count = edOpenTags[which].length;
    for (o = 0; o < count; o++) {
        edInsertTag(which, edOpenTags[which][edOpenTags[which].length - 1]);
    }
}

function edQuickLink(i, thisSelect) {
    if (i > -1) {
        var newWin = '';
        if (edLinks[i].newWin == 1) {
            newWin = ' target="_blank"';
        }
        var tempStr = '<a href="' + edLinks[i].URL + '"' + newWin + '>'
                + edLinks[i].display
                + '</a>';
        thisSelect.selectedIndex = 0;
        edInsertContent(edCanvas, tempStr);
    }
    else {
        thisSelect.selectedIndex = 0;
    }
}

function edSpell(which) {
    myField = document.getElementById(which);
    var word = '';
    if (document.selection) {
        myField.focus();
        var sel = document.selection.createRange();
        if (sel.text.length > 0) {
            word = sel.text;
        }
    }
    else if (myField.selectionStart || myField.selectionStart == '0') {
        var startPos = myField.selectionStart;
        var endPos = myField.selectionEnd;
        if (startPos != endPos) {
            word = myField.value.substring(startPos, endPos);
        }
    }
    if (word == '') {
        word = prompt('Enter a word to look up:', '');
    }
    if (word != '') {
        window.open(dictionaryUrl + escape(word));
    }
}

function edToolbar(which) {
    document.write('<div id="ed_toolbar_' + which + '"><span>');
    for (i = 0; i < extendedStart; i++) {
        edShowButton(which, edButtons[i], i);
    }
    if (edShowExtraCookie()) {
        document.write(
                '<input type="button" id="ed_close_' + which + '" class="ed_button" onclick="edCloseAllTags(\'' + which + '\');" value="Close Tags" />'
                + '<input type="button" id="ed_spell_' + which + '" class="ed_button" onclick="edSpell(\'' + which + '\');" value="Dict" />'
                + '<input type="button" id="ed_extra_show_' + which + '" class="ed_button" onclick="edShowExtra(\'' + which + '\')" value="&raquo;" style="visibility: hidden;" />'
                + '</span><br />'
                + '<span id="ed_extra_buttons_' + which + '">'
                + '<input type="button" id="ed_extra_hide_' + which + '" class="ed_button" onclick="edHideExtra(\'' + which + '\');" value="&laquo;" />'
                );
    }
    else {
        document.write(
                '<input type="button" id="ed_close_' + which + '" class="ed_button" onclick="edCloseAllTags(\'' + which + '\');" value="Close Tags" />'
                + '<input type="button" id="ed_spell_' + which + '" class="ed_button" onclick="edSpell(\'' + which + '\');" value="Dict" />'
                + '<input type="button" id="ed_extra_show_' + which + '" class="ed_button" onclick="edShowExtra(\'' + which + '\')" value="&raquo;" />'
                + '</span><br />'
                + '<span id="ed_extra_buttons_' + which + '" style="display: none;">'
                + '<input type="button" id="ed_extra_hide_' + which + '" class="ed_button" onclick="edHideExtra(\'' + which + '\');" value="&laquo;" />'
                );
    }
    for (i = extendedStart; i < edButtons.length; i++) {
        edShowButton(which, edButtons[i], i);
    }
    document.write('</span>');
//	edShowLinks();
    document.write('</div>');
    edOpenTags[which] = new Array();
}

function edShowExtra(which) {
    document.getElementById('ed_extra_show_' + which).style.visibility = 'hidden';
    document.getElementById('ed_extra_buttons_' + which).style.display = 'block';
    edSetCookie(
            'js_quicktags_extra'
            , 'show'
            , new Date("December 31, 2100")
            );
}

function edHideExtra(which) {
    document.getElementById('ed_extra_buttons_' + which).style.display = 'none';
    document.getElementById('ed_extra_show_' + which).style.visibility = 'visible';
    edSetCookie(
            'js_quicktags_extra'
            , 'hide'
            , new Date("December 31, 2100")
            );
}

// insertion code

function edInsertTag(which, i) {
    myField = document.getElementById(which);
    //IE support
    if (document.selection) {
        myField.focus();
        sel = document.selection.createRange();
        if (sel.text.length > 0) {
            sel.text = edButtons[i].tagStart + sel.text + edButtons[i].tagEnd;
        }
        else {
            if (!edCheckOpenTags(which, i) || edButtons[i].tagEnd == '') {
                sel.text = edButtons[i].tagStart;
                edAddTag(which, i);
            }
            else {
                sel.text = edButtons[i].tagEnd;
                edRemoveTag(which, i);
            }
        }
        myField.focus();
    }
    //MOZILLA/NETSCAPE support
    else if (myField.selectionStart || myField.selectionStart == '0') {
        var startPos = myField.selectionStart;
        var endPos = myField.selectionEnd;
        var cursorPos = endPos;
        var scrollTop = myField.scrollTop;
        if (startPos != endPos) {
            myField.value = myField.value.substring(0, startPos)
                    + edButtons[i].tagStart
                    + myField.value.substring(startPos, endPos)
                    + edButtons[i].tagEnd
                    + myField.value.substring(endPos, myField.value.length);
            cursorPos += edButtons[i].tagStart.length + edButtons[i].tagEnd.length;
        }
        else {
            if (!edCheckOpenTags(which, i) || edButtons[i].tagEnd == '') {
                myField.value = myField.value.substring(0, startPos)
                        + edButtons[i].tagStart
                        + myField.value.substring(endPos, myField.value.length);
                edAddTag(which, i);
                cursorPos = startPos + edButtons[i].tagStart.length;
            }
            else {
                myField.value = myField.value.substring(0, startPos)
                        + edButtons[i].tagEnd
                        + myField.value.substring(endPos, myField.value.length);
                edRemoveTag(which, i);
                cursorPos = startPos + edButtons[i].tagEnd.length;
            }
        }
        myField.focus();
        myField.selectionStart = cursorPos;
        myField.selectionEnd = cursorPos;
        myField.scrollTop = scrollTop;
    }
    else {
        if (!edCheckOpenTags(which, i) || edButtons[i].tagEnd == '') {
            myField.value += edButtons[i].tagStart;
            edAddTag(which, i);
        }
        else {
            myField.value += edButtons[i].tagEnd;
            edRemoveTag(which, i);
        }
        myField.focus();
    }
}

function edInsertContent(which, myValue) {
    myField = document.getElementById(which);
    //IE support
    if (document.selection) {
        myField.focus();
        sel = document.selection.createRange();
        sel.text = myValue;
        myField.focus();
    }
    //MOZILLA/NETSCAPE support
    else if (myField.selectionStart || myField.selectionStart == '0') {
        var startPos = myField.selectionStart;
        var endPos = myField.selectionEnd;
        var scrollTop = myField.scrollTop;
        myField.value = myField.value.substring(0, startPos)
                + myValue
                + myField.value.substring(endPos, myField.value.length);
        myField.focus();
        myField.selectionStart = startPos + myValue.length;
        myField.selectionEnd = startPos + myValue.length;
        myField.scrollTop = scrollTop;
    } else {
        myField.value += myValue;
        myField.focus();
    }
}

function edInsertLink(which, i, defaultValue) {
    myField = document.getElementById(which);
    if (!defaultValue) {
        defaultValue = 'http://';
    }
    if (!edCheckOpenTags(which, i)) {
        var URL = prompt('Enter the URL', defaultValue);
        if (URL) {
            edButtons[i].tagStart = '<a href="' + URL + '">';
            edInsertTag(which, i);
        }
    }
    else {
        edInsertTag(which, i);
    }
}

function edInsertExtLink(which, i, defaultValue) {
    myField = document.getElementById(which);
    if (!defaultValue) {
        defaultValue = 'http://';
    }
    if (!edCheckOpenTags(which, i)) {
        var URL = prompt('Enter the URL', defaultValue);
        if (URL) {
            edButtons[i].tagStart = '<a href="' + URL + '" rel="external">';
            edInsertTag(which, i);
        }
    }
    else {
        edInsertTag(which, i);
    }
}

function edInsertImage(which) {
    myField = document.getElementById(which);
    var myValue = prompt('Enter the URL of the image', 'http://');
    if (myValue) {
        myValue = '<table style="width: ' + prompt('Largo de la imagen', '')
                + 'px;height:' + prompt('Alto de la imagen', '')
                + 'px;background-image: url('
                + myValue
                + ');  background-repeat: no-repeat;float:' + prompt('Posición de la imagen', '')
                + ';">\n<tbody><tr><td></td></tr></tbody>\n</table>\n\n';
        edInsertContent(which, myValue);
    }
}

function edInsertVariable(which) {
    myField = document.getElementById(which);
    var myValue = prompt('Ingresa nombre de variable', '$variable');
    if (myValue) {
        myValue = '\' . $' + myValue
                + ' . \'';
        edInsertContent(which, myValue);
    }
}

function countInstances(string, substr) {
    var count = string.split(substr);
    return count.length - 1;
}

function edInsertTabla(which) {
    myField = document.getElementById(which);
    var myValue = prompt('Cantidad de Columnas', '2');
    if (myValue) {
        myValueC = '<table style="text-align:center; margin: auto" width="720" border="0" cellspacing="1" cellpadding="0">\n';
        myValueC += ' <thead  bgcolor="#333333" style="color:#ffffff">\n  <tr>\n';
        for (var i = 1; i <= myValue; i++) { // filas cabezeras
            myValueC += '   <th>cabezera' + i + '</th>\n';
        }
        myValueC += '  </tr>\n </thead>\n <tbody><!--aqui entra el php si hubiese mas de 1 registro-->\n  <tr bgcolor="#F0F0F0">\n';
        for (var i = 1; i <= myValue; i++) { //filas detalle
            myValueC += '   <td style="font-size: 13px;">dato' + i + '</td>\n';
        }
        myValueC += '  </tr>\n </tbody>\n</table>\n';
        edInsertContent(which, myValueC);
    }
}


function edSetCookie(name, value, expires, path, domain) {
    document.cookie = name + "=" + escape(value) +
            ((expires) ? "; expires=" + expires.toGMTString() : "") +
            ((path) ? "; path=" + path : "") +
            ((domain) ? "; domain=" + domain : "");
}

function edShowExtraCookie() {
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
        var cookieData = cookies[i];
        while (cookieData.charAt(0) == ' ') {
            cookieData = cookieData.substring(1, cookieData.length);
        }
        if (cookieData.indexOf('js_quicktags_extra') == 0) {
            if (cookieData.substring(19, cookieData.length) == 'show') {
                return true;
            }
            else {
                return false;
            }
        }
    }
    return false;
}
